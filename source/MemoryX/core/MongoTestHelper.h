/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <memory>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>


/**
* Helper class for using MongoDB in tests
*/
class MongoTestHelper
{
public:
    MongoTestHelper() :
        cmake("MemoryX")
    {
        //            stopMongo();
        startMongo();
    }

    ~MongoTestHelper()
    {
        //            stopMongo();
    }

    void startMongo()
    {
        std::string memoryXDir = armarx::test::getCmakeValue("MemoryX_BINARY_DIR");
        memoryXDir = cmake.getBinaryDir();
        sleep(1);
        std::string command = memoryXDir + "/mongod.sh assureRunning";
        exec(command);
    }

    void stopMongo()
    {

        std::string memoryXDir = armarx::test::getCmakeValue("MemoryX_BINARY_DIR");
        memoryXDir = cmake.getBinaryDir();
        std::string command = memoryXDir + "/mongod.sh stop";
        exec(command);
    }

protected:
    void exec(const std::string& command)
    {
        std::cout << "Executing command: " << command << std::endl;
        if (system(command.c_str()))
        {
            std::cout << "Executing command '" << command << "' failed" << std::endl;
            throw std::runtime_error {"Executing command '" + command + "' failed"};
        }
    }
    armarx::CMakePackageFinder cmake;
};

