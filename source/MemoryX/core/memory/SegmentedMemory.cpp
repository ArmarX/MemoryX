/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <mutex>

#include "SegmentedMemory.h"

#include <ArmarXCore/interface/core/Log.h>
#include <ArmarXCore/core/Component.h>

namespace memoryx
{
    SegmentedMemory::SegmentedMemory()
    {
    }

    SegmentedMemory::~SegmentedMemory()
    {
    }

    AbstractMemorySegmentPrx SegmentedMemory::addSegment(const std::string& segmentName, const AbstractMemorySegmentPtr& segment, const ::Ice::Current& c)
    {
        std::unique_lock lock(segmentsMutex);
        // check if a segment with the same name already exists in memory
        MemorySegmentMap::const_iterator it = segments.find(segmentName);

        if (it != segments.end())
        {
            throw SegmentAlreadyExistsException();
        }

        // assign name
        segment->setSegmentName(segmentName);
        segment->setParentMemory(this);

        auto id = segment->getIceId();
        // register proxy
        AbstractMemorySegmentPrx segmentProxy = AbstractMemorySegmentPrx::uncheckedCast(getObjectAdapter()->add(segment, id));

        // insert into known segments
        MemorySegmentEntry segmentEntry;
        segmentEntry.proxy = segmentProxy;
        segmentEntry.pointer = segment;

        segments[segmentName] = segmentEntry;

        return segmentProxy;
    }

    bool SegmentedMemory::hasSegment(const std::string& segmentName, const ::Ice::Current& c) const
    {
        std::unique_lock lock(segmentsMutex);
        return segments.find(segmentName) != segments.end();
    }

    AbstractMemorySegmentPrx SegmentedMemory::getSegment(const std::string& segmentName, const ::Ice::Current& c) const
    {
        std::unique_lock lock(segmentsMutex);

        // find segment
        MemorySegmentMap::const_iterator iter =  segments.find(segmentName);

        if (iter == segments.end())
        {
            ARMARX_ERROR_S << "Could not find segment with name " << segmentName << std::endl;
            throw InvalidEntityException();
        }

        return iter->second.proxy;
    }

    void SegmentedMemory::removeSegment(const std::string& segmentName, const ::Ice::Current& c)
    {
        std::unique_lock lock(segmentsMutex);

        MemorySegmentMap::iterator iter =  segments.find(segmentName);

        if (iter != segments.end())
        {
            AbstractMemorySegmentPrx proxy = iter->second.proxy;

            // remove from adapter
            Ice::Identity id = proxy->ice_getIdentity();
            getObjectAdapter()->remove(id);

            // remove from known segments
            segments.erase(iter);
        }
    }

    NameList SegmentedMemory::getSegmentNames(const ::Ice::Current&) const
    {
        std::unique_lock lock(segmentsMutex);

        NameList result;

        for (MemorySegmentMap::const_iterator it = segments.begin(); it != segments.end(); ++it)
        {
            result.push_back(it->first);
        }

        return result;
    }

    AbstractMemorySegmentPtr SegmentedMemory::getSegmentPtr(const std::string& segmentName)
    {
        std::unique_lock lock(segmentsMutex);

        // find segment
        MemorySegmentMap::iterator iter =  segments.find(segmentName);

        if (iter == segments.end())
        {
            ARMARX_ERROR_S << "Could not find segment with name " << segmentName << std::endl;
            throw InvalidEntityException();
        }

        return iter->second.pointer;
    }


    EntityBasePtr memoryx::SegmentedMemory::findEntityById(const std::string& entityId, const Ice::Current& c)
    {
        NameList names = getSegmentNames();

        for (auto name : names)
        {
            EntityMemorySegmentInterfacePtr segment = memoryx::EntityMemorySegmentInterfacePtr::dynamicCast(getSegmentPtr(name));

            if (!segment)
            {
                continue;
            }

            auto entity = segment->getEntityById(entityId);

            if (entity)
            {
                return entity;
            }
        }

        return NULL;
    }


    EntityRefBasePtr memoryx::SegmentedMemory::findEntityRefById(const std::string& entityId, const Ice::Current& c)
    {
        NameList names = getSegmentNames();

        for (auto name : names)
        {
            EntityMemorySegmentInterfacePtr segment = memoryx::EntityMemorySegmentInterfacePtr::dynamicCast(getSegmentPtr(name));

            if (!segment)
            {
                continue;
            }

            auto entity = segment->getEntityRefById(entityId);

            if (entity)
            {
                return entity;
            }
        }

        return NULL;
    }
}
