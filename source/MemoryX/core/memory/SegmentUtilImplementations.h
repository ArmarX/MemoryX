/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <MemoryX/interface/core/MemoryInterface.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <atomic>
#include <shared_mutex>
#include <mutex>

#define MEMORYX_TOKEN(lock) {{"token", lock->token}}

namespace memoryx
{
    class SegmentLock :
        public SegmentLockBase
    {
    protected:
        ~SegmentLock() override;
    public:
        SegmentLock(bool start_watchdog = false);

        // SegmentLockBase interface
        void unlock(const Ice::Current& c = Ice::emptyCurrent) override;
        void ice_postUnmarshal() override;
        void startWatchdog();

    protected:
        void keepAlive();
        armarx::PeriodicTask<SegmentLock>::pointer_type keepAliveTask;
    };


    class SegmentUtilImplementations :
        virtual public EntityMemorySegmentInterface
    {
    public:
        SegmentUtilImplementations();

    public:
        using ScopedSharedLock = std::unique_lock<std::shared_mutex>;
        using ScopedSharedLockPtr = std::unique_ptr<ScopedSharedLock>;
        using ScopedUniqueLock = std::unique_lock<std::shared_mutex>;
        using ScopedUniqueLockPtr = std::unique_ptr<ScopedSharedLock>;

        // EntityMemorySegmentInterface interface
        ScopedSharedLockPtr getReadLock(const Ice::Current& c) const;
        ScopedUniqueLockPtr getWriteLock(const Ice::Current& c) const;
        std::string getJSONEntityById(const std::string& id, const Ice::Current&) const override;
        EntityBaseList getEntityWithChildrenById(const std::string& id, bool includeMetaEntities, const Ice::Current& c = Ice::emptyCurrent) const override;
        EntityBaseList getEntityWithChildrenByName(const std::string&, bool includeMetaEntities, const Ice::Current& c = Ice::emptyCurrent) const override;

        SegmentLockBasePtr lockSegment(const Ice::Current& c) override;
        bool keepLockAlive(const std::string& token, const Ice::Current&) override;
        bool unlockSegment(const SegmentLockBasePtr& lock, const Ice::Current& c) override;
        bool unlockSegmentWithToken(const std::string& token, const Ice::Current& c) override;

    protected:
        void keepAliveCheck();

        mutable std::mutex mutex;

        EntityMemorySegmentInterfacePrx selfProxy;
        ScopedSharedLockPtr readLock;
        ScopedUniqueLockPtr writeLock;
        mutable std::shared_mutex segmentMutex;
        std::mutex keepAliveTimestampMutex;
        IceUtil::Time keepAliveTimestamp;
        mutable std::mutex tokenMutex;
        std::string lockToken;
        armarx::PeriodicTask<SegmentUtilImplementations>::pointer_type keepAliveCheckTask;
    };

} // namespace memoryx

