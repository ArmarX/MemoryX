/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "SegmentUtilImplementations.h"

#include <MemoryX/interface/core/MemoryInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>

#include <mutex>

namespace memoryx
{
    class MongoSerializer;
    using MongoSerializerPtr = IceUtil::Handle<MongoSerializer>;

    /**
    \page MemoryX-HowTos-create-new-persisten-segment How to create a new persistent memory segment in MemoryX

    \li Create Ice interface for new segment in MemoryX/interface/memorytypes/MemorySegments.ice which inherits from memoryx::PersistentEntitySegmentBase
    \code
    ["cpp:virtual"]
    class PersistentDataSegmentBase extends PersistentEntitySegmentBase
    {
        // add some special methods here
    };
    \endcode

    \li Create C++ header file
    \code
    #ifndef ARMARX_MEMORYX_NEW_DATA_SEGMENT_H
    #define ARMARX_MEMORYX_NEW_DATA_SEGMENT_H

    #include "../entity/profiler/ProfilerTransition.h"

    #include <MemoryX/core/memory/PersistentEntitySegment.h>
    #include <MemoryX/interface/memorytypes/MemorySegments.h>

    #include <ArmarXCore/core/system/ImportExportComponent.h>

    namespace memoryx
    {
        class ARMARXCOMPONENT_IMPORT_EXPORT PersistentDataSegment :
            virtual public PersistentEntitySegment,
            virtual public PersistentDataSegmentBase
        {
            public:
                PersistentDataSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr communicator, bool useMongoIds = true);

            // implementation of Ice interface methods
            ["cpp:const"]
            hasDataEntity(string name);
        };

        using PersistentProfilerDataSegmentPtr = IceInternal::Handle<PersistentProfilerDataSegment>;
    }

    #endif
    \endcode

    \li Create C++ implementation file
    \code
    #include "PersistentDataSegment.h"

    using namespace memoryx;

    PersistentDataSegment::PersistentDataSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr communicator, bool useMongoIds) :
        PersistentEntitySegment(entityCollection, communicator, useMongoIds),
        PersistentDataSegmentBase()
    {
    }

    // implementation of further methods
    \endcode

    \li Add header and implementation files to the appropriate CMake target.

    \li Add/register new segment in memoryx::PriorKnowledge or memoryx::LongtermMemory and add accessor functions to the recpective memory interface.
    \code
    \endcode

    */

    /**
     * @brief The PersistentEntitySegment class is the base class for all memory segments containing memoryx::Entity instances to be stored permanently in MongoDB.
     */
    class PersistentEntitySegment :
        public virtual PersistentEntitySegmentBase,
        public virtual SegmentUtilImplementations
    {
    public:
        PersistentEntitySegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true);
        ~PersistentEntitySegment() override;

        NameList getReadCollectionsNS(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void clearReadCollections(const ::Ice::Current& = Ice::emptyCurrent) override;
        void addReadCollection(const CollectionInterfacePrx& coll, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getWriteCollectionNS(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setWriteCollection(const CollectionInterfacePrx& coll, const ::Ice::Current& = Ice::emptyCurrent) override;
        void setSingleRWCollection(const CollectionInterfacePrx& coll, const ::Ice::Current& = Ice::emptyCurrent) override;

        std::string getObjectTypeId(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief addEntity add new \p entity and return the newly generated entity ID
         * @param entity
         * @return
         */
        std::string addEntity(const EntityBasePtr& entity, const ::Ice::Current& c = Ice::emptyCurrent) override;
        virtual std::string addEntityThreadUnsafe(const EntityBasePtr& entity);
        std::string upsertEntity(const std::string& entityId, const EntityBasePtr& entity, const ::Ice::Current& = Ice::emptyCurrent) override;
        virtual std::string upsertEntityThreadUnsafe(const std::string& entityId, const EntityBasePtr& entity);
        std::string upsertEntityByName(const std::string& entityName, const EntityBasePtr& entity, const ::Ice::Current& = Ice::emptyCurrent) override;
        virtual std::string upsertEntityByNameThreadUnsafe(const std::string& entityName, const EntityBasePtr& entity);
        /**
         * @brief addEntityList adds all entities contained in \entityList to this segment and returns a list of created entity IDs
         * @return list of generated entity IDs
         */
        EntityIdList addEntityList(const EntityBaseList& entityList, const Ice::Current& = Ice::emptyCurrent) override;
        virtual EntityIdList addEntityListThreadUnsafe(const EntityBaseList& entityList);

        EntityIdList upsertEntityList(const EntityBaseList& entityList, const Ice::Current& = Ice::emptyCurrent) override;
        virtual EntityIdList upsertEntityListThreadUnsafe(const EntityBaseList& entityList);

        void updateEntity(const ::std::string& entityId, const EntityBasePtr& update, const ::Ice::Current& = Ice::emptyCurrent) override;
        virtual void updateEntityThreadUnsafe(const ::std::string& entityId, const EntityBasePtr& update);

        /**
         * @brief removeEntity removes an entity with the ID \p entityId
         * @param entityId
         */
        void removeEntity(const ::std::string& entityId, const ::Ice::Current& = Ice::emptyCurrent) override;
        virtual void removeEntityThreadUnsafe(const ::std::string& entityId);
        /**
         * @brief removeAllEntities collects all entities managed by this memory segment and removes them from the segment
         * @param c
         */
        void removeAllEntities(const ::Ice::Current& c = Ice::emptyCurrent) override;

        bool hasEntityById(const std::string& entityId, const Ice::Current& = Ice::emptyCurrent) const override;
        bool hasEntityByIdThreadUnsafe(const std::string& entityId) const;
        bool hasEntityByName(const std::string& entityName, const Ice::Current& = Ice::emptyCurrent) const override;
        bool hasEntityByNameThreadUnsafe(const std::string& entityName) const;

        EntityBasePtr getEntityById(const ::std::string& entityId, const ::Ice::Current& = Ice::emptyCurrent) const override;
        virtual EntityBasePtr getEntityByIdThreadUnsafe(const ::std::string& entityId) const;
        EntityBasePtr getEntityByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override;
        virtual EntityBasePtr getEntityByNameThreadUnsafe(const ::std::string& name) const;
        EntityBaseList getEntitiesByAttrValue(const ::std::string& attrName, const ::std::string& attrValue, const ::Ice::Current& = Ice::emptyCurrent) const override;
        EntityBaseList getEntitiesByAttrValueList(const ::std::string& attrName, const NameList& attrValueList, const ::Ice::Current& = Ice::emptyCurrent) const override;
        EntityIdList getAllEntityIds(const ::Ice::Current& = Ice::emptyCurrent) const override;
        virtual EntityIdList getAllEntityIdsThreadUnsafe() const;
        /**
         * @brief getAllEntities returns a list of all entities managed by this memory segment
         * @return
         */
        EntityBaseList getAllEntities(const ::Ice::Current& = Ice::emptyCurrent) const override;
        IdEntityMap getIdEntityMap(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief size counts the number of memoryx::Entity instances contained available reachable throuhg memoryx::PersistentEntitySegment::readCollections.
         * @return number of entities contained in this memory segment
         */
        Ice::Int size(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void print(const ::Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief clear removes all elements from the current memoryx::PersistentEntitySegment::writeCollection
         */
        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;

        ::Ice::Identity getIceId(const ::Ice::Current& = Ice::emptyCurrent) const override;
        std::string getSegmentName(const ::Ice::Current& = Ice::emptyCurrent) const override;

        // EntityMemorySegmentInterface interface
    public:
        EntityRefBasePtr getEntityRefById(const std::string& id, const Ice::Current&) const override;
        EntityRefBasePtr getEntityRefByName(const std::string& name, const Ice::Current& c) const override;

        void setEntityAttribute(const std::string& entityId, const EntityAttributeBasePtr& attribute, const Ice::Current&) override;
        void setEntityAttributes(const std::string& entityId, const EntityAttributeList& attributeMap, const Ice::Current&) override;

        // AbstractMemorySegment interface
    public:
        void setParentMemory(const MemoryInterfacePtr& memory, const Ice::Current&) override;

        // PersistentEntitySegmentBase interface
    public:
        /**
         * @brief retrieves Entity Refs that match the query. The query
         * should be a mongo query like:  { "attrs.endNode.value.value.entityId": "54b2a43b43b5d3199e3cb5b3" }
         * @param query
         * @param c
         * @return
         */
        EntityRefList findRefsByQuery(const std::string& query, const Ice::Current& c) override;

    protected:
        // set segment name
        void setSegmentName(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;

        MemoryInterfacePtr parentMemory;

        CollectionPrxList readCollections;
        //        mutable boost::mutex readCollectionsMutex;

        CollectionInterfacePrx writeCollection;
        MongoSerializerPtr dbSerializer;
        mutable std::recursive_mutex dbSerializerMutex;

        bool useMongoIds;

        EntityBasePtr deserializeEntity(const DBStorableData& dbEntity) const;
        std::string segmentName;


    };


    using PersistentEntitySegmentPtr = IceUtil::Handle<PersistentEntitySegment>;

}

