/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       Sep 23, 2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "MongoDBRef.h"

namespace memoryx
{
    MongoDBRef::MongoDBRef()
        = default;

    MongoDBRef::MongoDBRef(const std::string& dbName, const std::string& docId) :
        MongoDBRefBase::MongoDBRefBase(dbName, docId)
    {
    }

    MongoDBRef::~MongoDBRef()
        = default;

    void MongoDBRef::serialize(const armarx::ObjectSerializerBasePtr& serializer,
                               const Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setString("dbName", this->dbName);
        obj->setString("docId", this->docId);
    }

    void MongoDBRef::deserialize(const armarx::ObjectSerializerBasePtr& serializer,
                                 const Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        this->dbName = obj->getString("dbName");
        this->docId = obj->getString("docId");
    }

}
