/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <memory>

namespace memoryx
{
    class GridFileManager;
    using GridFileManagerPtr = std::shared_ptr<GridFileManager>;
}

namespace memoryx::EntityWrappers
{

    /**
     *  AbstractEntityWrapper is a superclass for all covenience classes, which provide typed access to entity attributes.
     *  See memoryx::EntityWrappers::SimoxObjectWrapper for sample implementation.
     *
     *  Apart of convenience, such wrapper classes give a maintance advantage, since they encapsulate all
     *  the implementation details (attribute names, storing formats), avoiding the need to duplicate them
     *  in multiple places in client code. So it's recommended to use this approach whenever possible.
     *
     */
    class AbstractEntityWrapper :
        public AbstractEntityWrapperBase
    {
    public:
        AbstractEntityWrapper();
        ~AbstractEntityWrapper() override;

        /**
         *  Get the identifier of the stored entity.
         */
        std::string getId() const;

        /**
         *  Get the name of the stored entity.
         */
        std::string getName() const;

        /**
         *  Get the stored name of the stored entity.
         */
        EntityBasePtr getEntity() const;

        /**
         *  Replace the stored entity with a new one
         *
         *  @param entity  entity to set
         */
        //            void setEntity(EntityBase* entity);
        void setEntity(const EntityBasePtr& entity);
    private:
        EntityBasePtr entity;
    };



    class AbstractFileEntityWrapper :
        public AbstractEntityWrapper
    {
    public:
        AbstractFileEntityWrapper(GridFileManagerPtr fileManager);
    protected:
        /**
         * @brief cleanUpAttributeFiles compares the files attached to the two
         * given attributes and removes the files of the oldAttr, if they dont
         * match. Thus, this function should only be called with attributes of
         * the same kind (e.g. Attribute for texture files).
         * If multiple files are attached to that attribute, they must appear
         * in the same order.
         * @param oldAttr
         * @param newAttr
         */
        void cleanUpAttributeFiles(EntityAttributeBasePtr oldAttr, EntityAttributeBasePtr newAttr);
        /**
         * @brief removes all files of an attribute from the gridfs
         * @param attr
         */
        void removeAttributeFiles(const memoryx::EntityAttributeBasePtr& attr);
        std::string cacheAttributeFile(const std::string& attrName, bool preserveOriginalFName = false) const;

        GridFileManagerPtr fileManager;
    };

    /**
    * Typedef of AbstractEntityWrapperPtr as IceUtil::Handle<AbstractEntityWrapper> for convenience.
    */
    using AbstractEntityWrapperPtr = IceInternal::Handle<AbstractEntityWrapper>;

}
