/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "AbstractEntityWrapper.h"
#include <ArmarXCore/observers/variant/Variant.h>
#include "../MongoDBRef.h"
#include "../GridFileManager.h"

namespace memoryx::EntityWrappers
{
    AbstractEntityWrapper::AbstractEntityWrapper() :
        entity(NULL)
    {
    }

    AbstractEntityWrapper::~AbstractEntityWrapper()
    {
    }

    std::string AbstractEntityWrapper::getId() const
    {
        return entity ? entity->getId() : "";
    }

    std::string AbstractEntityWrapper::getName() const
    {
        return entity ? entity->getName() : "";
    }

    EntityBasePtr AbstractEntityWrapper::getEntity() const
    {
        return entity;
    }

    void AbstractEntityWrapper::setEntity(const EntityBasePtr& entity)
    {
        this->entity = entity;
    }

    //void AbstractEntityWrapper::setEntity(EntityBase* entity)
    //{
    //    this->entity = entity;
    //}

    void AbstractFileEntityWrapper::cleanUpAttributeFiles(memoryx::EntityAttributeBasePtr oldAttr, memoryx::EntityAttributeBasePtr newAttr)
    {
        if (!oldAttr || !newAttr)
        {
            return;
        }

        int size = oldAttr->size();
        int newSize = newAttr->size();

        for (int i = 0; i < size; ++i)
        {
            armarx::VariantPtr oldMongoRef = armarx::VariantPtr::dynamicCast(oldAttr->getValueAt(i));

            std::string oldDocId;

            if (oldMongoRef)
            {
                oldDocId = oldMongoRef->getClass<MongoDBRef>()->docId;
            }

            std::string newDocId;

            if (i < newSize)
            {
                armarx::VariantPtr newMongoRef = armarx::VariantPtr::dynamicCast(newAttr->getValueAt(i));
                newDocId = newMongoRef->get<MongoDBRef>()->docId;
            }

            if (i >= newSize || (!oldDocId.empty() && newDocId != oldDocId))
            {
                ARMARX_INFO_S << "Removing old file: " << oldAttr->getName() ;
                fileManager->removeAttrFile(oldAttr, i);
            }
        }

    }

    void AbstractFileEntityWrapper::removeAttributeFiles(const memoryx::EntityAttributeBasePtr& attr)
    {
        if (!attr)
        {
            return;
        }

        int size = attr->size();
        for (int i = 0; i < size; ++i)
        {
            armarx::VariantPtr oldMongoRef = armarx::VariantPtr::dynamicCast(attr->getValueAt(i));

            std::string oldDocId;

            if (oldMongoRef)
            {
                oldDocId = oldMongoRef->getClass<MongoDBRef>()->docId;
                if (!oldDocId.empty())
                {
                    ARMARX_INFO_S << "Removing file: " << attr->getName() ;
                    fileManager->removeAttrFile(attr, i);
                }
            }
        }

    }

    std::string AbstractFileEntityWrapper::cacheAttributeFile(const std::string& attrName, bool preserveOriginalFName /* = false */) const
    {
        std::string result = "";

        if (getEntity()->hasAttribute(attrName))
        {
            fileManager->ensureFileInCache(getEntity()->getAttribute(attrName), result, preserveOriginalFName);
        }

        return result;
    }


    AbstractFileEntityWrapper::AbstractFileEntityWrapper(GridFileManagerPtr fileManager) :
        fileManager(fileManager)
    {

    }
}
