/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu), Kai Welke (welke at kit got edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/core/logging/Logging.h>

#include "EntityAttribute.h"

namespace memoryx
{

    // ********************************************************************
    // construction
    // ********************************************************************
    EntityAttribute::EntityAttribute(const std::string& name)
    {
        this->name = name;
    }

    EntityAttribute::EntityAttribute(const std::string& name, const armarx::VariantBasePtr& val)
    {
        this->name = name;
        setValue(val);
    }

    EntityAttribute::EntityAttribute(const EntityAttribute& other) :
        IceUtil::Shared(other),
        EntityAttributeBase()
        //    EntityAttributeBase(other)
    {
        this->name = other.name;
        //    this->clear();
        std::scoped_lock lock(other.valuesMutex);
        size_t size = other.values.size();
        this->values.reserve(size);

        for (size_t i = 0; i < size; ++i)
        {
            AttributeElement elem;
            const AttributeElement& origElem = other.values.at(i);
            armarx::VariantPtr var = armarx::VariantPtr::dynamicCast(origElem.value);
            ProbabilityMeasureBasePtr uncertainty = origElem.uncertainty;

            if (uncertainty)
            {
                elem.uncertainty = ProbabilityMeasureBasePtr::dynamicCast(uncertainty->clone());
            }

            elem.value = var->clone();
            values.push_back(elem);
        }
    }

    EntityAttribute::~EntityAttribute()
    {
        //    ARMARX_INFO_S << "~EntityAttribute() " << getName();
    }

    // ********************************************************************
    // member access
    // ********************************************************************
    ::std::string EntityAttribute::getName(const ::Ice::Current&) const
    {
        return name;
    }

    void EntityAttribute::setValue(const armarx::VariantBasePtr& val, const ::Ice::Current& c)
    {
        std::scoped_lock lock(valuesMutex);

        // only resize if neccessary!!! Issues with garbage collection
        if (size() != 1)
        {
            clear();
            addValue(val);
        }
        else
        {
            setValueWithUncertainty(val, ProbabilityMeasureBasePtr(), c);
        }
    }

    void EntityAttribute::setValueWithUncertainty(const armarx::VariantBasePtr& val,
            const ProbabilityMeasureBasePtr& uncertainty,
            const ::Ice::Current&)
    {
        std::scoped_lock lock(valuesMutex);

        // only resize if neccessary!!! Issues with garbage collection
        if (size() != 1)
        {
            clear();
            addValueWithUncertainty(val, uncertainty);
        }
        else
        {
            AttributeElement elem;
            elem.value = armarx::VariantPtr::dynamicCast(val)->clone();

            if (uncertainty)
            {
                elem.uncertainty = ProbabilityMeasureBasePtr::dynamicCast(uncertainty->ice_clone());
            }

            {

                values.at(0) = elem;
            }
        }
    }

    void EntityAttribute::addValue(const armarx::VariantBasePtr& val, const ::Ice::Current& c)
    {
        addValueWithUncertainty(val, ProbabilityMeasureBasePtr(), c);
    }

    void EntityAttribute::addValueWithUncertainty(const armarx::VariantBasePtr& val,
            const ProbabilityMeasureBasePtr& uncertainty,
            const ::Ice::Current&)
    {
        AttributeElement elem;
        elem.value = armarx::VariantPtr::dynamicCast(val)->clone();

        if (uncertainty)
        {
            elem.uncertainty = ProbabilityMeasureBasePtr::dynamicCast(uncertainty->ice_clone());
        }

        {
            std::scoped_lock lock(valuesMutex);
            values.push_back(elem);
        }
    }

    bool EntityAttribute::hasValue(const ::armarx::VariantBasePtr& val, const ::Ice::Current& c) const
    {
        // TODO extend for other types: compare operator for armarx::Variant needed
        if (val->getType() != armarx::VariantType::String)
        {
            throw armarx::NotImplementedYetException();
        }

        const std::string valStr = val->getString();

        std::scoped_lock lock(valuesMutex);

        for (AttributeElementList::const_iterator it = values.begin(); it != values.end(); ++it)
        {
            if (it->value->getType() == armarx::VariantType::String && it->value->getString() == valStr)
            {
                return true;
            }
        }

        return false;
    }

    armarx::VariantBasePtr EntityAttribute::getValue(const ::Ice::Current&) const
    {
        return getValueAt(0);
    }

    ProbabilityMeasureBasePtr EntityAttribute::getUncertainty(const ::Ice::Current&) const
    {
        return getUncertaintyAt(0);
    }

    armarx::VariantBasePtr EntityAttribute::getValueAt(::Ice::Int index, const ::Ice::Current&) const
    {
        std::scoped_lock lock(valuesMutex);
        return values.size() > (size_t) index ? values.at(index).value : armarx::VariantBasePtr();
    }

    ProbabilityMeasureBasePtr EntityAttribute::getUncertaintyAt(
        ::Ice::Int index, const ::Ice::Current&) const
    {
        std::scoped_lock lock(valuesMutex);
        return values.size() > (size_t) index ? values.at(index).uncertainty : ProbabilityMeasureBasePtr();
    }


    void EntityAttribute::removeValueAt(::Ice::Int index, const ::Ice::Current&)
    {
        std::scoped_lock lock(valuesMutex);

        if (values.size() > (size_t) index)
        {
            values.erase(values.begin() + index);
        }
    }

    void EntityAttribute::setElement(const AttributeElement& elem,
                                     const ::Ice::Current&)
    {
        std::scoped_lock lock(valuesMutex);
        clear();
        addElement(elem);
    }

    void EntityAttribute::addElement(const AttributeElement& elem,
                                     const ::Ice::Current&)
    {
        addValueWithUncertainty(elem.value, elem.uncertainty);
    }

    AttributeElement EntityAttribute::getElement(const ::Ice::Current&) const
    {
        return getElementAt(0);
    }

    AttributeElement EntityAttribute::getElementAt(::Ice::Int index, const ::Ice::Current&) const
    {
        std::scoped_lock lock(valuesMutex);
        return (values.size() > (size_t) index) ? values.at(index) : AttributeElement();
    }

    ::Ice::Int EntityAttribute::size(const ::Ice::Current&) const
    {
        std::scoped_lock lock(valuesMutex);
        return values.size();
    }

    void EntityAttribute::clear(const ::Ice::Current&)
    {
        std::scoped_lock lock(valuesMutex);
        values.clear();
    }

    Ice::ObjectPtr EntityAttribute::ice_clone() const
    {
        return this->clone();
    }

    EntityAttributePtr EntityAttribute::clone(const Ice::Current& c) const
    {
        std::scoped_lock lock(valuesMutex);
        return new EntityAttribute(*this);
    }

    // ********************************************************************
    // private methods
    // ********************************************************************
    void EntityAttribute::output(std::ostream& stream) const
    {
        std::scoped_lock lock(valuesMutex);

        for (AttributeElementList::const_iterator it = values.begin(); it != values.end(); ++it)
        {
            stream << "value: " << armarx::VariantPtr::dynamicCast(it->value);

            if (it->uncertainty)
            {
                stream << ", uncertainty: " << ProbabilityMeasureBasePtr::dynamicCast(it->uncertainty)->output();
            }

            stream << std::endl;
        }
    }

    void EntityAttribute::serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setElementType(armarx::ElementTypes::eArray);

        std::scoped_lock lock(valuesMutex);

        for (AttributeElementList::const_iterator it = values.begin(); it != values.end(); ++it)
        {
            armarx::AbstractObjectSerializerPtr objValue = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer)->createElement();
            objValue->setVariant("value", armarx::VariantPtr::dynamicCast(it->value));

            if (it->uncertainty)
            {
                armarx::VariantPtr uncertaintyVar = new armarx::Variant(armarx::VariantDataClassPtr::dynamicCast(it->uncertainty));
                objValue->setVariant("uncertainty", uncertaintyVar);
            }

            obj->append(objValue);
        }
    }

    void EntityAttribute::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        std::scoped_lock lock(valuesMutex);
        values.clear();
        //    values.reserve(localObj->size());

        for (unsigned int i = 0; i < obj->size(); ++i)
        {
            const armarx::AbstractObjectSerializerPtr objValue = obj->getElement(i);
            AttributeElement elem;

            try
            {
                elem.value = objValue->getVariant("value");
            }
            catch (std::exception& e)
            {
                ARMARX_IMPORTANT_S << "Could not get variant for: " << objValue->toString().substr(0, 2000) << ": " << e.what() << "\n Backtrace: \n" << armarx::LogSender::CreateBackTrace();
            }
            catch (...)
            {
                ARMARX_IMPORTANT_S << "Could not get variant for: " << objValue->toString().substr(0, 2000) << "\n Backtrace: \n" << armarx::LogSender::CreateBackTrace();
            }

            if (objValue->hasElement("uncertainty"))
            {
                armarx::VariantPtr uncertaintyVar = objValue->getVariant("uncertainty");
                elem.uncertainty = uncertaintyVar->getClass<ProbabilityMeasureBase>();
            }

            values.push_back(elem);
        }
    }

}
