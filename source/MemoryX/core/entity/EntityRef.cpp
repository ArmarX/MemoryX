/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EntityRef.h"

#include "../MongoSerializer.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/interface/serialization/JSONSerialization.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <Ice/ObjectAdapter.h>

namespace memoryx
{
    EntityRef::EntityRef()
    {
    }

    EntityRef::EntityRef(const std::string entityName)
    {
        this->entityName = entityName;
    }

    EntityRef::EntityRef(EntityBasePtr entity, const std::string& segmentName, const std::string& memoryName, MemoryInterfacePrx memoryPrx, EntityMemorySegmentInterfacePrx segmentPrx)
    {
        this->entityId = entity->getId();
        this->entityName = entity->getName();
        this->memory = memoryPrx;
        this->segment = segmentPrx;
        this->segmentName = segmentName;
        this->memoryName = memoryName;
    }

    EntityRef::EntityRef(const EntityRef& entityRef) :
        IceUtil::Shared(entityRef),
        EntityRefBase(entityRef)
    {
        this->entityId = entityRef.entityId;
        this->segment = entityRef.segment;
        this->segmentName = entityRef.segmentName;
        this->memory = entityRef.memory;
        this->memoryName = entityRef.memoryName;
        this->entityName = entityRef.entityName;
    }

    void memoryx::EntityRef::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setString("entityId", entityId);
        obj->setString("entityName", entityName);
        obj->setString("segmentName", this->segmentName);
        obj->setString("memoryName", this->memoryName);
    }

    void memoryx::EntityRef::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        entityId = obj->getString("entityId");
        entityName = obj->getString("entityName");
        memoryName = obj->getString("memoryName");
        segmentName = obj->getString("segmentName");


        if (c.adapter && c.adapter->getCommunicator())
        {
            memory = MemoryInterfacePrx::checkedCast(c.adapter->getCommunicator()->stringToProxy(memoryName));

            if (memory)
            {
                segment = memoryx::EntityMemorySegmentInterfacePrx::checkedCast(memory->getSegment(segmentName));
            }
            else
            {
                ARMARX_ERROR_S << "cannot create proxy to memory " << memoryName;
            }

            if (!segment)
            {
                ARMARX_ERROR_S << "cannot create proxy to segment " << segmentName << " in memory " << memoryName;
            }
        }
        else
        {
            ARMARX_ERROR_S << "adapter or communicator zero - cannot create proxy to segment " << segmentName;
        }
    }

    armarx::VariantDataClassPtr memoryx::EntityRef::clone(const Ice::Current& c) const
    {
        return new EntityRef(*this);
    }

    Ice::ObjectPtr EntityRef::ice_clone() const
    {
        return this->clone();
    }

    std::string memoryx::EntityRef::output(const Ice::Current&) const
    {
        return entityName + "_" + entityId + " in segment (" + segmentName + ")";
    }

    Ice::Int memoryx::EntityRef::getType(const Ice::Current&) const
    {
        return ::armarx::VariantType::EntityRef;
    }

    bool memoryx::EntityRef::validate(const Ice::Current&)
    {
        return true;
    }

    EntityBasePtr memoryx::EntityRef::getEntity(const Ice::Current&) const
    {
        if (segment)
        {
            return segment->getEntityById(entityId);
        }
        else
        {
            return nullptr;
        }
    }

    //"hey", you may say, "can't you just define operator== on EntityRefBase in memoryx/global ns?",
    //but unfortunately Ice already defines those in the memx ns and it takes precedence over the global definitions
    bool EntityRef::equals(const EntityRefBasePtr& other, const Ice::Current&) const
    {
        return entityId == other->entityId && segmentName == other->segmentName && memoryName == other->memoryName;
    }
}
