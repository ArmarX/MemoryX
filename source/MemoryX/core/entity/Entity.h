/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu), Kai Welke ( welke at kit dot edu )
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "AbstractEntityWrapper.h"
#include "EntityAttribute.h"

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/core/ProbabilityMeasures.h>

#include <ArmarXCore/observers/variant/Variant.h>

#include <type_traits>
#include <shared_mutex>
#include <mutex>
#include <list>

namespace memoryx
{

    class Entity;
    class EntityRef;
    /**
    * Typedef of EntityPtr as IceInternal::Handle<Entity> for convenience.
    */
    using EntityPtr = IceInternal::Handle<Entity>;

    /**
    \page MemoryX-HowTos-create-new-entities How to create new Entities in MemoryX

    \li define a class in slice which inherits from EntityBase and add custom accessor methods
    \code
    #include <MemoryX/interface/core/EntityBase.ice>

    memoryx
    {
        ["cpp:virtual"]
        class CountBase extends EntityBase
        {
            ["cpp:const"]
            int getCount();
            void setCount(int count);
        };
    };
    \endcode

    \li Define C++ class
    \code

    #ifndef ARMARX_MEMORYX_CORE_COUNT_ENTITY_H_
    #define ARMARX_MEMORYX_CORE_COUNT_ENTITY_H_

    #include <MemoryX/interface/core/EntityBase.h>
    #include <MemoryX/interface/memorytypes/MyNewEntities.h>
    #include <MemoryX/core/entity/Entity.h>

    // the following is required for the friend class declaration used together with private/protected constructors
    // see http://stackoverflow.com/a/2060691
    //
    //namespace armarx
    //{
    //    template <class IceBaseClass, class DerivedClass> class GenericFactory;
    //}

    namespace memoryx
    {
        class Count;
        using CountPtr = IceInternal::Handle<Count>;

        class Count :
            public memoryx::CountBase,
            public memoryx::Entity
        {
            // add this line if you want to make your constructor private/protected
            // you also need to uncomment the namespace declaration for this to work
            // template <class IceBaseClass, class DerivedClass> friend class armarx::GenericFactory;
        public:
            Count();
            Count(const Count& source);
            virtual ~Count();

            // cloning
            Ice::ObjectPtr ice_clone() const;
            CountPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

            // interface
            Ice::Int getCount(const Ice::Current& context = Ice::emptyCurrent) const;
            void setCount(Ice::Int count, const Ice::Current& context = Ice::emptyCurrent);

        private:
            void output(std::ostream &stream) const;

        public:
            // streaming operator
            friend std::ostream &operator<<(std::ostream &stream, const Count& rhs)
            {
                rhs.output(stream);
                return stream;
            }

            friend std::ostream &operator<<(std::ostream &stream, const CountPtr& rhs)
            {
                rhs->output(stream);
                return stream;
            }

            friend std::ostream &operator<<(std::ostream &stream, const CountBasePtr& rhs)
            {
                stream << CountPtr::dynamicCast(rhs);
                return stream;
            }
        };
    }
    #endif
    \endcode

    \li implement C++ class
    \code

    #include "Count.h"

    #include <ArmarXCore/core/system/Synchronization.h>

    namespace memoryx
    {
        Count::Count() :
            Entity()
        {
            EntityAttributePtr countAttribute = new EntityAttribute("count");
            putAttribute(countAttribute);
        }

        Count::Count(const Count& source):
            IceUtil::Shared(source),
            ::armarx::Serializable(source),
            EntityBase(),// dont copy
            CountBase(source),
            Entity(source)
        {
        }

        Count::~Count()
        {
        }

        void Count::output(std::ostream &stream) const
        {
            Entity::output(stream);
        }


        Ice::ObjectPtr Count::ice_clone() const
        {
            return this->clone();
        }

        CountPtr Count::clone(const Ice::Current& c) const
        {
            boost::shared_lock<boost::shared_mutex> entityLock(entityMutex);
            armarx::ScopedLock attributesLock(attributesMutex);
            armarx::RecursiveMutex::scoped_lock wrappersLock(wrappersMutex);
            CountPtr ret = new Count(*this);
            return ret;
        }

        Ice::Int Count::getCount(const Ice::Current& context) const
        {
            return getAttribute("count")->getValue()->getInt();
        }

        void Count::setCount(Ice::Int count, const Ice::Current& context)
        {
            getAttribute("count")->setValue(new armarx::Variant(count));
        }
    }
    \endcode

    \li add files to a library SOURCES/HEADERS variables in respective CMakeLists.txt

    \li create and register new ObjectFactory for your specialized memoryx::Entity subclass (this is required so Ice knows how to deserialize your new entity)
    \code

        namespace ObjectFactories
        {
            class NewTypesObjectFactories : public armarx::FactoryCollectionBase
            {
            public:
                armarx::ObjectFactoryMap getFactories()
                {
                    armarx::ObjectFactoryMap map;

                    // here be other types as well
                    // add<XBase, X>(map);

                    // call template function with Ice type and C++ specific type
                    add<CountBase, Count>(map);

                    return map;
                }
            };

            const armarx::FactoryCollectionBaseCleanUp NewTypesObjectFactoriesVar = armarx::FactoryCollectionBase::addToPreregistration(new NewTypesObjectFactories());
        }
    \endcode

        */

    /**
     * \class Entity
     *
     * Entity is the superclass for all MemoryX memory chunks.
     *
     * An entity can be stored within different memory types (WorkingMemory, LTM, ...) and can be serialized to a database for persistence.
     * Each entity is identified with an id, which is unique within a scope define by the memory processes (so it depends on the memory type and entity type).
     * Further, each entity has a friendly name identifying its content.
     *
     * Entities contain an arbitrary set of attributes (see memoryx::EntityAttribute).
     * This mechanism allows to add new attributes on the fly, without changing the declaration of the entity. Further, all subclasses of entities use
     * this attribute mechanism to store their properties.
     *
     * Convenient access to the attributes can be realized by implementing a subclass off memory::AbstractAttributeWrapper. Wrappers map the attributes
     * to c++ functions. Further extended functionality such as grid file attachement to attributes can be implemented in the wrapper.
     * Wrappers are registered to the entity with the addWrapper() method. Only one wrapper of a specific type can be added.
     *
     */
    class Entity :
        virtual public memoryx::EntityBase
    {
    protected:
        Entity(const Entity& source);
    public:
        /**
         * @brief Creates an entity without any convenience getter/setter functions. This should only be used with GenericSegments (@see memoryx::MemoryInterface::addGenericSegment()).
         *
         */
        static EntityPtr CreateGenericEntity();

        /**
         * Retrieve id of this entity which is an integer in string representation. Id is unique within one memory segment and is returned by e.g. WorkingMemorySegment::addEntity()
         * @return id as string
         */
        ::std::string getId(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set id of this entity. Id is usually set by adding the entity to a memory segment.
         * @param id as string
         */
        void setId(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve name of this entity
         * @return name
         */
        ::std::string getName(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set name of this entity
         * @param name
         */
        void setName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Indicates whether this entity only contains meta information
         * @return isMetaEntity
         */
        bool isMetaEntity(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Mark this entity as meta (i.e. it only contains meta information)
         * @param isMetaEntity
         */
        void setMetaEntity(bool isMetaEntity, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve parent entity references
         * @return parent entity references
         */
        virtual EntityRefBaseList getDirectParentRefs() const;

        /**
         * Retrieve all parents by traversing the whole hierarchy
         * @return all parents
         */
        virtual EntityRefBaseList getAllParentRefs(bool includeMetaEntities = true) const;

        virtual std::vector<std::string> getAllParentsAsStringList() const;

        /**
         * Replace parent entity references
         * @param entityRefs new parent entity references
         */
        virtual void setDirectParentRefs(const EntityRefBaseList& entityRefs);

        /**
         * Check whether this entity has an attribute with the given name.
         * @param attrName name of the attribute
         * @return attribute available
         */
        bool hasAttribute(const ::std::string& attrName, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Retrieve attribute from entity. If attribute is not present, a new attribute is returned. See Entity::hasAttribute()
         * @param attrName name of the attribute
         * @return shared pointer to attribute
         */
        EntityAttributeBasePtr getAttribute(const ::std::string& attrName, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Retrieve value of an attribute from entity. If attribute is not present, a new variant is returned. See Entity::hasAttribute()
         * @param attrName name of the attribute
         * @return shared pointer to variant containing the value
         */
        virtual armarx::VariantPtr getAttributeValue(const ::std::string& attrName) const;

        /**
         * Store attribute in entity. Name is taken from attribute member. Overwrites attribute with the same name if present.
         *
         * @param attr pointer to attribute
         */
        void putAttribute(const ::memoryx::EntityAttributeBasePtr& attr, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Create and store attribute from name, value, and optionally uncertainty measure. Overwrites attribute with the same name if present.
         *
         * @param attrName name of the attribute
         * @param attrValue value of the attribute
         * @param uncertainty probability measure
         */
        template<typename T>
        void putAttribute(const std::string& attrName, T attrValue,
                          ProbabilityMeasureBasePtr uncertainty = ProbabilityMeasureBasePtr())
        {
            const ::memoryx::EntityAttributeBasePtr attr = new EntityAttribute(attrName);
            attr->setValueWithUncertainty(armarx::VariantPtr(new armarx::Variant(attrValue)), uncertainty);

            putAttribute(attr);
        }

        /**
         * Remove attribute with given name from entity.
         *
         * @param attrName name of the attribute
         */
        void removeAttribute(const ::std::string& attrName, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve list of all attribute names
         *
         * @return list of names
         */
        memoryx::NameList getAttributeNames(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief equals computes if two Entity instances are equal.
         * Attributes of both Entities in the order Id, name, and attributes have to match in order to be considered equal.
         *
         * @see Entity::equalsAttributes
         *
         * @param otherEntity the Entity to compare with
         * @return true if both entities are equal, false otherise
         */
        bool equals(const EntityBasePtr& otherEntity, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief equalsAttributes computes if two Entity instances are equal.
         * Attributes of both Entities in the order name and attributes have to match in order to be considered equal.
         *
         * @see Entity::equals
         *
         * @param otherEntity the Entity to compare with
         * @return true if both entities are equal, false otherise
         */
        bool equalsAttributes(const EntityBasePtr& otherEntity, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Add EntityWrapper to entity. EntityWrapper provide a specific view on the entity and allow convenient c++ style access to the
         * attributes of the entity. They can also handle associated files. See e.g. EntityWrappers::SimoxObjectWrapper
         *
         * Example:
         *
         * SimoxObjectWrapperPtr simoxWrapper = entity->addWrapper(new SimoxObjectWrapper(...));
         * ManipulationObjectPtr mo = simoxWrapper->getManipulationObject();
         *
         *
         * @param pointer to wrapper
         */
        template<typename T>
        IceInternal::Handle<T> addWrapper(T* wrapper)
        {
            static_assert(!std::is_same_v<T, EntityWrappers::AbstractEntityWrapper>,
                          "The template parameter for Entity::addWrapper should not be of base class AbstractEntityWrapper");

            static_assert(!std::is_base_of_v<T, EntityWrappers::AbstractEntityWrapper>,
                          "The template parameter for Entity::addWrapper needs to inherit from AbstractEntityWrapper");

            IceInternal::Handle<T> result = getWrapper<T>();

            if (!result)
            {
                wrapper->setEntity(this);
                std::scoped_lock lock(wrappersMutex);
                wrappers.push_back(wrapper);
                return wrapper;
            }

            return result;
        }


        /**
         * Retrieve EntityWrapper that has previously been added with Entity::addWrapper().
         * @see addWrapper
         *
         * @return NULL if no wrapper of type T
         */
        template<typename T>
        IceInternal::Handle<T> getWrapper()
        {
            static_assert(!std::is_same_v<T, EntityWrappers::AbstractEntityWrapper>,
                          "The template parameter for Entity::getWrapper should not be of base class AbstractEntityWrapper");

            static_assert(!std::is_base_of_v<T, EntityWrappers::AbstractEntityWrapper>,
                          "The template parameter for Entity::getWrapper needs to inherit from AbstractEntityWrapper");
            std::scoped_lock lock(wrappersMutex);
            AbstractEntityWrapperBaseList::iterator iter = wrappers.begin();

            while (iter != wrappers.end())
            {
                IceInternal::Handle<T> h = IceInternal::Handle<T>::dynamicCast(*iter);

                if (h)
                {
                    return h;
                }

                iter++;
            }

            //            ARMARX_ERROR_S << "No Wrapper found - " << wrappers.size() << " in list";
            return NULL;
        }

        // cloning
        Ice::ObjectPtr ice_clone() const override;
        EntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        /**
        * Subclasses should use serializable attributes and consider these methods final.
        *
        * Exemplary JSON serialization:
        * \code
            EntityPtr entity = new Entity;
            armarx::JSONObjectPtr jsonSerializer = new armarx::JSONObject;
            entity->serialize(jsonSerializer);
            ARMARX_ERROR << "Entity: " << jsonSerializer->asString(true);
        * \endcode
        */
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * Subclasses should use serializable attributes and consider these methods final.
        *
        * Exemplary JSON deserialization:
        * \code
            EntityPtr entity = new EntityPtr;
            armarx::JSONObjectPtr jsonSerializer = new armarx::JSONObject(getIceManager()->getCommunicator());
            jsonSerializer->fromString(json);
            entity->deserialize(jsonSerializer);
        * \endcode
        */
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override;


    protected:
        template <class BaseClass, class VariantClass>
        friend class armarx::GenericFactory;

        Entity() {}
        ~Entity() override; // only shared pointers allowed
        void output(std::ostream& stream) const;

        AbstractEntityWrapperBaseList wrappers;

        mutable std::mutex attributesMutex;
        mutable std::shared_mutex entityMutex;
        mutable std::recursive_mutex wrappersMutex;

        void ice_preMarshal() override;
        void ice_postUnmarshal() override;
    private:
        /**
           * This copies the content of the entity and clones at its pointers.
           * @see Entity(const Entity& source)
           */
        void deepCopy(const Entity& source);
    public:    // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const Entity& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const EntityPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const EntityBasePtr& rhs)
        {
            stream << EntityPtr::dynamicCast(rhs);
            return stream;
        }

        //        // Shared interface
    public:
        void __decRef() override;
    };

}
