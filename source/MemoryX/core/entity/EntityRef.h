/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <MemoryX/interface/core/MemoryInterface.h>

#include <ArmarXCore/observers/variant/Variant.h>

namespace armarx::VariantType
{
    const armarx::VariantTypeId EntityRef = armarx::Variant::addTypeName("::memoryx::EntityRefBase");
}

namespace memoryx
{

    class EntityRef;
    using EntityRefPtr = IceInternal::Handle<EntityRef>;

    class MongoSerializer;
    using MongoSerializerPtr = IceUtil::Handle<MongoSerializer>;

    /**
     * @brief The EntityRef class is used to store references to Entities as values in other Entity instances.
     */
    class EntityRef :
        public EntityRefBase
    {
    public:
        EntityRef();
        EntityRef(const std::string entityName);
        EntityRef(EntityBasePtr entity, const std::string& segmentName, const std::string& memoryName, MemoryInterfacePrx memoryPrx, EntityMemorySegmentInterfacePrx segmentPrx);
        EntityRef(const EntityRef& entityRef);

        // Serializable interface
    public:
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) override;

        // VariantDataClass interface
    public:
        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override;
        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override;
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override;
        Ice::Int getType(const Ice::Current& c = Ice::emptyCurrent) const override;

        bool validate(const Ice::Current& c = Ice::emptyCurrent) override;
        EntityBasePtr getEntity(const Ice::Current& c = Ice::emptyCurrent) const override;
    public:
        bool equals(const EntityRefBasePtr& other, const Ice::Current& c = Ice::emptyCurrent) const override;
    };
}

