/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu), Kai Welke (welke at kit got edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/EntityBase.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <type_traits>
#include <mutex>

namespace memoryx
{

    class EntityAttribute;
    /**
    * Typedef of EntityAttributePtr as IceInternal::Handle<EntityAttribute> for convenience.
    */
    using EntityAttributePtr = IceInternal::Handle<EntityAttribute>;

    /**
    * Attribute of MemoryX entities.
    *
    * Each memory entity can have an arbitraty number of attributes, identified by their names. Essentially, they are stored as a map std::string -> EntityAttribute.
    * EntityAttribute has one or many AttributeElements, each of them representing a value with uncertainty as a pair (value, probabilityMeasure).
    * See documentation of memoryx::AttributeElement for details on what types are supported as values.
    */
    class EntityAttribute :
        public memoryx::EntityAttributeBase
    {
        // required for the object factory
        template <class IceBaseClass, class DerivedClass>
        friend class ::armarx::GenericFactory;
    public:
        /** Constructs a new EntityAttribute.
         *
         * @param name the attribute is identified via this name
         **/
        EntityAttribute(const std::string& name);

        /** Constructs a new EntityAttribute and assigns a value
         *
         * @param name the attribute is identified via this name
         * @param value value for this attribute
         **/
        EntityAttribute(const std::string& name, const armarx::VariantBasePtr& val);

        /** Constructs a copy from another EntityAttribute instance.
          *
          * @param other EntityAttribute to copy from
          **/
        EntityAttribute(const EntityAttribute& other);

        ~EntityAttribute() override;

        /**
        * Retrieve name of the attribute.
        *
        * @return attribute name
        **/
        ::std::string getName(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * Sets (single) value of the attribute. Uncertainty assumed to be empty (=unknown/not specified).
        * This is equivalent to clearing the attribute value list (clear()) and calling addValue().
        *
        * @param val value to set
        **/
        void setValue(const ::armarx::VariantBasePtr& val, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
        * Sets (single) value of the attribute along with the corresponding uncertainty.
        * This is equivalent to clearing the attribute values list (clear()) and calling addValueWithUncertainty().
        *
        * @param val            value to set
        * @param uncertainty    probability distribution representing the value uncertainty
        *
        **/
        void setValueWithUncertainty(const ::armarx::VariantBasePtr& val,
                                     const ::memoryx::ProbabilityMeasureBasePtr& uncertainty, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Adds value to the end of attribute values list. Uncertainty assumed to be empty (=unknown/not specified).
         *
         * @param val value to add
         **/
        void addValue(const ::armarx::VariantBasePtr& val, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
        * Adds value along with the corresponding uncertainty to the end of attribute values list.
        * This is equivalent to clearing the attribute value list (clear()) and calling addValueWithUncertainty().
        *
        * @param val            value to set
        * @param uncertainty    probability distribution representing the value uncertainty
        *
        **/
        void addValueWithUncertainty(const ::armarx::VariantBasePtr& val,
                                     const ::memoryx::ProbabilityMeasureBasePtr& uncertainty, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Removes the value at a given index from the list of values stored in the attribute.
         *
         * @param index of value
         **/
        void removeValueAt(::Ice::Int index, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
        * Checks whether the value val is currently stored in the attribute. If attribute stores multiple values,
        * each of them will be compared with val and in case of at least one match the result will be positive.
        *
        * NOTE: currently, this method can be used for String values ONLY! To make it applicable for other datatypes,
        * general comparison method for armarx::Variant should be implemented in Core first.
        *
        * @return true if attribute has a value val, false otherwise
        **/
        bool hasValue(const ::armarx::VariantBasePtr& val, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * Retrieves the value of the attribute. Corresponds to calling getValueAt(0)
        *
        * @return value of attribute
        **/
        ::armarx::VariantBasePtr getValue(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * Retrieves the uncertainty of the attribute value. Corresponds to calling getUncertaintyAt(0)
        *
        * @return uncertainty of attribute value
        **/
        ::memoryx::ProbabilityMeasureBasePtr getUncertainty(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Retrieve the value at a given index from the list of values stored in the attribute.
         *
         * @param index of value
         * @return value at index
         **/
        ::armarx::VariantBasePtr getValueAt(::Ice::Int index, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Retrieve the uncertainty of the value at a given index from the list of values stored in the attribute.
         *
         * @param index of value
         * @return uncertainty of value at index
         **/
        ::memoryx::ProbabilityMeasureBasePtr getUncertaintyAt(::Ice::Int index, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * Sets (single) attribute element (=value+uncertainty) to the attribute.
        * This is equivalent to clearing the attribute value list (clear()) and calling addValue().
        *
        * @param elem attribute element to set
        **/
        void setElement(const ::memoryx::AttributeElement& elem, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
        * Adds attribute element (=value+uncertainty) to the attribute.
        *
        * @param elem attribute element to add
        **/
        void addElement(const ::memoryx::AttributeElement& elem, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
        * Retrieves the attribute element (=value+uncertainty).
        * Corresponds to calling getElementAt(0).
        *
        * @return uncertainty of attribute value
        **/
        ::memoryx::AttributeElement getElement(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * Retrieves an attribute element (=value+uncertainty) with a given index.
        *
        * @return uncertainty of attribute value
        **/
        ::memoryx::AttributeElement getElementAt(::Ice::Int index, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Retrieve number of values stored with the attribute
         *
         * @return number of values
         **/
        ::Ice::Int size(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Clears all values. The attribute will not contain any value after calling clear.
         **/
        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;



        // TODO: cloning does not work since clone needs to be called explicilety on each Variant!!!
        // But need to solve Value issue before cloning
        Ice::ObjectPtr ice_clone() const override;
        EntityAttributePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    public: // serialization
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override;

    protected:
        mutable std::recursive_mutex valuesMutex;

    private:
        void output(std::ostream& stream) const;

    public:    // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const EntityAttribute& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const EntityAttributePtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const EntityAttributeBasePtr& rhs)
        {
            stream << EntityAttributePtr::dynamicCast(rhs);
            return stream;
        }

    private:
        EntityAttribute()
        {
        }
    };

}

