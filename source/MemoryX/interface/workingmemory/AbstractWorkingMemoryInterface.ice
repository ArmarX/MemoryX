/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu), Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/MemoryInterface.ice>
#include <MemoryX/interface/core/FusionMethods.ice>
#include <MemoryX/interface/components/CommonStorageInterface.ice>
#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.ice>
#include <MemoryX/interface/workingmemory/WorkingMemoryListenerInterface.ice>

#include <ArmarXCore/interface/observers/Serialization.ice>

module memoryx
{
    ["cpp:virtual"]
    class AbstractWorkingMemorySegment extends AbstractMemorySegment
    {
        ["protected"]
        void setListenerProxy(WorkingMemoryListenerInterface* observerProxy);
    };

    /*
      * Generic WorkingMemorySegment
      *
      */
    ["cpp:virtual"]
    class WorkingMemoryEntitySegmentBase extends AbstractWorkingMemorySegment
        implements EntityMemorySegmentInterface
    {
        void addFusionMethod(EntityFusionMethodBase fusionMethod);

        WorkingMemoryListenerInterface* getListenerProxy();

        ["protected"]
        FusionMethodList fusionMethods;
        ["protected"]
        WorkingMemoryListenerInterface* listenerProxy;
        ["protected"]
        string objectTypeId;
    };


     /*
      * WorkingMemory definitions
      *
      */
    interface AbstractWorkingMemoryInterface extends MemoryInterface
    {
        // updater handling
        WorkingMemoryUpdaterBase* registerUpdater(string updaterName, WorkingMemoryUpdaterBase updater);
        WorkingMemoryUpdaterBase* getUpdater(string updaterName);
        void unregisterUpdater(string updaterName);

        // misc
        ["cpp:const"]
        idempotent void print();
    };
};

