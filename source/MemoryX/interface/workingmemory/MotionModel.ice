/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     David Schiebener (schiebener at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/ChannelRefBase.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <RobotAPI/interface/core/LinkedPoseBase.ice>
#include <MemoryX/interface/components/WorkingMemoryInterface.ice>
#include <MemoryX/interface/components/LongtermMemoryInterface.ice>
#include <MemoryX/interface/core/ProbabilityMeasures.ice>
#include <MemoryX/interface/memorytypes/MemorySegments.ice>


module memoryx
{

    /**
      see MemoryX/interface/memorytypes/MemorySegments.ice for the base class MotionModelInterface, because zeroc broke forward declaration
      */

    ["cpp:virtual"]
    class MotionModelAttachedToOtherObjectBase extends MotionModelInterface
    {
        armarx::ChannelRefBase channelRefToObjectToWhichThisIsAttached;
        armarx::VariantBase initialGlobalPoseOfObjectToWhichThisIsAttached; // this is a 4x4 matrix
        armarx::VariantBase globalPoseOfObjectToWhichThisIsAttachedAtLocalizationStart; // this is a 4x4 matrix
        bool channelRefIsValid;
    };

    ["cpp:virtual"]
    class MotionModelRobotHandBase extends MotionModelInterface
    {
        string handNodeName;
    };

    ["cpp:virtual"]
    class MotionModelKBMBase extends MotionModelInterface
    {
        string nodeSetName;
        string referenceNodeName;
        int nDoF;
        memoryx::LongtermMemoryInterface* longtermMemory;
    };

    ["cpp:virtual"]
    class MotionModelStaticObjectBase extends MotionModelInterface
    {
        memoryx::WorkingMemoryInterface* workingMemoryProxy;
    };

};

