/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu), Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.ice>
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <MemoryX/interface/memorytypes/MemorySegments.ice>
#include <MemoryX/interface/components/PriorKnowledgeInterface.ice>

module memoryx
{
    ["cpp:virtual"]
    interface WorkingMemoryInterface extends AbstractWorkingMemoryInterface
    {
        ObjectInstanceMemorySegmentBase* getObjectInstancesSegment();
        ObjectClassMemorySegmentBase* getObjectClassesSegment();
        RelationMemorySegmentBase* getRelationsSegment();
        ActiveOacMemorySegmentBase* getActiveOacSegment();
        AgentInstancesSegmentBase* getAgentInstancesSegment();
        WorldStateSegmentBase* getWorldStateSegment();
        AffordanceSegmentBase* getAffordanceSegment();
        EnvironmentalPrimitiveSegmentBase* getEnvironmentalPrimitiveSegment();
        
        ObjectLocalizationMemoryUpdaterBase* getObjectLocalizationUpdater();
        
        ["cpp:const"]
        idempotent CommonStorageInterface* getCommonStorage();

        ["cpp:const"]
        idempotent PriorKnowledgeInterface* getPriorKnowledge();
    };    
};

