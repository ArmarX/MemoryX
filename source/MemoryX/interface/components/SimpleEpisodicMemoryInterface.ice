/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::SimpleEpisodicMemory
* @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
* @date       2020
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

module memoryx
{
////////////////////////////////////
// Image Information
////////////////////////////////////
    sequence<byte> ImageData;
    enum ColourSpace {
        RGB,
        GRAYSCALE
    };
    struct ImageEvent {
        string providerName;
        ImageData data;
        int width;
        int height;
        int sequenceId;
        ColourSpace colourType;
        double receivedInMs;
    };
    sequence<ImageEvent> ImageEventList;
    dictionary<string, ImageEventList> ImageEventMap;

////////////////////////////////////
// Action Information
////////////////////////////////////
    enum ActionStatus {
        ACTION_STARTED,
        ACTION_RUNNING,
        ACTION_REPEATED,
        ACTION_COMPLETED_SUCCESS,
        ACTION_COMPLETED_FAILURE
    };
    struct ActionEvent {
        string actionName;
        ActionStatus status;
        double receivedInMs;
    };
    sequence<ActionEvent> ActionEventList;

////////////////////////////////////
// Human Pose Information
////////////////////////////////////
    struct HumanKeypoint
    {
        string label;
        float x;
        float y;
        float z;
        float globalX;
        float globalY;
        float globalZ;
        float confidence;
        bool enabled;
    };
    dictionary<string, HumanKeypoint> HumanKeypointMap;
    struct Body25HumanPoseEvent {
        HumanKeypointMap keypoints;
        double receivedInMs;
    };
    sequence<Body25HumanPoseEvent> Body25HumanPoseEventList;

////////////////////////////////////
// Speech Information
////////////////////////////////////
    struct SpeechEvent {
        string text;
        double receivedInMs;
    };
    sequence<SpeechEvent> SpeechEventList;

////////////////////////////////////
// KinematicUnit Information
////////////////////////////////////
    struct KinematicUnitData {
        float jointAngle;
        float jointVelocity;
        float jointTorque;
        float jointAcceleration;
        float current;
        float temperature;
        bool enabled;
    };
    dictionary<string, KinematicUnitData> KinematicUnitDataMap;
    struct KinematicUnitEvent {
        KinematicUnitDataMap data;
        double receivedInMs;
    };
    sequence<KinematicUnitEvent> KinematicUnitEventList;

////////////////////////////////////
// PlatformPose Information
////////////////////////////////////
    struct PlatformUnitEvent {
        float x;
        float y;
        float rot;
        float acc_x;
        float acc_y;
        float acc_rot;
        double receivedInMs;
    };
    sequence<PlatformUnitEvent> PlatformUnitEventList;

    struct PlatformUnitTargetEvent {
        float target_x;
        float target_y;
        float target_rot;
        double receivedInMs;
    };
    sequence<PlatformUnitTargetEvent> PlatformUnitTargetEventList;

////////////////////////////////////
// Episode Information
////////////////////////////////////
    enum ObjectPoseEventType {
        NEW_OBJECT_RECOGNIZED,
        OBJECT_POSE_UPDATE
    };
    struct ObjectPoseEvent {
        string objectName;
        float x;
        float y;
        float z;
        string frame;
        ObjectPoseEventType type;
        double receivedInMs;
    };
    sequence<ObjectPoseEvent> ObjectPoseEventList;

////////////////////////////////////
// Episode Information
////////////////////////////////////
    enum EpisodeStatus {
        EPISODE_STARTED,
        EPISODE_COMPLETED_SUCCESS,
        EPISODE_COMPLETED_FAILURE,
        EPISODE_COMPLETED_ABORT
    };

    struct EpisodeEvent {
        string episodeName;
        EpisodeStatus status;
        double receivedInMs;
    };

    struct Episode {
        string episodeName;
        double startedInMs;
        double endedInMs;
        EpisodeStatus status;

        ImageEventMap imageEvents;
        ObjectPoseEventList objectPoseEvents;
        ActionEventList actionEvents;
        Body25HumanPoseEventList humanPoseEvents;
        SpeechEventList speechEvents;
        KinematicUnitEventList kinematicUnitEvents;
        PlatformUnitEventList platformUnitEvents;
        PlatformUnitTargetEventList platformUnitTargetEvents;
    };

    // NotificationType
    enum NotificationType {
        ITS_BEEN_A_LONG_TIME,
        CONTACT_RELATION_CHANGE_DETECTED,
        NEW_HUMAN_DETECTED,
        IMPORTANT_HUMAN_POSE_DETECTED,
        NEW_OBJECT_DETECTED,
        IMPORTANT_OBJECT_POSE_DETECTED,
        ACTION_START_DETECTED,
        ACTION_SUCCESS_DETECTED,
        ACTION_FAILURE_DETECTED
    };

    interface SimpleEpisodicMemoryInterface
    {
        void registerEpisodeEvent(EpisodeEvent episode);
        void registerImageEvent(ImageEvent image);
        void registerObjectPoseEvent(ObjectPoseEvent object);
        void registerActionEvent(ActionEvent action);
        void registerHumanPoseEvent(Body25HumanPoseEvent pose);
        void registerSpeechEvent(SpeechEvent speech);
        void registerKinematicUnitEvent(KinematicUnitEvent unit);
        void registerPlatformUnitEvent(PlatformUnitEvent unit);
        void registerPlatformUnitTargetEvent(PlatformUnitTargetEvent target);

        void notifyKeyframe();
    };

    interface SimpleEpisodicMemoryListener
    {
        void EpisodeEventReceived();
        void ImageEventReceived();
        void ObjectPoseEventReceived();
        void ActionEventReceived();
        void HumanPoseEventReceived();
        void SpeechEventReceived();
    };
};

