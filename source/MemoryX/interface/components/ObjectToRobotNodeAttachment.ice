/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu), Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/BuiltinSequences.ice>

#include <RobotAPI/interface/core/PoseBase.ice>


module memoryx
{
    struct ObjectIdOrClass
    {
        /// If non-empty, the object's memory ID.
        string memoryID;
        /// Only used if `objectMemoryID` is empty.
        string className;
    };


    struct AttachObjectToRobotNodeInput
    {
        ObjectIdOrClass object;

        string robotNodeName;

        /// False: Object is drawn at `relativePose`
        /// True: Object is drawn at current offset + relative pose.
        bool useCurrentPose = true;

        /**
         * @brief objectOffset
         *
         * If left empty, identity.
         * In local object coordinate system.
         */
        armarx::PoseBase objectOffset;
    };


    struct DetachObjectFromRobotNodeInput
    {
        /// The object to detach.
        ObjectIdOrClass object;
    };


    interface ObjectToRobotNodeAttachmentInterface
    {
        void attachObjectToRobotNode(AttachObjectToRobotNodeInput input);
        void detachObjectFromRobotNode(DetachObjectFromRobotNodeInput input);
    };
};

