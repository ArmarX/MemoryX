/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::PriorKnowledge
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/MemoryInterface.ice>
#include <MemoryX/interface/components/CommonStorageInterface.ice>
#include <MemoryX/interface/memorytypes/MemorySegments.ice>

#include <ArmarXCore/interface/observers/Serialization.ice>

module memoryx
{
     /*
      * PriorKnowledge definitions
      * 
      */    

    const string PRIOR_COLLECTION_PREFIX = "Prior_";
    
    interface PriorKnowledgeInterface extends MemoryInterface
    {
    	["cpp:const"]
    	PersistentObjectClassSegmentBase* getObjectClassesSegment(); 
    	
    	["cpp:const"]
    	PersistentRelationSegmentBase* getRelationsSegment(); 

        ["cpp:const"]
        GraphMemorySegmentBase* getGraphSegment();

        ["cpp:const"]
        bool hasGraphSegment();

    	["cpp:const"]
        idempotent CommonStorageInterface* getCommonStorage();

        
        bool isPriorCollection(string collNS);
    };
    
};

