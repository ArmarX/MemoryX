/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonPlacesLearner
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/ProbabilityMeasures.ice>
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>

module memoryx
{
    struct Point3D
    {
        float x;
        float y;
        float z;
    };
    
    struct Cluster3D 
    {
        Point3D center;
        float weight;
    };
    
    sequence<Cluster3D> Cluster3DList;
    
    enum DeviationType 
    {
        eDevAABB,
        eDevOrientedBBox,
        eDevEqualSphere
    };

    interface CommonPlacesLearnerInterface
    {
        void setLTMSegmentName(string segmentName);
        void setAgingFactor(float factor);
        void setMergingThreshold(float threshold);
        
        void learnFromSnapshot(string snapshotName);
        void learnFromObject(ObjectInstanceBase newObject);
        void learnFromObjectMCA(string objectName, MultivariateNormalDistributionBase posDist);
        
        NormalDistributionBase getPositionAsGaussian(string objectName);
        GaussianMixtureDistributionBase getPositionFull(string objectName);
        GaussianMixtureDistributionBase getPositionReducedByComponentCount(string objectName, int compCount);
        GaussianMixtureDistributionBase getPositionReducedByMaxDeviation(string objectName, float maxDeviation, DeviationType devType);
        
        Cluster3DList getPositionClustersByComponentCount(string objectName, int compCount);
        Cluster3DList getPositionClustersByMaxDeviation(string objectName, float maxDeviation, DeviationType devType);
    };
    
};

