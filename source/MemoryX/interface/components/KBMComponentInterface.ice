/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::ViconStatechartContext
 * @author     Jonas Rauber ( kit at jonasrauber dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/observers/Matrix.ice>
#include <RobotAPI/interface/core/PoseBase.ice>

module armarx
{   
    interface KBMComponentInterface
    {
        /**
         * @brief createKBM creates a new KBM and makes it available under the given name
         * @param name the name of the KBM
         * @param nDoF degrees of freedom
         * @param dim dimensions
         * @param spreadAngle the common spreadAngle for joints
         */
        void createKBM(string name, int nDoF, int dim, float spreadAngle);

        /**
         * @brief createArmar3KBM creates a KBM from a VirtualRobot model
         * @param name the KBM will be available under this name
         * @param activeJoints the names of the joints that are used
         * @param tcpName name of the joint that corresponds best to the tool center point
         * @param useOrientation use only the position (3 dimensions) or position and orientation (9 dimensions in total)
         */
        void createArmar3KBM(string name,
                             Ice::StringSeq activeJoints,
                             Ice::DoubleSeq jointValueAverages,
                             Ice::DoubleSeq spreadAngles,
                             string tcpName,
                             bool useOrientation);

        /**
         * @brief batch learning of n training samples
         * @param name the name of the KBM
         * @param proprioception length must be DoF * n
         * @param position length must be Dim * n
         * @param method will be mapped to the Enum in the KBM class, currently 0 = STANDARD, 1 = PLS
         * @param threshold should equal the expected mean positioning error
         */
        void batch(string name,
                   Ice::DoubleSeq proprioception,
                   Ice::DoubleSeq position,
                   int method,
                   float threshold);

        /**
         * @brief online learning of n training samples
         * @param name the name of the KBM
         * @param proprioception length must be DoF * n
         * @param position length must be Dim * n
         * @param learnRate learning rate between 0 and 1
         */
        void online(string name,
                    Ice::DoubleSeq proprioception,
                    Ice::DoubleSeq position,
                    float learnRate);

        /**
         * @brief online learning of n training samples
         * @param name the name of the KBM
         * @param proprioception length must be DoF * n
         * @param position length must be Dim * n
         * @param learnRate learning rate between 0 and 1
         */
        void onlineVerbose(string name,
                    Ice::DoubleSeq proprioception,
                    Ice::DoubleSeq position,
                    float learnRate,
                    Ice::DoubleSeq evaluationProprioception,
                    Ice::DoubleSeq evaluationPosition);

        /**
         * @brief predict the position for n samples of proprioception (forward kinematics)
         * @param name the name of the KBM
         * @param proprioception length must be DoF * n
         * @return the predicted position with length Dim * n
         */
        Ice::DoubleSeq predict(string name, Ice::DoubleSeq proprioception);

        /**
         * @brief calls getErrors on the KBM and prints the result
         * @param name the name of the KBM
         * @param proprioception the proprioception to pass to getErrors
         * @param position the position to pass to getErrors
         */
        void printErrors(string name,
                         Ice::DoubleSeq proprioception,
                         Ice::DoubleSeq position);

        /**
         * @brief accumulate adds the given position to the accumulator to use it later
         * @param position length must be Dim * n
         */
        void accumulatePositions(Ice::DoubleSeq position);

        /**
         */
        void accumulateEvaluationPositions(Ice::DoubleSeq position);

        /**
         * @brief getRawJointValuesAverages calculates the average raw joint values for each joint
         * from the data stored in the raw joint values accumulator.
         */
        Ice::DoubleSeq getRawJointValuesAverages(int nDoF);

        /**
         * @brief getSpreadAngles returns the spreadAngles calculated using the data in the proprioceptionAccumulator
         */
        Ice::DoubleSeq getSpreadAngles(int nDoF);

        /**
         * @brief accumualteProprioceptions accumulates proprioceptions
         */
        void accumulateProprioceptions(Ice::DoubleSeq proprioceptions);

        /**
         */
        void accumulateEvaluationProprioceptions(Ice::DoubleSeq proprioceptions);

        /**
         * @brief accumualteRawJointValues accumulates raw joint values that will be transformed to proprioceptions later
         */
        void accumulateRawJointValues(Ice::DoubleSeq rawJointValuesSequence);

        /**
         * @brief setProprioceptionAccumulatorFromRawJointAccumulator updates the proprioception accumulator
         */
        void setProprioceptionAccumulatorFromRawJointAccumulator(Ice::DoubleSeq rawJointValuesAverages);

        /**
         * @brief setPositionLimits sets the position limits using the data in the position accumulator
         */
        void setPositionLimits(int nDim);

        /**
         * @brief onlineAccumulator calls online after transforming the raw joint accumulator to proprioceptions
         */
        void onlineAccumulator(string name, float learningRate);

        /**
         * @brief onlineAccumulatorVerbose same as onlineAccumulator, but evaluates after each learning step
         */
        void onlineAccumulatorVerbose(string name, float learningRate);

        /**
         * @brief the same as batch but with the data from the accumulator
         */
        void batchAccumulator(string name, int method, float threshold);

        /**
         * @brief the same as printErrors but with the data from the accumulator
         */
        void printErrorsAccumulator(string name);

        /**
         * @brief printAccumulatorStatistics prints information about the values in the accumulators
         */
        void printAccumulatorStatistics(int nDim, int nDoF);

        /**
         * @brief the same as predict but with the data from the accumulator, however the results are currently just printed and not returned
         */
        void predictAccumulator(string name);

        /**
         * @brief solveGlobalIK solves the global inverse kinematics for the given position
         * @param name the name of the KBM
         * @param targetPosition
         * @return the proprioception
         */
        Ice::DoubleSeq solveGlobalIK(string name, Ice::DoubleSeq targetPosition);

        /**
         * @brief solveDifferentialIK solves the differential inverse kinematics for the given position
         * @param name the name of the KBM
         * @param targetPosition
         * @param currentPosition
         * @param currentProprioception
         * @return the proprioception
         */
        Ice::DoubleSeq solveDifferentialIK(string name,
                                          Ice::DoubleSeq targetPosition,
                                          Ice::DoubleSeq currentPosition,
                                          Ice::DoubleSeq currentProprioception,
                                          Ice::DoubleSeq lowerProprioceptionLimits,
                                          Ice::DoubleSeq upperProprioceptionLimits);

        void storeToMemory(string kbmName, string nodeSetName, string referenceNodeName, string robotName);

        void restoreFromMemory(string name, string nodeSetName, string referenceNodeName, string robotName);

        bool isInMemory(string nodeSetName, string referenceNodeName, string robotName);

        /**
         * Returns the names of the existing KBMs
         */
        Ice::StringSeq kbmNames();

        /**
         * Clears the accumulators
         */
        void clearAccumulators();
    };
};

