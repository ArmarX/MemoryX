/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/CommonTypes.ice>

module memoryx {

    exception SegmentAlreadyExistsException extends MemoryXException {};
    exception SegmentNotFoundException extends MemoryXException {};
    
    /*
     * WorkingMemory exceptions
     * 
     */    
    exception WorkingMemoryException extends MemoryXException {};
    
    exception EntityNotFoundException extends WorkingMemoryException {};

    exception InvalidEntityException extends WorkingMemoryException {};

    exception InvalidEntityIdException extends WorkingMemoryException {};

    exception RelationNotFoundException extends WorkingMemoryException {};

    exception LongtermMemoryNotAvailableException extends WorkingMemoryException {};

    exception PriorKnowledgeNotAvailableException extends WorkingMemoryException {};
    
    /*
     * CommonStorage exceptions
     * 
     */    
    exception CommonStorageException extends MemoryXException {};
    
    exception NotConnectedException extends CommonStorageException {}; 

    exception MongoAuthenticationException extends CommonStorageException {}; 

    exception DBNotSpecifiedException extends CommonStorageException {};

    exception InvalidLockTokenException extends CommonStorageException {};

    exception InvalidMongoIdException extends CommonStorageException
    {
        string mongoId;
    };
    
    exception FieldNotFoundException extends CommonStorageException 
    {
        string fieldName;
    };

    exception FileNotFoundException extends CommonStorageException
    {
        string fileName;
    };

    /*
     * LongtermMemory exceptions
     * 
     */    
    exception LongtermMemoryException extends MemoryXException {};
    
    exception SnapshotNotFoundException extends LongtermMemoryException 
    {
        string snapshotName;
    };
    
};

