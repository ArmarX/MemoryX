/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/MemoryInterface.ice>
#include <MemoryX/interface/memorytypes/MemorySegments.ice>
#include <MemoryX/interface/components/CommonStorageInterface.ice>

module memoryx
{
    module LTM
    {
        module SegmentNames
        {
            const string OBJECTINSTANCES = "instances";
            const string SNAPSHOTS = "snapshots";
            const string OACS = "oacs";
            const string KBM = "kbm";
            const string PROFILER = "profilerdata";
            const string RESOURCE_PROFILES = "resourceprofiles";
            const string PREDICTION_DATA = "predictiondata";
            const string OBJECTCLASSES = "objects";
            const string DMP = "dmp";
            const string SELF_LOCALISATION = "selflocalisation";
        };
    };


    interface WorkingMemorySnapshotInterface extends armarx::Serializable
    {
        PersistentEntitySegmentBase* addSegment(string segName);
        PersistentEntitySegmentBase* getSegment(string segName);
        bool hasSegment(string segName);
        NameList getSegmentNames();
        void clear();

        void restoreWorkingMemory(AbstractWorkingMemoryInterface* workingMemory);
        void saveWorkingMemory(AbstractWorkingMemoryInterface* workingMemory);
        void saveWorkingMemorySubset(AbstractWorkingMemoryInterface* workingMemory, Ice::StringSeq entityIdList);

    };

    sequence<WorkingMemorySnapshotInterface> WorkingMemorySnapshotList;

    ["cpp:virtual"]
    class WorkingMemorySnapshotListSegmentBase extends PersistentMemorySegment
    {
        WorkingMemorySnapshotInterface* createSnapshot(string name, AbstractWorkingMemoryInterface* workingMemory);
        /**
         * @brief createSubsetSnapshot
         * @param name Name of the snapshot
         * @param workingMemory Proxy to the working memory from which the snapshot should be created
         * @param entityIdList List of entity IDs which should be saved in the snapshot
         * @return Pointer to the Snapshot
         */
        WorkingMemorySnapshotInterface* createSubsetSnapshot(string name, AbstractWorkingMemoryInterface* workingMemory, Ice::StringSeq entityIdList);
        WorkingMemorySnapshotInterface* openSnapshot(string name) throws SnapshotNotFoundException;
        void closeSnapshot(WorkingMemorySnapshotInterface* snapshot);
        void loadSnapshot(string name, AbstractWorkingMemoryInterface* workingMemory) throws SnapshotNotFoundException;
        bool removeSnapshot(string name);
        ["cpp:const"]
        NameList getSnapshotNames();
    };

    interface LongtermMemoryInterface extends MemoryInterface
    {
        WorkingMemorySnapshotListSegmentBase* getWorkingMemorySnapshotListSegment();
        PersistentObjectInstanceSegmentBase* getCustomInstancesSegment(string segmentName, bool createIfMissing);
        OacMemorySegmentBase* getOacSegment();
        KBMSegmentBase* getKBMSegment();
        PersistentProfilerDataSegmentBase* getProfilerDataSegment();
        PersistentResourceProfileSegmentBase* getResourceProfileSegment();
        PersistentPredictionDataSegmentBase* getPredictionDataSegment();
        PersistentDMPDataSegmentBase* getDMPSegment();
        PersistentEntitySegmentBase* getSelfLocalisationSegment();

        ["cpp:const"]
        PersistentObjectClassSegmentBase* getObjectClassesSegment();
        ["cpp:const"]
        idempotent CommonStorageInterface* getCommonStorage();

        void loadWorkingMemorySnapshot(string name, AbstractWorkingMemoryInterface* workingMemory) throws SnapshotNotFoundException;
        bool saveWorkingMemorySnapshot(string name, AbstractWorkingMemoryInterface* workingMemory);
        WorkingMemorySnapshotInterface* openWorkingMemorySnapshot(string name);
        bool removeWorkingMemorySnapshot(string name);
        NameList getSnapshotNames();
    };
};

