/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/core/LinkedPoseBase.ice>

#include <MemoryX/interface/components/MemoryException.ice>
#include <MemoryX/interface/core/EntityBase.ice>
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <MemoryX/interface/memorytypes/ProfilerEntities.ice>
#include <MemoryX/interface/memorytypes/PredictionEntities.ice>
#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.ice>


module memoryx
{
    // forward declaration
    class MotionModelInterface
    {
        armarx::LinkedPoseBase getPredictedPose();
        MultivariateNormalDistributionBase getUncertainty();
        void setPoseAtLastLocalisation(armarx::LinkedPoseBase poseAtLastLocalization, armarx::PoseBase globalRobotPoseAtLastLocalization, MultivariateNormalDistributionBase uncertaintyAtLastLocalization);
        armarx::LinkedPoseBase getPoseAtLastLocalisation();
        void savePredictedPoseAtStartOfCurrentLocalization();
        armarx::LinkedPoseBase getPredictedPoseAtStartOfCurrentLocalization();
        MultivariateNormalDistributionBase getUncertaintyAtStartOfCurrentLocalization();

        armarx::RobotStateComponentInterface* robotStateProxy;
        armarx::LinkedPoseBase poseAtLastLocalization;
        armarx::LinkedPoseBase predictedPoseAtStartOfCurrentLocalization;
        armarx::PoseBase globalRobotPoseAtLastLocalization;
        MultivariateNormalDistributionBase uncertaintyAtLastLocalization;
        MultivariateNormalDistributionBase uncertaintyAtStartOfCurrentLocalization;
        armarx::TimestampBase timeOfLastLocalizationStart;
        armarx::TimestampBase timeOfLastSuccessfulLocalization;
    };

    /**
     * MemorySegment for WorldState
     *
     */
    interface WorldStateSegmentInterface
    {
        ["cpp:const"]
        idempotent ObjectInstanceBase getWorldInstanceById(string entityId);

        ["cpp:const"]
        idempotent ObjectInstanceBase getWorldInstanceByName(string entityName);

        ["cpp:const"]
        idempotent ObjectInstanceList getWorldInstancesByClass(string className);

        ["cpp:const"]
            idempotent ObjectInstanceList getWorldInstancesByClassList(NameList classList);
        };

        ["cpp:virtual"]
        class WorldStateSegmentBase extends WorkingMemoryEntitySegmentBase implements WorldStateSegmentInterface
        {
    };

    ["cpp:virtual"]
    class PersistentWorldStateSegmentBase extends PersistentEntitySegmentBase implements WorldStateSegmentInterface
    {
    };

    sequence<AgentInstanceBase> AgentInstanceBaseList;

    /**
     * MemorySegment for Agents
     *
     */
    interface AgentInstancesSegmentInterface
    {
        ["cpp:const"]
        idempotent AgentInstanceBaseList getAllAgentInstances();

        ["cpp:const"]
        idempotent AgentInstanceBase getAgentInstanceById(string id);

        ["cpp:const"]
        idempotent AgentInstanceBase getAgentInstanceByName(string name);

        ["cpp:const"]
        idempotent armarx::FramedPoseBase convertToWorldPose(string agentName, armarx::FramedPoseBase localPose);

        ["cpp:const"]
        idempotent armarx::FramedPoseBase convertToLocalPose(string agentName, armarx::PoseBase worldPose, string targetFrame);
    };

    ["cpp:virtual"]
    class AgentInstancesSegmentBase extends WorkingMemoryEntitySegmentBase implements AgentInstancesSegmentInterface
    {
    };

    ["cpp:virtual"]
    class PersistentAgentInstancesSegmentBase extends PersistentEntitySegmentBase implements AgentInstancesSegmentInterface
    {
    };

    /**
     * MemorySegment for Objects
     *
     */
    interface ObjectInstanceSegmentInterface {
        ["cpp:const"]
        idempotent ObjectInstanceBase getObjectInstanceById(string entityId);

        ["cpp:const"]
        idempotent ObjectInstanceBase getObjectInstanceByName(string entityName);

        ["cpp:const"]
        idempotent ObjectInstanceList getObjectInstancesByClass(string className);

        ["cpp:const"]
        idempotent ObjectInstanceList getObjectInstancesByClassList(NameList classList);

        void setNewMotionModel(string entityId, MotionModelInterface newMotionModel);

        void setObjectPose(string entityId, armarx::LinkedPoseBase objectPose);
        void setObjectPoseWithoutMotionModel(string entityId, armarx::FramedPoseBase objectPose);

        string addObjectInstance(string instanceName, string className, armarx::LinkedPoseBase objectPose, MotionModelInterface motionModel);
    };

    ["cpp:virtual"]
    class ObjectInstanceMemorySegmentBase extends WorkingMemoryEntitySegmentBase
        implements ObjectInstanceSegmentInterface
    {
        ["cpp:const"]
        idempotent ObjectInstanceBase getCorrespondingObjectInstance(ObjectInstanceBase objectInstance);
    };

    ["cpp:virtual"]
    class PersistentObjectInstanceSegmentBase extends PersistentEntitySegmentBase
        implements ObjectInstanceSegmentInterface
    {
    };


    /**
     * MemorySegment for Classes
     *
     */
    ["cpp:virtual"]
    class PersistentObjectClassSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        idempotent ObjectClassBase getObjectClassById(string classId);

        ["cpp:const"]
        idempotent ObjectClassBase getObjectClassByName(string className);

        ["cpp:const"]
        idempotent ObjectClassBase getObjectClassByNameWithAllParents(string className);

        ["cpp:const"]
        idempotent ObjectClassList getChildClasses(string parentClassName);


        ["cpp:const"]
        idempotent ObjectClassList getParentClasses(string className);

        ["cpp:const"]
        idempotent ObjectClassList getAllParentClasses(string className);

        ["cpp:const"]
        idempotent bool isParentClass(string className, string parentCandidate);


        ["cpp:const"]
        ObjectClassList getClassWithSubclasses(string rootClassName);

        ["cpp:const"]
        idempotent ObjectComparisonResult compare(ObjectClassBase objClass, ObjectClassBase other);

    };

    ["cpp:virtual"]
    class ObjectClassMemorySegmentBase extends WorkingMemoryEntitySegmentBase
    {
        ObjectClassList getClassWithSubclasses(string className);
        ObjectClassBase updateClass(string className, ObjectClassBase update);
        ObjectClassList addPriorClassWithSubclasses(string className);
        void removePriorClassWithSubclasses(string className);
    };

    /**
     * MemorySegment for Relations
     *
     */
    interface RelationSegmentInterface
    {
        ["cpp:const"]
        idempotent RelationBase getRelationById(string entityId);

        ["cpp:const"]
        idempotent RelationList getRelationsByName(string relationName);

        ["cpp:const"]
        idempotent RelationList getRelationsBySign(bool sign);

        ["cpp:const"]
        idempotent RelationList getRelationsByEntityId(string entityId);

        ["cpp:const"]
        idempotent RelationList getRelationsByEntityRef(EntityRefBase entityRef);

        ["cpp:const"]
        idempotent RelationList getRelationsByEntityRefs(EntityRefList entities);

        ["cpp:const"]
        idempotent RelationBase getRelationByAttrValues(string name, EntityRefList entities, bool sign);

        void removeRelations(string name);

        void replaceRelations(RelationList newRelations);

    };

    ["cpp:virtual"]
    class RelationMemorySegmentBase extends WorkingMemoryEntitySegmentBase
        implements RelationSegmentInterface
    {
    };

    ["cpp:virtual"]
    class PersistentRelationSegmentBase extends PersistentEntitySegmentBase
        implements RelationSegmentInterface
    {
    };


    ["cpp:virtual"]
    class KBMSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"] idempotent KBMDataBase getKBMData(string entityId);
    };


    /**
     * MemorySegment for OACs
     *
     */
    ["cpp:virtual"]
    class ActiveOacMemorySegmentBase extends WorkingMemoryEntitySegmentBase
    {
        ActiveOacFull getActiveOac();
        void changeState(OacExecState state);
    };

    ["cpp:virtual"]
    class OacMemorySegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        idempotent OacBase getOacById(string entityId);

        ["cpp:const"]
        idempotent OacBase getOacByName(string entityName);

        ["cpp:const"]
        idempotent OacBaseList getAll();
    };

    /**
     * MemorySegment for SECs
     *
     */
    ["cpp:virtual"]
    class SECObjectRelationsSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        idempotent SECObjectRelationsBase getObjectRelationsById(string entityId);

        ["cpp:const"]
        idempotent OacBase getOacByName(string entityName);

        ["cpp:const"]
        idempotent OacBaseList getAll();
    };

    /**
     * MemorySegment for Affordances
     */
    ["cpp:virtual"]
    class AffordanceSegmentBase extends WorkingMemoryEntitySegmentBase
    {
        ["cpp:const"]
        idempotent AffordanceBaseList getAffordances();
    };

    /**
     * MemorySegment for Environmental Primitives
     */
    ["cpp:virtual"]
    class EnvironmentalPrimitiveSegmentBase extends WorkingMemoryEntitySegmentBase
    {
        // Access to all primitives

        ["cpp:const"]
        idempotent EnvironmentalPrimitiveBaseList getEnvironmentalPrimitives();

        ["cpp:const"]
        idempotent PlanePrimitiveBaseList getPlanes();

        ["cpp:const"]
        idempotent SpherePrimitiveBaseList getSpheres();

        ["cpp:const"]
        idempotent CylinderPrimitiveBaseList getCylinders();

        ["cpp:const"]
        idempotent BoxPrimitiveBaseList getBoxes();

        // ID-based access

        ["cpp:const"]
        idempotent EnvironmentalPrimitiveBase getEnvironmentalPrimitiveById(string id);

        ["cpp:const"]
        idempotent PlanePrimitiveBase getPlaneById(string id);

        ["cpp:const"]
        idempotent SpherePrimitiveBase getSphereById(string id);

        ["cpp:const"]
        idempotent CylinderPrimitiveBase getCylinderById(string id);

        ["cpp:const"]
        idempotent BoxPrimitiveBase getBoxById(string id);

        // Timestamp-based access

        ["cpp:const"]
        idempotent EnvironmentalPrimitiveBaseList getEnvironmentalPrimitivesByTimestamp(armarx::TimestampBase timestamp);

        ["cpp:const"]
        idempotent PlanePrimitiveBaseList getPlanesByTimestamp(armarx::TimestampBase timestamp);

        ["cpp:const"]
        idempotent SpherePrimitiveBaseList getSpheresByTimestamp(armarx::TimestampBase timestamp);

        ["cpp:const"]
        idempotent CylinderPrimitiveBaseList getCylindersByTimestamp(armarx::TimestampBase timestamp);

        ["cpp:const"]
        idempotent BoxPrimitiveBaseList getBoxesByTimestamp(armarx::TimestampBase timestamp);

        // Access to most recent elements

        ["cpp:const"]
        idempotent EnvironmentalPrimitiveBaseList getMostRecentEnvironmentalPrimitives();

        ["cpp:const"]
        idempotent PlanePrimitiveBaseList getMostRecentPlanes();

        ["cpp:const"]
        idempotent SpherePrimitiveBaseList getMostRecentSpheres();

        ["cpp:const"]
        idempotent CylinderPrimitiveBaseList getMostRecentCylinders();

        ["cpp:const"]
        idempotent BoxPrimitiveBaseList getMostRecentBoxes();

        // Timestamp-based element removal

        idempotent void removePrimitivesByTimestamp(armarx::TimestampBase timestamp);
        idempotent void removeOlderPrimitives(armarx::TimestampBase olderThan);
    };


    /**
     * Memory segment for storing and retrieving Profiler data.
     */
    ["cpp:virtual"]
    class PersistentProfilerDataSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        ProfilerEntityBaseList getProfilerDataEntities();
        ["cpp:const"]
        ProfilerEventBaseList getProfilerEventEntities();
        ["cpp:const"]
        ProfilerTransitionBaseList getProfilerTransitionEntities();
        ["cpp:const"]
        ProfilerMemorySnapshotBaseList getProfilerMemorySnapshotEntities();
        ["cpp:const"]
        ProfilerProcessBaseList getProfilerProcessCpuUsageEntities();
        ["cpp:const"]
        ProfilerMemoryUsageBaseList getProfilerMemoryUsageBaseEntities();

        string saveUniqueMemorySnapshot(ProfilerMemorySnapshotBase memorySnapshot);
        string saveOrUpdateTransition(ProfilerTransitionBase transition);
    };


    /**
     * Memory segment for storing and retrieving Profiling data.
     */
    ["cpp:virtual"]
    class PersistentPredictionDataSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        PredictionEntityBaseList getPredictionEntities();
        ["cpp:const"]
        PredictionTaskEntityBaseList getPredictionTaskEntities();
    };


    /**
     * Memory segment for storing and retrieving ResourceProfileEntities.
     */
    ["cpp:virtual"]
    class PersistentResourceProfileSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        ResourceProfileEntityBaseList getResourceProfileEntities();
        ["cpp:const"]
        ResourceProfileEntityBase getResourceProfileByName(string profileName);
    };


    /**
     * @brief Segment to store graphs represented by GraphNodes.
     */
    ["cpp:virtual"]
    class GraphMemorySegmentBase extends PersistentEntitySegmentBase
    {
        /**
         * @brief Returns a list containing all used scene names.
         * @return A list containing all used scene names.
         */
        ["cpp:const"]
        Ice::StringSeq getScenes();

        /**
         * @brief Returns all nodes assoicated to given scene.
         * @param name The name of the scene
         * @return All nodes assoicated to given scene.
         */
        GraphNodeBaseList getNodesByScene(string name);

        /**
         * @brief Returns the node with given id.
         * @param entityId The node's id.
         * @return The node with given id.
         */
        GraphNodeBase getNodeById(string entityId);

        /**
         * @brief Returns the node with given name.
         * @param entityName The node's name.
         * @return The node with given name.
         */
        GraphNodeBase getNodeByName(string entityName);

        /**
         * @brief Returns all nodes.
         * @return All nodes.
         */
        GraphNodeBaseList getAllNodes();

        /**
         * @brief Removes all nodes associated to given scene.
         * @param sceneName The scene to clear.
         */
        void clearScene(string sceneName);

        /**
         * @brief Adds a node to the segment.
         * @param node The node to add.
         * @return The entity id.
         */
        string addNode(GraphNodeBase node);

        /**
         * @brief Adds an edge between given nodes.
         * @param fromId The source node's id.
         * @param toId The destination node's id.
         * @return Whether the node was added.
         */
        bool addEdge(string fromId, string toId);

        /**
         * @brief Returns whether the scene exists (whether it contains nodes).
         * @param sceneName The scene's name.
         * @return Whether the scene exists (whether it contains nodes).
         */
        bool hasScene(string sceneName);

        /**
         * @brief Returns whether the requested scene has a node with the requested name.
         * @param sceneName The requested scene.
         * @param name The requested node name.
         * @return Whether the requested scene has a node with the requested name.
         */
        bool hasNodeWithName(string sceneName, string nodeName);

        /**
         * @brief Returns the node from the requested scene with the requested name.
         * @param sceneName The requested scene.
         * @param name The requested node.
         * @return The node from the requested scene with the requested name name.
         */
        GraphNodeBase getNodeFromSceneByName(string sceneName, string nodeName);

        bool removeNode(string nodeId);
        bool removeEdge(string startNodeId, string endNodeId);

        /**
         * @brief Returns the nearest node's id.
         * @param sceneName The requested scene.
         * @param x X coordinate
         * @param y Y coordinate
         * @return The nearest node's id.
         */
        //moved to placesetting resolver
//        string getIdOfNearestNode(string sceneName, float x, float y);

        /**
         * @brief Searches a path from idFrom to idTo using a*.
         * http://en.wikipedia.org/wiki/A*_search_algorithm
         *
         * @param idFrom The node to use as starting point.
         * @param idTo The target node.
         * @return The path from idFrom to idTo.
         */
        GraphNodeBaseList aStar(string idFrom, string idTo) throws EntityNotFoundException, armarx::InvalidArgumentException;
    };


    ["cpp:virtual"]
    class PersistentDMPDataSegmentBase extends PersistentEntitySegmentBase
    {
        ["cpp:const"]
        idempotent DMPEntityBase getDMPEntityById(string classId);

        ["cpp:const"]
        idempotent DMPEntityBase getDMPEntityByName(string className);


    };
};

