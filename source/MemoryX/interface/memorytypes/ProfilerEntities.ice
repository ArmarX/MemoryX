/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/core/EntityBase.ice>
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <MemoryX/interface/memorytypes/StatisticMeasures.ice>

#include <ArmarXCore/interface/core/Profiler.ice>

#include <Ice/Current.ice> // for Ice::Context

module memoryx
{
    ["cpp:virtual"]
    class ProfilerEntityBase extends EntityBase
    {
        ["cpp:const"]
        Ice::StringSeq getTags();
        void setTags(Ice::StringSeq tags);
        void addTag(string tag);
    };

    sequence<ProfilerEntityBase> ProfilerEntityBaseList;

    ["cpp:virtual"]
    class ProfilerMemorySnapshotBase extends ProfilerEntityBase
    {
        ["cpp:const"]
        string getStateName();
        void setStateName(string stateName);

        ["cpp:const"]
        Ice::Context getMemoryParameterMap();
        void setMemoryParameterMap(Ice::Context parameters);
    };

    sequence<ProfilerMemorySnapshotBase> ProfilerMemorySnapshotBaseList;

    ["cpp:virtual"]
    class ProfilerTransitionBase extends ProfilerEntityBase
    {
        ["cpp:const"]
        string getParentStateName();
        void setParentStateName(string parentStateName);

        ["cpp:const"]
        string getSourceStateName();
        void setSourceStateName(string sourceStateName);

        ["cpp:const"]
        string getTargetStateName();
        void setTargetStateName(string targetStateName);

        ["cpp:const"]
        EntityRefBase getSourceStateMemorySnapshotRef();
        void setSourceStateMemorySnapshotRef(EntityRefBase memorySnapshotRef);

        ["cpp:const"]
        EntityRefBase getTargetStateMemorySnapshotRef();
        void setTargetStateMemorySnapshotRef(EntityRefBase memorySnapshotRef);

        ["cpp:const"]
        int getCount();
        void setCount(int count);
    };

    sequence<ProfilerTransitionBase> ProfilerTransitionBaseList;

    ["cpp:virtual"]
    class ProfilerEventBase extends ProfilerEntityBase
    {
        ["cpp:const"]
        int getPid();
        void setPid(int pid);

        ["cpp:const"]
        string getExecutableName();
        void setExecutableName(string executableName);

        ["cpp:const"]
        int getTimestamp();
        void setTimestamp(int timestamp);

        ["cpp:const"]
        string getTimestampUnit();
        void setTimestampUnit(string unit);

        ["cpp:const"]
        string getEventName();
        void setEventName(string event);

        ["cpp:const"]
        string getParentName();
        void setParentName(string parentName);

        ["cpp:const"]
        string getFunctionName();
        void setFunctionName(string functionName);
    };

    sequence<ProfilerEventBase> ProfilerEventBaseList;

    ["cpp:virtual"]
    class ResourceProfileEntityBase extends EntityBase
    {
        ["cpp:const"]
        StatisticMeasuresBase getDuration();
        void setDuration(StatisticMeasuresBase duration);

        ["cpp:const"]
        StatisticMeasuresBase getCpuUsage();
        void setCpuUsage(StatisticMeasuresBase cpuUsage);

        ["cpp:const"]
        StatisticMeasuresBase getMemoryUsage();
        void setMemoryUsage(StatisticMeasuresBase memoryUsage);
    };

    sequence<ResourceProfileEntityBase> ResourceProfileEntityBaseList;

    ["cpp:virtual"]
    class ProfilerProcessBase extends ProfilerEntityBase
    {

        ["cpp:const"]
        int getPid();
        void setPid(int pid);

        ["cpp:const"]
        string getProcessName();
        void setProcessName(string processName);

        ["cpp:const"]
        int getTimestamp();
        void setTimestamp(int timestamp);

        ["cpp:const"]
        double getProcessCpuUsage();
        void setProcessCpuUsage(double processCpuUsage);
    };

    sequence<ProfilerProcessBase> ProfilerProcessBaseList;

    ["cpp:virtual"]
    class ProfilerMemoryUsageBase extends ProfilerEntityBase
    {
        ["cpp:const"]
        int getPid();
        void setPid(int pid);

        ["cpp:const"]
        string getProcessName();
        void setProcessName(string processName);

        ["cpp:const"]
        int getTimestamp();
        void setTimestamp(int timestamp);

        ["cpp:const"]
        int getMemoryUsage();
        void setMemoryUsage(int memoryUsage);
    };

    sequence<ProfilerMemoryUsageBase> ProfilerMemoryUsageBaseList;
};

