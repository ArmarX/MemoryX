/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Mirko Wächter
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once
#include <MemoryX/interface/core/EntityBase.ice>

module memoryx
{

    enum ObjectComparisonResult
    {
        eNotEqualClass,
        eEqualClass,
        eEqualParentClass
    };


    ["cpp:virtual"]
    class ObjectClassBase extends EntityBase
    {
        ["cpp:const"]
        NameList getParentClasses();
        void setParentClass(string className);
        void addParentClass(string className);
        void clearParentClasses();

        void setInstanceable(bool isInstanceable);
        /**
         * @brief isInstanceable
         * @return whether this class is meant to have instances (default:yes)
         */
        ["cpp:const"]
        bool isInstanceable();
        ["cpp:const"]
        ObjectComparisonResult compare(ObjectClassBase other);
    };
};
