###
### CMakeLists.txt file for MemoryX interfaces
###

set(MemoryX_INTERFACE_DEPEND ArmarXCore RobotAPI)

set(SLICE_FILES
components/CommonPlacesLearnerInterface.ice
components/CommonStorageInterface.ice
components/LongtermMemoryInterface.ice
components/MemoryException.ice
components/GraphNodePoseResolverInterface.ice
components/PriorKnowledgeInterface.ice
components/ProfilerStorageInterface.ice
components/WorkingMemoryInterface.ice
components/KBMComponentInterface.ice
components/SimpleEpisodicMemoryInterface.ice

components/ObjectToRobotNodeAttachment.ice
components/WorkingMemoryToArVizInterface.ice
components/WorkingMemoryObjectPoseProviderInterface.ice

core/CommonTypes.ice
core/EntityBase.ice
core/FusionMethods.ice
core/GridFileInterface.ice
core/MemoryInterface.ice
core/MongoDBRefBase.ice
core/ProbabilityMeasures.ice

memorytypes/MemoryEntities.ice
memorytypes/ObjectClass.ice
memorytypes/MemorySegments.ice
memorytypes/ProfilerEntities.ice
memorytypes/PredictionEntities.ice
memorytypes/StatisticMeasures.ice

observers/ObjectMemoryObserverInterface.ice
observers/WorldStateObserver.ice

workingmemory/AbstractWorkingMemoryInterface.ice
workingmemory/MotionModel.ice
workingmemory/WorkingMemoryListenerInterface.ice
workingmemory/WorkingMemoryUpdaterBase.ice

gui/GraphVisualizerInterface.ice
gui/EntityDrawerInterface.ice
)

# generate the interface library
armarx_interfaces_generate_library(MemoryX "${MemoryX_INTERFACE_DEPEND}")
