/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/CommonTypes.ice>

#include <ArmarXCore/interface/observers/VariantBase.ice>

module memoryx
{
    class ProbabilityMeasureBase extends armarx::VariantDataClass
    {
    };

    class DiscreteProbabilityBase extends ProbabilityMeasureBase
    {
        ["cpp:const"]
        float getProbability();
        void setProbability(float prob);

        ["protected"]
        float prob;
    };

    ["cpp:virtual"]
    class NormalDistributionBase extends ProbabilityMeasureBase
    {
        ["cpp:const"]
        int getDimensions();
        
        ["cpp:const"]
        FloatVector getMean();
        
        void setMean(FloatVector mean);

        ["cpp:const"]
        float getCovariance(int row, int col);
        
        ["protected"]
        int dimensions;
        ["protected"]
        FloatVector mean;
    };

    ["cpp:virtual"]
    class UnivariateNormalDistributionBase extends NormalDistributionBase 
    {
        ["cpp:const"]
        float getVariance();
        void setVariance(float var);

        ["protected"]
        float variance;
    };

    ["cpp:virtual"]
    class IsotropicNormalDistributionBase extends NormalDistributionBase
    {
        ["cpp:const"]
        float getVariance(int dim);
        void setVariance(int dim, float var);

        ["protected"]
        FloatVector varVector;
    };

    ["cpp:virtual"]
    class MultivariateNormalDistributionBase extends NormalDistributionBase
    {
        void setCovariance(int row, int col, float cov);

        ["cpp:const"]
        float getVarianceScalar();

        ["protected"]
        FloatVector covMatrix;
    };
    
    struct GaussianMixtureComponent
    {
        NormalDistributionBase gaussian;
        float weight;
    };
    
    sequence<GaussianMixtureComponent> GaussianMixtureComponentList;
    
    ["cpp:virtual"]
    class GaussianMixtureDistributionBase extends ProbabilityMeasureBase
    {
        ["cpp:const"]
        int getDimensions();

        ["cpp:const"]
        int size();
        void clear();
        
        ["cpp:const"]
        GaussianMixtureComponent getComponent(int index)
            throws armarx::IndexOutOfBoundsException;
        ["cpp:const"]
        GaussianMixtureComponent getModalComponent();
        void addComponent(GaussianMixtureComponent component)
            throws InvalidDimensionException;
        void addGaussian(NormalDistributionBase gaussian, float weight)
            throws InvalidDimensionException;
        void setComponent(int index, GaussianMixtureComponent component)
            throws armarx::IndexOutOfBoundsException, InvalidDimensionException;
        void removeComponent(int index)
            throws armarx::IndexOutOfBoundsException;

        void addComponents(GaussianMixtureDistributionBase other);
        void scaleComponents(float factor);
        void pruneComponents(float threshold);
        void normalize();

        ["protected"]
        int dimensions;
        ["protected"]
        GaussianMixtureComponentList components;
    };

};

