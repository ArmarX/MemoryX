/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/components/MemoryException.ice>
#include <MemoryX/interface/core/ProbabilityMeasures.ice>

module memoryx
{
    /*
    * AttributeElement definitions
    *
    */
    struct AttributeElement
    {
        armarx::VariantBase value;

        ProbabilityMeasureBase uncertainty;
    };

    sequence<AttributeElement> AttributeElementList;

    /*
    * Attribute definitions
    *
    */
    class AttributeBase implements armarx::Serializable
    {
        ["cpp:const"]
        string getName();

        ["protected"]
        string name;
    };

    class EntityAttributeBase extends AttributeBase
    {
        void setValue(armarx::VariantBase val);
        void setValueWithUncertainty(armarx::VariantBase val, ProbabilityMeasureBase uncertainty);
        void addValue(armarx::VariantBase val);
        void addValueWithUncertainty(armarx::VariantBase val, ProbabilityMeasureBase uncertainty);
        void removeValueAt(int index);

        ["cpp:const"]
        bool hasValue(armarx::VariantBase val);
        ["cpp:const"]
        armarx::VariantBase getValue();
        ["cpp:const"]
        ProbabilityMeasureBase getUncertainty();
        ["cpp:const"]
        armarx::VariantBase getValueAt(int index);
        ["cpp:const"]
        ProbabilityMeasureBase getUncertaintyAt(int index);

        void setElement(AttributeElement elem);
        void addElement(AttributeElement elem);
        ["cpp:const"]
        AttributeElement getElement();
        ["cpp:const"]
        AttributeElement getElementAt(int index);

        ["cpp:const"]
        int size();
        void clear();

        ["protected"]
        AttributeElementList values;
    };


    sequence<EntityAttributeBase> EntityAttributeList;
    dictionary<string, EntityAttributeBase> AttributeMap;

    /*
    * Entity definitions
    *
    */
    sequence<string> EntityIdList;

    class AbstractEntityWrapperBase;
    sequence <AbstractEntityWrapperBase> AbstractEntityWrapperBaseList;

    class EntityBase implements armarx::Serializable
    {
        ["cpp:const"]
        string getId();
        void setId(string id);

        ["cpp:const"]
        string getName();
        void setName(string name);

        ["cpp:const"]
        bool isMetaEntity();
        void setMetaEntity(bool isMetaEntity);

        ["cpp:const"]
        EntityAttributeBase getAttribute(string attrName);
        ["cpp:const"]
        bool hasAttribute(string attrName);
        void putAttribute(EntityAttributeBase attr);
        void removeAttribute(string attrName);

        ["cpp:const"]
        NameList getAttributeNames();

        ["cpp:const"]
        bool equals(EntityBase otherEntity);

        ["cpp:const"]
        bool equalsAttributes(EntityBase otherEntity);

        ["protected"]
        string id;

        ["protected"]
        string name;

        ["protected"]
        AttributeMap attributes;

    };

    class AbstractEntityWrapperBase
    {
    };

    sequence<EntityBase> EntityBaseList;
};

