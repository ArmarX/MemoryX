/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::WorkingMemoryToArVizGroup
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AttachObjectToRobotNode.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::WorkingMemoryToArVizGroup
{
    // DO NOT EDIT NEXT LINE
    AttachObjectToRobotNode::SubClassRegistry AttachObjectToRobotNode::Registry(AttachObjectToRobotNode::GetName(), &AttachObjectToRobotNode::CreateInstance);

    void AttachObjectToRobotNode::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)

        memoryx::AttachObjectToRobotNodeInput input;
        input.object.memoryID = in.getObjectMemoryID();
        input.object.className = in.getObjectClassName();
        input.objectOffset = in.getObjectOffset();
        input.robotNodeName = in.getRobotNodeName();
        input.useCurrentPose = in.getUseCurrentPose();

        getWorkingMemoryToArViz()->attachObjectToRobotNode(input);

        emitSuccess();
    }

    //void AttachObjectToRobotNode::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void AttachObjectToRobotNode::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void AttachObjectToRobotNode::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr AttachObjectToRobotNode::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new AttachObjectToRobotNode(stateData));
    }
}
