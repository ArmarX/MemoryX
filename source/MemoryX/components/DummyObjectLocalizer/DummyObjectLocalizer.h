/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::DummyObjectLocalizer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>


namespace armarx
{

    struct DummyObject
    {
        std::string name;
        FramedPosePtr pose;
    };

    /**
     * @class DummyObjectLocalizerPropertyDefinitions
     * @brief
     */
    class DummyObjectLocalizerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DummyObjectLocalizerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("ObjectPoseFile", "MemoryX/DummyObjectLocations.json", "A file containing framed poses for each object which should be reported by this localizer.");

            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
        }
    };

    /**
     * @defgroup Component-DummyObjectLocalizer DummyObjectLocalizer
     * @ingroup MemoryX-Components
     * A description of the component DummyObjectLocalizer.
     *
     * @class DummyObjectLocalizer
     * @ingroup Component-DummyObjectLocalizer
     * @brief Brief description of class DummyObjectLocalizer.
     *
     * Detailed description of class DummyObjectLocalizer.
     */
    class DummyObjectLocalizer :
        virtual public memoryx::ObjectLocalizerInterface,
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DummyObjectLocalizer";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ObjectLocalizerInterface interface
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& classes, const Ice::Current&) override;

    private:
        std::vector<DummyObject> dummyObjects;


        armarx::RobotStateComponentInterfacePrx robotStateComponent;

    };
}
