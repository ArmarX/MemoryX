armarx_component_set_name(PriorKnowledge)

set(COMPONENT_LIBS ArmarXCore MemoryXCore MemoryXMemoryTypes)

set(COMPONENT_SOURCES PriorKnowledge.cpp)
set(COMPONENT_HEADERS PriorKnowledge.h)

set(APP_SOURCES main.cpp PriorKnowledge.h )

armarx_add_component("${COMPONENT_SOURCES}" "${COMPONENT_HEADERS}")

armarx_add_component_executable("${APP_SOURCES}")

armarx_add_component_test(PriorKnowledgeTest test/PriorKnowledgeTest.cpp)
