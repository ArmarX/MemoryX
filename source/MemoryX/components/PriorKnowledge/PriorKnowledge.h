/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::PriorKnowledge
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>

#include <MemoryX/core/memory/SegmentedMemory.h>


namespace memoryx
{

    class PriorKnowledgePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PriorKnowledgePropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ClassCollections", "Comma separated list of MongoDB collection (<db>.<collection>) which store known object classes. First collection will be used for writing.");
            defineOptionalProperty<std::string>("RelationCollections", "", "Comma separated list of MongoDB collection (<db>.<collection>) which store known object classes relations. First collection will be used for writing.");
            defineOptionalProperty<std::string>("GraphCollections", "", "Comma separated list of MongoDB collection (<db>.<collection>) which store graphs. First collection will be used for writing.");
        }
    };


    using EntityMap = std::map<std::string, EntityBasePtr>;

    /*!
     * \brief The PriorKnowledge class provides a database with common sense and/or prior knowldge.
     *
     * See \ref priorknowledge "memoryx::PriorKnowledge" - common sense knowledge database.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT PriorKnowledge :
        virtual public PriorKnowledgeInterface,
        virtual public SegmentedMemory,
        virtual public armarx::Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "PriorKnowledge";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new PriorKnowledgePropertyDefinitions(
                           getConfigIdentifier()));
        }

        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;
        Ice::ObjectAdapterPtr getObjectAdapter() const override
        {
            return Component::getObjectAdapter();
        }

        // implementation of PriorKnowledgeInterface
        PersistentObjectClassSegmentBasePrx getObjectClassesSegment(const ::Ice::Current& = Ice::emptyCurrent) const override;

        PersistentRelationSegmentBasePrx getRelationsSegment(const ::Ice::Current& = Ice::emptyCurrent) const override;

        GraphMemorySegmentBasePrx getGraphSegment(const ::Ice::Current& c = Ice::emptyCurrent) const override;

        CommonStorageInterfacePrx getCommonStorage(const ::Ice::Current& = Ice::emptyCurrent) const override;

        bool hasGraphSegment(const Ice::Current& c = Ice::emptyCurrent) const override;

        bool isPriorCollection(const ::std::string& collNS, const ::Ice::Current& = Ice::emptyCurrent) override;

    protected:
        CommonStorageInterfacePrx dataBasePrx;

        NameList classCollNames;
        NameList relationCollNames;
        NameList graphCollNames;

        void setSegmentReadCollections(const PersistentEntitySegmentBasePtr& segmentPrx, const NameList& collNameList);

        // MemoryInterface interface
    public:
        std::string getMemoryName(const Ice::Current&) const override;
        AbstractMemorySegmentPrx addGenericSegment(const std::string& segmentName, const Ice::Current&) override;
    };

    using PriorKnowledgePtr = IceUtil::Handle<PriorKnowledge>;

}

