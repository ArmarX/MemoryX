/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::PriorKnowledge
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PriorKnowledge.h"

#include <ArmarXCore/interface/core/Log.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include <MemoryX/libraries/memorytypes/segment/PersistentObjectClassSegment.h>
#include <MemoryX/libraries/memorytypes/segment/PersistentRelationSegment.h>
#include <MemoryX/libraries/memorytypes/segment/GraphMemorySegment.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <IceUtil/UUID.h>


#define PRIOR_SEGMENT_NAME_CLASSES "classes"
#define PRIOR_SEGMENT_NAME_RELATIONS "relations"
#define PRIOR_SEGMENT_NAME_GRAPH "graphs"
namespace memoryx
{
    void PriorKnowledge::onInitComponent()
    {
        usingProxy("CommonStorage");

        const std::string clsCollNamesStr = getProperty<std::string>("ClassCollections").getValue();
        classCollNames = simox::alg::split(clsCollNamesStr, ",");

        const std::string relCollNamesStr = getProperty<std::string>("RelationCollections").getValue();
        relationCollNames = simox::alg::split(relCollNamesStr, ",");

        const std::string graphCollNamesStr = getProperty<std::string>("GraphCollections").getValue();
        graphCollNames = simox::alg::split(graphCollNamesStr, ",");
    }


    void PriorKnowledge::onConnectComponent()
    {
        ARMARX_INFO << "Starting MemoryX::PriorKnowledge";

        const Ice::CommunicatorPtr ic = getIceManager()->getCommunicator();

        dataBasePrx = getProxy<CommonStorageInterfacePrx>("CommonStorage");

        CollectionInterfacePrx writeClassColl = dataBasePrx->requestCollection(classCollNames[0]);
        PersistentObjectClassSegmentPtr classesSegment = new PersistentObjectClassSegment(writeClassColl, ic);
        setSegmentReadCollections(classesSegment, classCollNames);
        addSegment(PRIOR_SEGMENT_NAME_CLASSES, classesSegment);

        if (relationCollNames.size() > 0 && !relationCollNames[0].empty())
        {
            CollectionInterfacePrx writeRelationColl = dataBasePrx->requestCollection(relationCollNames[0]);
            PersistentRelationSegmentPtr relationsSegment = new PersistentRelationSegment(writeRelationColl, ic);
            setSegmentReadCollections(relationsSegment, relationCollNames);
            addSegment(PRIOR_SEGMENT_NAME_RELATIONS, relationsSegment);
        }

        if (graphCollNames.size() > 0 && !graphCollNames[0].empty())
        {
            ARMARX_INFO << "Adding segments: " << graphCollNames;
            CollectionInterfacePrx graphClassColl = dataBasePrx->requestCollection(graphCollNames[0]);
            GraphMemorySegmentPtr graphSegment {new GraphMemorySegment{graphClassColl, ic}};
            setSegmentReadCollections(graphSegment, graphCollNames);
            addSegment(PRIOR_SEGMENT_NAME_GRAPH, graphSegment);
        }
    }

    PersistentObjectClassSegmentBasePrx PriorKnowledge::getObjectClassesSegment(const ::Ice::Current& c) const
    {
        return PersistentObjectClassSegmentBasePrx::uncheckedCast(getSegment(PRIOR_SEGMENT_NAME_CLASSES, c));
    }

    PersistentRelationSegmentBasePrx PriorKnowledge::getRelationsSegment(const ::Ice::Current& c) const
    {
        return PersistentRelationSegmentBasePrx::uncheckedCast(getSegment(PRIOR_SEGMENT_NAME_RELATIONS, c));
    }

    GraphMemorySegmentBasePrx PriorKnowledge::getGraphSegment(const ::Ice::Current& c) const
    {
        return GraphMemorySegmentBasePrx::uncheckedCast(getSegment(PRIOR_SEGMENT_NAME_GRAPH, c));
    }

    bool PriorKnowledge::hasGraphSegment(const Ice::Current& c) const
    {
        return hasSegment(PRIOR_SEGMENT_NAME_GRAPH, c);
    }

    CommonStorageInterfacePrx PriorKnowledge::getCommonStorage(const ::Ice::Current&) const
    {
        return dataBasePrx;
    }

    bool PriorKnowledge::isPriorCollection(const ::std::string& collNS, const ::Ice::Current&)
    {
        const size_t found = collNS.find_first_of('.');
        if (found == std::string::npos)
        {
            return false;
        }
        std::string postfix = collNS.substr(found + 1, std::string::npos);
        return simox::alg::starts_with(postfix, PRIOR_COLLECTION_PREFIX);
    }

    void PriorKnowledge::clear(const ::Ice::Current&)
    {
        throw armarx::NotImplementedYetException();
    }

    void PriorKnowledge::setSegmentReadCollections(const PersistentEntitySegmentBasePtr& segmentPrx, const NameList& collNameList)
    {
        ARMARX_INFO << "Setting read collections";
        segmentPrx->clearReadCollections();

        for (memoryx::NameList::const_iterator it = collNameList.begin(); it != collNameList.end(); ++it)
        {
            ARMARX_INFO << "Adding collection " << *it;
            CollectionInterfacePrx readColl = dataBasePrx->requestCollection(*it);

            if (!readColl)
            {
                ARMARX_ERROR << " Could not find collection with name " << *it << std::endl;
            }
            else
            {
                segmentPrx->addReadCollection(readColl);
            }
        }
    }

    std::string PriorKnowledge::getMemoryName(const Ice::Current&) const
    {
        return getName();
    }

    AbstractMemorySegmentPrx PriorKnowledge::addGenericSegment(const std::string& segmentName, const Ice::Current&)
    {
        PersistentEntitySegmentPtr segment = new PersistentEntitySegment(dataBasePrx->requestCollection(segmentName), getIceManager()->getCommunicator());
        return addSegment(segmentName, segment);
    }
}

