/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::EntityDrawerComponent
 * @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


// Coin3D & SoQt
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/sensors/SoTimerSensor.h>

#include <VirtualRobot/VirtualRobotCommon.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <MemoryX/interface/gui/EntityDrawerInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/GridFileManager.h>


namespace memoryx
{

    /*!
     * \class EntityDrawerPropertyDefinitions
     * \brief
     */
    class EntityDrawerPropertyDefinitions:
        public armarx::DebugDrawerPropertyDefinitions
    {
    public:
        EntityDrawerPropertyDefinitions(std::string prefix):
            armarx::DebugDrawerPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("CommonStorageName", "CommonStorage", "Name of the the CommonStorage memory component");
            defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Name of the the PriorKnowledge memory component");
        }
    };

    /*!
      \class EntityDrawerComponent
      \ingroup MemoryX-Components
     * \brief The EntityDrawerComponent class implements a component that listens to layered / debug drawer commands and creates internal representations of the visualization
     * which can be conveniently used by gui components to visualize the content.
     * Basically it is a armarx::DebugDrawerComponent with additional memroy features. In addition to all debug drawer fucntions it is capable of showing memory entities, e.g. objects.
     *
     * \see armarx::DebugDrawerComponent
     *
     * The following plugins listen to the EntityDrawer topic:
     * \see memoryx::WorkingMemoryGuiPlugin
     */
    class EntityDrawerComponent :
        virtual public memoryx::EntityDrawerInterface,
        virtual public armarx::DebugDrawerComponent
    {
    public:

        /*!
         * \brief EntityDrawerComponent Constructs a debug drawer
         */
        EntityDrawerComponent();


        // inherited from Component
        std::string getDefaultName() const override
        {
            return "EntityDrawer";
        }
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


        void setObjectVisu(const std::string& layerName, const std::string& objectName, const memoryx::ObjectClassBasePtr& objectClassBase, const armarx::PoseBasePtr& globalPose, const Ice::Current& c = Ice::emptyCurrent) override;
        void setObjectVisuByName(const std::string& layerName, const std::string& customObjectName, const std::string& objectClassName, const armarx::PoseBasePtr& globalPose, const Ice::Current& c = Ice::emptyCurrent) override;
        void updateObjectPose(const std::string& layerName, const std::string& objectName, const armarx::PoseBasePtr& globalPose, const Ice::Current& c = Ice::emptyCurrent) override;
        /*!
         * \brief updateObjectColor Colorizes the object visualization
         * \param layerName The layer
         * \param objectName The object identifier
         * \param c The draw color, if all is set to 0, the colorization is disabled (i.e. the original vizualization shows up)
         */
        void updateObjectColor(const std::string& layerName, const std::string& objectName, const armarx::DrawColor& color, const Ice::Current& c = Ice::emptyCurrent) override;

        void updateObjectTransparency(const std::string& layerName, const std::string& objectName, float alpha, const Ice::Current& c = Ice::emptyCurrent) override;
        void removeObjectVisu(const std::string& layerName, const std::string& objectName, const Ice::Current& c = Ice::emptyCurrent) override;

        /*!
         * \see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new EntityDrawerPropertyDefinitions(
                    getConfigIdentifier()));
        }


        ~EntityDrawerComponent() override;
    protected:

        struct EntityData : public DrawData
        {
            EntityData()
            {
                updateColor = false;
                updatePose = false;
            }
            memoryx::ObjectClassPtr objectClass;

            bool updateColor;
            VirtualRobot::VisualizationFactory::Color color;

            bool updatePose;
            Eigen::Matrix4f globalPose;
        };

        struct EntityUpdateData
        {
            std::map<std::string, EntityData> entities;
        };

        EntityUpdateData accumulatedCustomUpdateData;

        void onUpdateVisualization() override;
        void removeCustomVisu(const std::string& layerName, const std::string& name) override;

        void drawEntity(const EntityData& d);

        void removeEntity(const std::string& layerName, const std::string& name);

        VirtualRobot::SceneObjectPtr requestEntity(const EntityData& d);

        std::map < std::string, VirtualRobot::SceneObjectPtr > activeObjects;
        std::map< std::string, VirtualRobot::ManipulationObjectPtr > objectCache;

        memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;
        memoryx::PersistentObjectClassSegmentBasePrx objectClassSegment;

        GridFileManagerPtr fileManager;


        void onRemoveAccumulatedData(const std::string& layerName) override;
    };

    using EntityDrawerComponentPtr = IceInternal::Handle<EntityDrawerComponent>;

}

