/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE MemoryX::LongtermMemory
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <MemoryX/armarx-objects/CommonStorage/CommonStorage.h>
#include <MemoryX/armarx-objects/WorkingMemory/WorkingMemory.h>
#include <MemoryX/armarx-objects/LongtermMemory/LongtermMemory.h>
//#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/entity/Oac.h>
#include <MemoryX/libraries/memorytypes/segment/OacMemorySegment.h>

BOOST_AUTO_TEST_CASE(testManageSnapshots)
{
    Ice::PropertiesPtr properties = Ice::createProperties();
    properties->setProperty("MemoryX.WorkingMemory.UsePriorMemory", "0");
    properties->setProperty("MemoryX.WorkingMemory.UseLongtermMemory", "0");
    properties->setProperty("MemoryX.WorkingMemory.PublishUpdates", "0");
    properties->setProperty("MemoryX.WorkingMemory2.UsePriorMemory", "0");
    properties->setProperty("MemoryX.WorkingMemory2.UseLongtermMemory", "0");
    properties->setProperty("MemoryX.WorkingMemory2.PublishUpdates", "0");
    properties->setProperty("MemoryX.LongtermMemory.DatabaseName", "testdb");
    properties->setProperty("MemoryX.CommonStorage.MongoUser", "testuser");
    properties->setProperty("MemoryX.CommonStorage.MongoPassword", "testpass");

    TestArmarXManagerPtr manager = new TestArmarXManager("ManageSnapshotsTest", properties);

    CommonStorageInterfacePrx storageProxy =
        manager->createComponentAndRun<CommonStorage, CommonStorageInterfacePrx>("MemoryX", "CommonStorage");
    LongtermMemoryInterfacePrx ltmProxy =
        manager->createComponentAndRun<LongtermMemory, LongtermMemoryInterfacePrx>("MemoryX", "LongtermMemory");
    WorkingMemoryInterfacePrx wm1Proxy =
        manager->createComponentAndRun<WorkingMemory, WorkingMemoryInterfacePrx>("MemoryX", "WorkingMemory");
    WorkingMemoryInterfacePrx wm2Proxy =
        manager->createComponentAndRun<WorkingMemory, WorkingMemoryInterfacePrx>("MemoryX", "WorkingMemory2");

    ObjectInstancePtr obj = new ObjectInstance("obj1");
    std::string objId = wm1Proxy->getObjectInstancesSegment()->addEntity(obj);

    BOOST_CHECK(wm1Proxy->getObjectInstancesSegment());
    BOOST_CHECK(wm1Proxy->getObjectInstancesSegment()->getObjectInstanceById(objId));
    BOOST_CHECK_EQUAL(wm1Proxy->getObjectInstancesSegment()->getEntityById(objId)->getName(), std::string("obj1"));

    ltmProxy->saveWorkingMemorySnapshot("foo", wm1Proxy);
    ltmProxy->loadWorkingMemorySnapshot("foo", wm2Proxy);

    BOOST_CHECK(wm2Proxy->getObjectInstancesSegment());
    BOOST_CHECK(wm2Proxy->getObjectInstancesSegment()->getObjectInstanceById(objId));
    BOOST_CHECK_EQUAL(wm2Proxy->getObjectInstancesSegment()->getEntityById(objId)->getName(), std::string("obj1"));

    ltmProxy->removeWorkingMemorySnapshot("foo");
}

BOOST_AUTO_TEST_CASE(testManageOacs)
{
    Ice::PropertiesPtr properties = Ice::createProperties();
    properties->setProperty("MemoryX.LongtermMemory.DatabaseName", "testdb");
    properties->setProperty("MemoryX.CommonStorage.MongoUser", "testuser");
    properties->setProperty("MemoryX.CommonStorage.MongoPassword", "testpass");

    TestArmarXManagerPtr manager = new TestArmarXManager("ManageOacsTest", properties);

    CommonStorageInterfacePrx storageProxy =
        manager->createComponentAndRun<CommonStorage, CommonStorageInterfacePrx>("MemoryX", "CommonStorage");
    LongtermMemoryInterfacePrx ltmProxy =
        manager->createComponentAndRun<LongtermMemory, LongtermMemoryInterfacePrx>("MemoryX", "LongtermMemory");

    ltmProxy->getOacSegment()->clear();

    OacPtr oac = new Oac("oac1");
    std::string oacId = ltmProxy->getOacSegment()->addEntity(oac);

    BOOST_CHECK(ltmProxy->getOacSegment());
    BOOST_CHECK(ltmProxy->getOacSegment()->getOacById(oacId));
    BOOST_CHECK_EQUAL(ltmProxy->getOacSegment()->getOacById(oacId)->getName(), std::string("oac1"));

    OacBaseList allOacs = ltmProxy->getOacSegment()->getAll();

    BOOST_CHECK_EQUAL(allOacs.size(), 1);
    BOOST_CHECK_EQUAL(allOacs[0]->getName(), std::string("oac1"));

    ltmProxy->getOacSegment()->removeEntity(oacId);
    OacBaseList allOacsAfter = ltmProxy->getOacSegment()->getAll();

    BOOST_CHECK_EQUAL(allOacsAfter.size(), 0);
}
