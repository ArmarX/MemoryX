/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once
#include <MemoryX/components/WorldStateObserver/WorldStateUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>



namespace memoryx
{

    class OnTopPredicateProvider : public WorldStateUpdaterBase
    {
    public:
        OnTopPredicateProvider();

        // ManagedIceObject interface
    protected:
        void onInitWorldStateUpdater() override;
        void onConnectWorldStateUpdater() override;
        std::string getDefaultName() const override;

        // WorldStateUpdaterInterface interface
    public:
        PredicateInfo getPredicateInfo(const Ice::Current& c = Ice::emptyCurrent);
        PredicateInstanceList calcPredicates(const Ice::Current&) override;
    private:
        WorkingMemoryInterfacePrx workingMemoryProxy;
        ObjectInstanceMemorySegmentBasePrx objectInstancesProxy;
        WorldStateObserverInterfacePrx wsObs;

    };

} // namespace memoryx

