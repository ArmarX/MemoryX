/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/components/WorldStateObserver/WorldStateUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/libraries/observers/ObjectMemoryObserver.h>

namespace memoryx
{

    class HandPredicateProviderPropertyDefinitions:
        public WorldStateUpdaterPropertyDefinitions
    {
    public:
        HandPredicateProviderPropertyDefinitions(std::string prefix):
            WorldStateUpdaterPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("DistanceThreshold", 150.f, "Threshold below which an object is considered to be grasped (one hand)");
        }
    };

    class HandPredicateProvider : public WorldStateUpdaterBase
    {
    public:
        HandPredicateProvider();

        // ManagedIceObject interface
    protected:
        void onInitWorldStateUpdater() override;
        void onConnectWorldStateUpdater() override;
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new HandPredicateProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // WorldStateUpdaterInterface interface
    public:
        PredicateInfoList getPredicateInfos(const Ice::Current& = Ice::emptyCurrent) override;
        PredicateInstanceList calcPredicates(const Ice::Current& = Ice::emptyCurrent) override;

    private:
        struct ObjectInfo
        {
            enum class ObjectType
            {
                OneHandGraspable,
                TwoHandGraspable,
                Other
            };

            ObjectInstancePtr objectInstance;
            ObjectType type;
        };

        WorkingMemoryInterfacePrx wm;
        PriorKnowledgeInterfacePrx prior;

        AgentInstancesSegmentBasePrx agentInstances;
        ObjectInstanceMemorySegmentBasePrx objectInstances;
        PersistentObjectClassSegmentBasePrx objectClasses;

        float distanceThreshold;
        float bothHandGraspedDistanceThreshold;

        PredicateInfo graspedPredicate;
        PredicateInfo graspedBothPredicate;
        PredicateInfo handEmptyPredicate;
    };

} // namespace memoryx

