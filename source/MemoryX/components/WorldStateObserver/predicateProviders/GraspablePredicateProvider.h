/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck ( valerij.wittenbeck at  student dot kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "Box.hpp"

#include <MemoryX/components/WorldStateObserver/WorldStateUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <ArmarXCore/core/Component.h>
#include <MemoryX/components/GraphNodePoseResolver/GraphNodePoseResolver.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

namespace memoryx
{
    class GraspablePredicateProviderPropertyDefinitions:
        public WorldStateUpdaterPropertyDefinitions
    {
    public:
        GraspablePredicateProviderPropertyDefinitions(std::string prefix):
            WorldStateUpdaterPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Where to draw the provided bounding boxes (if empty -> don't draw)");
            //countertop;sideboard?;fridge;table?
            defineOptionalProperty<std::string>("BoundingBoxesR", "2960.0 9885.0 1000.0 1970.0 990.0 500.0;4490.0 7100.0 1000.0 1000.0 1180.0 500.0;4465.0 10255.0 1750 490.0 500.0 2000;2680 6051 1100 800 800 500", "Format: 'xCenter yCenter zCenter xSize ySize zSize' for one 6-tuple, separate multiple tuples with ';'");
            defineOptionalProperty<std::string>("BoundingBoxesL", "3350.0 9960.0 1000.0 1820.0 840.0 500.0;4490.0 7100.0 1000.0 1000.0 1180.0 500.0;4465.0 10255.0 1750 490.0 500.0 2000;2680 6051 1100 800 800 500", "Format: 'xCenter yCenter zCenter xSize ySize zSize' for one 6-tuple, separate multiple tuples with ';'");
        }
    };

    class GraspablePredicateProvider : public WorldStateUpdaterBase
    {
    public:
        GraspablePredicateProvider();

        // ManagedIceObject interface
    protected:
        void onInitWorldStateUpdater() override;
        void onConnectWorldStateUpdater() override;
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new GraspablePredicateProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // WorldStateUpdaterInterface interface
    public:
        PredicateInfoList getPredicateInfos(const Ice::Current& = Ice::emptyCurrent) override;
        PredicateInstanceList calcPredicates(const Ice::Current& = Ice::emptyCurrent) override;

    private:
        struct GraspabilityDescriptor
        {
            std::string className;
            bool hasGraspableSuperclass;
            bool hasLeftGrasp;
            bool hasRightGrasp;
        };

        std::vector<Box> stringToBoxes(const std::string& boxesString);
        void addPredicateIfInside(PredicateInstanceList& result, const std::string& predicateName, const ObjectInstancePtr& obj, const std::vector<Box>& boxes);
        std::string debugDrawerTopicName;
        std::vector<Box> boxesL;
        std::vector<Box> boxesR;
        WorkingMemoryInterfacePrx wm;
        WorkingMemoryEntitySegmentBasePrx objectInstances;
        PriorKnowledgeInterfacePrx prior;
        PersistentObjectClassSegmentBasePrx objectClasses;

        armarx::DrawColor leftColor;
        armarx::DrawColor rightColor;
        PredicateInfo leftGraspable;
        PredicateInfo rightGraspable;

        memoryx::GridFileManagerPtr fileManager;
        std::vector<GraspabilityDescriptor> graspabilityDescriptors;
    };

} // namespace memoryx

