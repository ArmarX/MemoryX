/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck ( valerij.wittenbeck at  student dot kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/components/WorldStateObserver/WorldStateUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <ArmarXCore/core/Component.h>
#include <MemoryX/components/GraphNodePoseResolver/GraphNodePoseResolver.h>
#include <MemoryX/components/CommonPlacesLearner/CommonPlacesLearner.h>
#include <MemoryX/interface/components/GraphNodePoseResolverInterface.h>

namespace memoryx
{
    class PutAwayLocationPredicateProviderPropertyDefinitions:
        public WorldStateUpdaterPropertyDefinitions
    {
    public:
        PutAwayLocationPredicateProviderPropertyDefinitions(std::string prefix):
            WorldStateUpdaterPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("CommonPlacesLearnerName", "CommonPlacesLearnerPutAwayLocations", "The CommonPlacesLearner to use");
            defineOptionalProperty<std::string>("CPLSegmentName", "CPL_PutAwayLocations", "The LTM segment to use for common places");
            defineOptionalProperty<std::string>("PlatformGraphSceneName", "XperienceDemoKitchenRM", "Name of the scene in the graph segment");
        }
    };

    class PutAwayLocationPredicateProvider : public WorldStateUpdaterBase
    {
    public:
        PutAwayLocationPredicateProvider();

        // ManagedIceObject interface
    protected:
        void onInitWorldStateUpdater() override;
        void onConnectWorldStateUpdater() override;
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new PutAwayLocationPredicateProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // WorldStateUpdaterInterface interface
    public:
        PredicateInfoList getPredicateInfos(const Ice::Current& c = Ice::emptyCurrent) override;
        PredicateInstanceList calcPredicates(const Ice::Current&) override;

    private:
        struct CachedCommonPlace
        {
            std::string className;
            EntityRefBasePtr putAwayNodeRef;
        };

        CachedCommonPlace getCacheEntry(const std::string& className);

        std::map<std::string, CachedCommonPlace> cpCache;

        std::string cplSegmentName;
        std::string sceneName;

        WorkingMemoryInterfacePrx wm;
        PriorKnowledgeInterfacePrx prior;
        LongtermMemoryInterfacePrx ltm;
        GraphNodePoseResolverInterfacePrx psr;
        CommonPlacesLearnerInterfacePrx cpl;

        ObjectInstanceMemorySegmentBasePrx objectInstances;
        GraphMemorySegmentBasePrx graphSegment;
        PersistentObjectInstanceSegmentBasePrx cplObjectsSegment;

        std::vector<ObjectInstancePtr> cplObjects;

        PredicateInfo putAwayLocation;
    };

} // namespace memoryx

