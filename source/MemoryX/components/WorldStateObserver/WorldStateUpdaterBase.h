/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once

#include <MemoryX/interface/observers/WorldStateObserver.h>

#include <ArmarXCore/core/Component.h>

namespace memoryx
{

    class WorldStateUpdaterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        WorldStateUpdaterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorldStateObserverName", "WorldStateObserver", "Name of the WorldStateObserver proxy");
        }
    };

    class WorldStateUpdaterBase :
        public memoryx::WorldStateUpdaterInterface,
        public armarx::Component
    {
    public:
        WorldStateUpdaterBase();
        virtual void onInitWorldStateUpdater() = 0;
        virtual void onConnectWorldStateUpdater() = 0;
        // WorldStateUpdaterInterface interface
    public:
        void setObserver(const WorldStateObserverInterfacePrx& worldStateObserver, const Ice::Current&) override;
        void requestUpdateOnRegisteredObserver(const Ice::Current&) override;
        void requestUpdate(const WorldStateObserverInterfacePrx& observer, const Ice::Current& c = Ice::emptyCurrent) override;
    protected:
        WorldStateObserverInterfacePrx worldStateObserver;

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new WorldStateUpdaterPropertyDefinitions(
                           getConfigIdentifier()));
        }

    };

}

