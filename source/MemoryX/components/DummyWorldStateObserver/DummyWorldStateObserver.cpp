/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::DummyWorldStateObserver
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DummyWorldStateObserver.h"


namespace armarx
{
    void DummyWorldStateObserver::onInitComponent()
    {
    }

    void DummyWorldStateObserver::onConnectComponent()
    {
    }

    void DummyWorldStateObserver::onDisconnectComponent()
    {
    }

    void DummyWorldStateObserver::onExitComponent()
    {
    }

    armarx::PropertyDefinitionsPtr DummyWorldStateObserver::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DummyWorldStateObserverPropertyDefinitions(
                getConfigIdentifier()));
    }

    memoryx::PredicateInstanceList DummyWorldStateObserver::getWorldState(const Ice::Current&)
    {
        ARMARX_WARNING << "getWorldState is a stub";

        std::vector<memoryx::PredicateInstance> result;
        return result;
    }

    bool DummyWorldStateObserver::isObservable(const std::string& predicateName, const Ice::Current&)
    {
        ARMARX_WARNING << "isObservable is a stub";
        return false;
    }

    bool DummyWorldStateObserver::updatePredicateValue(const memoryx::PredicateInstance& pi, bool removePredicate, const Ice::Current&)
    {
        ARMARX_WARNING << "updatePredicateValue is a stub";
        return true;
    }

    void DummyWorldStateObserver::setPredicateArgumentWhitelist(const memoryx::EntityBaseList& argumentWhitelist, const Ice::Current&)
    {
        ARMARX_WARNING << "setPredicateArgumentWhitelist is a stub";
    }

    void DummyWorldStateObserver::resetPredicateArgumentWhitelist(const Ice::Current&)
    {
        ARMARX_WARNING << "resetPredicateArgumentWhitelist is a stub";
    }

    void DummyWorldStateObserver::addObservablePredicateInstances(const memoryx::PredicateInstanceList& predicates, const Ice::Current&)
    {
        ARMARX_WARNING << "addObservablePredicateInstances is a stub";
    }

    void DummyWorldStateObserver::registerAsUpdater(const std::string& name, const memoryx::WorldStateUpdaterInterfacePrx& updater, const Ice::Current&)
    {
        ARMARX_WARNING << "registerAsUpdater is a stub";
    }

    memoryx::WorldStateUpdaterInterfaceList DummyWorldStateObserver::getRegisteredUpdaters(const Ice::Current&)
    {
        ARMARX_WARNING << "getRegisteredUpdaters is a stub";

        std::vector<memoryx::WorldStateUpdaterInterfacePrx> result;
        return result;
    }
}
