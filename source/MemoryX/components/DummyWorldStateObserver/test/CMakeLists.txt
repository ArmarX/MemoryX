
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore DummyWorldStateObserver)
 
armarx_add_test(DummyWorldStateObserverTest DummyWorldStateObserverTest.cpp "${LIBS}")