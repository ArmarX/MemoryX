/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::WorkingMemoryToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WorkingMemoryToArViz.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <SimoxUtility/algorithm/string.h>

namespace armarx
{
    WorkingMemoryToArVizPropertyDefinitions::WorkingMemoryToArVizPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }


    armarx::PropertyDefinitionsPtr WorkingMemoryToArViz::createPropertyDefinitions()
    {
        ARMARX_TRACE;
        armarx::PropertyDefinitionsPtr defs = (new WorkingMemoryToArVizPropertyDefinitions(getConfigIdentifier()));

        defs->defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Prior knowledge name.");
        defs->defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Working memory name.");
        defs->defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Robot state component name.");


        defs->defineOptionalProperty<std::string>(
            "ObjectClassBlackWhitelistTopic", "WorkingMemoryToArVizObjectClassBlackWhitelistUpdates",
            "The topic where updates to the object class black-whitelist are published.");
        defs->defineOptionalProperty<std::vector<std::string>>("ObjectClassWhitelist", {},
                "If not empty, only these object classes are shown (comma separated list).")
                .map("[empty whitelist]", {});
        defs->defineOptionalProperty<std::vector<std::string>>("ObjectClassBlacklist", {},
                "These object classes will never be shown (comma separated list).")
                .map("[empty blacklist]", {});


        defs->optional(p.updateFrequency, "p.UpdateFrequency", "Target number of updates per second.");

        defs->optional(p.floor.show, "p.floor.Show", "Whether to show the floor.");
        defs->optional(p.floor.height, "p.floor.Height",
                       "Height (z) of the floor plane. \n"
                       "Set slightly below 0 to avoid z-fighting when drawing planes on the ground.");
        defs->optional(p.loadObjectDatasetsStr, "p.loadDatasets", "Only load the files for the following datasets, separated by ;. Load all if input is empty.");


        return defs;
    }


    std::string WorkingMemoryToArViz::getDefaultName() const
    {
        return "WorkingMemoryToArViz";
    }


    void WorkingMemoryToArViz::onInitComponent()
    {
        ARMARX_TRACE;
        usingProxyFromProperty("PriorKnowledgeName");
        usingProxyFromProperty("WorkingMemoryName");
        usingProxyFromProperty("RobotStateComponentName");

        usingTopicFromProperty("ObjectClassBlackWhitelistTopic");
    }


    void WorkingMemoryToArViz::onConnectComponent()
    {
        ARMARX_TRACE;
        getProxyFromProperty(priorKnowledge, "PriorKnowledgeName");
        getProxyFromProperty(workingMemory, "WorkingMemoryName");
        getProxyFromProperty(robotStateComponent, "RobotStateComponentName");

        auto datasets = simox::alg::split(p.loadObjectDatasetsStr, ";");

        drawer.setArViz(arviz);
        drawer.initFromProxies(priorKnowledge, workingMemory, robotStateComponent, datasets);
        {
            ARMARX_TRACE;
            armarx::BlackWhitelistUpdate update;
            getProperty(update.whitelist.set, "ObjectClassWhitelist");
            getProperty(update.blacklist.set, "ObjectClassBlacklist");
            drawer.updateObjectClassBlackWhitelist(update);
        }

        if (p.floor.show > 0)
        {
            drawer.updateFloorObject(p.floor.height);
        }

        // A periodic task logs an important info when the cycle time is not met.
        // To avoid this, we use a running task.
        task = new armarx::SimpleRunningTask<>([this]()
        {
            ARMARX_TRACE_LITE;
            CycleUtil cycle(int(1000 / p.updateFrequency));

            while (task && !task->isStopped())
            {
                {
                    std::scoped_lock lock(drawerMutex);
                    drawer.updateObjects();
                }
                cycle.waitForCycleDuration();
            }
        });

        task->start();
    }


    void WorkingMemoryToArViz::onDisconnectComponent()
    {
        ARMARX_TRACE;
        task->stop();
        task = nullptr;
    }


    void WorkingMemoryToArViz::onExitComponent()
    {
        ARMARX_TRACE;
    }


    void WorkingMemoryToArViz::updateBlackWhitelist(const BlackWhitelistUpdate& update, const Ice::Current&)
    {
        ARMARX_TRACE;
        std::scoped_lock lock(drawerMutex);
        drawer.updateObjectClassBlackWhitelist(update);
    }

    void WorkingMemoryToArViz::attachObjectToRobotNode(
        const memoryx::AttachObjectToRobotNodeInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;
        std::scoped_lock lock(drawerMutex);
        drawer.attachObjectToRobotNode(input);
    }

    void WorkingMemoryToArViz::detachObjectFromRobotNode(
        const memoryx::DetachObjectFromRobotNodeInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;
        std::scoped_lock lock(drawerMutex);
        drawer.detachObjectFromRobotNode(input);
    }

}
