armarx_component_set_name("WorkingMemoryToArViz")


set(COMPONENT_LIBS
    ArmarXCore
    RobotAPICore RobotAPIComponentPlugins
    DebugDrawerToArViz  # (RobotAPI) To be removed/updated when BlackWhitelist utils is moved.
    MemoryXInterfaces MemoryXVirtualRobotHelpers
)

set(SOURCES
    WorkingMemoryToArViz.cpp
    ObjectInstancesToArViz.cpp
)
set(HEADERS
    WorkingMemoryToArViz.h
    ObjectInstancesToArViz.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")

#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
# all target_include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    target_include_directories(WorkingMemoryToArViz PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()

# add unit tests
#add_subdirectory(test)

armarx_component_set_name("WorkingMemoryToArVizApp")
set(COMPONENT_LIBS WorkingMemoryToArViz)
armarx_add_component_executable(main.cpp)

