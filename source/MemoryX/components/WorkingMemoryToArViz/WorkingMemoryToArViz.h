/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::WorkingMemoryToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/WorkingMemoryToArVizInterface.h>

#include "ObjectInstancesToArViz.h"



namespace armarx
{
    /**
     * @class WorkingMemoryToArVizPropertyDefinitions
     * @brief Property definitions of `WorkingMemoryToArViz`.
     */
    class WorkingMemoryToArVizPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        WorkingMemoryToArVizPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-WorkingMemoryToArViz WorkingMemoryToArViz
     * @ingroup MemoryX-Components
     * A description of the component WorkingMemoryToArViz.
     *
     * @class WorkingMemoryToArViz
     * @ingroup Component-WorkingMemoryToArViz
     * @brief Brief description of class WorkingMemoryToArViz.
     *
     * Detailed description of class WorkingMemoryToArViz.
     */
    class WorkingMemoryToArViz :
        virtual public armarx::Component,
        virtual public memoryx::WorkingMemoryToArVizInterface,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // WorkingMemoryToArVizInterface interface

        void updateBlackWhitelist(const BlackWhitelistUpdate& update,
                                  const Ice::Current& = Ice::emptyCurrent) override;

        void attachObjectToRobotNode(const memoryx::AttachObjectToRobotNodeInput& input,
                                     const Ice::Current& = Ice::emptyCurrent) override;
        void detachObjectFromRobotNode(const memoryx::DetachObjectFromRobotNodeInput& input,
                                       const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        struct Properties
        {
            float updateFrequency = 30;

            struct Floor
            {
                bool show = true;
                float height = -1;
            };
            Floor floor;

            std::string loadObjectDatasetsStr = "";
        };
        Properties p;

        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::WorkingMemoryInterfacePrx workingMemory;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;

        std::mutex drawerMutex;

        memoryx::ObjectInstancesToArViz drawer;

        armarx::SimpleRunningTask<>::pointer_type task;

    };
}
