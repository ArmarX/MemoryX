/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::ArmarXObjectsImporter
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/core/GridFileManager.h>


namespace memoryx
{

    /**
     * @defgroup Component-ArmarXObjectsImporter ArmarXObjectsImporter
     * @ingroup MemoryX-Components
     * A description of the component ArmarXObjectsImporter.
     *
     * @class ArmarXObjectsImporter
     * @ingroup Component-ArmarXObjectsImporter
     * @brief Brief description of class ArmarXObjectsImporter.
     *
     * Detailed description of class ArmarXObjectsImporter.
     */
    class ArmarXObjectsImporter :
        virtual public armarx::Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;



    private:


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            Properties();

            std::string mongoDbName = "CeBITdb";
            bool dryRun = false;

            bool importObjects = true;
            std::string objectsRepository;  // Set in constructor
            std::string datasetName = "";
            std::string className = "";

            bool importScenes = true;
            std::string scenesDirectory;  // Set in constructor
            std::string sceneNames;
        };
        Properties properties;


        struct Proxies
        {
            memoryx::CommonStorageInterfacePrx commonStorage;
            memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
            memoryx::WorkingMemoryInterfacePrx workingMemory;
            memoryx::LongtermMemoryInterfacePrx longtermMemory;
        };
        Proxies proxies;

        memoryx::GridFileManagerPtr gridFileManager;

    };
}
