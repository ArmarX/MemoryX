set(LIB_NAME       ArmarXObjectsImporter)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")


# If your component needs a special ice interface, define it here:
# armarx_add_component_interface_lib(
#     SLICE_FILES
#         ArmarXObjectsImporter.ice
#     ICE_LIBS
#         # RobotAPI
#)


# Add the component
armarx_add_component(
    COMPONENT_LIBS
        # ArmarXCore
        ArmarXCore
        ## ArmarXCoreComponentPlugins  # For DebugObserver plugin.
        # ArmarXGui
        ## ArmarXGuiComponentPlugins  # For RemoteGui plugin.
        # RobotAPI
        RobotAPICore
        ## RobotAPIInterfaces
        ## RobotAPIComponentPlugins  # For ArViz and other plugins.
        RobotAPIArmarXObjects
        armem_objects

        # MemoryX
        MemoryXInterfaces
        MemoryXImporters

        # This project
        ## ${PROJECT_NAME}Interfaces  # For ice interfaces from this package.
        # This component
        ## ArmarXObjectsImporterInterfaces  # If you defined a component ice interface above.

    SOURCES
        ArmarXObjectsImporter.cpp

    HEADERS
        ArmarXObjectsImporter.h
)


# Add dependencies
#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")

# All target_include_directories must be guarded by if(Xyz_FOUND)
# For multiple libraries write: if(X_FOUND AND Y_FOUND) ...
#if(MyLib_FOUND)
#    target_include_directories(ArmarXObjectsImporter PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()


# Add ARON files
#armarx_enable_aron_file_generation_for_target(
#    TARGET_NAME
#        ${ARMARX_COMPONENT_NAME}
#    ARON_FILES
#        aron/ExampleData.xml
#)


# Add unit tests
# add_subdirectory(test)

# Generate the application
armarx_generate_and_add_component_executable(
    COMPONENT_NAMESPACE "memoryx"
)
