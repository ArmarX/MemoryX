/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::ArmarXObjectsImporter
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXObjectsImporter.h"

#include <unordered_set>

#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <MemoryX/libraries/importers/ArmarXObjectsToMemory.h>


namespace memoryx
{

    armarx::PropertyDefinitionsPtr ArmarXObjectsImporter::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->component(proxies.commonStorage);
        def->component(proxies.priorKnowledge);
        def->component(proxies.workingMemory);
        def->component(proxies.longtermMemory);

        def->optional(properties.mongoDbName, "p.MongoDbName",
                      "Name of snapshot Mongo database to load files into");
        def->optional(properties.dryRun, "p.dryRun",
                      "If true, don't actually change anything.");


        def->optional(properties.importObjects, "p.obj.00_DoImport",
                      "Whether to import objects.");

        def->optional(properties.objectsRepository, "p.obj.10_Repository",
                      "Repository such as ArmarXObjects containing the object classes.");

        def->optional(properties.datasetName, "p.obj.20_DatasetName",
                      "If set, only the given dataset in ArmarXObjects is imported.\n"
                      "Otherwise (default), all objects are imported.");
        def->optional(properties.className, "p.obj.21_ClassName",
                      "If set, only the given dataset in ArmarXObjects is imported.\n"
                      "Otherwise (default), all objects are imported.");


        def->optional(properties.importScenes, "p.scenes.00_DoImport",
                      "Whether to import scenes.");
        def->optional(properties.scenesDirectory, "p.scenes.10_Directory",
                      "Directory containing the scene files (Scene_*.json).");
        def->optional(properties.sceneNames, "p.scenes.20_Scenes",
                      "Scenes to load by name. Comma separated. ({scene_name}.json).");

        return def;
    }


    void ArmarXObjectsImporter::onInitComponent()
    {
    }


    void ArmarXObjectsImporter::onConnectComponent()
    {
        gridFileManager.reset(new memoryx::GridFileManager(proxies.commonStorage));

        memoryx::PersistentObjectClassSegmentBasePrx objectClassesSegment =
            proxies.priorKnowledge->getObjectClassesSegment();


        ArmarXObjectsToMemory importer;
        importer.dryRun = properties.dryRun;

        if (properties.dryRun)
        {
            ARMARX_IMPORTANT << "Performing dry run - not going to change anything!";
        }

        if (properties.importObjects)
        {
            armarx::ObjectFinder finder(properties.objectsRepository);
            std::vector<armarx::ObjectInfo> objectInfos;
            if (properties.datasetName.size() > 0)
            {
                if (properties.className.size() > 0)
                {
                    ARMARX_INFO << "Finding " << properties.datasetName << properties.className << "...";
                    if (auto info = finder.findObject(properties.datasetName, properties.className))
                    {
                        objectInfos.push_back(info.value());
                    }
                }
                else
                {
                    ARMARX_INFO << "Finding objects in dataset " << properties.datasetName << " ...";
                    objectInfos = finder.findAllObjectsOfDataset(properties.datasetName, false);
                }
            }
            else
            {
                ARMARX_INFO << "Finding all objects ...";
                objectInfos = finder.findAllObjects(false);
            }
            ARMARX_INFO << "Found " << objectInfos.size() << " object classes in " << properties.objectsRepository << ".";

            ARMARX_INFO << "Importing " << objectInfos.size() << " object classes ...";
            importer.importObjectsToPriorKnowledge(
                objectInfos, gridFileManager, objectClassesSegment, properties.mongoDbName);
        }

        if (properties.importScenes)
        {
            const std::filesystem::path scenesDir = armarx::ArmarXDataPath::resolvePath(properties.scenesDirectory);
            std::unordered_set<std::string> scenes;
            if (not properties.sceneNames.empty())
            {
                bool trim = true, removeEmpty = true;
                const auto scenesTmp = armarx::split(properties.sceneNames, ",", trim, removeEmpty);
                ARMARX_IMPORTANT << properties.sceneNames << "\n" << scenesTmp;
                scenes = std::unordered_set<std::string> (scenesTmp.begin(), scenesTmp.end());
            }

            if (not scenes.empty())
            {
                ARMARX_INFO << "The following scenes will be loaded (if available): " << std::vector<std::string>(scenes.begin(), scenes.end());
            }

            ARMARX_INFO << "Importing scenes in " << scenesDir;
            importer.importScenesAsSnapshots(
                scenesDir, proxies.longtermMemory, proxies.workingMemory, objectClassesSegment, scenes);
        }
    }


    void ArmarXObjectsImporter::onDisconnectComponent()
    {
    }


    void ArmarXObjectsImporter::onExitComponent()
    {
    }


    std::string ArmarXObjectsImporter::getDefaultName() const
    {
        return "ArmarXObjectsImporter";
    }

    ArmarXObjectsImporter::Properties::Properties() :
        objectsRepository(armarx::ObjectFinder::DefaultObjectsPackageName),
        scenesDirectory(armarx::ObjectFinder::DefaultObjectsPackageName + "/scenes")
    {
    }

}
