/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::component::ProfilerStorage
 * @author     haass ( tobias dot haass at student dot kit dot edu )
 * @author     Manfred Kroehnert ( manfred dot kroehnert at kit dot edu )
 * @date       2013, 2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ProfilerStorage.h"

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <ArmarXCore/statechart/StatechartObjectFactories.h>
#include <MemoryX/libraries/memorytypes/entity/profiler/ProfilerEvent.h>
#include <MemoryX/libraries/memorytypes/entity/profiler/ProfilerMemoryUsage.h>
#include <MemoryX/libraries/memorytypes/entity/profiler/ProfilerProcess.h>

#include <IceUtil/Time.h>

#include <mongo/client/dbclient.h>


namespace memoryx
{
    std::string ProfilerStorage::getDefaultName() const
    {
        return "ProfilerStorage";
    }


    void ProfilerStorage::onInitComponent()
    {
        setTag("ProfilerStorage");

        usingProxy(getCommonStorageName());
        usingProxy(getWorkingMemoryName());
        usingProxy(getLongtermMemoryName());

        enabled = getProperty<bool>("enabled").getValue();

        if (enabled)
        {
            usingTopic(armarx::Profiler::PROFILER_TOPIC_NAME);
        }

        saveTimeout = getProperty<int>("SaveTimeout").getValue();
        saveToLongtermMemoryTask = new armarx::RunningTask<ProfilerStorage>(this, &ProfilerStorage::saveToLongtermMemory);
        saveTransitionsToLongtermMemoryTask = new armarx::RunningTask<ProfilerStorage>(this, &ProfilerStorage::saveTransitionsToLongtermMemory);

        // the initial transition does not have a source state, thus the name of the initial transition is empty
        memorySnapshot = CreateProfilerMemorySnapshot("", workingMemoryPrx);
    }


    void ProfilerStorage::onConnectComponent()
    {
        getProxy(commonStoragePrx, getCommonStorageName());
        getProxy(workingMemoryPrx, getWorkingMemoryName());
        getProxy(longtermMemoryPrx, getLongtermMemoryName());
        profilerDataSegmentPrx = longtermMemoryPrx->getProfilerDataSegment();

        saveToLongtermMemoryTask->start();
        saveTransitionsToLongtermMemoryTask->start();
    }


    void ProfilerStorage::onDisconnectComponent()
    {
    }


    void ProfilerStorage::onExitComponent()
    {
        enabled = false;
        saveToLongtermMemoryTask->stop();
        saveTransitionsToLongtermMemoryTask->stop();
    }


    void ProfilerStorage::saveToLongtermMemory()
    {
        IceUtil::Time t = IceUtil::Time::now();

        while (enabled)
        {
            size_t logSize = entityLog.size();

            if (logSize == 0)
            {
                continue;
            }

            if ((logSize < 1000) && ((IceUtil::Time::now() - t).toMilliSeconds() < saveTimeout))
            {
                continue;
            }

            EntityBaseList entityLogCopy;
            {
                std::scoped_lock lock(entityLogMutex);
                entityLog.swap(entityLogCopy);
            }
            t = IceUtil::Time::now();
            profilerDataSegmentPrx->addEntityList(entityLogCopy);
            ARMARX_WARNING_S << "Took " << (IceUtil::Time::now() - t).toMilliSeconds() << " [ms] to add " << entityLogCopy.size() << " Entities";
            t = IceUtil::Time::now();
        }
    }


    void ProfilerStorage::saveTransitionsToLongtermMemory()
    {
        IceUtil::Time t = IceUtil::Time::now();

        while (enabled)
        {
            size_t logSize = transitionSnapshotList.size();

            if (logSize == 0)
            {
                continue;
            }

            if ((logSize < 1000) && ((IceUtil::Time::now() - t).toMilliSeconds() < saveTimeout))
            {
                continue;
            }

            TransitionSnapshotList transitionSnapshotListCopy;
            {
                std::scoped_lock lock(transitionSnapshotListMutex);
                transitionSnapshotList.swap(transitionSnapshotListCopy);
            }
            t = IceUtil::Time::now();

            for (TransitionSnapshot& snapshot : transitionSnapshotListCopy)
            {
                std::string sourceSnapshot = profilerDataSegmentPrx->saveUniqueMemorySnapshot(snapshot.sourceStateMemorySnapshot);
                std::string targetSnapshot = profilerDataSegmentPrx->saveUniqueMemorySnapshot(snapshot.targetStateMemorySnapshot);
                EntityRefBasePtr sourceSnapshotRef = profilerDataSegmentPrx->getEntityRefById(sourceSnapshot);
                EntityRefBasePtr targetSnapshotRef = profilerDataSegmentPrx->getEntityRefById(targetSnapshot);
                ProfilerTransitionPtr profilerTransition = new memoryx::ProfilerTransition(snapshot.transition.parentStateIdentifier, snapshot.transition.sourceStateIdentifier, snapshot.transition.targetStateIdentifier, sourceSnapshotRef, targetSnapshotRef, 1);
                std::string entityId = profilerDataSegmentPrx->saveOrUpdateTransition(profilerTransition);
            }
            ARMARX_WARNING_S << "Took " << (IceUtil::Time::now() - t).toMilliSeconds() << " [ms] to add " << transitionSnapshotListCopy.size() << " Transitions";
            t = IceUtil::Time::now();
        }
    }


    void ProfilerStorage::reportNetworkTraffic(const std::string&, const std::string&, Ice::Int, Ice::Int, const Ice::Current&)
    {
        // ignore network traffic for now
    }


    void ProfilerStorage::reportEvent(const armarx::ProfilerEvent& profilerEvent, const Ice::Current&)
    {
        std::scoped_lock lock(entityLogMutex);
        entityLog.push_back(new memoryx::ProfilerEvent(profilerEvent));
    }


    void ProfilerStorage::reportStatechartTransition(const armarx::ProfilerStatechartTransition& transition, const ::Ice::Current& context)
    {
        std::scoped_lock lock(transitionSnapshotListMutex);
        TransitionSnapshot transitionSnapshot;
        transitionSnapshot.transition = transition;
        transitionSnapshot.sourceStateMemorySnapshot = memorySnapshot;
        memorySnapshot = CreateProfilerMemorySnapshot(transition.targetStateIdentifier, workingMemoryPrx);
        transitionSnapshot.targetStateMemorySnapshot = memorySnapshot;
        transitionSnapshotList.push_back(transitionSnapshot);
    }


    void ProfilerStorage::reportStatechartInputParameters(const armarx::ProfilerStatechartParameters& inputParameters, const Ice::Current&)
    {
    }


    void ProfilerStorage::reportStatechartLocalParameters(const armarx::ProfilerStatechartParameters& localParameters, const Ice::Current&)
    {
    }


    void ProfilerStorage::reportStatechartOutputParameters(const armarx::ProfilerStatechartParameters& outputParameters, const Ice::Current&)
    {
    }


    void ProfilerStorage::reportProcessCpuUsage(const armarx::ProfilerProcessCpuUsage& process, const Ice::Current& context)
    {
        std::scoped_lock lock(entityLogMutex);
        entityLog.push_back(new memoryx::ProfilerProcess(process));
    }

    void ProfilerStorage::reportProcessMemoryUsage(const armarx::ProfilerProcessMemoryUsage& memoryUsage, const Ice::Current& context)
    {
        std::scoped_lock lock(entityLogMutex);
        entityLog.push_back(new memoryx::ProfilerMemoryUsage(memoryUsage));
    }




    void ProfilerStorage::reportEventList(const armarx::ProfilerEventList& events, const Ice::Current&)
    {
        memoryx::EntityBaseList eventEntities(events.size());
        std::transform(events.begin(), events.end(), eventEntities.begin(),
                       [](const armarx::ProfilerEvent & event)
        {
            return new memoryx::ProfilerEvent(event);
        });

        {
            std::scoped_lock lock(entityLogMutex);
            entityLog.insert(entityLog.end(), eventEntities.begin(), eventEntities.end());
        }
    }


    void ProfilerStorage::reportStatechartTransitionList(const armarx::ProfilerStatechartTransitionList& transitions, const Ice::Current&)
    {
        if (transitions.empty())
        {
            return;
        }

        TransitionSnapshotList transitionSnapshots(transitions.size());

        ProfilerMemorySnapshotPtr sourceMemorySnapshot = memorySnapshot->clone();

        std::transform(transitions.begin(), transitions.end(), transitionSnapshots.begin(),
                       [](const armarx::ProfilerStatechartTransition & transition)
        {
            TransitionSnapshot transitionSnapshot = {transition, new ProfilerMemorySnapshot(), new ProfilerMemorySnapshot()};
            return transitionSnapshot;
        });

        transitionSnapshots.front().sourceStateMemorySnapshot = sourceMemorySnapshot;
        memorySnapshot = ProfilerStorage::CreateProfilerMemorySnapshot(transitionSnapshots.back().transition.targetStateIdentifier, workingMemoryPrx);
        if (memorySnapshot)
        {
            transitionSnapshots.back().targetStateMemorySnapshot = memorySnapshot;
        }
        {
            std::scoped_lock lock(transitionSnapshotListMutex);
            transitionSnapshotList.insert(transitionSnapshotList.end(), transitionSnapshots.begin(), transitionSnapshots.end());
        }
    }


    void ProfilerStorage::reportStatechartInputParametersList(const armarx::ProfilerStatechartParametersList& inputParametersList, const Ice::Current&)
    {
    }


    void ProfilerStorage::reportStatechartLocalParametersList(const armarx::ProfilerStatechartParametersList& localParametesList, const Ice::Current&)
    {
    }


    void ProfilerStorage::reportStatechartOutputParametersList(const armarx::ProfilerStatechartParametersList& outputParametersList, const Ice::Current&)
    {
    }


    void ProfilerStorage::reportProcessCpuUsageList(const armarx::ProfilerProcessCpuUsageList& processes, const Ice::Current&)
    {
        memoryx::EntityBaseList eventEntities(processes.size());
        std::transform(processes.begin(), processes.end(), eventEntities.begin(),
                       [](const armarx::ProfilerProcessCpuUsage & process)
        {
            return new memoryx::ProfilerProcess(process);
        });

        {
            std::scoped_lock lock(entityLogMutex);
            entityLog.insert(entityLog.end(), eventEntities.begin(), eventEntities.end());
        }
    }

    void ProfilerStorage::reportProcessMemoryUsageList(const armarx::ProfilerProcessMemoryUsageList& memoryUsages, const Ice::Current&)
    {
        memoryx::EntityBaseList eventEntities(memoryUsages.size());
        std::transform(memoryUsages.begin(), memoryUsages.end(), eventEntities.begin(),
                       [](const armarx::ProfilerProcessMemoryUsage & memoryUsage)
        {
            return new memoryx::ProfilerMemoryUsage(memoryUsage);
        });

        {
            std::scoped_lock lock(entityLogMutex);
            entityLog.insert(entityLog.end(), eventEntities.begin(), eventEntities.end());
        }
    }


    ProfilerMemorySnapshotPtr  ProfilerStorage::CreateProfilerMemorySnapshot(const std::string& stateName, const WorkingMemoryInterfacePrx& workingMemoryProxy)
    {
        if (!workingMemoryProxy)
        {
            return new ProfilerMemorySnapshot(stateName, Ice::Context());
        }

        Ice::Context snapshot;

        memoryx::ObjectInstancePtr humanInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemoryProxy->getObjectInstancesSegment()->getObjectInstanceByName("human"));
        if (humanInstance)
        {
            snapshot["human"] = humanInstance->getAttribute("name")->getValue()->getString();
            if (humanInstance->hasAttribute("humanObject"))
            {
                snapshot["humanObject"] = humanInstance->getAttribute("humanObject")->getValue()->getString();
            }

            if (humanInstance->hasAttribute("intention"))
            {
                snapshot["intention"] = humanInstance->getAttribute("intention")->getValue()->getString();
            }
        }

        memoryx::ObjectInstancePtr objectInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemoryProxy->getObjectInstancesSegment()->getObjectInstanceByName("taskObject"));
        if (objectInstance)
        {
            snapshot["taskObject"] = objectInstance->getAttribute("name")->getValue()->getString();

            bool inHand = objectInstance->getAttribute("inHand")->getValue()->getBool();
            snapshot["objectInHand"] = inHand ? "true" : "false";
        }

        return new ProfilerMemorySnapshot(stateName, snapshot);
    }


    std::string ProfilerStorage::getCommonStorageName()
    {
        return getProperty<std::string>("CommonStorageName").getValue();
    }


    std::string ProfilerStorage::getWorkingMemoryName()
    {
        return getProperty<std::string>("WorkingMemoryName").getValue();
    }

    std::string ProfilerStorage::getLongtermMemoryName()
    {
        return getProperty<std::string>("LongtermMemoryName").getValue();
    }


    armarx::PropertyDefinitionsPtr ProfilerStorage::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(
                   new ProfilerStoragePropertyDefinitions(
                       getConfigIdentifier()));
    }

    void ProfilerStorage::reportStatechartTransitionWithParameters(const armarx::ProfilerStatechartTransitionWithParameters& transition, const Ice::Current& context)
    {

    }

    void ProfilerStorage::reportStatechartTransitionWithParametersList(const armarx::ProfilerStatechartTransitionWithParametersList& transitions, const Ice::Current&)
    {

    }


    ProfilerStoragePropertyDefinitions::ProfilerStoragePropertyDefinitions(std::string prefix) :
        ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<int>("SaveTimeout", 500, "How often should the statistics be saved (ms)");
        defineOptionalProperty<std::string>("CommonStorageName", "CommonStorage", "Name of the CommonStorage proxy to use");
        defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory proxy to use");
        defineOptionalProperty<std::string>("LongtermMemoryName", "LongtermMemory", "Name of the LongtermMemory proxy to use");
        defineOptionalProperty<bool>("enabled", true, "disable/enable saver");
    }
}
