/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::component::ProfilerStorage
 * @author     haass ( tobias dot haass at student dot kit dot edu )
 * @author     Manfred Kroehnert ( manfred dot kroehnert at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../../libraries/memorytypes/entity/profiler/ProfilerMemorySnapshot.h"
#include "../../libraries/memorytypes/entity/profiler/ProfilerTransition.h"

#include <MemoryX/interface/components/ProfilerStorageInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>


namespace memoryx
{
    class ProfilerStoragePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ProfilerStoragePropertyDefinitions(std::string prefix);
    };

    /**
     * @class ProfilerStorage
     * @brief A brief description
     *
     * Detailed Description
     */
    class ProfilerStorage :
        virtual public armarx::Component,
        virtual public ProfilerStorageInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        static ProfilerMemorySnapshotPtr CreateProfilerMemorySnapshot(const std::string& stateName, const WorkingMemoryInterfacePrx& workingMemoryProxy);

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        std::string getCommonStorageName();

        std::string getWorkingMemoryName();

        std::string getLongtermMemoryName();

        /**
         * @brief reportNetworkTraffic
         * @param context
         */
        void reportNetworkTraffic(const std::string&, const std::string&, Ice::Int, Ice::Int, const Ice::Current& context = Ice::emptyCurrent) override;
        void reportEvent(const armarx::ProfilerEvent& profilerEvent, const Ice::Current& context = Ice::emptyCurrent) override;

        void reportStatechartTransition(const armarx::ProfilerStatechartTransition& transition, const ::Ice::Current& context = Ice::emptyCurrent) override;
        void reportStatechartInputParameters(const armarx::ProfilerStatechartParameters& inputParameters, const Ice::Current& = Ice::emptyCurrent) override;
        void reportStatechartLocalParameters(const armarx::ProfilerStatechartParameters& localParameters, const Ice::Current& = Ice::emptyCurrent) override;
        void reportStatechartOutputParameters(const armarx::ProfilerStatechartParameters& outputParameters, const Ice::Current& = Ice::emptyCurrent) override;

        void reportProcessCpuUsage(const armarx::ProfilerProcessCpuUsage& process, const Ice::Current& context = Ice::emptyCurrent) override;
        void reportProcessMemoryUsage(const armarx::ProfilerProcessMemoryUsage& memoryUsage, const Ice::Current& context = Ice::emptyCurrent) override;


        void reportEventList(const armarx::ProfilerEventList& events, const Ice::Current& = Ice::emptyCurrent) override;

        void reportStatechartTransitionList(const armarx::ProfilerStatechartTransitionList& transitions, const Ice::Current& = Ice::emptyCurrent) override;
        void reportStatechartInputParametersList(const armarx::ProfilerStatechartParametersList& inputParametersList, const Ice::Current& = Ice::emptyCurrent) override;
        void reportStatechartLocalParametersList(const armarx::ProfilerStatechartParametersList& localParametesList, const Ice::Current& = Ice::emptyCurrent) override;
        void reportStatechartOutputParametersList(const armarx::ProfilerStatechartParametersList& outputParametersList, const Ice::Current& = Ice::emptyCurrent) override;

        void reportProcessCpuUsageList(const armarx::ProfilerProcessCpuUsageList& processes, const Ice::Current& = Ice::emptyCurrent) override;
        void reportProcessMemoryUsageList(const armarx::ProfilerProcessMemoryUsageList& memoryUsages, const Ice::Current& = Ice::emptyCurrent) override;

        void reportStatechartTransitionWithParameters(const armarx::ProfilerStatechartTransitionWithParameters& transition, const ::Ice::Current& context = Ice::emptyCurrent) override;
        void reportStatechartTransitionWithParametersList(const armarx::ProfilerStatechartTransitionWithParametersList& transitions, const Ice::Current& = Ice::emptyCurrent) override;

    private:
        void saveToLongtermMemory();
        void saveTransitionsToLongtermMemory();

        struct TransitionSnapshot
        {
            armarx::ProfilerStatechartTransition transition;
            ProfilerMemorySnapshotPtr sourceStateMemorySnapshot;
            ProfilerMemorySnapshotPtr targetStateMemorySnapshot;
        };
        using TransitionSnapshotList = std::vector<TransitionSnapshot>;

        CommonStorageInterfacePrx commonStoragePrx;
        WorkingMemoryInterfacePrx workingMemoryPrx;
        LongtermMemoryInterfacePrx longtermMemoryPrx;
        PersistentProfilerDataSegmentBasePrx profilerDataSegmentPrx;

        armarx::RunningTask<ProfilerStorage>::pointer_type saveToLongtermMemoryTask;
        armarx::RunningTask<ProfilerStorage>::pointer_type saveTransitionsToLongtermMemoryTask;

        std::mutex entityLogMutex;
        memoryx::EntityBaseList entityLog;
        std::mutex transitionSnapshotListMutex;
        TransitionSnapshotList transitionSnapshotList;

        ProfilerMemorySnapshotPtr memorySnapshot;

        bool enabled;
        int saveTimeout;
    };
}

