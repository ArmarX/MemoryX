/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "MissingAttributeFusion.h"
#include "MotionModelRestoreFusion.h"
#include "WorkingMemory.h"

#include <MemoryX/libraries/memorytypes/segment/ObjectInstanceMemorySegment/ObjectInstanceMemorySegment.h>
#include <MemoryX/libraries/memorytypes/segment/ObjectClassMemorySegment.h>
#include <MemoryX/libraries/memorytypes/segment/RelationsMemorySegment.h>
#include <MemoryX/libraries/memorytypes/segment/AgentInstancesSegment.h>
#include <MemoryX/libraries/memorytypes/segment/WorldStateSegment.h>
#include <MemoryX/libraries/memorytypes/segment/ActiveOacMemorySegment.h>
#include <MemoryX/libraries/memorytypes/segment/AffordanceSegment.h>
#include <MemoryX/libraries/memorytypes/segment/EnvironmentalPrimitiveSegment.h>

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/libraries/updater/ObjectLocalization/MemoryXUpdaterObjectFactories.h>


#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/libraries/workingmemory/fusion/AttributeReplacementFusion.h>
#include <MemoryX/components/WorkingMemory/PriorAttributeEnrichmentFusion.h>
#include <MemoryX/components/WorkingMemory/KalmanFilterFusion.h>
#include <MemoryX/libraries/updater/ObjectLocalization/ObjectLocalizationMemoryUpdater.h>

namespace memoryx
{
    const std::string OBJ_INSTANCES_SEGMENT_NAME = "objectInstances";
    const std::string OBJ_CLASSES_SEGMENT_NAME = "objectClasses";
    const std::string OBJ_RELATIONS_SEGMENT_NAME = "objectRelations";
    const std::string ACTIVE_OAC_SEGMENT_NAME = "activeOac";
    const std::string AGENT_INSTANCES_SEGMENT_NAME = "agentInstances";
    const std::string WORLD_STATE_SEGMENT_NAME = "worldState";
    const std::string AFFORDANCE_SEGMENT_NAME = "affordances";
    const std::string ENVIRONMENTAL_PRIMITIVE_SEGMENT_NAME = "environmentalPrimitives";
    const std::string DMP_SEGMENT_NAME = "dmpEntity";

    const std::string OBJ_LOCALIZATION_UPDATER_NAME = "objectLocalization";

    void WorkingMemory::onInitWorkingMemory()
    {
    }

    void WorkingMemory::onConnectWorkingMemory()
    {
        if (!usePriorMemory)
        {
            ARMARX_FATAL << "WorkingMemory will not work when started UsePriorMemory=no";
            return;
        }

        float matchThreshold = getProperty<float>("MatchThreshold").getValue();
        bool matchByClassName = getProperty<bool>("MatchByClassName").getValue();

        // create object instances segment
        ObjectInstanceMemorySegmentPtr objectInstancesSegment = new ObjectInstanceMemorySegment(matchThreshold, matchByClassName);
        //    objectInstancesSegment->addFusionMethod(new MissingAttributeFusion());
        //    objectInstancesSegment->addFusionMethod(new AttributeReplacementFusion());
        objectInstancesSegment->addFusionMethod(new MotionModelRestoreFusion());

        bool useKalmanFilter = getProperty<bool>("UseKalmanFilter").getValue();

        if (useKalmanFilter)
        {
            objectInstancesSegment->addFusionMethod(new KalmanFilterFusion());
        }
        objectInstancesSegment->addFusionMethod(new PriorAttributeEnrichmentFusion(getPriorKnowledge()));


        // create object instances segment
        addSegment(OBJ_INSTANCES_SEGMENT_NAME, objectInstancesSegment);

        // create object classes segment
        ObjectClassMemorySegmentPtr objectClassesSegment = new ObjectClassMemorySegment(getPriorKnowledge());
        addSegment(OBJ_CLASSES_SEGMENT_NAME, objectClassesSegment);

        // create object relations segment
        addSegment(OBJ_RELATIONS_SEGMENT_NAME, new RelationMemorySegment());

        // Create agent entities segment.
        addSegment(AGENT_INSTANCES_SEGMENT_NAME, new AgentInstancesSegment(getIceManager()));

        // Create world state segment.
        addSegment(WORLD_STATE_SEGMENT_NAME, new WorldStateSegment());

        // Create affordance segment
        addSegment(AFFORDANCE_SEGMENT_NAME, new AffordanceSegment());

        // Create Environmental Primitive segment
        addSegment(ENVIRONMENTAL_PRIMITIVE_SEGMENT_NAME, new EnvironmentalPrimitiveSegment());

        if (useLongtermMemory)
        {
            // create active oac segment
            addSegment(ACTIVE_OAC_SEGMENT_NAME, new ActiveOacMemorySegment(getObjectInstancesSegment(), getOacSegment()));
        }

        // create updater required for object localization
        if (locUpdater)
        {
            locUpdater->setSegmentNames(OBJ_CLASSES_SEGMENT_NAME, OBJ_INSTANCES_SEGMENT_NAME);
            registerUpdater(OBJ_LOCALIZATION_UPDATER_NAME, locUpdater);
        }
        else
        {
            ARMARX_WARNING << "No Location Updater set!";
        }
    }

    void WorkingMemory::setUpdater(ObjectLocalizationMemoryUpdaterPtr updater)
    {
        this->locUpdater = updater;
    }

    ObjectInstanceMemorySegmentBasePrx WorkingMemory::getObjectInstancesSegment(
        const ::Ice::Current&)
    {
        return ObjectInstanceMemorySegmentBasePrx::uncheckedCast(getSegment(OBJ_INSTANCES_SEGMENT_NAME));
    }

    ObjectClassMemorySegmentBasePrx WorkingMemory::getObjectClassesSegment(
        const ::Ice::Current&)
    {
        return ObjectClassMemorySegmentBasePrx::uncheckedCast(getSegment(OBJ_CLASSES_SEGMENT_NAME));
    }

    RelationMemorySegmentBasePrx WorkingMemory::getRelationsSegment(
        const ::Ice::Current&)
    {
        return RelationMemorySegmentBasePrx::uncheckedCast(getSegment(OBJ_RELATIONS_SEGMENT_NAME));
    }

    ActiveOacMemorySegmentBasePrx WorkingMemory::getActiveOacSegment(
        const ::Ice::Current&)
    {
        return ActiveOacMemorySegmentBasePrx::uncheckedCast(getSegment(ACTIVE_OAC_SEGMENT_NAME));
    }

    AgentInstancesSegmentBasePrx WorkingMemory::getAgentInstancesSegment(const Ice::Current&)
    {
        return AgentInstancesSegmentBasePrx::uncheckedCast(getSegment(AGENT_INSTANCES_SEGMENT_NAME));
    }

    WorldStateSegmentBasePrx WorkingMemory::getWorldStateSegment(const Ice::Current&)
    {
        return WorldStateSegmentBasePrx::uncheckedCast(getSegment(WORLD_STATE_SEGMENT_NAME));
    }

    AffordanceSegmentBasePrx WorkingMemory::getAffordanceSegment(const Ice::Current&)
    {
        return AffordanceSegmentBasePrx::uncheckedCast(getSegment(AFFORDANCE_SEGMENT_NAME));
    }

    EnvironmentalPrimitiveSegmentBasePrx WorkingMemory::getEnvironmentalPrimitiveSegment(const Ice::Current&)
    {
        return EnvironmentalPrimitiveSegmentBasePrx::uncheckedCast(getSegment(ENVIRONMENTAL_PRIMITIVE_SEGMENT_NAME));
    }

    std::string WorkingMemory::getMemoryName(const Ice::Current&) const
    {
        return getName();
    }

    ObjectLocalizationMemoryUpdaterBasePrx WorkingMemory::getObjectLocalizationUpdater(
        const ::Ice::Current&)
    {
        return ObjectLocalizationMemoryUpdaterBasePrx::uncheckedCast(getUpdater(OBJ_LOCALIZATION_UPDATER_NAME));
    }

    CommonStorageInterfacePrx WorkingMemory::getCommonStorage(const ::Ice::Current&) const
    {
        return dataBasePrx;
    }

    PriorKnowledgeInterfacePrx WorkingMemory::getPriorKnowledge(const ::Ice::Current&) const
    {
        return priorKnowledgePrx;
    }

    OacMemorySegmentBasePrx WorkingMemory::getOacSegment(const ::Ice::Current&)
    {
        return longtermMemoryPrx->getOacSegment();
    }
}
