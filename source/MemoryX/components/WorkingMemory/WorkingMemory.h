/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::ShortTermMemory
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

#include <MemoryX/libraries/workingmemory/AbstractWorkingMemory.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>


namespace memoryx
{
    class ObjectLocalizationMemoryUpdater;
    using ObjectLocalizationMemoryUpdaterPtr = IceInternal::Handle<ObjectLocalizationMemoryUpdater>;


    class WorkingMemoryPropertyDefinitions :
        public AbstractWorkingMemoryPropertyDefinitions
    {
    public:
        WorkingMemoryPropertyDefinitions(std::string prefix) :
            AbstractWorkingMemoryPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("UseKalmanFilter", true, "Switch fusion with Kalman Filter on/off.");

            defineOptionalProperty<float>("MatchThreshold", 0., "Threshold value of spatial proximity between observations \nto consider them belonging to the same instance (0..1)")
            .setMin(0).setMax(1);

            defineOptionalProperty<bool>("MatchByClassName", true, "Check for class equality when searching for corresponding instance");
        }
    };


    /*!
     * \brief The WorkingMemory class provides methods for storing runtime information. The data is stored in segments.
     *
     * On startup the working memory gets proxies of LongtermMemory, PriorKnowledge, CommonStorage
     * and creates a topic to publish updates
     * (established connections depend on the configuration, \see AbstractWorkingMemory).
     *
     * By default the WorkingMemory holds a segment to store object instances (e.g. recognized objects).
     * This segment is initialized with a fusion method which appends attributes from prior knowledge.
     * This means every time an entity is added, attribute enrichment (e.g. 3d model loading etc) is performed.
     * \see PriorAttributeEnrichmentFusion
     *
     * See \ref workingmemory "memoryx::WorkingMemory"    - abstract working memory, that gets updated "in the loop"
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT WorkingMemory :
        virtual public AbstractWorkingMemory,
        virtual public WorkingMemoryInterface
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "WorkingMemory";
        }
        void onInitWorkingMemory() override;
        void onConnectWorkingMemory() override;

        void setUpdater(ObjectLocalizationMemoryUpdaterPtr locUpdater);
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new WorkingMemoryPropertyDefinitions(
                           getConfigIdentifier()));
        }

        /*!
         * \brief getObjectInstancesSegment Get the segment where all object instances are stored.
         *\see ObjectInstanceMemorySegment
         * \return The segment.
         */
        ObjectInstanceMemorySegmentBasePrx getObjectInstancesSegment(const ::Ice::Current& = Ice::emptyCurrent) override;

        /*!
         * \brief getObjectClassesSegment Get the segment where all used object classes are stored.
         *\see ObjectClassMemorySegment
         * \return The segment.
         */
        ObjectClassMemorySegmentBasePrx getObjectClassesSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        RelationMemorySegmentBasePrx getRelationsSegment(const ::Ice::Current& = Ice::emptyCurrent) override;

        ObjectLocalizationMemoryUpdaterBasePrx getObjectLocalizationUpdater(const ::Ice::Current& = Ice::emptyCurrent) override;

        CommonStorageInterfacePrx getCommonStorage(const ::Ice::Current& = Ice::emptyCurrent) const override;
        PriorKnowledgeInterfacePrx getPriorKnowledge(const ::Ice::Current& = Ice::emptyCurrent) const override;
        ActiveOacMemorySegmentBasePrx getActiveOacSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        AgentInstancesSegmentBasePrx getAgentInstancesSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        WorldStateSegmentBasePrx getWorldStateSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        virtual OacMemorySegmentBasePrx getOacSegment(const ::Ice::Current& = Ice::emptyCurrent);
        AffordanceSegmentBasePrx getAffordanceSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        EnvironmentalPrimitiveSegmentBasePrx getEnvironmentalPrimitiveSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getMemoryName(const Ice::Current&) const override;
    private:
        ObjectLocalizationMemoryUpdaterPtr locUpdater;

    };

    using WorkingMemoryPtr = IceInternal::Handle<WorkingMemory>;

}

