/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2012 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>

#include <ArmarXCore/core/system/ImportExport.h>

#include <MemoryX/libraries/workingmemory/fusion/EntityFusionMethod.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>


namespace memoryx
{
    /**
     * @class PriorAttributeEnrichmentFusion
     * @ingroup WorkingMemory
     */
    class ARMARXCORE_IMPORT_EXPORT PriorAttributeEnrichmentFusion :
        virtual public EntityFusionMethod
    {
    public:

        /// Creates a new PriorAttributeEnrichmentFusion
        PriorAttributeEnrichmentFusion(const PriorKnowledgeInterfacePrx& priorKnowledgePrx, bool forceOverwrite = false);

        void initEntityInPlace(const EntityBasePtr& updateEntity);

        /**
        * This method should be called each time a new entity (currently: object instance) is created and
        * performs attribute enrichment for it. Concretely, it complements the entity with attributes of the corresponding
        * class stored in PriorKnowledge. If entity class is not specified or absent in PriorKnoledge, no action will be taken.
        * If the same attribute exists both in the entity and in its class definition, value from entity will be used.
        *
        * @param updateEntity entity
        * @return entity enriched with prior attributes
        */
        EntityBasePtr initEntity(const EntityBasePtr& updateEntity, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * This method should be called each time an existing entity gets updated. It checks whether entity class has changed
         * and if this is a case, adds/removes prior attributes accordingly.
         *
         * @param oldEntity entity before update
         * @param newEntity entity after update
         *
         * @return entity enriched with prior attributes
         */
        EntityBasePtr fuseEntity(const EntityBasePtr& oldEntity, const EntityBasePtr& newEntity, const ::Ice::Current&) override;


    private:

        void addPriorAttrs(const EntityBasePtr& entity) const;
        void copyMissingAttrs(const EntityBasePtr& srcEntity, const EntityBasePtr& destEntity) const;

        PriorKnowledgeInterfacePrx priorKnowledgePrx;
        bool forceOverwrite;

    };
}

