/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke at kit dot edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>

#include <ArmarXCore/core/system/ImportExport.h>
#include <MemoryX/libraries/workingmemory/fusion/EntityFusionMethod.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>




namespace memoryx
{
    /**
     * @class KalmanFilterFusion
     * @ingroup WorkingMemory
     */
    class ARMARXCORE_IMPORT_EXPORT KalmanFilterFusion :
        virtual public EntityFusionMethod
    {
    public:

        /// Creates a new KalmanFilterFusion
        KalmanFilterFusion();


        EntityBasePtr initEntity(const EntityBasePtr& updateEntity, const ::Ice::Current& = Ice::emptyCurrent) override;

        EntityBasePtr fuseEntity(const EntityBasePtr& oldEntity, const EntityBasePtr& newEntity, const ::Ice::Current& = Ice::emptyCurrent) override;

    };
}

