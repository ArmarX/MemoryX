/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include "WorkingMemory.h"
#include <MemoryX/libraries/updater/ObjectLocalization/ObjectLocalizationMemoryUpdater.h>
#include <MemoryX/core/MemoryXApplication.h>

namespace armarx
{
    class WorkingMemoryApp :
        virtual public memoryx::MemoryXApplication
    {
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            memoryx::WorkingMemoryPtr wm = Component::create<memoryx::WorkingMemory>(properties, "WorkingMemory", "MemoryX");
            wm->setUpdater(Component::create<memoryx::ObjectLocalizationMemoryUpdater>(properties, "ObjectLocalizationMemoryUpdater", "MemoryX"));
            registry->addObject(wm);
        }

    };
}
