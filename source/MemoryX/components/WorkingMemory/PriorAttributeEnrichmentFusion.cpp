/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2012 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PriorAttributeEnrichmentFusion.h"

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace memoryx
{

    PriorAttributeEnrichmentFusion::PriorAttributeEnrichmentFusion(const PriorKnowledgeInterfacePrx& priorKnowledgePrx, bool forceOverwrite) :
        EntityFusionMethod("PriorAttributeEnrichmentFusion"), priorKnowledgePrx(priorKnowledgePrx), forceOverwrite(forceOverwrite)
    {
    }

    void PriorAttributeEnrichmentFusion::initEntityInPlace(const EntityBasePtr& updateEntity)
    {
        if (!priorKnowledgePrx)
        {
            return;
        }

        addPriorAttrs(updateEntity);
    }

    EntityBasePtr PriorAttributeEnrichmentFusion::initEntity(const EntityBasePtr& updateEntity, const Ice::Current&)
    {
        if (!priorKnowledgePrx)
        {
            return updateEntity;
        }

        EntityBasePtr fusedEntity = EntityBasePtr::dynamicCast(updateEntity->ice_clone());
        addPriorAttrs(fusedEntity);

        return fusedEntity;
    }

    EntityBasePtr PriorAttributeEnrichmentFusion::fuseEntity(const EntityBasePtr& oldEntity, const EntityBasePtr& newEntity, const Ice::Current&)
    {
        ARMARX_DEBUG_S << "PriorAttributeEnrichmentFusion::fuseEntity() called";

        copyMissingAttrs(oldEntity, newEntity);
        addPriorAttrs(newEntity);

        return newEntity;
    }

    void PriorAttributeEnrichmentFusion::addPriorAttrs(const EntityBasePtr& entity) const
    {
        ObjectInstancePtr inst = ObjectInstancePtr::dynamicCast(entity);

        if (inst)
        {
            std::string classname = inst->getMostProbableClass();
            //ARMARX_INFO_S << entity->getName() << " has most probable class: " << classname;
            if (!classname.empty())
            {
                EntityBasePtr priorEntity = priorKnowledgePrx->getObjectClassesSegment()->getEntityByName(classname);

                if (priorEntity)
                {
                    copyMissingAttrs(priorEntity, entity);
                }
            }
            else
            {
                ARMARX_VERBOSE << "No classname set - could not enrich entity";
            }
        }
    }

    void PriorAttributeEnrichmentFusion::copyMissingAttrs(const EntityBasePtr& srcEntity, const EntityBasePtr& destEntity) const
    {
        NameList attrNames = srcEntity->getAttributeNames();

        for (NameList::const_iterator it = attrNames.begin(); it != attrNames.end(); ++it)
            if (forceOverwrite || !destEntity->hasAttribute(*it))
            {
                ARMARX_VERBOSE << "Adding attribute: " << srcEntity->getAttribute(*it)->getName();
                destEntity->putAttribute(srcEntity->getAttribute(*it));
            }
    }

}

