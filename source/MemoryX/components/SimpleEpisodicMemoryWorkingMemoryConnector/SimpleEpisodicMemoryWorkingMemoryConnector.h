/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::SimpleEpisodicMemoryWorkingMemoryConnector
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <vector>
#include <map>

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/components/SimpleEpisodicMemory/SimpleEpisodicMemoryConnector.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT SimpleEpisodicMemoryWorkingMemoryConnector :
        virtual public armarx::Component,
        virtual public WorkingMemoryListenerInterface,
        virtual public SimpleEpisodicMemoryConnector
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        SimpleEpisodicMemoryWorkingMemoryConnector();

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void reportEntityCreated(const std::string&, const EntityBasePtr&, const Ice::Current&) override;
        void reportEntityUpdated(const std::string&, const EntityBasePtr&, const EntityBasePtr&, const Ice::Current&) override;
        void reportEntityRemoved(const std::string&, const EntityBasePtr&, const Ice::Current&) override;
        void reportSnapshotLoaded(const std::string&, const Ice::Current&) override;
        void reportSnapshotCompletelyLoaded(const Ice::Current&) override;
        void reportMemoryCleared(const std::string&, const Ice::Current&) override;

    private:
        WorkingMemoryInterfacePrx memoryPrx;

        float minOrientationDistance = 0.2;
        float minPositionDistance = 100;

        std::map<std::string, std::map<int, memoryx::EntityBasePtr>> entityMap;
    };
}
