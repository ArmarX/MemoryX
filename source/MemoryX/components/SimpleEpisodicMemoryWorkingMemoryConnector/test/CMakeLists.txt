
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore SimpleEpisodicMemoryWorkingMemoryConnector)
 
armarx_add_test(SimpleEpisodicMemoryWorkingMemoryConnectorTest SimpleEpisodicMemoryWorkingMemoryConnectorTest.cpp "${LIBS}")
