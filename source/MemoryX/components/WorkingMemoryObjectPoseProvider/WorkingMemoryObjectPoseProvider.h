/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::WorkingMemoryObjectPoseProvider
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/json/json.hpp>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/WorkingMemoryObjectPoseProviderInterface.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectInstanceToRobotNodeAttachments.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectClassSegmentWrapper.h>


namespace memoryx
{


    struct Config
    {
        std::map<std::string, std::string> objectNames;
    };

    void to_json(nlohmann::json& j, const Config& config);
    void from_json(const nlohmann::json& j, Config& config);




    /**
     * @defgroup Component-WorkingMemoryObjectPoseProvider WorkingMemoryObjectPoseProvider
     * @ingroup MemoryX-Components
     * A description of the component WorkingMemoryObjectPoseProvider.
     *
     * @class WorkingMemoryObjectPoseProvider
     * @ingroup Component-WorkingMemoryObjectPoseProvider
     * @brief Brief description of class WorkingMemoryObjectPoseProvider.
     *
     * Detailed description of class WorkingMemoryObjectPoseProvider.
     */
    class WorkingMemoryObjectPoseProvider :
        virtual public armarx::Component
        , virtual public memoryx::WorkingMemoryObjectPoseProviderInterface
        , virtual public armarx::ObjectPoseProviderPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // ObjectPoseProvider interface
        armarx::objpose::ProviderInfo getProviderInfo(const Ice::Current& = Ice::emptyCurrent) override;

        // ObjectToRobotNodeAttachmentInterface interface
        void attachObjectToRobotNode(const AttachObjectToRobotNodeInput& attachment, const Ice::Current&) override;
        void detachObjectFromRobotNode(const DetachObjectFromRobotNodeInput& detachment, const Ice::Current&) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        void provideObjectInstancesPoses();
        void provideObjectInstancesPoses(const std::vector<ObjectInstancePtr>& objectInstances);

        armarx::objpose::ProvidedObjectPose toProvidedObjectPose(const ObjectInstancePtr& instance);


    private:

        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;

        ObjectClassSegmentWrapper objectClassSegment;


        float updateFrequency = 50;
        armarx::SimpleRunningTask<>::pointer_type task;

        Config config;
        std::string loadObjectDatasetsStr = "";
        std::string configFile = "MemoryX/WorkingMemoryObjectPoseProvider/config.json";

        std::mutex mutex;
        ObjectInstanceToRobotNodeAttachments attachments;

    };
}
