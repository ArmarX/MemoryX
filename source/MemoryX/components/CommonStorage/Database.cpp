/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <IceUtil/UUID.h>

#include "Database.h"

using namespace memoryx;

Database::Database(const CommonStorageInterfacePtr& dbConn, const std::string& dbName) :
    dbConn(dbConn),
    dbName(dbName)
{
    iceId.name = IceUtil::generateUUID();
}

Database::~Database()
    = default;

CollectionInterfacePrx Database::requestCollection(const std::string& collName, const ::Ice::Current& c)
{
    std::unique_lock lock(dbConnectionMutex);
    return dbConn->requestCollection(getCollectionNS(collName), c);
}

void Database::releaseCollection(const ::memoryx::CollectionInterfacePrx& coll, const ::Ice::Current& c)
{
    std::unique_lock lock(dbConnectionMutex);
    return dbConn->releaseCollection(coll, c);
}

void Database::dropCollection(const ::std::string& collName, const ::Ice::Current& c)
{
    std::unique_lock lock(dbConnectionMutex);
    dbConn->dropCollection(getCollectionNS(collName), c);
}

std::string Database::getName(const ::Ice::Current&)
{
    return dbName;
}

Ice::Identity Database::getIceId() const
{
    return iceId;
}

std::string Database::getCollectionNS(std::string collName)
{
    return dbName + "." + collName;
}
