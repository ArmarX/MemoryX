/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "GridFileWrapper.h"
#include <IceUtil/UUID.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/Exception.h>

#include <mongo/client/dbclient.h>

using namespace memoryx;

GridFileWrapper::GridFileWrapper(const mongo::GridFile& gridFile, const std::shared_ptr<std::mutex>& accessGridFSFilesMutex) :
    gridFile(new mongo::GridFile(gridFile)),
    nextChunkNo(0),
    accessGridFSFilesMutex(accessGridFSFilesMutex)
{
    iceId.name = IceUtil::generateUUID();
}

GridFileWrapper::~GridFileWrapper()
    = default;

std::string GridFileWrapper::getId(const Ice::Current&)
{
    return gridFile->getFileField("_id").OID().toString();
}

std::string GridFileWrapper::getFilename(const Ice::Current&)
{
    return gridFile->getFilename();
}

std::string GridFileWrapper::getMD5(const Ice::Current&)
{
    return gridFile->getMD5();
}

Ice::Long GridFileWrapper::getFileSize(const Ice::Current&)
{
    return gridFile->getContentLength();
}

Ice::Long GridFileWrapper::getUploadDate(const Ice::Current&)
{
    return gridFile->getUploadDate();
}

Ice::Int GridFileWrapper::getNextChunk(memoryx::Blob& buffer,
                                       const Ice::Current&)
{
    ARMARX_TRACE;
    // this mutex was needed to avoid crashes when multiple clients access GridFS
    std::unique_lock l(*accessGridFSFilesMutex);
    try
    {

        if (nextChunkNo < gridFile->getNumChunks())
        {
            mongo::GridFSChunk chunk = gridFile->getChunk(nextChunkNo++);
            int datalen = 0;
            const char* data = chunk.data(datalen);
            buffer.resize(datalen);
            memcpy(buffer.data(), data, datalen);
            return datalen;
        }
        else
        {
            return 0;
        }
    }
    catch (...)
    {
        ARMARX_ERROR << VAROUT(nextChunkNo) << "\n"
                     << VAROUT(gridFile) << "\n"
                     << VAROUT(gridFile->getFilename()) << "\n"
                     << VAROUT(gridFile->exists()) << "\n"
                     << VAROUT(gridFile->getChunkSize()) << "\n"
                     << VAROUT(gridFile->getNumChunks()) << "\n"
                     << VAROUT(gridFile->getContentLength()) << "\n"
                     << VAROUT(gridFile->getContentType()) << "\n"
                     << VAROUT(gridFile->getMD5()) << "\n"
                     << VAROUT(gridFile->getMetadata().jsonString()) << "\n"
                     << armarx::GetHandledExceptionString();

        throw;
    }
}

Ice::Identity GridFileWrapper::getIceId() const
{
    return iceId;
}

