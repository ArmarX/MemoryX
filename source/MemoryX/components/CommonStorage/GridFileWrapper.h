/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/GridFileInterface.h>

#include <memory>
#include <mutex>

namespace mongo
{
    class GridFile;
}

namespace memoryx
{
    using GridFilePtr = std::shared_ptr<mongo::GridFile>;
    class GridFileWrapper :
        public memoryx::GridFileInterface
    {
    public:
        GridFileWrapper(const mongo::GridFile& gridFile, const std::shared_ptr<std::mutex>& accessGridFSFilesMutex);
        ~GridFileWrapper() override;

        // implement methods of GridFileInterface
        ::std::string getId(const ::Ice::Current& = Ice::emptyCurrent) override;

        ::std::string getFilename(const ::Ice::Current& = Ice::emptyCurrent) override;

        ::std::string getMD5(const ::Ice::Current& = Ice::emptyCurrent) override;

        ::Ice::Long getFileSize(const ::Ice::Current& = Ice::emptyCurrent) override;

        ::Ice::Long getUploadDate(const ::Ice::Current& = Ice::emptyCurrent) override;

        ::Ice::Int getNextChunk(::memoryx::Blob& buffer, const ::Ice::Current& = Ice::emptyCurrent) override;

        // helper methods
        Ice::Identity getIceId() const;

    private:
        GridFilePtr gridFile;
        int nextChunkNo;

        Ice::Identity iceId;
        std::shared_ptr<std::mutex> accessGridFSFilesMutex;
    };

    using GridFileWrapperPtr = IceInternal::Handle<GridFileWrapper>;

}
