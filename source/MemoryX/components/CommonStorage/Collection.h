/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "CommonStorage.h"

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>

namespace memoryx
{

    class ARMARXCOMPONENT_IMPORT_EXPORT Collection :
        virtual public CollectionInterface
    {
    public:
        Collection(const CommonStoragePtr& dbConn, const std::string& collNS);

        // inherited from CollectionInterface
        Ice::Int count(const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getNS(const ::Ice::Current& = Ice::emptyCurrent) override;

        DBStorableData findByMongoId(const std::string& id, const ::Ice::Current& c) override;
        DBStorableDataList findByFieldValue(const std::string& fieldName, const ::std::string& fieldValue, const ::Ice::Current& = Ice::emptyCurrent) override;
        DBStorableData findOneByFieldValue(const std::string& fieldName, const ::std::string& fieldValue, const ::Ice::Current& = Ice::emptyCurrent) override;
        DBStorableDataList findByFieldValueList(const std::string& fieldName, const NameList& fieldValueList, const ::Ice::Current& = Ice::emptyCurrent) override;
        DBStorableDataList findByQuery(const std::string& query, const ::Ice::Current& c) override;
        DBStorableDataList findByConstraintQuery(const std::string& query, const std::string& where, const Ice::Current&) override;
        DBStorableData findOneByQuery(const std::string& query, const ::Ice::Current& c) override;
        DBStorableDataList findAll(const ::Ice::Current& = Ice::emptyCurrent) override;
        DBStorableData findAllUniqueByFieldName(const std::string& fieldName, const ::Ice::Current& = Ice::emptyCurrent) override;
        EntityIdList findAllIds(const ::Ice::Current& = Ice::emptyCurrent) override;
        NameList findAllFieldValues(const std::string& fieldName, const ::Ice::Current& = Ice::emptyCurrent) override;

        std::string insert(const DBStorableData& obj, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::vector<std::string> insertList(const DBStorableDataList& objectList, const Ice::Current& = Ice::emptyCurrent) override;
        bool update(const DBStorableData& obj, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool updateByQuery(const std::string& query, const DBStorableData& obj, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool updateWithUserKey(const DBStorableData& obj, const std::string& keyField, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string save(const DBStorableData& obj, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool saveWithUserKey(const DBStorableData& obj, const std::string& keyField, const ::Ice::Current& = Ice::emptyCurrent) override;

        bool removeByMongoId(const std::string& id, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool removeByFieldValue(const std::string& fieldName, const std::string& fieldValue, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool removeByQuery(const std::string& query, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool clear(const ::Ice::Current& c) override;

        bool ensureIndex(const std::string& fieldName, bool unique, const ::Ice::Current& = Ice::emptyCurrent) override;

        Ice::Identity getIceId() const;

    private:
        CommonStoragePtr dbConn;
        std::mutex dbConnectionMutex;
        std::string ns;

        Ice::Identity iceId;
    };

    using CollectionPtr = IceUtil::Handle<Collection>;
}

