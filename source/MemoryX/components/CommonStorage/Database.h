/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once

#include <MemoryX/interface/components/CommonStorageInterface.h>

#include <mutex>

namespace memoryx
{
    /*!
     * \brief The Database class provides an interface to a database.
     */
    class Database :
        virtual public DatabaseInterface
    {
    public:
        Database(const CommonStorageInterfacePtr& dbConn, const std::string& dbName);
        ~Database() override;

        CollectionInterfacePrx requestCollection(const ::std::string& collName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void releaseCollection(const ::memoryx::CollectionInterfacePrx& coll, const ::Ice::Current& = Ice::emptyCurrent) override;
        void dropCollection(const ::std::string& collName, const ::Ice::Current& = Ice::emptyCurrent) override;

        std::string getName(const ::Ice::Current& = Ice::emptyCurrent) override;

        Ice::Identity getIceId() const;

    private:
        CommonStorageInterfacePtr dbConn;
        std::mutex dbConnectionMutex;
        std::string dbName;

        Ice::Identity iceId;

        std::string getCollectionNS(std::string collName);
    };

    using DatabasePtr = IceUtil::Handle<Database>;

}

