/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SnapshotControlWidgetController
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SnapshotControlWidgetController.h"

#include <SimoxUtility/algorithm/string/string_tools.h>


namespace armarx
{
    SnapshotControlWidgetController::SnapshotControlWidgetController() :
        LONGTERM_SNAPSHOT_PREFIX("Snapshot_")
    {
        widget.setupUi(getWidget());
        connectSlots();
        getWidget()->setEnabled(false);
    }


    SnapshotControlWidgetController::~SnapshotControlWidgetController()
    {

    }


    void SnapshotControlWidgetController::loadSettings(QSettings* settings)
    {

    }


    void SnapshotControlWidgetController::saveSettings(QSettings* settings)
    {

    }


    void SnapshotControlWidgetController::onInitComponent()
    {
        usingProxy("WorkingMemory");
        usingProxy("LongtermMemory");
    }


    void SnapshotControlWidgetController::onConnectComponent()
    {
        workingMemoryPrx = getProxy<memoryx::WorkingMemoryInterfacePrx>("WorkingMemory");
        longtermMemoryPrx = getProxy<memoryx::LongtermMemoryInterfacePrx>("LongtermMemory");
        enableMainWidgetAsync(true);
        loadSnapshotNames();
    }


    void SnapshotControlWidgetController::onDisconnectComponent()
    {
        enableMainWidgetAsync(false);
    }


    void SnapshotControlWidgetController::connectSlots()
    {
        connect(widget.loadSnapshotButton, SIGNAL(clicked()), this, SLOT(loadSnapshot()));
        connect(widget.removeSnapshotButton, SIGNAL(clicked()), this, SLOT(removeSnapshot()));
        connect(widget.saveSnapshotButton, SIGNAL(clicked()), this, SLOT(saveSnapshot()));
        connect(widget.snapshotName, SIGNAL(textChanged(QString)), this, SLOT(snapshotNameChanged(QString)));
        widget.loadSnapshotButton->setEnabled(false);
        widget.removeSnapshotButton->setEnabled(false);
        widget.saveSnapshotButton->setEnabled(false);
    }


    void SnapshotControlWidgetController::snapshotNameChanged(const QString& snapshotName)
    {
        widget.saveSnapshotButton->setEnabled(!snapshotName.isEmpty());
    }


    void SnapshotControlWidgetController::loadSnapshotNames()
    {
        NameList snapshotNames = longtermMemoryPrx->getSnapshotNames();
        widget.availableSnapshotsList->clear();

        for (auto snapshotName : snapshotNames)
        {
            widget.availableSnapshotsList->addItem(QString::fromStdString(snapshotName));
        }

        if (!snapshotNames.empty())
        {
            widget.loadSnapshotButton->setEnabled(true);
            widget.removeSnapshotButton->setEnabled(true);
        }
    }



    void SnapshotControlWidgetController::loadSnapshot()
    {
        const std::string snapshotName = widget.availableSnapshotsList->currentText().toStdString();

        if (snapshotName.empty())
        {
            return;
        }

        try
        {
            ARMARX_INFO << "Loading Snapshot with Name: " << snapshotName;
            longtermMemoryPrx->loadWorkingMemorySnapshot(snapshotName, workingMemoryPrx);
            ARMARX_INFO << "Snapshot succcessfully loaded: " << snapshotName;
        }
        catch (const armarx::LocalException& e)
        {
            ARMARX_ERROR << "SnapshotName could not be loaded: " << snapshotName;
        }

        loadSnapshotNames();
    }


    void SnapshotControlWidgetController::removeSnapshot()
    {
        const std::string snapshotName = widget.availableSnapshotsList->currentText().toStdString();

        if (snapshotName.empty())
        {
            return;
        }

        try
        {
            ARMARX_INFO << "Removing Snapshot with Name: " << snapshotName;
            longtermMemoryPrx->removeWorkingMemorySnapshot(snapshotName);
            ARMARX_INFO << "Snapshot succcessfully removed: " << snapshotName;
        }
        catch (const armarx::LocalException& e)
        {
            ARMARX_ERROR << "SnapshotName could not be removed: " << snapshotName;
        }

        loadSnapshotNames();
    }


    void SnapshotControlWidgetController::saveSnapshot()
    {
        std::string snapshotName = widget.snapshotName->text().toStdString();

        if (snapshotName.empty())
        {
            return;
        }

        if (!simox::alg::starts_with(snapshotName, LONGTERM_SNAPSHOT_PREFIX))
        {
            snapshotName = std::string(LONGTERM_SNAPSHOT_PREFIX) + snapshotName;
            ARMARX_WARNING << "Snapshot name must start with <" << LONGTERM_SNAPSHOT_PREFIX << ">. Changing to " << snapshotName;
        }

        ARMARX_LOG << "Saving snapshot: " << snapshotName;
        longtermMemoryPrx->saveWorkingMemorySnapshot(snapshotName, workingMemoryPrx);
        loadSnapshotNames();
    }
}
