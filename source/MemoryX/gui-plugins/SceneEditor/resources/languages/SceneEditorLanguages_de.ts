<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>GroupExplorerDialog</name>
    <message>
        <source>Group Editor</source>
        <translation type="finished">Gruppeneditor</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="finished">Gruppenname</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="finished">Ändern</translation>
    </message>
    <message>
        <source>Object Properties</source>
        <translation type="finished">Objekteigenschaften</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation type="finished">Gruppe löschen</translation>
    </message>
    <message>
        <source>Add Group</source>
        <translation type="finished">Gruppe hinzufügen</translation>
    </message>
    <message>
        <source>Delete Object</source>
        <translation type="finished">Objekt löschen</translation>
    </message>
</context>
<context>
    <name>GroupExplorerWidget</name>
    <message>
        <source>Form</source>
        <translation type="finished">Formular</translation>
    </message>
    <message>
        <source>Add selection to new group</source>
        <translation type="finished">Selektion zu einer neuen Gruppe hinzufügen</translation>
    </message>
</context>
<context>
    <name>LoadSnapshotDialog</name>
    <message>
        <source>Load Snapshot</source>
        <translation type="finished">Schnappschuss laden</translation>
    </message>
    <message>
        <source>Select Snapshot</source>
        <translation type="finished">Schnappschuss auswählen</translation>
    </message>
</context>
<context>
    <name>SceneEditorConfigDialog</name>
    <message>
        <source>Initialization</source>
        <translation type="finished">Initialisierung</translation>
    </message>
    <message>
        <source>Select Working Memory</source>
        <translation type="finished">Working-Memory auswählen</translation>
    </message>
    <message>
        <source>Object Instances Segment Name</source>
        <translation type="finished">Objektinstanzsegmentname</translation>
    </message>
    <message>
        <source>Working Memory Name</source>
        <translation type="finished">Name des Working-Memory</translation>
    </message>
    <message>
        <source>Working Memory Updates Topic</source>
        <translation type="finished">Working-Memory-Updates</translation>
    </message>
    <message>
        <source>Select PriorKnowledge</source>
        <translation type="finished">Prior-Knowledge auswählen</translation>
    </message>
    <message>
        <source>PriorMemory Name</source>
        <translation type="finished">Name des Prior-Memory</translation>
    </message>
    <message>
        <source>Select Settings File</source>
        <translation type="finished">Konfigurationsdatei auswählen</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="finished">...</translation>
    </message>
    <message>
        <source>3D-Modelling-Tool</source>
        <translation type="finished">3D Modellierungs-Werkzeug</translation>
    </message>
    <message>
        <source>for humanoid Roboter Scenes</source>
        <translation type="finished">für humanoide Roboter-Szenen</translation>
    </message>
</context>
<context>
    <name>ObjectExplorerWidget</name>
    <message>
        <source>Form</source>
        <translation type="finished">Formular</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="finished">Filter</translation>
    </message>
</context>
<context>
    <name>ObjectInspectorWidget</name>
    <message>
        <source>Form</source>
        <translation type="finished">Formular</translation>
    </message>
</context>
<context>
    <name>SaveSnapshotDialog</name>
    <message>
        <source>Save Snapshot</source>
        <translation type="finished">Schnappschuss speichern</translation>
    </message>
    <message>
        <source>Select Snapshot</source>
        <translation type="finished">Schnappschuss auswählen</translation>
    </message>
    <message>
        <source>Save as new Snapshot</source>
        <translation type="finished">Als neuen Schnappschuss speichern</translation>
    </message>
    <message>
        <source>Replace existing Snapshot</source>
        <translation type="finished">Bestehenden Schnappschuss ersetzen</translation>
    </message>
    <message>
        <source>Select Objects</source>
        <translation type="finished">Objekte auswählen</translation>
    </message>
    <message>
        <source>Save all</source>
        <translation type="finished">Alle Speichern</translation>
    </message>
    <message>
        <source>Save Groups</source>
        <translation type="finished">Bestimmte Gruppen speichern</translation>
    </message>
</context>
<context>
    <name>ShortcutDialog</name>
    <message>
        <source>Keyboard Shortcuts Settings</source>
        <translation type="finished">Tastaturkurzbefehl-Einstellungen</translation>
    </message>
    <message>
        <source>Keyboard Shortcuts</source>
        <translation type="finished">Tastaturkurzbefehle</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="finished">Tastenkürzel</translation>
    </message>
    <message>
        <source>Key Sequence:</source>
        <translation type="finished">Tastenkombination:</translation>
    </message>
</context>
<context>
    <name>gui::GroupExplorerWidget</name>
    <message>
        <source>Add selection to group</source>
        <translation type="finished">Selektion zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <source>Add group to selection</source>
        <translation type="finished">Gruppe zur Selektion hinzufügen</translation>
    </message>
    <message>
        <source>Remove group</source>
        <translation type="finished">Gruppe auflösen</translation>
    </message>
    <message>
        <source>Show in Group Editor...</source>
        <translation type="finished">Im Gruppeneditor anzeigen...</translation>
    </message>
    <message>
        <source>New Group </source>
        <translation type="finished">Neue Gruppe </translation>
    </message>
</context>
<context>
    <name>gui::SceneEditorMainWindow</name>
    <message>
        <source>Object Inspector</source>
        <translation type="finished">Objekt-Inspektor</translation>
    </message>
    <message>
        <source>Object Explorer</source>
        <translation type="finished">Objekt-Explorer</translation>
    </message>
    <message>
        <source>Minimap</source>
        <translation type="finished">Minimap</translation>
    </message>
    <message>
        <source>Group Explorer</source>
        <translation type="finished">Gruppen-Explorer</translation>
    </message>
</context>
<context>
    <name>gui::SceneEditorMainWindowController</name>
    <message>
        <source>Load Snapshot</source>
        <translation type="finished">Schnappschuss laden</translation>
    </message>
    <message>
        <source>Save Snapshot</source>
        <translation type="finished">Schnappschuss speichern</translation>
    </message>
    <message>
        <source>Keyboard Shortcuts</source>
        <translation type="finished">Tastenkurzbefehle</translation>
    </message>
    <message>
        <source>Group Editor</source>
        <translation type="finished">Gruppen-Editor</translation>
    </message>
    <message>
        <source>Clear Scene</source>
        <translation type="finished">Szene zurücksetzen</translation>
    </message>
</context>
<context>
    <name>gui::ObjectInspectorModel</name>
    <message>
        <source>Instance Attributes</source>
        <translation type="finished">Instanzeigenschaften</translation>
    </message>
    <message>
        <source>Class Attributes</source>
        <translation type="finished">Klasseneigenschaften</translation>
    </message>
    <message>
        <source>Class Name</source>
        <translation type="finished">Class Name</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation type="finished">Collection</translation>
    </message>
    <message>
        <source>Object ID</source>
        <translation type="finished">Object ID</translation>
    </message>
</context>
<context>
    <name>gui::SceneEditorWidget</name>
    <message>
        <source>Toggle Collisionmesh</source>
        <translation type="finished">Kollisionsgitter umschalten</translation>
    </message>
    <message>
        <source>Toggle Rotationmanipulator</source>
        <translation type="finished">Rotationsmanipulator umschalten</translation>
    </message>
    <message>
        <source>Toggle Translationmanipulator</source>
        <translation type="finished">Translationsmanipulator umschalten</translation>
    </message>
    <message>
        <source>Undo last Action</source>
        <translation type="finished">Letzte Aktion rückgängig machen</translation>
    </message>
    <message>
        <source>Redo last Action</source>
        <translation type="finished">Letzte Aktion wiederholen</translation>
    </message>
    <message>
        <source>Viewer</source>
        <translation type="finished">Betrachten</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation type="finished">Bearbeiten</translation>
    </message>
    <message>
        <source>Remove selected Objects from scene</source>
        <translation type="finished">Ausgewählte Objekte aus der Szene löschen</translation>
    </message>
</context>
<context>
    <name>gui::ShortcutTableModel</name>
    <message>
        <source>General: Load Snapshot</source>
        <translation type="finished">Allgemein: Schnappschuss laden</translation>
    </message>
    <message>
        <source>General: Save Snapshot</source>
        <translation type="finished">Allgemein: Schnappschuss speichern</translation>
    </message>
    <message>
        <source>General: Edit Shortcuts</source>
        <translation type="finished">Allgemein: Tastenkürzel bearbeiten</translation>
    </message>
    <message>
        <source>General: Edit Groups</source>
        <translation type="finished">Allgemein: Gruppen bearbeiten</translation>
    </message>
    <message>
        <source>General: Clear Scene</source>
        <translation type="finished">Allgemein: Szene zurücksetzen</translation>
    </message>
    <message>
        <source>Object Explorer: Set Focus on Search Field</source>
        <translation type="finished">Objekt-Explorer: Fokus auf Suchfeld setzen</translation>
    </message>
    <message>
        <source>Group Explorer: Add Selection to new group</source>
        <translation type="finished">Gruppen-Explorer: Selektion zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <source>Scene: Copy selection</source>
        <translation type="finished">Szene: Auswahl kopieren</translation>
    </message>
    <message>
        <source>Scene: Duplicate selection</source>
        <translation type="finished">Szene: Auswahl duplizieren</translation>
    </message>
    <message>
        <source>Scene: Paste selection</source>
        <translation type="finished">Szene: Auswahl einfügen</translation>
    </message>
    <message>
        <source>Scene: Select all or deselect all</source>
        <translation type="finished">Szene: Alles aus-/abwählen</translation>
    </message>
    <message>
        <source>Scene: Switch between Editor and Viewer Mode</source>
        <translation type="finished">Szene: Zwischen Bearbeitungs- und Betrachungsmodus wechseln</translation>
    </message>
    <message>
        <source>Scene: Redo last Action</source>
        <translation type="finished">Szene: Letzte Aktion wiederholen</translation>
    </message>
    <message>
        <source>Scene: Toggle Collision Meshes</source>
        <translation type="finished">Szene: Kollisionsgitter umschalten</translation>
    </message>
    <message>
        <source>Scene: Toggle Rotation Manipulator</source>
        <translation type="finished">Szene: Rotationsmanipulator umschalten</translation>
    </message>
    <message>
        <source>Scene: Toggle Translation Manipulator</source>
        <translation type="finished">Szene: Translationsmanipulator umschalten</translation>
    </message>
    <message>
        <source>Scene: Switch to Editor Mode</source>
        <translation type="finished">Szene: Zum Bearbeitungsmodus wechseln</translation>
    </message>
    <message>
        <source>Scene: Switch to Viewer Mode</source>
        <translation type="finished">Szene: Zum Betrachtungsmodus wechseln</translation>
    </message>
    <message>
        <source>Scene: Undo last Action</source>
        <translation type="finished">Szene: Letzte Aktion rückgängig machen</translation>
    </message>
    <message>
        <source>Scene: Remove the selected Objects</source>
        <translation type="finished">Szene: Ausgewählte Objekte entfernen</translation>
    </message>
    <message>
        <source>Scene: Camera view all</source>
        <translation type="finished">Szene: Alles zeigen</translation>
    </message>
    <message>
        <source>Scene: Camera view from X</source>
        <translation type="finished">Szene: Kamera-Ansicht aus X</translation>
    </message>
    <message>
        <source>Scene: Camera view from Y</source>
        <translation type="finished">Szene: Kamera-Ansicht aus Y</translation>
    </message>
    <message>
        <source>Scene: Camera view from Z</source>
        <translation type="finished">Szene: Kamera-Ansicht aus Z</translation>
    </message>
    <message>
        <source>Scene: Camera view from negative X</source>
        <translation type="finished">Szene: Kamera-Ansicht aus -X</translation>
    </message>
    <message>
        <source>Scene: Camera view from negative Y</source>
        <translation type="finished">Szene: Kamera-Ansicht aus -Y</translation>
    </message>
    <message>
        <source>Scene: Camera view from negative Z</source>
        <translation type="finished">Szene: Kamera-Ansicht aus -Z</translation>
    </message>
    <message>
        <source>Scene: Reset rotation</source>
        <translation>Szene: Rotation zurücksetzen</translation>
    </message>
    <message>
        <source>Scene: Reset translation</source>
        <translation>Szene: Translation zurücksetzen</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="finished">Name</translation>
    </message>
    <message>
        <source>Key-Sequence</source>
        <translation type="finished">Tastenkombination</translation>
    </message>
</context>
<context>
    <name>gui::dialog::GroupExplorerDialog</name>
    <message>
        <source>New Group </source>
        <translation type="finished">Neue Gruppe </translation>
    </message>
    <message>
        <source>Object ID</source>
        <translation type="finished">Object ID</translation>
    </message>
    <message>
        <source>Class ID</source>
        <translation type="finished">Class ID</translation>
    </message>
    <message>
        <source>Collection ID</source>
        <translation type="finished">Collection ID</translation>
    </message>
</context>
<context>
    <name>gui::dialog::SceneEditorConfigDialog</name>
    <message>
        <source>Invalid Configuration</source>
        <translation type="finished">Ungültige Konfigation</translation>
    </message>
    <message>
        <source>The WorkingMemory name must not be empty.</source>
        <translation type="finished">Bitte einen gültigen Working-Memory auswählen.</translation>
    </message>
    <message>
        <source>The WorkingMemory updates topic name must not be empty.</source>
        <translation type="finished">Bitte gültige Working-Memory-Updates auswählen.</translation>
    </message>
    <message>
        <source>The WorkingMemory updates topic name has to be &quot;WorkingMemoryUpdates&quot;.</source>
        <translation type="finished">Der Name der Working-Memory-Updates muss &quot;WorkingMemoryUpdates&quot; lauten.</translation>
    </message>
    <message>
        <source>The PriorMemory name must not be empty</source>
        <translation type="finished">Bitte einen gültigen PriorMemory auswählen</translation>
    </message>
    <message>
        <source>The settings file path name must not be empty</source>
        <translation type="finished">Der Pfad zur Konfigurationsdatei darf nicht leer sein</translation>
    </message>
    <message>
        <source>Open settings file</source>
        <translation type="finished">Konfigurationsdatei öffnen</translation>
    </message>
    <message>
        <source>Settings (*.ini)</source>
        <translation type="finished">Konfiguration (*.ini)</translation>
    </message>
</context>
</TS>
