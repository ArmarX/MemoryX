/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <exception>
#include <string>
#include <vector>

// MemoryXInterface
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

/* Boost headers */
#include <memory>

#include "WorkingMemoryController.h"
#include "PriorKnowledgeController.h"
#include "../controller/Controller.h"
#include "../controller/Operation.h"

namespace memoryxcontroller
{
    class WorkingMemoryController;
    class PriorKnowledgeController;
    /**
     * @brief
     *
     */
    class MemoryXController
    {
    public:
        /**
         * @brief Constructor.
         *
         * Creates an instance of this class.
         */
        MemoryXController();
        /**
         * @brief Destructor
         *
         */
        ~MemoryXController();
        /**
         * @brief Initialisates the MemoryXController.
         *
         * @param priorKnowledgePrx the proxy to the priorknowledge
         * @param workingMemoryPrx the proxy to the working memory
         * @param workingMemoryUpdatesTopic the name of the topic where changes of the working memory are reported
         * @param objectInstancesSegmentName the name of the segment in the working memory where the object instances are stored
         * @param longtermMemoryPrx the proxy to the longtermmemory where snapshots are stored
         * @param mainController the controller of this programm
         *
         * @see controller::Controller
         */
        void init(const memoryx::PriorKnowledgeInterfacePrx& priorKnowledgePrx,
                  const memoryx::WorkingMemoryInterfacePrx& workingMemoryPrx,
                  const std::string& workingMemoryUpdatesTopic,
                  const std::string& objectInstancesSegmentName,
                  const memoryx::LongtermMemoryInterfacePrx& longtermMemoryPrx,
                  const controller::ControllerPtr& mainController);
        /**
         * @brief Saves the whole content of the working memory in a snapshot with the given name.
         *
         * If the given name does not start with 'Snapshot_', this prefix will be added.
         *
         * @param snapshotName the name of the snapshot
         * @return bool true when the saving was successful
         */
        bool saveSceneInSnapshot(std::string& snapshotName);
        /**
         * @brief Saves all object instances whose ids are given in a snapshot with the given name.
         *
         * @param snapshotName the name of the snapshot
         * @param objectIds the list with the ids of the object instances that shold be saved
         * @return bool true when the saving was successful
         */
        bool saveObjectsInSnapshot(std::string& snapshotName, const std::vector<std::string>& objectIds);
        /**
         * @brief Loads the content of the snapshot with the given name and add it to the current working memory.
         *
         * Returns a list with the ids of the added object instances.
         * If there is no snapshot with the given name nothing is added to the working memory.
         *
         * @param snapshotName the name of the snapshot that should be loaded
         * @return std::vector<std::string> the list with the ids of the added object instances
         */
        std::vector<std::string> loadSnapshot(const std::string& snapshotName);
        /**
         * @brief Returns a shared pointer to the WorkingMemoryController.
         *
         * @return IceInternal::Handle<WorkingMemoryController> the pointer to the WorkingMemoryController
         */
        IceInternal::Handle<WorkingMemoryController> getWorkingMemoryController() const;
        /**
         * @brief Returns a shared pointer to the PriorKnowledgeController.
         *
         * @return std::shared_ptr<PriorKnowledgeController> the pointer to the PriorKnowledgeController
         */
        std::shared_ptr<PriorKnowledgeController> getPriorKnowlegdeController() const;
        /**
         * @brief Returns a list with the names of all existing snapshots
         *
         * @return std::vector<std::string> list of names of all existing snapshots
         */
        std::vector<std::string> getAllSnapshots();
        /**
         * @brief Returns the main controller of this plugin.
         *
         * @return controller::ControllerPtr the pointer to the main controller
         */
        controller::ControllerPtr getController() const;

    private:
        memoryx::LongtermMemoryInterfacePrx longtermMemoryPrx;
        IceInternal::Handle<WorkingMemoryController> workingMemoryController;
        std::shared_ptr<PriorKnowledgeController> priorKnowledgeController;
        controller::ControllerWeakPtr mainController;

        /**
         * @brief LONGTERM_SNAPSHOT_PREFIX is set to "Snapshot_" and needs to be the prefix of each Snapshot name
         */
        const std::string LONGTERM_SNAPSHOT_PREFIX;
    };
    using MemoryXControllerPtr = std::shared_ptr<MemoryXController>;
}

