/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <exception>
#include <string>
#include <vector>

// MemoryXInterface
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include <ArmarXCore/core/ArmarXManager.h>

#include "MemoryXController.h"
#include "WorkingMemoryController.h"
#include "PriorKnowledgeController.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

memoryxcontroller::MemoryXController::MemoryXController() :
    LONGTERM_SNAPSHOT_PREFIX("Snapshot_")
{
}

memoryxcontroller::MemoryXController::~MemoryXController()
{
    if (this->workingMemoryController && this->workingMemoryController->getArmarXManager())
    {
        this->workingMemoryController->getArmarXManager()->removeObjectBlocking(this->workingMemoryController);
    }
}

void memoryxcontroller::MemoryXController::init(const memoryx::PriorKnowledgeInterfacePrx& priorKnowledgePrx,
        const memoryx::WorkingMemoryInterfacePrx& workingMemoryPrx,
        const std::string& workingMemoryUpdatesTopic,
        const std::string& objectInstancesSegmentName,
        const  memoryx::LongtermMemoryInterfacePrx& longtermMemoryPrx,
        const controller::ControllerPtr& mainController)
{
    this->mainController = mainController;
    priorKnowledgeController.reset(new PriorKnowledgeController(priorKnowledgePrx));
    workingMemoryController = new WorkingMemoryController(workingMemoryPrx, workingMemoryUpdatesTopic, objectInstancesSegmentName, mainController->getMemoryXController());
    this->longtermMemoryPrx = longtermMemoryPrx;
}

bool memoryxcontroller::MemoryXController::saveSceneInSnapshot(std::string& snapshotName)
{
    if (snapshotName.empty())
    {
        return false;
    }

    if (!simox::alg::starts_with(snapshotName, LONGTERM_SNAPSHOT_PREFIX))
    {
        snapshotName = std::string(LONGTERM_SNAPSHOT_PREFIX) + snapshotName;
    }

    bool worked = this->workingMemoryController->saveSceneInSnapshot(snapshotName, this->longtermMemoryPrx);

    return worked;
}

bool memoryxcontroller::MemoryXController::saveObjectsInSnapshot(std::string& snapshotName, const std::vector<std::string>& objectIds)
{
    if (snapshotName.empty())
    {
        return false;
    }

    if (!simox::alg::starts_with(snapshotName, LONGTERM_SNAPSHOT_PREFIX))
    {
        snapshotName = std::string(LONGTERM_SNAPSHOT_PREFIX) + snapshotName;
    }

    bool worked = this->workingMemoryController->saveObjectsInSnapshot(snapshotName, this->longtermMemoryPrx, objectIds);

    return worked;
}

std::vector<std::string> memoryxcontroller::MemoryXController::loadSnapshot(const std::string& snapshotName)
{
    std::vector<std::string> addedIds;

    if (!snapshotName.empty())
    {
        try
        {
            memoryx::WorkingMemorySnapshotInterfacePrx snapshot = longtermMemoryPrx->getWorkingMemorySnapshotListSegment()->openSnapshot(snapshotName);
            memoryx::PersistentEntitySegmentBasePrx segObjects = snapshot->getSegment("objectInstances");

            std::vector<memoryx::ObjectInstancePtr> objectInstances;

            if (segObjects)
            {
                memoryx::EntityIdList ids = segObjects->getAllEntityIds();

                for (memoryx::EntityIdList::const_iterator it = ids.begin(); it != ids.end(); ++it)
                {
                    memoryx::ObjectInstancePtr objectInstance = memoryx::ObjectInstancePtr::dynamicCast(segObjects->getEntityById(*it));
                    objectInstances.push_back(objectInstance);
                }
            }

            addedIds = this->workingMemoryController->addObjectInstances(objectInstances);
        }
        catch (const armarx::LocalException& e)
        {
            std::cout << "SnapshotName could not be loaded: " << snapshotName << std::endl;
        }
    }

    return addedIds;
}

IceInternal::Handle<memoryxcontroller::WorkingMemoryController> memoryxcontroller::MemoryXController::getWorkingMemoryController() const
{
    return workingMemoryController;
}

std::shared_ptr<memoryxcontroller::PriorKnowledgeController> memoryxcontroller::MemoryXController::getPriorKnowlegdeController() const
{
    return priorKnowledgeController;
}

controller::ControllerPtr memoryxcontroller::MemoryXController::getController() const
{
    return mainController.lock();
}

std::vector<std::string> memoryxcontroller::MemoryXController::getAllSnapshots()
{
    return longtermMemoryPrx->getSnapshotNames();
}
