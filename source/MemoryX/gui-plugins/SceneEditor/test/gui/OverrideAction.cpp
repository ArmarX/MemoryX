/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::OverrideAction
#define ARMARX_BOOST_TEST

/* Qt headers */
#include <QApplication>

#include <MemoryX/Test.h>
#include <MemoryX/gui-plugins/SceneEditor/test/InitializeCoin.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

#include <MemoryX/gui-plugins/SceneEditor/gui/OverrideAction.h>
#include <QEvent>
#include <QShortcutEvent>
#include <QObject>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::OverrideAction* action = new gui::OverrideAction("test", new QObject());
    BOOST_CHECK(action);
    delete action;
}

BOOST_AUTO_TEST_CASE(sendEvents)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::OverrideAction* action = new gui::OverrideAction("test", new QObject());

    QEvent* normalEvent = new QEvent(QEvent::KeyPress);
    QEvent* shortcutEvent = new QEvent(QEvent::Shortcut);

    BOOST_CHECK(!action->publicEvent(normalEvent));
    BOOST_CHECK(action->publicEvent(shortcutEvent));

    delete action;
}
