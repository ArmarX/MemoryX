/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::SceneEditorMainWindowController
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

#include <MemoryX/gui-plugins/SceneEditor/gui/SceneEditorMainWindowController.h>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);


typedef struct MainController
{
    gui::SceneEditorMainWindowController* control;
    MainController()
    {
        control = new gui::SceneEditorMainWindowController();
    }
    ~MainController()
    {
        delete control;
    }
} MainController;


BOOST_AUTO_TEST_CASE(constructor)
{
    MainController mainController;
    BOOST_CHECK(mainController.control);
}


BOOST_AUTO_TEST_CASE(widgetName)
{
    MainController mainController;
    BOOST_CHECK(mainController.control->getWidgetName() == "MemoryX.SceneEditor");
}


BOOST_AUTO_TEST_CASE(getWidget)
{
    MainController mainController;
    QPointer<QWidget> widget = mainController.control->getWidget();
    BOOST_CHECK(widget->isWidgetType());
    BOOST_CHECK(widget);
}


BOOST_AUTO_TEST_CASE(getCustomTitlebar)
{
    MainController mainController;
    QPointer<QWidget> parent = new QWidget();
    QPointer<QWidget> widget = mainController.control->getCustomTitlebarWidget(parent);
    BOOST_CHECK(widget->isWidgetType());
    BOOST_CHECK(widget);
}


BOOST_AUTO_TEST_CASE(getConfigDialog)
{
    MainController mainController;
    QPointer<QWidget> parent = new QWidget();
    QPointer<QDialog> dialog = mainController.control->getConfigDialog(parent);
    BOOST_CHECK(dialog->isWidgetType());
    BOOST_CHECK(dialog);
}
