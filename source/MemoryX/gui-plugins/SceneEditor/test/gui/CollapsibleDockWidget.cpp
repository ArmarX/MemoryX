/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::CollapsibleDockWidget
#define ARMARX_BOOST_TEST

/* C++ headers */
#include <streambuf>
#include <iostream>


/* Qt headers */
#include <QApplication>
#include <QWidget>
#include <QListIterator>

#include <MemoryX/Test.h>

#include<MemoryX/gui-plugins/SceneEditor/gui/CollapsibleDockWidget.h>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

typedef struct CollapsibleDock
{
    gui::CollapsibleDockWidget* dock;

    CollapsibleDock()
    {
        dock = new gui::CollapsibleDockWidget();
    }
    CollapsibleDock(const QString& title)
    {
        dock = new gui::CollapsibleDockWidget(title);
    }

    ~CollapsibleDock()
    {
        delete dock;
    }
} CollapsibleDock;

typedef struct CerrRedirect
{
    CerrRedirect(std::streambuf* new_buffer) :
        old(std::cerr.rdbuf(new_buffer))
    {
    }

    ~CerrRedirect()
    {
        std::cerr.rdbuf(old);
    }

private:
    std::streambuf* old;
} CerrRedirect;

BOOST_AUTO_TEST_CASE(constructor)
{
    CollapsibleDock collapsibleDock;
    collapsibleDock.dock->show();
    BOOST_CHECK(collapsibleDock.dock);
}

BOOST_AUTO_TEST_CASE(constructorWithTitle)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    BOOST_CHECK(collapsibleDock.dock);
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->windowTitle().toStdString(), "Dock Widget");
}

BOOST_AUTO_TEST_CASE(isCollapsedNoCollapsibleWidget)
{
    CollapsibleDock collapsibleDock;
    {
        std::stringstream buffer;
        CerrRedirect redirect(buffer.rdbuf());
        collapsibleDock.dock->setCollapsed(true);
        BOOST_CHECK(buffer.str() != "");
    }
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(isCollapsed)
{
    CollapsibleDock collapsibleDock;
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    collapsibleDock.dock->setCollapsed(true);
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), true);
}
BOOST_AUTO_TEST_CASE(isCollapsedWithTitleNoCollapsibleWidget)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    {
        std::stringstream buffer;
        CerrRedirect redirect(buffer.rdbuf());
        collapsibleDock.dock->setCollapsed(true);
        BOOST_CHECK(buffer.str() != "");
    }
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(isCollapsedWithTitle)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    collapsibleDock.dock->setCollapsed(true);
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), true);
}

BOOST_AUTO_TEST_CASE(isNotCollapsedNoCollapsibleWidget)
{
    CollapsibleDock collapsibleDock;
    {
        std::stringstream buffer;
        CerrRedirect redirect(buffer.rdbuf());
        collapsibleDock.dock->setCollapsed(false);
        BOOST_CHECK(buffer.str() != "");
    }
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(isNotCollapsed)
{
    CollapsibleDock collapsibleDock;
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    collapsibleDock.dock->setCollapsed(false);
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(isNotCollapsedWithTitleNoCollapsibleWidget)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    {
        std::stringstream buffer;
        CerrRedirect redirect(buffer.rdbuf());
        collapsibleDock.dock->setCollapsed(false);
        BOOST_CHECK(buffer.str() != "");
    }
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(isNotCollapsedWithTitle)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    collapsibleDock.dock->setCollapsed(false);
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(toggleCollapsedNoCollapsibleWidget)
{
    CollapsibleDock collapsibleDock;
    {
        std::stringstream buffer;
        CerrRedirect redirect(buffer.rdbuf());
        collapsibleDock.dock->toggleCollapsed();
        BOOST_CHECK(buffer.str() != "");
    }
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(toggleCollapsed)
{
    CollapsibleDock collapsibleDock;
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    bool collapsed = collapsibleDock.dock->isCollapsed();
    collapsibleDock.dock->toggleCollapsed();
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), !collapsed);
}

BOOST_AUTO_TEST_CASE(toggleCollapsedWithTitleNoCollapsibleWidget)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    {
        std::stringstream buffer;
        CerrRedirect redirect(buffer.rdbuf());
        collapsibleDock.dock->toggleCollapsed();
        BOOST_CHECK(buffer.str() != "");
    }
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), false);
}

BOOST_AUTO_TEST_CASE(toggleCollapsedWithTitle)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    bool collapsed = collapsibleDock.dock->isCollapsed();
    collapsibleDock.dock->toggleCollapsed();
    collapsibleDock.dock->show();
    BOOST_CHECK_EQUAL(collapsibleDock.dock->isCollapsed(), !collapsed);
}

BOOST_AUTO_TEST_CASE(windowTitleChangedIsNotCollapsed)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    collapsibleDock.dock->setCollapsed(false);
    collapsibleDock.dock->windowTitleChanged();
    collapsibleDock.dock->show();
    QList<QLabel*> allLabels = collapsibleDock.dock->titleBarWidget()->findChildren<QLabel*>();
    QListIterator<QLabel*> i(allLabels);
    bool windowTitleFound = false;

    while (i.hasNext() && !windowTitleFound)
    {
        QLabel* next = i.next();

        if (next->isVisible())
        {
            windowTitleFound = next->text().toStdString() == "Dock Widget";
        }
    }

    BOOST_CHECK(windowTitleFound);
}

BOOST_AUTO_TEST_CASE(windowTitleChangedIsCollapsed)
{
    CollapsibleDock collapsibleDock("Dock Widget");
    collapsibleDock.dock->setCollapsibleWidget(new QWidget());
    collapsibleDock.dock->setCollapsed(true);
    collapsibleDock.dock->windowTitleChanged();
    collapsibleDock.dock->show();
    QList<QLabel*> allLabels = collapsibleDock.dock->titleBarWidget()->findChildren<QLabel*>();
    QListIterator<QLabel*> i(allLabels);
    bool windowTitleFound = false;

    while (i.hasNext() && !windowTitleFound)
    {
        QLabel* next = i.next();

        if (next->isVisible())
        {
            windowTitleFound = next->text().toStdString() == "Dock Widget";
        }
    }

    BOOST_CHECK(!windowTitleFound);
}


