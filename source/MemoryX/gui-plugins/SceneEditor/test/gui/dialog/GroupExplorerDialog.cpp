/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::dialog::GroupExplorerDialog
#define ARMARX_BOOST_TEST

/* Qt headers */
#include <QApplication>

#include <MemoryX/Test.h>

#include "../../InitializeCoin.h"
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>

#include <MemoryX/gui-plugins/SceneEditor/gui/dialog/GroupExplorerDialog.h>

#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::GroupExplorerDialog* groupEditor = new gui::dialog::GroupExplorerDialog(control);
    BOOST_CHECK(groupEditor);
    delete groupEditor;
}

BOOST_AUTO_TEST_CASE(addGroup)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::GroupExplorerDialog* groupEditor = new gui::dialog::GroupExplorerDialog(control);

    groupEditor->findChild<QPushButton*>("addGroupButton")->click();

    BOOST_CHECK_EQUAL(1, control->getScene()->getGroupManager()->getAllGroups().size());

    delete groupEditor;
}

BOOST_AUTO_TEST_CASE(selectAndRenameGroup)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::GroupExplorerDialog* groupEditor = new gui::dialog::GroupExplorerDialog(control);

    groupEditor->findChild<QPushButton*>("addGroupButton")->click();

    groupEditor->findChild<QLineEdit*>("groupNameLineEdit")->setText("testname 123");
    groupEditor->findChild<QPushButton*>("changeNameButton")->click();

    BOOST_CHECK(control->getScene()->getGroupManager()->getGroupById("testname 123"));

    delete groupEditor;
}

BOOST_AUTO_TEST_CASE(removeGroup)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::GroupExplorerDialog* groupEditor = new gui::dialog::GroupExplorerDialog(control);

    groupEditor->findChild<QPushButton*>("addGroupButton")->click();

    groupEditor->findChild<QPushButton*>("deleteGroupButton")->click();

    BOOST_CHECK(!control->getScene()->getGroupManager()->getGroupById("New Group 1"));

    delete groupEditor;
}

BOOST_AUTO_TEST_CASE(setCurrentGroup)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::GroupExplorerDialog* groupEditor = new gui::dialog::GroupExplorerDialog(control);

    groupEditor->findChild<QPushButton*>("addGroupButton")->click();

    groupEditor->setCurrentGroup("New Group 1");

    BOOST_CHECK(groupEditor->findChild<QLineEdit*>("groupNameLineEdit")->text() == "New Group 1");

    delete groupEditor;
}

BOOST_AUTO_TEST_CASE(showDialog)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::GroupExplorerDialog* groupEditor = new gui::dialog::GroupExplorerDialog(control);
    groupEditor->show();
    BOOST_CHECK(groupEditor);
    delete groupEditor;
}
