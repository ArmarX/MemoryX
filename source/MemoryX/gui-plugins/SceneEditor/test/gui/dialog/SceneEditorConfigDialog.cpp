/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::dialog::SceneEditorConfigDialog
#define ARMARX_BOOST_TEST

/* Qt headers */
#include <QApplication>

#include <MemoryX/Test.h>

#include "../../InitializeCoin.h"
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

#include <MemoryX/gui-plugins/SceneEditor/gui/dialog/SceneEditorConfigDialog.h>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

typedef struct ConfigDialog
{
    gui::dialog::SceneEditorConfigDialog* configDialog;

    ConfigDialog()
    {
        configDialog = new gui::dialog::SceneEditorConfigDialog();
    }

    ~ConfigDialog()
    {
        delete configDialog;
    }
} ConfigDialog;

BOOST_AUTO_TEST_CASE(constructor)
{
    ConfigDialog dialog;
    BOOST_CHECK(dialog.configDialog);
}


BOOST_AUTO_TEST_CASE(defaultName)
{
    ConfigDialog dialog;
    std::string name = dialog.configDialog->getDefaultName();
    BOOST_CHECK(name.length() != 0);
}

BOOST_AUTO_TEST_CASE(unicName)
{
    ConfigDialog dialog;
    std::string randomName = dialog.configDialog->getDefaultName();
    std::string anotherName = dialog.configDialog->getDefaultName();
    BOOST_CHECK(randomName != anotherName);
}
