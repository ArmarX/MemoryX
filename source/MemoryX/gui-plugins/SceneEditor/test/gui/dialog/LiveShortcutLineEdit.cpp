/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::dialog::LiveShortcutLineEdit
#define ARMARX_BOOST_TEST

/* Qt headers */
#include <QApplication>
#include <QTest>

#include <MemoryX/Test.h>

#include "../../InitializeCoin.h"
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

#include <MemoryX/gui-plugins/SceneEditor/gui/dialog/LiveShortcutLineEdit.h>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

BOOST_AUTO_TEST_CASE(constructor)
{
    gui::dialog::LiveShortcutLineEdit* lineEdit = new gui::dialog::LiveShortcutLineEdit();
    BOOST_CHECK(lineEdit);
    delete lineEdit;
}

BOOST_AUTO_TEST_CASE(testKeyEvents)
{
    gui::dialog::LiveShortcutLineEdit* lineEdit = new gui::dialog::LiveShortcutLineEdit();
    QKeyEvent* testKeyEventNoModifier = new QKeyEvent(QEvent::KeyPress, Qt::Key_X, 0);
    QKeyEvent* testKeyEventControl = new QKeyEvent(QEvent::KeyPress, Qt::Key_X, Qt::ControlModifier);
    QKeyEvent* testKeyEventShift = new QKeyEvent(QEvent::KeyPress, Qt::Key_X, Qt::ShiftModifier);
    QKeyEvent* testKeyEventAlt = new QKeyEvent(QEvent::KeyPress, Qt::Key_X, Qt::AltModifier);
    QKeyEvent* testKeyEventMeta = new QKeyEvent(QEvent::KeyPress, Qt::Key_X, Qt::MetaModifier);
    QKeyEvent* testKeyEventUnknown = new QKeyEvent(QEvent::KeyPress, Qt::Key_unknown, Qt::MetaModifier);
    QKeyEvent* testKeyEventShiftKey = new QKeyEvent(QEvent::KeyPress, Qt::Key_Shift, Qt::MetaModifier);

    lineEdit->publicKeyPressEvent(testKeyEventNoModifier);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "X");
    lineEdit->setText("");
    lineEdit->publicKeyPressEvent(testKeyEventControl);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "Ctrl+X");
    lineEdit->setText("");
    lineEdit->publicKeyPressEvent(testKeyEventShift);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "Shift+X");
    lineEdit->setText("");
    lineEdit->publicKeyPressEvent(testKeyEventAlt);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "Alt+X");
    lineEdit->setText("");
    lineEdit->publicKeyPressEvent(testKeyEventMeta);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "Meta+X");
    lineEdit->setText("");
    lineEdit->publicKeyPressEvent(testKeyEventUnknown);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "");
    lineEdit->setText("");
    lineEdit->publicKeyPressEvent(testKeyEventShiftKey);
    BOOST_CHECK_EQUAL(lineEdit->text().toStdString(), "");

    delete lineEdit;
}
