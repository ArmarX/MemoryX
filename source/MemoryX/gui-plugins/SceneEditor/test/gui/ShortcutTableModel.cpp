/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::ShortcutTableModel
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../../gui/ShortcutTableModel.h"

BOOST_AUTO_TEST_CASE(initEmptyShortcutTableModel)
{
    // test values

    // action done
    gui::ShortcutTableModel* testShortcutTableModel  = new gui::ShortcutTableModel();

    // compare values

    // result
    BOOST_CHECK(testShortcutTableModel);
}

BOOST_AUTO_TEST_CASE(initFilledShortcutTableModel)
{
    // test values
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);

    QHash<QString, QKeySequence> paramHash;
    paramHash.insert(testName1, testKeySequence1);
    paramHash.insert(testName2, testKeySequence2);

    // action done
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel(paramHash);

    // test
    BOOST_CHECK(testShortcutTableModel);
    BOOST_CHECK_EQUAL(testShortcutTableModel->rowCount(QModelIndex()), 2);
}

BOOST_AUTO_TEST_CASE(setShortcutHashTable)
{
    // test values
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);

    QHash<QString, QKeySequence> paramHash;
    paramHash.insert(testName1, testKeySequence1);
    paramHash.insert(testName2, testKeySequence2);

    // action done
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel();
    testShortcutTableModel->setShortcutHashTable(paramHash);

    // test
    BOOST_CHECK_EQUAL(testShortcutTableModel->rowCount(QModelIndex()), 2);
}

BOOST_AUTO_TEST_CASE(countModelRows)
{
    // test values
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);

    QHash<QString, QKeySequence> paramHash;
    paramHash.insert(testName1, testKeySequence1);
    paramHash.insert(testName2, testKeySequence2);

    // action done
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel();
    testShortcutTableModel->setShortcutHashTable(paramHash);

    // test
    BOOST_CHECK_EQUAL(testShortcutTableModel->rowCount(QModelIndex()), 2);
}

BOOST_AUTO_TEST_CASE(countModelColumns)
{
    // test values
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel();

    // test
    BOOST_CHECK_EQUAL(testShortcutTableModel->columnCount(QModelIndex()), 2);
}

BOOST_AUTO_TEST_CASE(getDataFromTable)
{
    // test values
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);
    int countName1 = 0, countName2 = 0, countKeys1 = 0, countKeys2 = 0, countOthers = 0;

    QHash<QString, QKeySequence> paramHash;
    paramHash.insert(testName1, testKeySequence1);
    paramHash.insert(testName2, testKeySequence2);

    // action done
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel();
    testShortcutTableModel->setShortcutHashTable(paramHash);

    if (testShortcutTableModel->data(testShortcutTableModel->index(0, 0), Qt::DisplayRole) == testName1)
    {
        countName1++;
    }
    else if (testShortcutTableModel->data(testShortcutTableModel->index(0, 0), Qt::DisplayRole) == testName2)
    {
        countName2++;
    }
    else
    {
        countOthers++;
    }

    if (testShortcutTableModel->data(testShortcutTableModel->index(1, 0), Qt::DisplayRole) == testName1)
    {
        countName1++;
    }
    else if (testShortcutTableModel->data(testShortcutTableModel->index(1, 0), Qt::DisplayRole) == testName2)
    {
        countName2++;
    }
    else
    {
        countOthers++;
    }

    if (testShortcutTableModel->data(testShortcutTableModel->index(0, 1), Qt::DisplayRole) == testKeySequence1)
    {
        countKeys1++;
    }
    else if (testShortcutTableModel->data(testShortcutTableModel->index(0, 1), Qt::DisplayRole) == testKeySequence2)
    {
        countKeys2++;
    }
    else
    {
        countOthers++;
    }

    if (testShortcutTableModel->data(testShortcutTableModel->index(1, 1), Qt::DisplayRole) == testKeySequence1)
    {
        countKeys1++;
    }
    else if (testShortcutTableModel->data(testShortcutTableModel->index(1, 1), Qt::DisplayRole) == testKeySequence2)
    {
        countKeys2++;
    }
    else
    {
        countOthers++;
    }

    // test
    BOOST_CHECK_EQUAL(testShortcutTableModel->rowCount(QModelIndex()), 2);
    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countKeys1, 1);
    BOOST_CHECK_EQUAL(countKeys2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
    BOOST_CHECK_EQUAL(testShortcutTableModel->data(testShortcutTableModel->index(-1, 0), Qt::DisplayRole).toString().toStdString(), "");
    BOOST_CHECK_EQUAL(testShortcutTableModel->data(testShortcutTableModel->index(0, 0), Qt::EditRole).toString().toStdString(), "");
    BOOST_CHECK_EQUAL(testShortcutTableModel->data(testShortcutTableModel->index(2, 0), Qt::DisplayRole).toString().toStdString(), "");
}

BOOST_AUTO_TEST_CASE(getHeaderDataFromTable)
{
    // action done
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel();

    // test
    BOOST_CHECK_EQUAL(testShortcutTableModel->headerData(0, Qt::Horizontal, Qt::DisplayRole).toString().toStdString(), "Name");
    BOOST_CHECK_EQUAL(testShortcutTableModel->headerData(1, Qt::Horizontal, Qt::DisplayRole).toString().toStdString(), "Key-Sequence");
    BOOST_CHECK_EQUAL(testShortcutTableModel->headerData(0, Qt::Vertical, Qt::DisplayRole).toString().toStdString(), "");
    BOOST_CHECK_EQUAL(testShortcutTableModel->headerData(0, Qt::Horizontal, Qt::EditRole).toString().toStdString(), "");
    BOOST_CHECK_EQUAL(testShortcutTableModel->headerData(2, Qt::Horizontal, Qt::DisplayRole).toString().toStdString(), "");
}

BOOST_AUTO_TEST_CASE(getActionNameFromTable)
{
    // test values
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);
    int countName1 = 0, countName2 = 0, countOthers = 0;

    QHash<QString, QKeySequence> paramHash;
    paramHash.insert(testName1, testKeySequence1);
    paramHash.insert(testName2, testKeySequence2);

    // action done
    gui::ShortcutTableModel* testShortcutTableModel = new gui::ShortcutTableModel();
    testShortcutTableModel->setShortcutHashTable(paramHash);

    if (testShortcutTableModel->actionName(testShortcutTableModel->index(0, 0), Qt::DisplayRole) == testName1)
    {
        countName1++;
    }
    else if (testShortcutTableModel->actionName(testShortcutTableModel->index(0, 0), Qt::DisplayRole) == testName2)
    {
        countName2++;
    }
    else
    {
        countOthers++;
    }

    if (testShortcutTableModel->actionName(testShortcutTableModel->index(1, 0), Qt::DisplayRole) == testName1)
    {
        countName1++;
    }
    else if (testShortcutTableModel->actionName(testShortcutTableModel->index(1, 0), Qt::DisplayRole) == testName2)
    {
        countName2++;
    }
    else
    {
        countOthers++;
    }

    if (testShortcutTableModel->actionName(testShortcutTableModel->index(0, 1), Qt::DisplayRole) == testName1)
    {
        countName1++;
    }
    else if (testShortcutTableModel->actionName(testShortcutTableModel->index(0, 1), Qt::DisplayRole) == testName2)
    {
        countName2++;
    }
    else
    {
        countOthers++;
    }

    if (testShortcutTableModel->actionName(testShortcutTableModel->index(1, 1), Qt::DisplayRole) == testName1)
    {
        countName1++;
    }
    else if (testShortcutTableModel->actionName(testShortcutTableModel->index(1, 1), Qt::DisplayRole) == testName2)
    {
        countName2++;
    }
    else
    {
        countOthers++;
    }

    // test
    BOOST_CHECK_EQUAL(testShortcutTableModel->rowCount(QModelIndex()), 2);
    BOOST_CHECK_EQUAL(countName1, 2);
    BOOST_CHECK_EQUAL(countName2, 2);
    BOOST_CHECK_EQUAL(countOthers, 0);
    BOOST_CHECK_EQUAL(testShortcutTableModel->actionName(testShortcutTableModel->index(-1, 0), Qt::DisplayRole).toString().toStdString(), "");
    BOOST_CHECK_EQUAL(testShortcutTableModel->actionName(testShortcutTableModel->index(0, 0), Qt::EditRole).toString().toStdString(), "");
    BOOST_CHECK_EQUAL(testShortcutTableModel->actionName(testShortcutTableModel->index(2, 0), Qt::DisplayRole).toString().toStdString(), "");
}
