/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::PropertyBrowserWidget
#define ARMARX_BOOST_TEST

/* Qt headers */
#include <QApplication>

#include <MemoryX/Test.h>
#include <MemoryX/gui-plugins/SceneEditor/test/InitializeCoin.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneObject.h>

#include<MemoryX/gui-plugins/SceneEditor/gui/PropertyBrowserWidget.h>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

typedef struct PropertyBrowser
{
    gui::PropertyBrowserWidget* browser;

    PropertyBrowser(const controller::ControllerPtr& control)
    {
        browser = new gui::PropertyBrowserWidget(control);
    }

    ~PropertyBrowser()
    {
        delete browser;
    }
} PropertyBrowser;

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr control = controller::Controller::create();
    PropertyBrowser propertyBrowser(control);
    BOOST_CHECK(propertyBrowser.browser);
}


BOOST_AUTO_TEST_CASE(setPropertiesNullObject)
{
    controller::ControllerPtr control = controller::Controller::create();
    PropertyBrowser propertyBrowser(control);
    propertyBrowser.browser->setProperties(NULL);
    BOOST_CHECK_EQUAL(propertyBrowser.browser->properties().length(), 0);
}

BOOST_AUTO_TEST_CASE(setPropertiesNullClass)
{
    controller::ControllerPtr control = controller::Controller::create();
    PropertyBrowser propertyBrowser(control);
    propertyBrowser.browser->setProperties(NULL, "collection");
    BOOST_CHECK_EQUAL(propertyBrowser.browser->properties().length(), 0);
}


BOOST_AUTO_TEST_CASE(setPropertiesNullClassAndEmptyCollection)
{
    controller::ControllerPtr control = controller::Controller::create();
    PropertyBrowser propertyBrowser(control);
    propertyBrowser.browser->setProperties(NULL, "");
    BOOST_CHECK_EQUAL(propertyBrowser.browser->properties().length(), 0);
}

BOOST_AUTO_TEST_CASE(updateSceneObjectInvalidID)
{
    controller::ControllerPtr control = controller::Controller::create();
    PropertyBrowser propertyBrowser(control);
    propertyBrowser.browser->updateSceneObject("");
    BOOST_CHECK_EQUAL(propertyBrowser.browser->properties().length(), 0);
}



