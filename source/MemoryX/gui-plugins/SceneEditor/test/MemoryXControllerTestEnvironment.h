/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <stdlib.h>
#include <vector>

#include <MemoryX/components/LongtermMemory/LongtermMemory.h>
#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/components/CommonStorage/CommonStorage.h>
#include <MemoryX/components/WorkingMemory/WorkingMemory.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <MemoryX/core/MongoTestHelper.h>
#include <ArmarXCore/core/application/Application.h>

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <MemoryX/libraries/updater/ObjectLocalization/ObjectLocalizationMemoryUpdater.h>


class MemoryXControllerTestEnvironment
{
public:
    MemoryXControllerTestEnvironment(const std::string& testName)
    {
        using namespace memoryx;
        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        properties->setProperty("MemoryX.PriorKnowledge.ClassCollections", "testdb.Objects1,testdb.Objects2");
        properties->setProperty("MemoryX.PriorKnowledge.RelationCollections", "");
        properties->setProperty("MemoryX.CommonStorage.MongoUser", "testuser");
        properties->setProperty("MemoryX.CommonStorage.MongoPassword", "testpass");
        properties->setProperty("Ice.ThreadPool.Client.SizeMax", "2");
        properties->setProperty("Ice.ThreadPool.Server.SizeMax", "2");
        properties->setProperty("MemoryX.LongtermMemory.DatabaseName", "testdb");
        properties->setProperty("MemoryX.WorkingMemory.UseLongtermMemory", "true");
        properties->setProperty("MemoryX.WorkingMemory.UsePriorMemory", "true");
        properties->setProperty("MemoryX.WorkingMemory.PublishUpdates", "true");
        properties->setProperty("MemoryX.WorkingMemory.UseKalmanFilter", "false");

        iceTestHelper = new IceTestHelper();
        iceTestHelper->startEnvironment();

        manager = new TestArmarXManager(testName, iceTestHelper->getCommunicator(), properties);

        storage = manager->createComponentAndRun<CommonStorage, CommonStorageInterfacePrx>("MemoryX", "CommonStorage");
        priorKnowledgePrx = manager->createComponentAndRun<PriorKnowledge, PriorKnowledgeInterfacePrx>("MemoryX", "PriorKnowledge");
        longtermMemory = manager->createComponentAndRun<LongtermMemory, LongtermMemoryInterfacePrx>("MemoryX", "LongtermMemory");
        workingMemoryPrx = manager->createComponentAndRun<WorkingMemory, WorkingMemoryInterfacePrx>("MemoryX", "WorkingMemory");

        priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
        priorKnowledgePrx->getObjectClassesSegment()->setWriteCollection(storage->requestCollection("testdb.Objects2"));
        priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
        priorKnowledgePrx->getObjectClassesSegment()->setWriteCollection(storage->requestCollection("testdb.Objects1"));

        ObjectClassPtr objectClass = new ObjectClass();
        objectClass->setName("testObject1");
        objectClass->putAttribute("ManipulationFile", "");
        objectClass->putAttribute("TestAttribute1", "test1");
        objectClass->putAttribute("IvFile", "");
        objectClass->putAttribute("featureFile", "");

        priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);

        ObjectClassPtr objectClass2 = new ObjectClass();
        objectClass2->setName("testObject2");
        objectClass2->putAttribute("ManipulationFile", "");
        objectClass2->putAttribute("TestAttribute1", "test2");
        objectClass2->putAttribute("TestAttribute2", "test2.1");
        objectClass2->putAttribute("IvFile", "");
        objectClass2->putAttribute("featureFile", "");
        priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass2);

        ObjectClassPtr objectClass3 = new ObjectClass();
        objectClass3->setName("testObject3");
        objectClass3->putAttribute("ManipulationFile", "");
        objectClass3->putAttribute("TestAttribute1", "test3");
        objectClass3->putAttribute("TestAttribute2", "test3.1");
        objectClass3->putAttribute("IvFile", "");
        objectClass3->putAttribute("featureFile", "");
        priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass3);

        objectInstancesPrx = ObjectInstanceMemorySegmentBasePrx::uncheckedCast(workingMemoryPrx->getSegment("objectInstances"));

        ObjectInstancePtr objectInstance1 = new ObjectInstance("testInstance1");
        objectInstance1->addClass(objectClass->getName(), 1.0);

        objectInstancesPrx->addEntity(objectInstance1);

        longtermMemory->getWorkingMemorySnapshotListSegment()->clear();

    }

    void shutDown()
    {
        manager->shutdown();
    }

    MongoTestHelper mongoTestHelper;
    memoryx::CommonStorageInterfacePrx storage;
    TestArmarXManagerPtr manager;
    IceTestHelperPtr iceTestHelper;
    memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;
    memoryx::LongtermMemoryInterfacePrx longtermMemory;
    memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx;

};
using MemoryXControllerTestEnvironmentPtr = std::shared_ptr<MemoryXControllerTestEnvironment>;

struct MemoryXEnvironment_P
{
    MemoryXEnvironment_P(std::string name) :
        env(name)
    {
    }

    ~MemoryXEnvironment_P()
    {
        env.priorKnowledgePrx->getObjectClassesSegment()->setWriteCollection(env.storage->requestCollection("testdb.Objects1"));
        env.priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
        env.shutDown();
    }

    MemoryXControllerTestEnvironment env;
};

typedef struct MemoryXEnvironment
{
    MemoryXEnvironment() :
        env("MemoryXControllerTestEnvironment"),
        controller(controller::Controller::create())
    {
        controller->getMemoryXController()->init(env.env.priorKnowledgePrx,
                env.env.workingMemoryPrx,
                "WorkingMemoryUpdates",
                "objectInstances",
                env.env.longtermMemory,
                controller);
        env.env.manager->addObject(controller->getMemoryXController()->getWorkingMemoryController());
    }

    ~MemoryXEnvironment()
    {
    }

    MemoryXEnvironment_P env;
    controller::ControllerPtr controller;
} MemoryXEnvironment;

#define ENVIRONMENT(memoryXEnvironment) memoryXEnvironment.env.env
#define CONTROLLER(memoryXEnvironment) memoryXEnvironment.controller

