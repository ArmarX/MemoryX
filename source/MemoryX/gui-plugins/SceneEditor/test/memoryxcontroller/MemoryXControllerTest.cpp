/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::memoryxcontroller::WorkingMemoryController
#define ARMARX_BOOST_TEST

#include <map>
#include <vector>

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/memoryxcontroller/MemoryXController.h>
#include <MemoryX/gui-plugins/SceneEditor/memoryxcontroller/PriorKnowledgeController.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>

#include <boost/thread.hpp>

using namespace memoryxcontroller;
using namespace memoryx;

static inline bool floatEqual(float f1, float f2)
{
    return (f1 < f2 ? f2 - f1 : f1 - f2) < 0.00001f;
}

static inline bool sbRotationEqual(SbRotation r1, SbRotation r2)
{
    float v1x, v1y, v1z, v1w, v2x, v2y, v2z, v2w;
    r1.getValue(v1x, v1y, v1z, v1w);
    r2.getValue(v2x, v2y, v2z, v2w);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z) && floatEqual(v1w, v2w);
}

static inline bool sbVec3fEqual(SbVec3f v1, SbVec3f v2)
{
    float v1x, v1y, v1z, v2x, v2y, v2z;
    v1.getValue(v1x, v1y, v1z);
    v2.getValue(v2x, v2y, v2z);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z);
}

BOOST_AUTO_TEST_CASE(testSaveSceneInSnapshot)
{
    MemoryXEnvironment environment;

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);

    string snapshotName = "testSnapshot1";

    CONTROLLER(environment)->getMemoryXController()->saveSceneInSnapshot(snapshotName);

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().at(0), snapshotName);
    ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->removeSnapshot(snapshotName);
    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);

    snapshotName = "";
    BOOST_CHECK_EQUAL(CONTROLLER(environment)->getMemoryXController()->saveSceneInSnapshot(snapshotName), false);
}

BOOST_AUTO_TEST_CASE(testSaveObjectsInSnapshot)
{
    MemoryXEnvironment environment;

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);

    string snapshotName = "testSnapshot2";
    NameList ids;
    ids.push_back("1");
    CONTROLLER(environment)->getMemoryXController()->saveObjectsInSnapshot(snapshotName, ids);

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().at(0), snapshotName);
    ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->removeSnapshot(snapshotName);
    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);

    snapshotName = "";
    BOOST_CHECK_EQUAL(CONTROLLER(environment)->getMemoryXController()->saveObjectsInSnapshot(snapshotName, ids), false);
}
