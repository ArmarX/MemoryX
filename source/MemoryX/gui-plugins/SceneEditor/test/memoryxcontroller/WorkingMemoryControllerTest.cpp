/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::memoryxcontroller::MemoryXController
#define ARMARX_BOOST_TEST

#include <map>
#include <vector>

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/memoryxcontroller/MemoryXController.h>
#include <MemoryX/gui-plugins/SceneEditor/memoryxcontroller/PriorKnowledgeController.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>

#include <boost/thread.hpp>

using namespace memoryxcontroller;
using namespace memoryx;

static inline bool floatEqual(float f1, float f2)
{
    return (f1 < f2 ? f2 - f1 : f1 - f2) < 0.00001f;
}

static inline bool sbRotationEqual(SbRotation r1, SbRotation r2)
{
    float v1x, v1y, v1z, v1w, v2x, v2y, v2z, v2w;
    r1.getValue(v1x, v1y, v1z, v1w);
    r2.getValue(v2x, v2y, v2z, v2w);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z) && floatEqual(v1w, v2w);
}

static inline bool sbVec3fEqual(SbVec3f v1, SbVec3f v2)
{
    float v1x, v1y, v1z, v2x, v2y, v2z;
    v1.getValue(v1x, v1y, v1z);
    v2.getValue(v2x, v2y, v2z);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z);
}


BOOST_AUTO_TEST_CASE(testAddObjectInstance)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    ObjectClassPtr ptr = priorKnowledgeController->getObjectClassPtr("testObject1", "testdb.Objects1");

    string id = workingMemoryController->addObjectInstance("testInstanceAddObjectInstance", ptr);


    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).objectInstancesPrx->getEntityById(id)->getName(), "testInstanceAddObjectInstance");
}


BOOST_AUTO_TEST_CASE(testAddObjectInstances)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    ObjectClassPtr objectClass = priorKnowledgeController->getObjectClassPtr("testObject1", "testdb.Objects1");

    vector<ObjectInstancePtr> vector;

    NameList ids_leer = workingMemoryController->addObjectInstances(vector);
    BOOST_CHECK_EQUAL(ids_leer.size(), 0);

    ObjectInstancePtr objectInstance1 = new ObjectInstance("testInstanceAddObjectInstances1");
    objectInstance1->addClass(objectClass->getName(), 1.0);
    vector.push_back(objectInstance1);

    ObjectInstancePtr objectInstance2 = new ObjectInstance("testInstanceAddObjectInstances2");
    objectInstance2->addClass(objectClass->getName(), 1.0);
    vector.push_back(objectInstance2);

    NameList ids = workingMemoryController->addObjectInstances(vector);

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).objectInstancesPrx->getEntityById(ids[0])->getName(), "testInstanceAddObjectInstances1");
    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).objectInstancesPrx->getEntityById(ids[1])->getName(), "testInstanceAddObjectInstances2");
}


BOOST_AUTO_TEST_CASE(testRemoveObjectInstance)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

    BOOST_CHECK_THROW(workingMemoryController->removeObjectInstance("42"), std::invalid_argument);

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).objectInstancesPrx->getAllEntityIds().size(), 1);
    workingMemoryController->removeObjectInstance("1");
    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).objectInstancesPrx->getAllEntityIds().size(), 0);
}

BOOST_AUTO_TEST_CASE(testRotateTranslateObject)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

    SbRotation oldRotation = SbRotation(0, 0, 0, 0);
    SbRotation newRotation = SbRotation(8, 23, 6, 2);

    SbVec3f oldPosition = SbVec3f(0, 0, 0);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    BOOST_CHECK_THROW(workingMemoryController->rotateTranslateObject("42", newRotation, newPosition), std::invalid_argument);

    ObjectInstancePtr objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));
    BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(objectInstance), oldRotation));
    BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(objectInstance), oldPosition));

    workingMemoryController->rotateTranslateObject("1", newRotation, newPosition);

    objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));
    BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(objectInstance), newRotation));
    BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(objectInstance), newPosition));
}

// reportEntityRemoved just fails in the test
// seems to fail because of multithreading in Controller::execute
//BOOST_AUTO_TEST_CASE(testReportEntityRemoved)
//{
//    MemoryXEnvironment environment;
//    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

//    scene3D::SceneObjectPtr sceneObject = scene3D::SceneObjectPtr(new scene3D::SceneObject("1", "testObject1", "testdb.Objects1", new SoSeparator, new SoSeparator));
//    CONTROLLER(environment)->getScene()->getObjectManager()->addObject(sceneObject);

//    BOOST_CHECK_EQUAL(CONTROLLER(environment)->getScene()->getObjectManager()->getAllObjects().size(), 1);

//    IceInternal::Handle<WorkingMemoryController> workingController = new WorkingMemoryController(ENVIRONMENT(environment).workingMemoryPrx,
//            "WorkingMemoryUpdates",
//            "objectInstances",
//            CONTROLLER(environment)->getMemoryXController());

//    ENVIRONMENT(environment).manager->addObject(workingController);
//    boost::this_thread::sleep(boost::posix_time::seconds(1));

//    workingController->removeObjectInstance("1");

//    boost::this_thread::sleep(boost::posix_time::seconds(1));
//    ENVIRONMENT(environment).manager->removeObjectBlocking(workingController->getName());

//    BOOST_CHECK_EQUAL(CONTROLLER(environment)->getScene()->getObjectManager()->getAllObjects().size(), 0);
//}

// reportEntityUpdated just fails in the test
// seems to fail because of multithreading in Controller::execute
//BOOST_AUTO_TEST_CASE(testReportEntityUpdated)
//{
//    MemoryXEnvironment environment;
//    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

//    scene3D::SceneObjectPtr sceneObject = scene3D::SceneObjectPtr(new scene3D::SceneObject("1", "testObject1", "testdb.Objects1", new SoSeparator, new SoSeparator));
//    CONTROLLER(environment)->getScene()->getObjectManager()->addObject(sceneObject);

//    BOOST_CHECK_EQUAL(CONTROLLER(environment)->getScene()->getObjectManager()->getAllObjects().size(), 1);

//    IceInternal::Handle<WorkingMemoryController> workingController = new WorkingMemoryController(ENVIRONMENT(environment).workingMemoryPrx,
//            "WorkingMemoryUpdates",
//            "objectInstances",
//            CONTROLLER(environment)->getMemoryXController());

//    ENVIRONMENT(environment).manager->addObject(workingController);
//    boost::this_thread::sleep(boost::posix_time::seconds(1));


//    SbRotation newRotation = SbRotation(8, 23, 6, 2);
//    SbVec3f newPosition = SbVec3f(42, 21, 4);

//    workingController->rotateTranslateObject("1", newRotation, newPosition);

//    boost::this_thread::sleep(boost::posix_time::seconds(1));
//    ENVIRONMENT(environment).manager->removeObjectBlocking(workingController->getName());


//    BOOST_CHECK(sbRotationEqual(sceneObject->getRotation(), newRotation));
//    BOOST_CHECK(sbVec3fEqual(sceneObject->getTranslation(), newPosition));
//    BOOST_CHECK_EQUAL(CONTROLLER(environment)->getScene()->getObjectManager()->getAllObjects().size(), 1);
//}

BOOST_AUTO_TEST_CASE(testSaveSceneInSnapshot)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);

    workingMemoryController->saveSceneInSnapshot("testSnapshot1", ENVIRONMENT(environment).longtermMemory);

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().at(0), "testSnapshot1");
    ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->removeSnapshot("testSnapshot1");
    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);
}

BOOST_AUTO_TEST_CASE(testSaveObjectsInSnapshot)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);

    NameList ids;
    ids.push_back("1");
    workingMemoryController->saveObjectsInSnapshot("testSnapshot2", ENVIRONMENT(environment).longtermMemory, ids);

    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().at(0), "testSnapshot2");
    ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->removeSnapshot("testSnapshot2");
    BOOST_CHECK_EQUAL(ENVIRONMENT(environment).longtermMemory->getWorkingMemorySnapshotListSegment()->getSnapshotNames().size(), 0);
}

BOOST_AUTO_TEST_CASE(testGetSbVec3fFromInstance)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

    ObjectInstancePtr objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));

    SbRotation newRotation = SbRotation(8, 23, 6, 2);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    workingMemoryController->rotateTranslateObject("1", newRotation, newPosition);

    objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));
    BOOST_CHECK(sbVec3fEqual(workingMemoryController->getSbVec3fFromInstance(objectInstance), newPosition));
}

BOOST_AUTO_TEST_CASE(testGetSbRotationFromInstance)
{
    MemoryXEnvironment environment;
    IceInternal::Handle<WorkingMemoryController> workingMemoryController = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController();

    ObjectInstancePtr objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));

    SbRotation newRotation = SbRotation(8, 23, 6, 2);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    workingMemoryController->rotateTranslateObject("1", newRotation, newPosition);

    objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));
    BOOST_CHECK(sbRotationEqual(workingMemoryController->getSbRotationFromInstance(objectInstance), newRotation));
}
