/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::memoryxcontroller::PriorKnowledgeController
#define ARMARX_BOOST_TEST

#include <map>
#include <vector>

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/memoryxcontroller/MemoryXController.h>
#include <MemoryX/gui-plugins/SceneEditor/memoryxcontroller/PriorKnowledgeController.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>

using namespace memoryxcontroller;
using namespace memoryx;

BOOST_AUTO_TEST_CASE(testGetCollections)
{
    MemoryXEnvironment environment;
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    const NameList collectionNames = priorKnowledgeController->getCollectionNames();

    BOOST_CHECK_EQUAL(priorKnowledgeController->getCollectionNames().size(), 2);
    BOOST_CHECK_EQUAL((std::find(collectionNames.begin(), collectionNames.end(), "testdb.Objects1")) != collectionNames.end(), true);
    BOOST_CHECK_EQUAL((std::find(collectionNames.begin(), collectionNames.end(), "testdb.Objects2")) != collectionNames.end(), true);
}


BOOST_AUTO_TEST_CASE(testGetAllObjectClasses_ValidCollectionName)
{
    MemoryXEnvironment environment;
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    std::vector<ObjectClassPtr> vec = priorKnowledgeController->getAllObjectClassesPtr("testdb.Objects1");

    BOOST_CHECK_EQUAL(vec.size(), 3);
    BOOST_CHECK_EQUAL(vec[0]->getName(), "testObject1");
}

BOOST_AUTO_TEST_CASE(testGetAllObjectClasses_InvalidCollectionName)
{
    MemoryXEnvironment environment;
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    std::vector<ObjectClassPtr> vec = priorKnowledgeController->getAllObjectClassesPtr("blabla");
    BOOST_CHECK_EQUAL(vec.size(), 0);
}

BOOST_AUTO_TEST_CASE(testGetObjectClassPtr)
{
    MemoryXEnvironment environment;
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    BOOST_CHECK_EQUAL(priorKnowledgeController->getObjectClassPtr("testObject1", "testdb.Objects1")->getName(), "testObject1");
    BOOST_CHECK_EQUAL(priorKnowledgeController->getObjectClassPtr("testObject2", "testdb.Objects1")->getName(), "testObject2");

    ObjectClassPtr ptr = priorKnowledgeController->getObjectClassPtr("blabla", "testdb.Objects1");
    BOOST_CHECK_EQUAL((ptr == 0), true);
    ptr = priorKnowledgeController->getObjectClassPtr("testObject1", "blabla");
    BOOST_CHECK_EQUAL((ptr == 0), true);
    ptr = priorKnowledgeController->getObjectClassPtr("blabla", "blabla");
    BOOST_CHECK_EQUAL((ptr == 0), true);
}

BOOST_AUTO_TEST_CASE(testGetAllAttributes)
{
    MemoryXEnvironment environment;
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    ObjectClassPtr ptr = priorKnowledgeController->getObjectClassPtr("testObject1", "testdb.Objects1");
    std::map<std::string, std::string> attributes = priorKnowledgeController->getAllAttributes(ptr);

    BOOST_CHECK_EQUAL(attributes.size(), 6);
    BOOST_CHECK_EQUAL(attributes.at("IvFile"), "");
    BOOST_CHECK_EQUAL(attributes.at("ManipulationFile"), "");
    BOOST_CHECK_EQUAL(attributes.at("TestAttribute1"), "test1");
    BOOST_CHECK_EQUAL(attributes.at("featureFile"), "");

    ptr = priorKnowledgeController->getObjectClassPtr("testObject2", "testdb.Objects1");
    attributes = priorKnowledgeController->getAllAttributes(ptr);

    BOOST_CHECK_EQUAL(attributes.size(), 7);
    BOOST_CHECK_EQUAL(attributes.at("TestAttribute1"), "test2");
    BOOST_CHECK_EQUAL(attributes.at("TestAttribute2"), "test2.1");

    ptr = priorKnowledgeController->getObjectClassPtr("blabla", "testdb.Objects1");
    attributes = priorKnowledgeController->getAllAttributes(ptr);

    BOOST_CHECK_EQUAL(attributes.size(), 0);
}

BOOST_AUTO_TEST_CASE(testGetCollection)
{
    MemoryXEnvironment environment;
    boost::shared_ptr<PriorKnowledgeController> priorKnowledgeController = CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController();

    ObjectInstancePtr objectInstance = ObjectInstancePtr::dynamicCast(ENVIRONMENT(environment).objectInstancesPrx->getEntityById("1"));
    std::string collectionName = priorKnowledgeController->getCollection(objectInstance);

    BOOST_CHECK_EQUAL(collectionName, "testdb.Objects1");


    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->setWriteCollection(ENVIRONMENT(environment).storage->requestCollection("testdb.Objects2"));

    ObjectClassPtr objectClass = new ObjectClass();
    objectClass->setName("testObject4");
    objectClass->putAttribute("ManipulationFile", "");
    objectClass->putAttribute("TestAttribute3", "test2");
    objectClass->putAttribute("IvFile", "");
    objectClass->putAttribute("featureFile", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);

    ObjectInstancePtr objectInstance1 = new ObjectInstance("testInstance1");
    objectInstance1->addClass(objectClass->getName(), 1.0);
    ENVIRONMENT(environment).objectInstancesPrx->addEntity(objectInstance1);

    std::string collectionName1 = priorKnowledgeController->getCollection(objectInstance1);
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();

    BOOST_CHECK_EQUAL(collectionName1, "testdb.Objects2");
}
