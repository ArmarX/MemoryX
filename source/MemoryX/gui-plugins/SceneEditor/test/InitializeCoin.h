/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include <Inventor/Qt/SoQt.h>

typedef struct CoinFixtureCleanupGlobal
{
    CoinFixtureCleanupGlobal()
    {
        SoQt::init("Boost::Test");
    }

    ~CoinFixtureCleanupGlobal()
    {
        SoQt::done();
    }
} CoinFixtureCleanupGlobal;

#if  BOOST_VERSION  < 106200
BOOST_GLOBAL_FIXTURE(CoinFixtureCleanupGlobal)
#else
BOOST_GLOBAL_FIXTURE(CoinFixtureCleanupGlobal);
#endif
