/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

BOOST_AUTO_TEST_CASE(setManipulatorMode)
{
    controller::ControllerPtr controller = controller::Controller::create();

    controller->getScene()->getManipulatorManager()->setManipulatorMode(scene3D::ALL);
}

BOOST_AUTO_TEST_CASE(getManipulatorMode)
{
    controller::ControllerPtr controller = controller::Controller::create();

    controller->getScene()->getManipulatorManager()->setManipulatorMode(scene3D::ALL);

    BOOST_CHECK(controller->getScene()->getManipulatorManager()->getManipulatorMode() == scene3D::ALL);
}

BOOST_AUTO_TEST_CASE(applyManipulatorALL)
{
    controller::ControllerPtr controller = controller::Controller::create();
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));

    controller->getScene()->getManipulatorManager()->setManipulatorMode(scene3D::ALL);

    controller->getScene()->getManipulatorManager()->addManipulator(obj);
    controller->getScene()->getManipulatorManager()->applyManipulator(obj);
}

BOOST_AUTO_TEST_CASE(applyManipulatorTRANSLATION)
{
    controller::ControllerPtr controller = controller::Controller::create();
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));

    controller->getScene()->getManipulatorManager()->setManipulatorMode(scene3D::TRANSLATION);

    controller->getScene()->getManipulatorManager()->addManipulator(obj);
    controller->getScene()->getManipulatorManager()->applyManipulator(obj);
}

BOOST_AUTO_TEST_CASE(applyManipulatorROTATION)
{
    controller::ControllerPtr controller = controller::Controller::create();
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));

    controller->getScene()->getManipulatorManager()->setManipulatorMode(scene3D::ROTATION);

    controller->getScene()->getManipulatorManager()->addManipulator(obj);
    controller->getScene()->getManipulatorManager()->applyManipulator(obj);
}

BOOST_AUTO_TEST_CASE(applyManipulatorNONE)
{
    controller::ControllerPtr controller = controller::Controller::create();
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));

    controller->getScene()->getManipulatorManager()->setManipulatorMode(scene3D::NONE);

    controller->getScene()->getManipulatorManager()->addManipulator(obj);
    controller->getScene()->getManipulatorManager()->applyManipulator(obj);
}

