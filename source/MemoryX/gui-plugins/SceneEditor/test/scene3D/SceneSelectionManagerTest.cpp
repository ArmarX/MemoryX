/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneObject.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneGroup.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneSelectionManager.h>

BOOST_AUTO_TEST_CASE(selectObject)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneObjectPtr object1(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    controller->getScene()->getObjectManager()->addObject(object1);

    controller->getScene()->getSelectionManager()->addToSelection(object1);
    BOOST_CHECK(controller->getScene()->getSelectionManager()->isSelected(object1));
}

BOOST_AUTO_TEST_CASE(deselectObject)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneObjectPtr object1(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    controller->getScene()->getObjectManager()->addObject(object1);

    controller->getScene()->getSelectionManager()->addToSelection(object1);
    BOOST_CHECK(controller->getScene()->getSelectionManager()->isSelected(object1));

    controller->getScene()->getSelectionManager()->removeFromSelection(object1);
    BOOST_CHECK(!controller->getScene()->getSelectionManager()->isSelected(object1));
}

BOOST_AUTO_TEST_CASE(deselectAll)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneObjectPtr object1(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    controller->getScene()->getObjectManager()->addObject(object1);
    scene3D::SceneObjectPtr object2(new scene3D::SceneObject("testObject2", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    controller->getScene()->getObjectManager()->addObject(object2);

    controller->getScene()->getSelectionManager()->addToSelection(object1);
    BOOST_CHECK(controller->getScene()->getSelectionManager()->isSelected(object1));
    controller->getScene()->getSelectionManager()->addToSelection(object2);
    BOOST_CHECK(controller->getScene()->getSelectionManager()->isSelected(object2));

    controller->getScene()->getSelectionManager()->deselectAll();

    BOOST_CHECK(!controller->getScene()->getSelectionManager()->isSelected(object1));
    BOOST_CHECK(!controller->getScene()->getSelectionManager()->isSelected(object2));
}

BOOST_AUTO_TEST_CASE(addAndRemoveGroup)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneObjectPtr object1(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    controller->getScene()->getObjectManager()->addObject(object1);
    scene3D::SceneObjectPtr object2(new scene3D::SceneObject("testObject2", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    controller->getScene()->getObjectManager()->addObject(object2);

    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    group->addObject(object1);
    group->addObject(object2);

    controller->getScene()->getSelectionManager()->addToSelection(group);

    BOOST_CHECK(controller->getScene()->getSelectionManager()->isSelected(object1));
    BOOST_CHECK(controller->getScene()->getSelectionManager()->isSelected(object2));

    controller->getScene()->getSelectionManager()->removeFromSelection(group);

    BOOST_CHECK(!controller->getScene()->getSelectionManager()->isSelected(object1));
    BOOST_CHECK(!controller->getScene()->getSelectionManager()->isSelected(object2));
}
