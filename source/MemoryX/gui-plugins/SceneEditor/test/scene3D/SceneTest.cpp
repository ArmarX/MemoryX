/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <Inventor/nodes/SoPerspectiveCamera.h>

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/Scene.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

BOOST_AUTO_TEST_CASE(createScene)
{
    controller::ControllerPtr controller = controller::Controller::create();
    //We create another scene here, even though controller already created one
    scene3D::ScenePtr scene(scene3D::Scene::create(controller));
    BOOST_CHECK(scene);
}

BOOST_AUTO_TEST_CASE(registerCamera)
{
    controller::ControllerPtr controller = controller::Controller::create();
    BOOST_CHECK(controller->getScene()->registerCamera(new SoPerspectiveCamera));
}

BOOST_AUTO_TEST_CASE(getObjectManager)
{
    controller::ControllerPtr controller = controller::Controller::create();
    BOOST_CHECK(controller->getScene()->getObjectManager());
}

BOOST_AUTO_TEST_CASE(getGroupManager)
{
    controller::ControllerPtr controller = controller::Controller::create();
    BOOST_CHECK(controller->getScene()->getGroupManager());
}

BOOST_AUTO_TEST_CASE(getSelectionManager)
{
    controller::ControllerPtr controller = controller::Controller::create();
    BOOST_CHECK(controller->getScene()->getSelectionManager());
}

BOOST_AUTO_TEST_CASE(getManipulatorManager)
{
    controller::ControllerPtr controller = controller::Controller::create();
    BOOST_CHECK(controller->getScene()->getManipulatorManager());
}
