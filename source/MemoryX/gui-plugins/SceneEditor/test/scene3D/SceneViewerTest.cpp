/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <QWidget>

#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneViewer.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/ClassDefinitions.h>


BOOST_AUTO_TEST_CASE(createSceneViewer)
{
    QWidget* widget = new QWidget();
    controller::ControllerPtr controller = controller::Controller::create();
    scene3D::SceneViewer* viewer = new scene3D::SceneViewer(controller, widget);

    BOOST_CHECK(viewer);

    delete (widget);
    delete (viewer);
}
