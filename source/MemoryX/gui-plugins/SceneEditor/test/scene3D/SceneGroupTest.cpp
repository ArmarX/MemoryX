/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneGroup.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneObject.h>

BOOST_AUTO_TEST_CASE(createSceneGroup)
{
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    BOOST_CHECK(group);
}

BOOST_AUTO_TEST_CASE(getGroupId)
{
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    BOOST_CHECK(!group->getGroupId().compare("testGroup"));
}

BOOST_AUTO_TEST_CASE(testObjectContained)
{
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    scene3D::SceneObjectPtr containedObject(new scene3D::SceneObject("containedObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    scene3D::SceneObjectPtr notContainedObject(new scene3D::SceneObject("notContainedObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    group->addObject(containedObject);

    BOOST_CHECK(group->contains(containedObject));
    BOOST_CHECK(!group->contains(notContainedObject));
}

BOOST_AUTO_TEST_CASE(addObjectToGroup)
{
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    scene3D::SceneObjectPtr containedObject(new scene3D::SceneObject("containedObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    group->addObject(containedObject);

    BOOST_CHECK(group->contains(containedObject));
}



BOOST_AUTO_TEST_CASE(removeObjectFromGroup)
{
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    scene3D::SceneObjectPtr containedObject(new scene3D::SceneObject("containedObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    group->addObject(containedObject);
    BOOST_CHECK(group->contains(containedObject));
    group->removeObject(containedObject);
    BOOST_CHECK(!group->contains(containedObject));
}

BOOST_AUTO_TEST_CASE(clearGroup)
{
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    scene3D::SceneObjectPtr containedObject(new scene3D::SceneObject("containedObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    scene3D::SceneObjectPtr containedObject2(new scene3D::SceneObject("containedObject2", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    group->addObject(containedObject);
    group->addObject(containedObject2);
    BOOST_CHECK(group->contains(containedObject));
    BOOST_CHECK(group->contains(containedObject2));
    group->clearGroup();
    BOOST_CHECK(!group->contains(containedObject));
    BOOST_CHECK(!group->contains(containedObject2));
}
