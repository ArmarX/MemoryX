/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneGroup.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneGroupManager.h>

BOOST_AUTO_TEST_CASE(addGroup)
{
    controller::ControllerPtr controller = controller::Controller::create();

    controller->getScene()->getGroupManager()->addGroup(scene3D::SceneGroupPtr(new scene3D::SceneGroup("testGroup")));
}

BOOST_AUTO_TEST_CASE(addGroupTwice)
{
    controller::ControllerPtr controller = controller::Controller::create();

    controller->getScene()->getGroupManager()->addGroup(scene3D::SceneGroupPtr(new scene3D::SceneGroup("testGroup")));
    BOOST_CHECK_THROW(controller->getScene()->getGroupManager()->addGroup(scene3D::SceneGroupPtr(new scene3D::SceneGroup("testGroup"))), std::logic_error);
}

BOOST_AUTO_TEST_CASE(getGroupByIdExist)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));

    controller->getScene()->getGroupManager()->addGroup(group);

    BOOST_CHECK(controller->getScene()->getGroupManager()->getGroupById("testGroup"));
}

BOOST_AUTO_TEST_CASE(getGroupByIdNotExist)
{
    controller::ControllerPtr controller = controller::Controller::create();

    BOOST_CHECK(!controller->getScene()->getGroupManager()->getGroupById("testGroup"));
}

BOOST_AUTO_TEST_CASE(removeGroup)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));

    controller->getScene()->getGroupManager()->addGroup(group);

    controller->getScene()->getGroupManager()->removeGroup(group);

    BOOST_CHECK(!controller->getScene()->getGroupManager()->getGroupById("testGroup"));
}

BOOST_AUTO_TEST_CASE(removeGroupNotExist)
{
    controller::ControllerPtr controller = controller::Controller::create();

    BOOST_CHECK_THROW(controller->getScene()->getGroupManager()->removeGroup(scene3D::SceneGroupPtr(new scene3D::SceneGroup("testGroup"))), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(removeGroupNull)
{
    controller::ControllerPtr controller = controller::Controller::create();

    BOOST_CHECK_THROW(controller->getScene()->getGroupManager()->removeGroup(scene3D::SceneGroupPtr()), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(renameGroup)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));

    controller->getScene()->getGroupManager()->addGroup(group);

    controller->getScene()->getGroupManager()->renameGroup(group, "newTestId");

    BOOST_CHECK(controller->getScene()->getGroupManager()->getGroupById("newTestId"));
    BOOST_CHECK(!group->getGroupId().compare("newTestId"));
}

BOOST_AUTO_TEST_CASE(renameGroupIdAlreadyExists)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneGroupPtr group(new scene3D::SceneGroup("testGroup"));
    scene3D::SceneGroupPtr group2(new scene3D::SceneGroup("newTestId"));

    controller->getScene()->getGroupManager()->addGroup(group);
    controller->getScene()->getGroupManager()->addGroup(group2);

    BOOST_CHECK_THROW(controller->getScene()->getGroupManager()->renameGroup(group, "newTestId"), std::invalid_argument);
}
