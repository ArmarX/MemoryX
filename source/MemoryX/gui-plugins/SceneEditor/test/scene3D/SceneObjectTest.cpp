/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneObject.h>

BOOST_AUTO_TEST_CASE(createSceneObject)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    BOOST_CHECK(obj);
}

BOOST_AUTO_TEST_CASE(createSceneObjectCheckInitialTranslation)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    BOOST_CHECK(obj->getTranslation().equals(SbVec3f(0, 0, 0), 0));
}

BOOST_AUTO_TEST_CASE(createSceneObjectCheckInitialRotation)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    BOOST_CHECK(obj->getRotation().equals(SbRotation(0, 0, 0, 1), 0));
}

BOOST_AUTO_TEST_CASE(SceneObjectCheckSetTranslation)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    SbVec3f translation(0.3, 2, 4.5);
    obj->setTranslation(translation);
    BOOST_CHECK(obj->getTranslation().equals(translation, 0));
}

BOOST_AUTO_TEST_CASE(SceneObjectCheckSetRotation)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    SbRotation rotation(0.3, 2, 4.5, 1);
    obj->setRotation(rotation);
    BOOST_CHECK(obj->getRotation().equals(rotation, 0));
}

BOOST_AUTO_TEST_CASE(SceneObjectCheckgetObjectId)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    BOOST_CHECK(!obj->getObjectId().compare("testObject"));
}

BOOST_AUTO_TEST_CASE(SceneObjectCheckgetClassId)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    BOOST_CHECK(!obj->getClassId().compare("testClass"));
}

BOOST_AUTO_TEST_CASE(SceneObjectCheckgetCollectionId)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    BOOST_CHECK(!obj->getCollection().compare("testCollection"));
}

BOOST_AUTO_TEST_CASE(SceneObjectChecksetObjectId)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    std::string test = "newTestObjectId";
    obj->setObjectId(test);
    BOOST_CHECK(!obj->getObjectId().compare("newTestObjectId"));
}

BOOST_AUTO_TEST_CASE(SceneObjectChecksetClassId)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    std::string test = "newTestClassId";
    obj->setClassId(test);
    BOOST_CHECK(!obj->getClassId().compare("newTestClassId"));
}

BOOST_AUTO_TEST_CASE(SceneObjectChecksetCollectionId)
{
    scene3D::SceneObjectPtr obj(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    std::string test = "newTestCollectionId";
    obj->setCollection(test);
    BOOST_CHECK(!obj->getCollection().compare("newTestCollectionId"));
}
