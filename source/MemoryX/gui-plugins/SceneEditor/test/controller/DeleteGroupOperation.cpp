/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::DeleteGroupOperation
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/DeleteGroupOperation.h>

class PublicDeleteGroupOperation : public controller::DeleteGroupOperation
{
public:
    PublicDeleteGroupOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& groupName) :
        DeleteGroupOperation(memoryXController, scene, groupName)
    {
    }

    virtual void executeOnWorkingMemory()
    {
        controller::DeleteGroupOperation::executeOnWorkingMemory();
    }

    virtual void executeOnScene()
    {
        controller::DeleteGroupOperation::executeOnScene();
    }
};

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::OperationPtr operation(new controller::DeleteGroupOperation(c->getMemoryXController(), c->getScene(), "group 1"));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
}

BOOST_AUTO_TEST_CASE(createInverseOperation)
{
    controller::ControllerPtr c = controller::Controller::create();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::DeleteGroupOperation(c->getMemoryXController(), c->getScene(), "group 1"));
    operations1->push_back(operation1);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation2 = operation1->createInverseOperation();
    operations2->push_back(operation2);

    BOOST_CHECK(operation2);
    BOOST_CHECK(operation2.get());

    c->getScene()->getGroupManager()->addGroup(scene3D::SceneGroupPtr(new scene3D::SceneGroup("group 1")));
    BOOST_CHECK(c->getScene()->getGroupManager()->getGroupById("group 1") != NULL);

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations1);

    BOOST_CHECK(c->getScene()->getGroupManager()->getGroupById("group 1") == NULL);

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations2);
    BOOST_CHECK(c->getScene()->getGroupManager()->getGroupById("group 1") != NULL);
}

BOOST_AUTO_TEST_CASE(executeOnWorkingMemory)
{
    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<PublicDeleteGroupOperation> operation(new PublicDeleteGroupOperation(c->getMemoryXController(), c->getScene(), "group 1"));
    //just check if no exception is thrown
    operation->executeOnWorkingMemory();
}

BOOST_AUTO_TEST_CASE(executeOnScene)
{
    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<PublicDeleteGroupOperation> operation(new PublicDeleteGroupOperation(c->getMemoryXController(), c->getScene(), "2345lhsaf!@#534#$^% ##@$%^?><'\"\\]}{["));
    c->getScene()->getGroupManager()->addGroup(scene3D::SceneGroupPtr(new scene3D::SceneGroup("2345lhsaf!@#534#$^% ##@$%^?><'\"\\]}{[")));
    BOOST_CHECK(c->getScene()->getGroupManager()->getGroupById("2345lhsaf!@#534#$^% ##@$%^?><'\"\\]}{[") != NULL);
    operation->executeOnScene();
    BOOST_CHECK(c->getScene()->getGroupManager()->getGroupById("2345lhsaf!@#534#$^% ##@$%^?><'\"\\]}{[") == NULL);
}

BOOST_AUTO_TEST_CASE(executeOnSceneExceptionOnNotExcistingGroup)
{
    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<PublicDeleteGroupOperation> operation(new PublicDeleteGroupOperation(c->getMemoryXController(), c->getScene(), "group 1"));
    BOOST_CHECK(c->getScene()->getGroupManager()->getGroupById("group 1") == NULL);
    BOOST_CHECK_THROW(operation->executeOnScene(), std::exception);
}
