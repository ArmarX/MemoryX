/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::RotateTranslateOperation
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/RotateTranslateOperation.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>
#include <QtCore/qprocess.h>
#include <QtDesigner/abstractpropertyeditor.h>

class PublicRotateTranslateOperation : public controller::RotateTranslateOperation
{
public:
    PublicRotateTranslateOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& objectId, const SbRotation& oldRotation, const SbRotation& newRotation, const SbVec3f& oldPosition, const SbVec3f& newPosition) :
        RotateTranslateOperation(memoryXController, scene, objectId, oldRotation, newRotation, oldPosition, newPosition)
    {
    }

    virtual void executeOnWorkingMemory()
    {
        controller::RotateTranslateOperation::executeOnWorkingMemory();
    }

    virtual void executeOnScene()
    {
        controller::RotateTranslateOperation::executeOnScene();
    }
};

static inline bool floatEqual(float f1, float f2)
{
    return (f1 < f2 ? f2 - f1 : f1 - f2) < 0.00001f;
}

static inline bool sbRotationEqual(SbRotation r1, SbRotation r2)
{
    float v1x, v1y, v1z, v1w, v2x, v2y, v2z, v2w;
    r1.getValue(v1x, v1y, v1z, v1w);
    r2.getValue(v2x, v2y, v2z, v2w);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z) && floatEqual(v1w, v2w);
}

static inline bool sbVec3fEqual(SbVec3f v1, SbVec3f v2)
{
    float v1x, v1y, v1z, v2x, v2y, v2z;
    v1.getValue(v1x, v1y, v1z);
    v2.getValue(v2x, v2y, v2z);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z);
}

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::OperationPtr operation(new controller::RotateTranslateOperation(c->getMemoryXController(), c->getScene(), "Object 1", SbRotation(), SbRotation(), SbVec3f(), SbVec3f()));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
}

BOOST_AUTO_TEST_CASE(createInverseOperationScene)
{
    controller::ControllerPtr c = controller::Controller::create();

    SbRotation oldRotation = SbRotation(1, 1, 1, 1);
    SbRotation newRotation = SbRotation(8, 23, 6, 2);

    SbVec3f oldPosition = SbVec3f(2, 3, 4);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    scene3D::SceneObjectPtr object = scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    c->getScene()->getObjectManager()->addObject(object);

    object->setRotation(oldRotation);
    object->setTranslation(oldPosition);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::RotateTranslateOperation(c->getMemoryXController(), c->getScene(), "Object 1", oldRotation, newRotation, oldPosition, newPosition));
    operations1->push_back(operation1);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation2 = operation1->createInverseOperation();
    operations2->push_back(operation2);

    BOOST_CHECK(operation2);
    BOOST_CHECK(operation2.get());

    BOOST_CHECK(sbRotationEqual(object->getRotation(), oldRotation));
    BOOST_CHECK(sbVec3fEqual(object->getTranslation(), oldPosition));

    c->execute(controller::Controller::EXECUTE_ON_SCENE, operations1);

    BOOST_CHECK(sbRotationEqual(object->getRotation(), newRotation));
    BOOST_CHECK(sbVec3fEqual(object->getTranslation(), newPosition));

    c->execute(controller::Controller::EXECUTE_ON_SCENE, operations2);

    BOOST_CHECK(sbRotationEqual(object->getRotation(), oldRotation));
    BOOST_CHECK(sbVec3fEqual(object->getTranslation(), oldPosition));
}

BOOST_AUTO_TEST_CASE(executeOnSceneObjectDoesNotExist)
{
    controller::ControllerPtr c = controller::Controller::create();
    BOOST_CHECK(!c->getScene()->getObjectManager()->getObjectById("Object 1"));
    boost::shared_ptr<PublicRotateTranslateOperation> operation(new PublicRotateTranslateOperation(c->getMemoryXController(), c->getScene(), "Object 1", SbRotation(), SbRotation(), SbVec3f(), SbVec3f()));
    BOOST_CHECK_THROW(operation->executeOnScene(), std::exception);
}

BOOST_AUTO_TEST_CASE(executeOnScene)
{
    controller::ControllerPtr c = controller::Controller::create();
    SbRotation oldRotation = SbRotation(1, 1, 1, 1);
    SbRotation newRotation = SbRotation(8, 23, 6, 2);

    SbVec3f oldPosition = SbVec3f(2, 3, 4);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    scene3D::SceneObjectPtr object = scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator));
    c->getScene()->getObjectManager()->addObject(object);

    object->setRotation(oldRotation);
    object->setTranslation(oldPosition);

    boost::shared_ptr<PublicRotateTranslateOperation> operation(new PublicRotateTranslateOperation(c->getMemoryXController(), c->getScene(), "Object 1", oldRotation, newRotation, oldPosition, newPosition));
    BOOST_CHECK(sbRotationEqual(object->getRotation(), oldRotation));
    BOOST_CHECK(sbVec3fEqual(object->getTranslation(), oldPosition));
    operation->executeOnScene();
    BOOST_CHECK(sbRotationEqual(object->getRotation(), newRotation));
    BOOST_CHECK(sbVec3fEqual(object->getTranslation(), newPosition));
}

BOOST_AUTO_TEST_CASE(executeOnWorkingMemoryObjectDoesNotExist)
{
    MemoryXEnvironment environment;
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();
    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));
    memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById("Object 1"));
    BOOST_CHECK(!object);
    boost::shared_ptr<PublicRotateTranslateOperation> operation(new PublicRotateTranslateOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), "Object 1", SbRotation(), SbRotation(), SbVec3f(), SbVec3f()));
    BOOST_CHECK_THROW(operation->executeOnWorkingMemory(), std::exception);
}

BOOST_AUTO_TEST_CASE(executeOnWorkingMemory)
{
    MemoryXEnvironment environment;
    SbRotation oldRotation = SbRotation(1, 1, 1, 1);
    SbRotation newRotation = SbRotation(8, 23, 6, 2);

    SbVec3f oldPosition = SbVec3f(2, 3, 4);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    memoryx::ObjectClassPtr objectClass = new memoryx::ObjectClass();
    objectClass->setName("testObject1");
    objectClass->putAttribute("IvFile", "");
    objectClass->putAttribute("IvFileCollision", "");
    objectClass->putAttribute("ManipulationFile", "");
    objectClass->putAttribute("parentClasses", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);

    std::string objectId = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->addObjectInstance("testObject1", objectClass);

    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));

    CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->rotateTranslateObject(objectId, oldRotation, oldPosition);

    boost::shared_ptr<PublicRotateTranslateOperation> operation(new PublicRotateTranslateOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), objectId, oldRotation, newRotation, oldPosition, newPosition));

    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), oldRotation));
        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), oldPosition));
    }
    operation->executeOnWorkingMemory();
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), newRotation));
        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), newPosition));
    }
}

BOOST_AUTO_TEST_CASE(createInverseOperationWorkingMemory)
{
    MemoryXEnvironment environment;

    SbRotation oldRotation = SbRotation(1, 1, 1, 1);
    SbRotation newRotation = SbRotation(8, 23, 6, 2);

    SbVec3f oldPosition = SbVec3f(2, 3, 4);
    SbVec3f newPosition = SbVec3f(42, 21, 4);

    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    memoryx::ObjectClassPtr objectClass = new memoryx::ObjectClass();
    objectClass->setName("testObject1");
    objectClass->putAttribute("IvFile", "");
    objectClass->putAttribute("IvFileCollision", "");
    objectClass->putAttribute("ManipulationFile", "");
    objectClass->putAttribute("parentClasses", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    std::string objectId = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->addObjectInstance("testObject1", objectClass);

    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));

    CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->rotateTranslateObject(objectId, oldRotation, oldPosition);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::RotateTranslateOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), objectId, oldRotation, newRotation, oldPosition, newPosition));
    operations1->push_back(operation1);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation2 = operation1->createInverseOperation();
    operations2->push_back(operation2);

    BOOST_CHECK(operation2);
    BOOST_CHECK(operation2.get());

    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), oldRotation));
        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), oldPosition));
    }

    CONTROLLER(environment)->execute(controller::Controller::EXECUTE_ON_WM, operations1);

    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), newRotation));
        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), newPosition));
    }

    CONTROLLER(environment)->execute(controller::Controller::EXECUTE_ON_WM, operations2);

    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), oldRotation));
        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), oldPosition));
    }
}
