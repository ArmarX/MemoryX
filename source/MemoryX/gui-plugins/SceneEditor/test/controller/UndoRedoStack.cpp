/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::UndoRedoStack
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include <MemoryX/gui-plugins/SceneEditor/controller/UndoRedoStack.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Operation.h>

namespace controller
{
    class Operation;
}

class TestOperation1Inverse : public controller::Operation
{
public:
    TestOperation1Inverse() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation1Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

class TestOperation1 : public controller::Operation
{
public:
    TestOperation1() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    TestOperation1(const std::string& s) :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>(), s)
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation1Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

class TestOperation2Inverse : public controller::Operation
{
public:
    TestOperation2Inverse() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation2Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

class TestOperation2 : public controller::Operation
{
public:
    TestOperation2() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation2Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

BOOST_AUTO_TEST_CASE(push)
{
    controller::UndoRedoStack s;
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    s.push(operations);
}

BOOST_AUTO_TEST_CASE(pushIfCanRedo)
{
    controller::UndoRedoStack s;
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    s.push(operations1);
    s.undo();
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    s.push(operations2);
}

BOOST_AUTO_TEST_CASE(canUndo)
{
    controller::UndoRedoStack s;
    BOOST_CHECK(!s.canUndo());
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    s.push(operations);
    BOOST_CHECK(s.canUndo());
}

BOOST_AUTO_TEST_CASE(undo)
{
    controller::UndoRedoStack s;
    boost::shared_ptr<std::vector<controller::OperationPtr>> allOperations[42];

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        for (int i = 0; i < 17; ++i)
        {
            controller::OperationPtr c;

            if (i % 5 == 0)
            {
                c = boost::shared_ptr<TestOperation1>(new TestOperation1());
            }
            else
            {
                c = boost::shared_ptr<TestOperation2>(new TestOperation2());
            }

            operations->push_back(c);
        }

        s.push(operations);
        allOperations[j] = operations;
    }

    for (int j = 41; j >= 0; --j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operations = allOperations[j];
        BOOST_CHECK(s.canUndo());
        boost::shared_ptr <std::vector<controller::OperationPtr>> operationsFromAction = s.undo();
        BOOST_CHECK_EQUAL(operations->size(), operationsFromAction->size());

        for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
        {
            if (i % 5 == 0)
            {
                controller::Operation* o = ((*operationsFromAction)[operationsFromAction->size() - i - 1]).get();
                BOOST_CHECK(dynamic_cast<TestOperation1Inverse*>(o) != NULL);
            }
            else
            {
                controller::Operation* o = ((*operationsFromAction)[operationsFromAction->size() - i - 1]).get();
                BOOST_CHECK(dynamic_cast<TestOperation2Inverse*>(o) != NULL);
            }
        }
    }

    BOOST_CHECK(!s.canUndo());
}

BOOST_AUTO_TEST_CASE(undoOnEmptyStack)
{
    controller::UndoRedoStack s;
    boost::shared_ptr <std::vector<controller::OperationPtr>> operations = s.undo();
    BOOST_CHECK(operations);
    BOOST_CHECK(operations->empty());
}

BOOST_AUTO_TEST_CASE(canRedo)
{
    controller::UndoRedoStack s;
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    s.push(operations);
    BOOST_CHECK(!s.canRedo());
    s.undo();
    BOOST_CHECK(s.canRedo());
    s.redo();
    BOOST_CHECK(!s.canRedo());
}

BOOST_AUTO_TEST_CASE(redo)
{
    controller::UndoRedoStack s;
    boost::shared_ptr<std::vector<controller::OperationPtr>> allOperations[42];

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        for (int i = 0; i < 17; ++i)
        {
            controller::OperationPtr c;

            if (i % 5 == 0)
            {
                c = boost::shared_ptr<TestOperation1>(new TestOperation1());
            }
            else
            {
                c = boost::shared_ptr<TestOperation2>(new TestOperation2());
            }

            operations->push_back(c);
        }

        s.push(operations);
        allOperations[j] = operations;
    }

    for (int j = 0; j < 21; ++j)
    {
        s.undo();
    }

    for (int j = 0; j < 21; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operations = allOperations[21 + j];
        BOOST_CHECK(s.canRedo());
        boost::shared_ptr <std::vector<controller::OperationPtr>> operationsFromAction = s.redo();
        BOOST_CHECK_EQUAL(operations->size(), operationsFromAction->size());

        for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
        {
            if (i % 5 == 0)
            {
                controller::Operation* o = ((*operationsFromAction)[i]).get();
                BOOST_CHECK(dynamic_cast<TestOperation1*>(o) != NULL);
            }
            else
            {
                controller::Operation* o = ((*operationsFromAction)[i]).get();
                BOOST_CHECK(dynamic_cast<TestOperation2*>(o) != NULL);
            }
        }
    }

    BOOST_CHECK(!s.canRedo());
}

BOOST_AUTO_TEST_CASE(redoOnEmptyStack)
{
    controller::UndoRedoStack s;
    boost::shared_ptr <std::vector<controller::OperationPtr>> operations1 = s.redo();
    BOOST_CHECK(operations1);
    BOOST_CHECK(operations1->empty());
}

BOOST_AUTO_TEST_CASE(clear)
{
    controller::UndoRedoStack s;
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    s.push(operations);
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    s.push(operations1);
    s.undo();
    BOOST_CHECK(s.canUndo());
    BOOST_CHECK(s.canRedo());
    s.clear();
    BOOST_CHECK(!s.canUndo());
    BOOST_CHECK(!s.canRedo());
}

BOOST_AUTO_TEST_CASE(updateObjectId)
{
    controller::UndoRedoStack s;
    std::string allIds[42][17];

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        for (int i = 0; i < 17; ++i)
        {
            allIds[j][i] = "" + (j ^ i);
            controller::OperationPtr c = boost::shared_ptr<TestOperation1>(new TestOperation1(allIds[j][i]));
            operations->push_back(c);
        }

        s.push(operations);
    }

    for (int j = 0; j < 21; ++j)
    {
        s.undo();
    }

    s.updateObjectId("" + (21 ^ 7), "1337");

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operationsFromAction = s.undo();

        for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
        {
            if ((*operationsFromAction)[i]->getObjectId() == "" + (21 ^ 7))
            {
                allIds[j][i] = "1337";
            }
        }
    }

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operationsFromAction = s.undo();

        for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
        {
            BOOST_CHECK_EQUAL((*operationsFromAction)[i]->getObjectId(), allIds[j][i]);
        }
    }
}

BOOST_AUTO_TEST_CASE(updateObjectIdSwap)
{
    controller::UndoRedoStack s;
    std::string allIds[42][17];

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        for (int i = 0; i < 17; ++i)
        {
            allIds[j][i] = "" + (j ^ i);
            controller::OperationPtr c = boost::shared_ptr<TestOperation1>(new TestOperation1(allIds[j][i]));
            operations->push_back(c);
        }

        s.push(operations);
    }

    for (int j = 0; j < 21; ++j)
    {
        s.undo();
    }

    s.updateObjectId("" + (21 ^ 7), "" + (21 ^ 5));

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operationsFromAction = s.undo();

        for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
        {
            if ((*operationsFromAction)[i]->getObjectId() == "" + (21 ^ 7))
            {
                allIds[j][i] = "" + (21 ^ 5);
            }
            else if ((*operationsFromAction)[i]->getObjectId() == "" + (21 ^ 5))
            {
                allIds[j][i] = "" + (21 ^ 7);
            }
        }
    }

    for (int j = 0; j < 42; ++j)
    {
        boost::shared_ptr <std::vector<controller::OperationPtr>> operationsFromAction = s.undo();

        for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
        {
            BOOST_CHECK_EQUAL((*operationsFromAction)[i]->getObjectId(), allIds[j][i]);
        }
    }
}