/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::RemoveOperation
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/RemoveOperation.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>
#include <qgesture.h>

class PublicRemoveOperation : public controller::RemoveOperation
{
public:
    PublicRemoveOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& objectId) :
        RemoveOperation(memoryXController, scene, objectId)
    {
    }

    virtual void executeOnWorkingMemory()
    {
        controller::RemoveOperation::executeOnWorkingMemory();
    }

    virtual void executeOnScene()
    {
        controller::RemoveOperation::executeOnScene();
    }
};

static inline bool floatEqual(float f1, float f2)
{
    return (f1 < f2 ? f2 - f1 : f1 - f2) < 0.00001f;
}

static inline bool sbRotationEqual(SbRotation r1, SbRotation r2)
{
    float v1x, v1y, v1z, v1w, v2x, v2y, v2z, v2w;
    r1.getValue(v1x, v1y, v1z, v1w);
    r2.getValue(v2x, v2y, v2z, v2w);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z) && floatEqual(v1w, v2w);
}

static inline bool sbVec3fEqual(SbVec3f v1, SbVec3f v2)
{
    float v1x, v1y, v1z, v2x, v2y, v2z;
    v1.getValue(v1x, v1y, v1z);
    v2.getValue(v2x, v2y, v2z);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z);
}

static std::string addObjectToEnvironment(MemoryXEnvironment& environment, SbRotation& rotation, SbVec3f& position)
{
    memoryx::ObjectClassPtr objectClass = new memoryx::ObjectClass();
    objectClass->setName("testObject1");
    objectClass->putAttribute("IvFile", "");
    objectClass->putAttribute("IvFileCollision", "");
    objectClass->putAttribute("ManipulationFile", "");
    objectClass->putAttribute("parentClasses", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);

    std::string objectId = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->addObjectInstance("testObject1", objectClass);

    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));
    memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));

    CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->rotateTranslateObject(objectId, rotation, position);

    scene3D::SceneObjectPtr sceneObject = scene3D::SceneObjectPtr(new scene3D::SceneObject(objectId, "testObject1", CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController()->getCollection(object), new SoSeparator, new SoSeparator));
    CONTROLLER(environment)->getScene()->getObjectManager()->addObject(sceneObject);
    sceneObject->setRotation(rotation);
    sceneObject->setTranslation(position);

    return objectId;
}

BOOST_AUTO_TEST_CASE(constructor)
{
    MemoryXEnvironment environment;
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    SbRotation rotation = SbRotation(1, 1, 1, 1);
    SbVec3f position = SbVec3f(2, 3, 4);

    std::string objectId = addObjectToEnvironment(environment, rotation, position);

    controller::OperationPtr operation(new controller::RemoveOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), objectId));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
}

BOOST_AUTO_TEST_CASE(createInverseOperation)
{
    MemoryXEnvironment environment;
    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    SbRotation rotation = SbRotation(1, 1, 1, 1);
    SbVec3f position = SbVec3f(2, 3, 4);

    std::string objectId = addObjectToEnvironment(environment, rotation, position);


    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::RemoveOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), objectId));
    operations1->push_back(operation1);

    BOOST_CHECK(CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId));
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), rotation));
        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), position));
        scene3D::SceneObjectPtr sceneObject = CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId);
        BOOST_CHECK(sbRotationEqual(sceneObject->getRotation(), rotation));
        BOOST_CHECK(sbVec3fEqual(sceneObject->getTranslation(), position));
    }
    CONTROLLER(environment)->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations1);
    BOOST_CHECK(!CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId));
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(!object);
    }

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation2 = operation1->createInverseOperation();
    operations2->push_back(operation2);

    BOOST_CHECK(operation2);
    BOOST_CHECK(operation2.get());


    //TODO: add iv file to object class
    BOOST_CHECK(!CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId));
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(!object);
    }
    //    CONTROLLER(environment)->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations2);
    //    BOOST_CHECK(CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId));
    //    {
    //        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
    //        BOOST_CHECK(object);
    //        BOOST_CHECK(sbRotationEqual(memoryxcontroller::WorkingMemoryController::getSbRotationFromInstance(object), rotation));
    //        BOOST_CHECK(sbVec3fEqual(memoryxcontroller::WorkingMemoryController::getSbVec3fFromInstance(object), position));
    //        scene3D::SceneObjectPtr sceneObject = CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId);
    //        BOOST_CHECK(sbRotationEqual(sceneObject->getRotation(), rotation));
    //        BOOST_CHECK(sbVec3fEqual(sceneObject->getTranslation(), position));
    //    }
}

BOOST_AUTO_TEST_CASE(createInverseOperationNotExecuted)
{
    MemoryXEnvironment environment;
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    SbRotation rotation = SbRotation(1, 1, 1, 1);
    SbVec3f position = SbVec3f(2, 3, 4);

    std::string objectId = addObjectToEnvironment(environment, rotation, position);


    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::RemoveOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), objectId));
    operations1->push_back(operation1);

    BOOST_CHECK_THROW(controller::OperationPtr operation2 = operation1->createInverseOperation(), std::exception);
}

BOOST_AUTO_TEST_CASE(executeOnScene)
{
    MemoryXEnvironment environment;
    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    SbRotation rotation = SbRotation(1, 1, 1, 1);
    SbVec3f position = SbVec3f(2, 3, 4);

    std::string objectId = addObjectToEnvironment(environment, rotation, position);


    boost::shared_ptr<PublicRemoveOperation> operation(new PublicRemoveOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), objectId));
    BOOST_CHECK(CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId));
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
    }
    operation->executeOnScene();
    BOOST_CHECK(!CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId));
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(object);
    }
}

BOOST_AUTO_TEST_CASE(executeOnSceneObjectDoesNotExist)
{
    MemoryXEnvironment environment;
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();


    boost::shared_ptr<PublicRemoveOperation> operation(new PublicRemoveOperation(CONTROLLER(environment)->getMemoryXController(), CONTROLLER(environment)->getScene(), "Object 1"));
    BOOST_CHECK(!CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById("Object 1"));
    BOOST_CHECK_THROW(operation->executeOnScene(), std::exception);
}
