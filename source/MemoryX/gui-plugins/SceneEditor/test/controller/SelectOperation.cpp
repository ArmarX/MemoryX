/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::SelectOperation
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/SelectOperation.h>

class PublicSelectOperation : public controller::SelectOperation
{
public:
    PublicSelectOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& objectId) :
        SelectOperation(memoryXController, scene, objectId)
    {
    }

    virtual void executeOnWorkingMemory()
    {
        controller::SelectOperation::executeOnWorkingMemory();
    }

    virtual void executeOnScene()
    {
        controller::SelectOperation::executeOnScene();
    }
};

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::OperationPtr operation(new controller::SelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
}

BOOST_AUTO_TEST_CASE(createInverseOperation)
{
    controller::ControllerPtr c = controller::Controller::create();

    //create sceneObject
    c->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator)));

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::SelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    operations1->push_back(operation1);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation2 = operation1->createInverseOperation();
    operations2->push_back(operation2);

    BOOST_CHECK(operation2);
    BOOST_CHECK(operation2.get());

    BOOST_CHECK(!c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations1);
    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations2);
    BOOST_CHECK(!c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));
}

BOOST_AUTO_TEST_CASE(createInverseOperationIfObjectWasSelected)
{
    controller::ControllerPtr c = controller::Controller::create();

    //create sceneObject
    c->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator)));
    c->getScene()->getSelectionManager()->addToSelection(c->getScene()->getObjectManager()->getObjectById("Object 1"));

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation1(new controller::SelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    operations1->push_back(operation1);

    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations1);
    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());
    controller::OperationPtr operation2 = operation1->createInverseOperation();
    operations2->push_back(operation2);

    BOOST_CHECK(operation2);
    BOOST_CHECK(operation2.get());

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM, operations2);
    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));
}

BOOST_AUTO_TEST_CASE(executeOnWorkingMemory)
{
    controller::ControllerPtr c = controller::Controller::create();

    //create sceneObject
    c->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator)));

    boost::shared_ptr<PublicSelectOperation> operation(new PublicSelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    //just check if no exception is thrown
    operation->executeOnWorkingMemory();
}

BOOST_AUTO_TEST_CASE(executeOnScene)
{
    controller::ControllerPtr c = controller::Controller::create();

    //create sceneObject
    c->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator)));

    boost::shared_ptr<PublicSelectOperation> operation(new PublicSelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    BOOST_CHECK(!c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));
    operation->executeOnScene();
    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));
}

BOOST_AUTO_TEST_CASE(executeOnSceneExceptionOnNotExistingObject)
{
    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<PublicSelectOperation> operation(new PublicSelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    BOOST_CHECK(c->getScene()->getObjectManager()->getObjectById("Object 1") == NULL);
    BOOST_CHECK_THROW(operation->executeOnScene(), std::exception);
}

BOOST_AUTO_TEST_CASE(executeOnSceneNothingDoneIfAlredySelected)
{
    controller::ControllerPtr c = controller::Controller::create();

    //create sceneObject
    c->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testClass", "testCollection", new SoSeparator, new SoSeparator)));
    c->getScene()->getSelectionManager()->addToSelection(c->getScene()->getObjectManager()->getObjectById("Object 1"));

    boost::shared_ptr<PublicSelectOperation> operation(new PublicSelectOperation(c->getMemoryXController(), c->getScene(), "Object 1"));
    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));
    operation->executeOnScene();
    BOOST_CHECK(c->getScene()->getSelectionManager()->isSelected(c->getScene()->getObjectManager()->getObjectById("Object 1")));
}
