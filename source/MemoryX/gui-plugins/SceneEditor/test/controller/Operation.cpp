/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Operation
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../../controller/Operation.h"

#include <unistd.h>

namespace controller
{
    class Operation;
}

class TestOperation : public controller::Operation
{
public:
    TestOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene) :
        controller::Operation(memoryXController, scene)
    {
    }

    const boost::shared_ptr<controller::Operation> createInverseOperation() const
    {
        boost::shared_ptr<controller::Operation> p(new TestOperation(getMemoryXController(), getScene()));
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

BOOST_AUTO_TEST_CASE(simpleAllocation)
{
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    BOOST_CHECK(t);
}

BOOST_AUTO_TEST_CASE(createInverseOperation)
{
    boost::shared_ptr<controller::Operation> t(new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>()));
    boost::shared_ptr<controller::Operation> i = t->createInverseOperation();
    BOOST_CHECK(i);
}
