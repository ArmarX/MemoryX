/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <exception>
#include <string>

// local
#include "RenameGroupOperation.h"
#include "../scene3D/SceneGroup.h"

controller::RenameGroupOperation::RenameGroupOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& oldGroupName, const std::string& newGroupName) :
    Operation(memoryXController, scene),
    oldGroupName(oldGroupName),
    newGroupName(newGroupName),
    failed(false)
{
    if (oldGroupName.empty() || oldGroupName == "" || newGroupName.empty() || newGroupName == "")
    {
        failed = true;
    }
}

const controller::OperationPtr controller::RenameGroupOperation::createInverseOperation() const
{
    std::shared_ptr<controller::Operation> inverseOperation(new controller::RenameGroupOperation(getMemoryXController(), getScene(), newGroupName, oldGroupName));
    return inverseOperation;
}

void controller::RenameGroupOperation::executeOnWorkingMemory()
{
    // do nothing, because the WorkingMemory does not manage groups
}

void controller::RenameGroupOperation::executeOnScene()
{
    if (!failed)
    {
        std::shared_ptr<scene3D::Scene> scene = getScene();
        scene3D::SceneGroupPtr group = scene->getGroupManager()->getGroupById(oldGroupName);
        scene->getGroupManager()->renameGroup(group, newGroupName);
    }
}
