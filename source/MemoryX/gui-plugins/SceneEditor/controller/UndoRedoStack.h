/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <vector>
#include <memory>

// local
#include "ClassDefinitions.h"
#include "UndoAction.h"
#include "Controller.h"

namespace controller
{
    /**
    * A Stack to save a history of Actions.
    * This Stack provides Functions to move forward and backward in the history.
    *
    * @see controller::UndoAction
    */
    class UndoRedoStack
    {
    public:
        /**
        * A constructor.
        * Creates a new Instance of this class.
        */
        UndoRedoStack();

        /**
        * Returns the possibility to undo a Action.
        *
        * @return True, if there is a Action to undo; false otherwise.
        */
        bool canUndo();

        /**
        * Returns the possibility to redo a Action.
        *
        * @return True, if there is a Action to redo; false otherwise.
        */
        bool canRedo();

        /**
        * Goes one step back in the history and returns the Operations need to redo.
        */
        std::shared_ptr<std::vector<OperationPtr> > undo();

        /**
        * Goes one step forward in the history and returns the Operations need to redo.
        */
        std::shared_ptr<std::vector<OperationPtr> > redo();

        /**
        * Removes all actions from the history.
        */
        void clear();

        /**
        * Adds a Action to the history.
        * The redo history will be cleared.
        *
        * @param action The action to add to the history.
        * @see controller::UndoAction
        */
        void push(const std::shared_ptr<std::vector<OperationPtr> >& operations);

        /**
        * Swaps oldId and newId in all Operations saved in this class.
        *
        * @param oldId the first ID
        * @param newId the second ID
        */
        void updateObjectId(std::string oldId, std::string newId);

    private:
        std::unique_ptr<std::vector<UndoActionPtr> > undoStack;
        std::unique_ptr<std::vector<UndoActionPtr> > redoStack;

    };
}

