/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <exception>
#include <string>
#include <QtDesigner/abstractpropertyeditor.h>

// local
#include "SelectOperation.h"
#include "DeselectOperation.h"
#include "EmptyOperation.h"

controller::SelectOperation::SelectOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId) :
    Operation(memoryXController, scene, objectId),
    wasSelected(false)
{
}

const controller::OperationPtr controller::SelectOperation::createInverseOperation() const
{
    if (wasSelected)
    {
        std::shared_ptr <controller::Operation> inverseOperation(new controller::EmptyOperation(getMemoryXController(), getScene(), getObjectId()));
        return inverseOperation;
    }
    else
    {
        std::shared_ptr <controller::Operation> inverseOperation(new controller::DeselectOperation(getMemoryXController(), getScene(), getObjectId()));
        return inverseOperation;
    }
}

void controller::SelectOperation::executeOnWorkingMemory()
{
    // do nothing, because the WorkingMemory does not manage groups
}

void controller::SelectOperation::executeOnScene()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    scene3D::SceneObjectPtr object = scene->getObjectManager()->getObjectById(getObjectId());

    if (!object)
    {
        throw std::logic_error("Object does not exist");
    }

    if (scene->getSelectionManager()->isSelected(object))
    {
        wasSelected = true;
    }
    else
    {
        scene->getSelectionManager()->addToSelection(object);
        wasSelected = false;
    }
}
