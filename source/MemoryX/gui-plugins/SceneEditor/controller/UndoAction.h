/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <vector>

// local
#include "ClassDefinitions.h"
#include "Operation.h"
#include "Controller.h"

namespace controller
{
    /**
    * A container class to store multiple Operations and undo/redo them.
    *
    * @see controller::Operation
    */
    class UndoAction
    {
    public:
        /**
        * A Constructor.
        * Creates a new UndoAction containing multiple Operations.
        *
        * @param operations The operations, the new instance should contain.
        * @see controller::Operation
        */
        UndoAction(const std::shared_ptr<std::vector<OperationPtr> >& operations);

        /**
        * Returns all Operations needed to redo this Action.
        */
        std::shared_ptr<std::vector<OperationPtr> > redo();

        /**
        * Returns all Operations needed to undo this Action.
        */
        std::shared_ptr<std::vector<OperationPtr> > undo();

        /**
        * Returns all Operations saved in this class.
        *
        * @return the operations
        */
        const std::shared_ptr<std::vector<OperationPtr> > getOperations() const;

    private:
        std::shared_ptr<std::vector<OperationPtr> > operations;
    };
}

