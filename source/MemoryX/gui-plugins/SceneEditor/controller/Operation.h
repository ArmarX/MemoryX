/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>
#include <memory>

// local
#include "ClassDefinitions.h"
#include "../scene3D/Scene.h"
#include "../memoryxcontroller/MemoryXController.h"

namespace controller
{
    /**
    * An abstract class, which offers methods to run operations on the WorkingMemory and the Scene.
    *
    * @see scene3D::Scene
    * @see memoryxcontroller::WorkingMemoryController
    */
    class Operation
    {
        friend class Controller;
        friend class UndoRedoStack;

    public:
        /**
        * A constructor.
        * Sets the Scene and WorkingMemory to execute the scene at.
        *
        * @param memoryXController the Controller for the WorkingMemory instance to run the Operation at
        * @param scene the Scene to run the Operation at
        * @param the ID of the object, which is affected by this operation
        * @see scene3D::Scene
        * @see memoryxcontroller::WorkingMemoryController
        */
        Operation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId);

        /**
        * A constructor.
        * Sets the Scene and WorkingMemory to execute the scene at.
        *
        * @param memoryXController the Controller for the WorkingMemory instance to run the Operation at
        * @param scene the Scene to run the Operation at
        * @see scene3D::Scene
        * @see memoryxcontroller::WorkingMemoryController
        */
        Operation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene);

        /**
        * Returns a Operation, which reverts the changes done in this instance of the Operation.
        *
        * @return the inverse Operation
        */
        virtual const OperationPtr createInverseOperation() const = 0;

        /**
        * Returns the ID of the object, this Operation is working on.
        *
        * @return the ID
        */
        std::string getObjectId() const;

        /**
         * Return wehether the operation can be executed currently.
         * If the Operation is never executeable this method return true.
         *
         * @return true, if the operation can be executed; false otherwise
         */
        virtual bool isExecuteable();

    protected:
        /**
        * Executes the Operation on the given WorkingMemory.
        *
        * @see memoryxcontroller::WorkingMemoryController
        */
        virtual void executeOnWorkingMemory() = 0;

        /**
        * Executes the Operation on the given Scene.
        *
        * @see scene3D::Scene
        */
        virtual void executeOnScene() = 0;

        /**
        * Returns the the Scene to execute the Operation at.
        *
        * @return the Scene
        * @see scene3D::Scene
        */
        const std::shared_ptr<scene3D::Scene> getScene() const;

        /**
        * Returns the the MemoryXController to execute the Operation at.
        *
        * @return the MemoryXController
        * @see memoryxcontroller::MemoryXController
        */
        const std::shared_ptr<memoryxcontroller::MemoryXController> getMemoryXController() const;

        /**
        * Sets the ID, this operation is working on.
        *
        * @param the new ID
        */
        void setObjectId(const std::string& objectId);

    private:
        std::weak_ptr<scene3D::Scene> scene;
        std::weak_ptr<memoryxcontroller::MemoryXController> memoryXController;
        std::string objectId;
    };
}

