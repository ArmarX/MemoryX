/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>

// scene3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>

// local
#include "../scene3D/Scene.h"
#include "Operation.h"
#include "../memoryxcontroller/MemoryXController.h"

namespace controller
{
    /**
    * A operation to rotate and translate a object.
    *
    * @see controller::Operation
    */
    class RotateTranslateOperation : public Operation
    {
    public:
        /**
        * Creates a new operation, which rotates and translates a object.
        *
        * @param memoryXController The MemoryXController to rotate and translate the object on.
        * @param scene The scene to rotate and translate the object on.
        * @param objectId The object to rotate and translate.
        * @param oldRotation The old rotation of the object.
        * @param newRotation The new rotation of the object.
        * @param oldPosition The old position of the object.
        * @param newPosition The new position of the object.
        *
        * @see controller::Operation::Operation
        */
        RotateTranslateOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId, const SbRotation& oldRotation, const SbRotation& newRotation, const SbVec3f& oldPosition, const SbVec3f& newPosition);

        /**
        * Returns a new operation, which reverts the rotation and translatioon of the object.
        *
        * @return The operation, which reverts the rotation and translatioon of the object.
        *
        * @see controller::Operation::createInverseOperation
        */
        const OperationPtr createInverseOperation() const override;

        /**
         * Returns true, if the SceneObject is mutable.
         *
         * @return true, if the SceneObject is mutable; false otherwise.
         */
        bool isExecuteable() override;

    protected:
        /**
        * Rotates and translates the object in the WorkingMemory.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnWorkingMemory() override;

        /**
        * Rotates and translates the object in the Scene.
        *
        * @see controller::Operation::executeOnScene
        */
        void executeOnScene() override;

    private:
        SbRotation oldRotation;
        SbRotation newRotation;
        SbVec3f oldPosition;
        SbVec3f newPosition;

    };
}

