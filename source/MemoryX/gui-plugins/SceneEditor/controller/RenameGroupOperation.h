/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>

// local
#include "../scene3D/Scene.h"
#include "Operation.h"

namespace controller
{
    /**
    * A operation to rename a group.
    *
    * @see controller::Operation
    */
    class RenameGroupOperation : public Operation
    {
    public:
        /**
        * Creates a new operation, which renames a group.
        *
        * @param memoryXController The MemoryXController to rename the group on. (This is currently not used.)
        * @param scene The scene to rename the group on.
        * @param oldGroupName The old name of the group.
        * @param newGroupName The new name of the group.
        *
        * @see controller::Operation::Operation
        */
        RenameGroupOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& oldGroupName, const std::string& newGroupName);

        /**
        * Returns a new operation, which reverts the renameing of the group.
        *
        * @return The operation, which reverts the renameing of the group.
        *
        * @see controller::Operation::createInverseOperation
        */
        const OperationPtr createInverseOperation() const override;

    protected:
        /**
        * Because the WorkingMemory does not support groups, nothing is done here.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnWorkingMemory() override;

        /**
        * Renames the group.
        *
        * @see controller::Operation::executeOnScene
        */
        void executeOnScene() override;

    private:
        std::string oldGroupName;
        std::string newGroupName;
        bool failed;

    };
}

