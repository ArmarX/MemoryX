/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <exception>
#include <string>

// local
#include "DeleteGroupOperation.h"
#include "CreateGroupOperation.h"

controller::DeleteGroupOperation::DeleteGroupOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& groupName) :
    Operation(memoryXController, scene),
    groupName(groupName)
{
}

const controller::OperationPtr controller::DeleteGroupOperation::createInverseOperation() const
{
    std::shared_ptr<controller::Operation> inverseOperation(new controller::CreateGroupOperation(getMemoryXController(), getScene(), groupName));
    return inverseOperation;
}

void controller::DeleteGroupOperation::executeOnWorkingMemory()
{
    // do nothing, because the WorkingMemory does not manage groups
}

void controller::DeleteGroupOperation::executeOnScene()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    scene3D::SceneGroupPtr sceneGroupToDelete = scene->getGroupManager()->getGroupById(groupName);
    scene->getGroupManager()->removeGroup(sceneGroupToDelete);
}
