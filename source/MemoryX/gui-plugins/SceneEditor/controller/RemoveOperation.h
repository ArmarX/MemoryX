/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>

// scene3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>

// local
#include "../scene3D/Scene.h"
#include "Operation.h"
#include "../memoryxcontroller/MemoryXController.h"
#include "AddOperation.h"

namespace controller
{
    /**
    * A Operation to remove a object in the scene and the WorkingMemory.
    *
    * @see controller::Operation
    */
    class RemoveOperation : public Operation
    {
        friend class AddOperation;

    public:
        /**
        * Creates a new operation, which removes a object.
        *
        * @param memoryXController The MemoryXController to remove the object at.
        * @param scene The scene to remove the object at.
        * @param objectId The ID of the object to remove.
        *
        * @see controller::Operation::Operation
        */
        RemoveOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId);

        /**
        * Returns a new operation which adds the object again.
        *
        * @return The operation, which adds the object again.
        *
        * @see controller::Operation::createInverseOperation
        */
        const OperationPtr createInverseOperation() const override;

        /**
         * Returns true, if the SceneObject is mutable.
         *
         * @return true, if the SceneObject is mutable; false otherwise.
         */
        bool isExecuteable() override;

    protected:
        /**
        * Removes the object from the WorkingMemory.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnWorkingMemory() override;

        /**
        * Removes the object from the local Scene.
        *
        * @see controller::Operation::executeOnScene
        */
        void executeOnScene() override;

    private:
        RemoveOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation, const std::string& objectId);

        std::string objectName;
        std::string objectCollection;
        SbVec3f objectPosition;
        SbRotation objectRotation;

    };
}

