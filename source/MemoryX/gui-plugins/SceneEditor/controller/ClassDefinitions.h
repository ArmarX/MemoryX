/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <memory>

namespace controller
{
    class AddOperation;
    using AddOperationPtr = std::shared_ptr<AddOperation>;

    class AddToGroupOperation;
    using AddToGroupOperationPtr = std::shared_ptr<AddToGroupOperation>;

    class ClassDefinitions;
    using ClassDefinitionsPtr = std::shared_ptr<ClassDefinitions>;

    class Controller;
    using ControllerPtr = std::shared_ptr<Controller>;
    using ControllerWeakPtr = std::weak_ptr<Controller>;

    class CreateGroupOperation;
    using CreateGroupOperationPtr = std::shared_ptr<CreateGroupOperation>;

    class DeleteGroupOperation;
    using DeleteGroupOperationPtr = std::shared_ptr<DeleteGroupOperation>;

    class DeselectOperation;
    using DeselectOperationPtr = std::shared_ptr<DeselectOperation>;

    class Operation;
    using OperationPtr = std::shared_ptr<Operation>;
    using OperationPtrListPtr = std::shared_ptr<std::vector<OperationPtr>>;

    class RemoveFromGroupOperation;
    using RemoveFromGroupOperationPtr = std::shared_ptr<RemoveFromGroupOperation>;

    class RemoveOperation;
    using RemoveOperationPtr = std::shared_ptr<RemoveOperation>;

    class RotateTranslateOperation;
    using RotateTranslateOperationPtr = std::shared_ptr<RotateTranslateOperation>;

    class SelectOperation;
    using SelectOperationPtr = std::shared_ptr<SelectOperation>;

    class UndoAction;
    using UndoActionPtr = std::shared_ptr<UndoAction>;

    class UndoRedoStack;
    using UndoRedoStackPtr = std::shared_ptr<UndoRedoStack>;
    using UndoRedoStackWeakPtr = std::weak_ptr<UndoRedoStack>;
}

