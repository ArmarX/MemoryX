/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <vector>
#include <mutex>

// qt
#include <QObject>
#include <QMetaType>

// local
#include "ClassDefinitions.h"
#include "../scene3D/Scene.h"
#include "../scene3D/SceneObject.h"
#include "UndoRedoStack.h"
#include "../memoryxcontroller/MemoryXController.h"
#include "Operation.h"
#include "../gui/ShortcutController.h"
#include <MemoryX/core/MongoDBRef.h>

namespace controller
{
    using vector_string = std::vector<std::string>;

    /**
    * A class to execute Operations, maintain the execution history and initialize Scene and MemoryXController.
    *
    * @see memoryxcontroller::MemoryXController
    * @see scene3D::scene
    */
    class Controller : public QObject
    {
        Q_OBJECT

    public:
        /**
        * A Flag to execute operations on the WorkingMemory.
        *
        * @see memoryxcontroller::WorkingMemoryController
        * @see controller::Controller::execute()
        */
        static const int EXECUTE_ON_WM = 1 << 0;
        /**
        * A flag to execute operations on the Scene.
        *
        * @see scene3D::Scene
        * @see controller::Controller::execute()
        */
        static const int EXECUTE_ON_SCENE = 1 << 1;
        /**
        * A flag to save the executed operations to the history
        *
        * @see controller::UndoRedoStack
        * @see controller::Controller::execute()
        */
        static const int UNDOABLE = 1 << 2;

        /**
        * Creates a new instance of the Controller, initializes the Scene and the MemoryXController and returns the new Controller.
        *
        * @return the newly created Controller
        * @see scene3D::Scene
        * @see memoryxcontroller::MemoryXController
        */
        static ControllerPtr create();

        /**
        * A destructor.
        * Deletes all initialized references.
        */
        ~Controller() override;

        /**
        * Executes multiple operations.
        * The Operations will be run depending on the flags:
        * flags & EXECUTE_ON_WM == 1 => The Operations will be run at the WorkingMemory.
        * flags & EXECUTE_ON_SCENE == 1 => The Operations will be run at the Scene.
        * flags & UNDOABLE == 1 => The Operations will be saved to the history.
        *
        * @param flags The flags for the execution.
        * @param operations The operations to execute.
        * @param blocking If true, wait for ending of execution.
        *
        * @see controller::Controller::EXECUTE_ON_WM
        * @see controller::Controller::EXECUTE_ON_SCENE
        * @see controller::Controller::UNDOABLE
        * @see scene3D::Scene
        * @see memoryxcontroller::WorkingMemoryController
        */
        void execute(int flags, const OperationPtrListPtr& operations, bool blocking = true);

        /**
        * Deletes all objects in the local Scene and loads the current objects from the WorkingMemory in the Scene.
        */
        void reloadLocalScene();

        /**
        * Returns the Scene.
        *
        * @return the Scene
        */
        const std::shared_ptr<scene3D::Scene> getScene() const;

        /**
        * Returns the MemoryXController.
        *
        * @return the MemoryXController
        */
        const std::shared_ptr<memoryxcontroller::MemoryXController> getMemoryXController() const;

        /**
        * Returns the ShortcutController.
        *
        * @return the ShortcutController
        */
        const std::shared_ptr<gui::ShortcutController> getShortcutController() const;

        /**
        * Goes one step back in the history and undoes the corresponding Operations.
        *
        * @see controller::UndoRedoStack::undo
        */
        void undo();

        /**
        * Goes one step forward in the history and redoes the corresponding Operations.
        *
        * @see controller::UndoRedoStack::redo
        */
        void redo();

        /**
        * Triggers signal controller:Controller::objectClassSelected with given parameter.
        *
        * @param objectClass The class of the selected class.
        * @param collection The collection of the selected class.
        */
        void triggerObjectClassSelected(const std::string& objectClass, const std::string& collection);

        /**
        * Triggers signal controller:Controller::sceneObjectSelected with given parameter.
        *
        * @param object The object which was selected.
        */
        void triggerSceneObjectSelected(scene3D::SceneObjectPtr object);

        /**
        * Triggers signal controller:Controller::objectsChanged with given parameter.
        *
        * @param objectIds The IDs of the changed objects.
        */
        void triggerObjectsChanged(controller::vector_string objectIds);

        /**
        * Triggers signal controller:Controller::minimapClicked.
        */
        void triggerMinimapClicked();

        /**
        * Deletes all objects in local scene and in WorkingMemory and deletes all groups.
        * The operations are performed using undoable operations.
        */
        void clearScene();

        /**
         * Executes the Operations, which are queued because the object, which would be affected by this was busy.
         *
         * @param blocking If true, wait for ending of execution.
         */
        void executeQueuedOperations(bool blocking = true);

        void saveSnapshotAsJSON(std::string const& snapshotName);

    Q_SIGNALS:

        /**
        * A signal which gets triggered, after the scene is reloaded.
        *
        * @see controller::Controller::reloadLocalScene
        */
        void reloadScene();

        /**
        * A signal which gets triggered, after a vector of operations is executed.
        *
        * @param objectIds The IDs of the objects affected by the executed operations.
        */
        void operationExecuted(controller::vector_string objectIds);

        /**
        * A signal which gets triggered, if objects are moved or rotated.
        *
        * @param objectIds The IDs of the changed objects.
        */
        void objectsChanged(controller::vector_string objectIds);

        /**
        * A signal which gets triggered, after a class gets selected.
        *
        * @param objectClass The class of the selected class.
        * @param collection The collection of the selected class.
        */
        void objectClassSelected(const std::string& objectClass, const std::string& collection);

        /**
        * A signal which gets triggered, after a object gets selected.
        *
        * @param object The object which was selected.
        */
        void sceneObjectSelected(scene3D::SceneObjectPtr object);

        /**
        * A signal which gets triggered, after a click was performed on the minimap.
        */
        void minimapClicked();

    private:
        Controller();

        UndoRedoStackPtr undoRedoStack;
        std::shared_ptr<scene3D::Scene> scene;
        std::shared_ptr<memoryxcontroller::MemoryXController> memoryXController;
        std::shared_ptr<gui::ShortcutController> shortcutController;
        mutable std::recursive_mutex execute_mutex;
        mutable std::mutex queue_mutex;
        scene3D::SceneObjectPtr lastSelected;
        std::vector<std::pair<int, OperationPtr>> queuedOperations;

    private slots:
        void executeQtThread(int flags, const OperationPtrListPtr& operations);
    };
}

Q_DECLARE_METATYPE(controller::vector_string)

