/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>

// local
#include "Operation.h"

namespace controller
{
    /**
    * A operation which does nothing.
    *
    * @see controller::Operation
    */
    class EmptyOperation : public Operation
    {
    public:
        /**
        * Creates a new instance.
        *
        * @param memoryXController A MemoryXController. (not used)
        * @param scene A scene. (not used)
        * @param objectId A ID. (not used)
        *
        * @see controller::Operation::Operation
        */
        EmptyOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId);

        /**
        * Returns a new operation, which does nothing.
        *
        * @return The operation, which does nothing.
        *
        * @see controller::Operation::createInverseOperation
        */
        const OperationPtr createInverseOperation() const override;

    protected:
        /**
        * Do nothing.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnWorkingMemory() override;

        /**
        * Do nothing.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnScene() override;
    };
}

