/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* QtPropertyBrowser headers */
#include <ArmarXGui/libraries/qtpropertybrowser/src/QtTreePropertyBrowser>
#include <ArmarXGui/libraries/qtpropertybrowser/src/QtDoubleSpinBoxFactory>
#include <ArmarXGui/libraries/qtpropertybrowser/src/QtDoublePropertyManager>
#include <ArmarXGui/libraries/qtpropertybrowser/src/QtGroupPropertyManager>
#include <ArmarXGui/libraries/qtpropertybrowser/src/QtStringPropertyManager>

#include "../controller/Controller.h"

namespace gui
{

    /**
    * This class provides a property browser which displays all properties of a scene object or prior knowledge object class.
    * The property browser is derived from the QtTreePropertyBrowser which is part of the QtPropertyBrowser library.
    *
    * @see  QtPropertyBrowser::QtTreePropertyBrowser
    *
    */
    class PropertyBrowserWidget : public QtTreePropertyBrowser
    {
        Q_OBJECT
    public:

        /**
        * Constructor.
        * Creates an instance of the property browser widget.
        *
        * @param    control shared pointer to the controller::Controller
        *
        */
        explicit PropertyBrowserWidget(const controller::ControllerPtr& control, QWidget* parent = 0);

        /**
        * Fill property browser with class attributes of the given objectclass.
        *
        * @param    objectClass shared pointer to a ObjectClass
        * @param    collection  name of the collection to which the object class belongs
        *
        */
        void setProperties(const memoryx::ObjectClassPtr& objectClass, std::string collection);

        /**
        * Fill property browser with instance and class attributes of the given scene object.
        *
        * @param    sceneObject pointer to a scene3D::SceneObject
        *
        */
        void setProperties(scene3D::SceneObjectPtr sceneObject);

        /**
        * Updates the instance properties of the selected object in the scene given by its ID.
        *
        * @param    objectID    string representing the object instance ID
        *
        */
        void updateSceneObject(std::string objectID);

    private slots:
        void valueChanged(QtProperty* property, double newValue);

    private:

        controller::ControllerWeakPtr control;
        QPointer<QtDoubleSpinBoxFactory> spinBoxFactory;
        QPointer<QtDoublePropertyManager> doubleManager;
        QPointer<QtGroupPropertyManager> groupManager;
        QPointer<QtStringPropertyManager> stringManager;

        QMap<QtProperty*, QString> propertyToId;
        QMap<QString, QtProperty*> idToProperty;
        QMap<QtProperty*, double> propertyToValue;
        scene3D::SceneObjectPtr currentSceneObject;

        void setInstanceAttributes();

        void setClassAttributes(const memoryx::ObjectClassPtr& objectClass, const std::string& collection, bool expanded);

        void clearAll();

        inline void insertInMap(QtProperty* property, QString id);

        SbRotation createNewRotation(QtProperty* property, double val);

        SbVec3f createNewTranslation(QtProperty* property, double val);

        std::string getMatrix();

    };
}

