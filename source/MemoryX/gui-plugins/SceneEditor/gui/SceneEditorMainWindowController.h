/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditorWidgetController
 * @author     Kiron Mirdha ( kiron dot mirdha at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


/* ArmarX headers */
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

/* C++ libraries */
#include <string>

/* Qt headers */
#include <QWidget>
#include <QToolBar>
#include <QTranslator>
#include <QVariant>
#include <QSettings>
#include <QComboBox>

/* SceneEditor headers */
#include "SceneEditorMainWindow.h"
#include "dialog/SceneEditorConfigDialog.h"
#include "dialog/GroupExplorerDialog.h"
#include "dialog/LoadSnapshotDialog.h"
#include "dialog/SaveSnapshotDialog.h"
#include "dialog/ShortcutDialog.h"
#include "ShortcutController.h"
#include "dialog/GroupExplorerDialog.h"
#include "../controller/Controller.h"

namespace gui
{
    class SceneEditorMainWindow;
}

namespace gui
{
    /**
    \page MemoryX-GuiPlugins-SceneEditor SceneEditor
    \tableofcontents
    This Plugin is an editor for the selected WorkingMemory which contains the current knowledge of the roboter about its environment.
    With this editor it is only possible to edit the scene and not relationships between objects or related properties.

    This is a live-editor which means, that any changes made to the scene are directly transfered to the used WorkingMemory.
    Therefore it might happen that other plugins show inconsistent states of the WorkingMemory.
    This plugin on the other hand detects changes made by other instances and adjusts the shown scene immediately.

    \section MemoryX-GuiPlugins-SceneEditor-startingEditor Starting the Editor
    @image html SceneEditor_ConfigDialog.png "The config-dialog of the plugin"

    When you add the widget to the Gui, you need to specify the following parameters:

    Parameter Name   | Example Value     | Required?     | Description
     :----------------  | :-------------:   | :-------------- |:--------------------
    Select Configuration-File | .../armarx/MemoryX/build/bin/SceneEditor.ini | yes | Settings for this plugin
    Working Memory Name | Working Memory | yes | ?
    Working-Memory-Updates | WorkingMemoryUpdates | yes | the topic that publishes changes in the selected working memory
    Prior Memory Name | PriorKnowledge | Yes | The name of the prior memory

    \note   In order to use this plugin following memories must be available:
    \note   - WorkingMemory
    \note   - PriorKnowledge
    \note   - CommonStorage
    \note   - LongtermMemory

    Make sure that these four components are getting started in the scenario-file you use.

    \section MemoryX-GuiPlugins-SceneEditor-overview Overview
    @image html SceneEditor_wholeWindow.png "The plugin's UI"
    -# Object Explorer
        - Contains the every readable collection of addable objects.
        - By selecting an object its properties are shown in the Object Inspector.
        - Objects can be added to scene via Drag-and-Drop.
    -# Scene-ViewMemoryX-GuiPlugins-SceneEditor-groups
        - A 3D-Representation of the current content of the WorkingMemory
        - Two modes:
            - Editor-Mode: The scene can be manipulated, buttons for undo/redo and different manipulators are accessible.
            - Viewer-Mode: Manipulation not possible, can be used for presentations.
        - \ref MemoryX-GuiPlugins-SceneEditor-navigationManipulation
    -# Object Inspector
        - \ref MemoryX-GuiPlugins-SceneEditor-objectInspector
    -# Minimap
        - Simply a top-view of the whole scene.
        - By clicking on it the camera of the Scene-View will be set to same position as in the minimap
    -# Group Explorer
        - List of all current groups
        - Two Buttons per Entry:
            - Add every object that is currently selected in the scene to that group
            - Add every object in that group to the current selection
        - All selected objects can be added to a new group
        - By double-clicking on a group the Group-Explorer opens. See \ref MemoryX-GuiPlugins-SceneEditor-groups
    -# Snapshots and Settings
        - Buttons for ...
            - Loading/Saving Snapshots (\ref MemoryX-GuiPlugins-SceneEditor-snapshots)
            - Opening shortcut-settings where every shortcut working in this plugin can be found and edited.
            - Opening group explorer (\ref MemoryX-GuiPlugins-SceneEditor-groups)
            - Clearing the whole scene and thus the whole working memory and deleting every object
            - Selecting language of the plugin

    \section MemoryX-GuiPlugins-SceneEditor-snapshots Load-Save Snapshot
    @image html SceneEditor_loadSnapshot.png "Loading a Snapshot into the scene/working-memory"

    - Select the snapshot to load and its content will be <b>added</b> to the current scene. The scene will not be cleared before the loading.
    - The objects in the loaded snapshot are always added together to a new group (\ref MemoryX-GuiPlugins-SceneEditor-groups).

    @image html SceneEditor_saveSnapshot.png "Saving the scene/working-memory into a snapshot"

    - It is either possible to create a new snapshot or override an existing one. The name of a new snapshot has to start with the prefix
    "Snapshot_" otherwise it will be added to the name automatically.
    - By selecting 'Save all' every object of the scene is saved.
    - On the other hand you can select specific \ref MemoryX-GuiPlugins-SceneEditor-groups and only the objects in these groups are saved.

    \section MemoryX-GuiPlugins-SceneEditor-navigationManipulation Navigation and Manipulation
    - The navigation through the scene is quite similar to other 3D-Manipulation-Programs:
        - Left click to select objects ('Shift' + left-click to select several objects successively)
        - Hold mouse wheel + 'Shift' to move the camera
        - Hold mouse wheel to rotate the camera (for rotation shortcuts check the shortcut-settings)
        - Use mouse wheel to zoom

    @image html SceneEditor_SceneViewButtons.png "Buttons accessible in Editor-Mode"

    -# Toggle between showing the visualisation meshes or the collision meshes of the objects
    -# (De-)Activate Rotation-Manipulator
        - Adds a sphere around selected objects which allows to rotate the object by clicking on and moving the green points.
        - If more than one object is selected all of them are rotated around their common center.
    -# (Se-)Activate Translation-Manipulator
        - Adds a cuboid around selected objects.
        - Left-CLick an -Hold on one side of the cuboid allows to move the objects within this level.
        - Additional holding 'Shift' allows to move only in one direction.
        - Additional holding 'Ctrl' allows to move only in the direction orthogonal to the selected side.
    -# Undo last action (select/move/rotate/add/remove object/s)
        - 'Ctrl' + 'Z'
    -# Redo last action
        - 'Ctrl' + 'Shift' + 'Z'
    -# Delete selected objects
        - 'Del'

    \section MemoryX-GuiPlugins-SceneEditor-objectInspector Object Inspector
    @image html SceneEditor_ObjectInspector.png "Object Inspector while an object in the scene is selected

    - Shows the instance attributes of the selected object
    - If several objects are selected the attributes of the object that has been selected last are shown (orange border in scene view)
    - Allows to edit the location and orientaion of an object in the scene. (Press 'Enter' to confirm input)

    \note Although it is possible to edit the orientation with the Object Inspector it is not very useful:
    \note Due to the fact that quanternions are used to store the orientation and location
    the angle-axis-representation is not very practical for editing.
    \note (You will see the values change after you hit 'Enter'.

    \section MemoryX-GuiPlugins-SceneEditor-groups Groups

    You can create groups of objects in the scene to simplify your task.
    They provide the advantage that it is possible to easily select a large number of objects over and over with only one click.
    Check \ref MemoryX-GuiPlugins-SceneEditor-overview to see how to create groups directly from the main-window.
    \note They exist only as long as the plugin is running, because they will not be saved in a snapshot.


    @image html SceneEditor_GroupEditor.png "The Group-Explorer for maintaining groups"

    Following actions are possible:
    - Changing the name of selected group
    - Deleting the selected group. (Only the group not the objects within the group)
    - Creating a new empty group
    - Showing the objects that belong to a group
    - Remove an object from a group (Button 'Delete Object')

    @see SceneEditorMainWindowController

    */

    /**
     * This class is a custom armarx::ArmarXComponentWidgetController.
     * It creates the main window of the plugin and provides all functions to integrate the plugin in the ArmarXGui.
     * It establishes the connection to MemoryX via Ice.
     *
     * @class SceneEditorMainWindow
     * @brief SceneEditorMainWindow brief one line description
     *
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SceneEditorMainWindowController:
        public armarx::ArmarXComponentWidgetControllerTemplate<SceneEditorMainWindowController>
    {
        Q_OBJECT

    public:
        /**
         * Constructor.
         * Creates an instance of SceneEditorMainWindowController.
         *
         */
        explicit SceneEditorMainWindowController();

        /**
         * Destructor.
         * Destroys the instance of SceneEditorMainWindowController.
         *
        */
        ~SceneEditorMainWindowController() override;

        /**
         * Reimplemented ArmarXWidgetController:loadSettings().
         *
         * @param   settings    Qt::QSettings
         * @see     ArmarXWidgetController:loadSettings()
         *
         */
        void loadSettings(QSettings* settings) override;

        /**
         * Reimplemented ArmarXWidgetController:saveSettings().
         *
         * @param   settings    Qt::QSettings
         * @see     ArmarXWidgetController:saveSettings()
         *
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Reimplemented armarx::Component:getWidgetName().
         *
         * Returns the widget name displayed in the ArmarXGui to create an
         * instance of this class.
         *
         * @see armarx::Component:getWidgetName()
         *
         */
        static QString GetWidgetName()
        {
            return "MemoryX.SceneEditor";
        }

        /**
         * Reimplemented armarx::Component:onInitComponent().
         *
         * @see armarx::Component:onInitComponent()
         *
         */
        void onInitComponent() override;

        /**
         * Reimplementiert armarx::Component:onConnectComponent().
         *
         * @see armarx::Component:onConnectComponent()
         *
         */
        void onConnectComponent() override;

        /**
         * @see armarx::Component::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        QPointer<QWidget> getWidget() override;

        /**
         * Reimplemented ArmarXWidgetController:getCustomTitlebarWidget().
         *
         * @param   parent  parent widget
         * @see     ArmarXWidgetController:getCustomTitlebarWidget()
         *
         */
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;

        /**
         * Reimplemented ArmarXWidgetController:getConfigDialog().
         *
         * @param   parent  parent widget
         * @see ArmarXWidgetController:getConfigDialog()
         *
         */
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        /**
         * Reimplemented ArmarXWidgetController:configured().
         *
         * @see ArmarXWidgetController:configured()
         *
         */
        void configured() override;

    private slots:
        /** this slot is called by the language menu actions
          */
        void slotLanguageChanged(const int languageIndex);
        /** this slot is called when a language change event occurs in the main window
          */
        void slotLanguageChangedByUser();

    private:
        QPointer<SceneEditorMainWindow> mainWindow;
        QPointer<dialog::SceneEditorConfigDialog> configDialog;
        QPointer<QToolBar> customToolbar;
        QPointer<dialog::GroupExplorerDialog> groupEditorDialog;
        QPointer<dialog::LoadSnapshotDialog> loadSnapshotDialog;
        QPointer<dialog::SaveSnapshotDialog> saveSnapshotDialog;
        QPointer<dialog::ShortcutDialog> shortcutDialog;
        QPointer<QComboBox> languageSelection;

        QPointer<QAction> loadSnapshotAction;
        QPointer<QAction> saveSnapshotAction;
        QPointer<QAction> openShortcutDialogAction;
        QPointer<QAction> openGroupEditorAction;
        QPointer<QAction> clearWorkingMemoryAction;

        controller::ControllerPtr control;

        std::string workingMemoryName;
        std::string workingMemoryUpdatesTopic;
        std::string objectInstancesSegmentName;
        std::string priorMemoryName;

        QString settingsPath;

        /** Loads a language by the given language abbreviation (e.g. de, en, ...)
          * @param language the abbreviation of the language to load
          */
        void loadLanguage(const QString& language);

        /** creates the language menu dynamically from the content of m_langPath
          */
        void createLanguageMenu();

        /**
          * @brief Removes the current translator and installs the given translator.
          * The translator loads its needed .qm file with the given filename from the given directory
          *
          * @param translator the new translator
          * @param filename the name of the .qm file the translator will load
          * @param directory the location of the .qm file
          */
        void switchTranslator(QTranslator& translator, const QString& filename, const QString& directory);

        QTranslator m_translator;   /**< contains the translations for this application */
        QTranslator m_translatorQt;
        QString m_currLang;     /**< contains the currently loaded language */
        QString m_langPath;     /**< Path of language files */

    private slots:
        void clearScene();
        void openLoadSnapshotDialog();
        void openSaveSnapshotDialog();
        void openShortcutDialog();
        void openGroupEditorDialog();
        void onConnectComponentQt();
        void onDisconnectComponentQt();

    signals:
        void connectComponent();
        void disconnectComponent();


        // ArmarXWidgetController interface
    public:
        void postDocking() override;
    };
}

