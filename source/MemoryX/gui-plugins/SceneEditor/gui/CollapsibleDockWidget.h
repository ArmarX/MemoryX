/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QDockWidget>
#include <QWidget>
#include <QHBoxLayout>
#include <QSize>
#include <QPushButton>
#include <QLabel>
#include <QPointer>

namespace gui
{
    /**
    * A QDockWidget which provides the possibility to collapse the dock.
    *
    * @see QDockWidget
    */
    class CollapsibleDockWidget :
        public QDockWidget
    {
        Q_OBJECT

    public:
        /**
        * Creates a new CollapsibleDockWidget.
        * This calls the constructor of the QDockWidget.
        *
        * @param title The window title of the DockWidget.
        * @param parent The parent QWidget.
        * @param The flags to create the DockWidget with.
        *
        * @see QDockWidget
        */
        explicit CollapsibleDockWidget(const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0);

        /**
        * Creates a new CollapsibleDockWidget.
        * This calls the constructor of the QDockWidget.
        *
        * @param parent The parent QWidget.
        * @param The flags to create the DockWidget with.
        *
        * @see QDockWidget
        */
        explicit CollapsibleDockWidget(QWidget* parent = 0, Qt::WindowFlags flags = 0);

        /**
        * Sets a QWidget to show in the DockWidget.
        * If you use this method the DockWidget is collapsible.
        *
        * @param w The widget to set.
        */
        void setCollapsibleWidget(QWidget* w);

        /**
        * Returns is the widget is collapsed.
        *
        * @return The current state of the widdget.
        */
        bool isCollapsed();

        /**
        * A method to inform the widget, that the window title has been changed.
        */
        void windowTitleChanged();

    public slots:
        /**
        * Sets the collapsed state. The widget will be collapsed if and only if "collapsed" is true.
        *
        * @param collapsed The new state of the widget.
        */
        void setCollapsed(bool collapsed);

        /**
        * Toggles the collapsed state of the widget.
        */
        void toggleCollapsed();

    private:
        class InnerWidgetWrapper :
            public QWidget
        {
        public:
            InnerWidgetWrapper(QDockWidget* parent);

            void setWidget(QWidget* widget);

            bool isCollapsed();

            void setCollapsed(bool collapsed);

            QSize const& getOldMaximumSizeParent() const;

        private:
            QPointer<QWidget> widget;
            QPointer<QHBoxLayout> hlayout;
            int widget_height;
            QSize oldSize;
            QSize oldMinimumSizeParent;
            QSize oldMaximumSizeParent;
            QSize oldMinimumSize;
            QSize oldMaximumSize;
        };

        class TitleBar :
            public QWidget
        {
        public:
            TitleBar(QWidget* parent);

            void windowTitleChanged();

            void showTitle(bool show);

        private:
            QPointer<QHBoxLayout> hlayout;
            QPointer<QPushButton> collapse;
            QPointer<QLabel> title;
        };

    private slots:
        void setCollapsedSizes();
    };
}

