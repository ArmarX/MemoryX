/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <QtConcurrent/qtconcurrentrun.h>
#include "SceneEditorWidget.h"
#include "../controller/DeselectOperation.h"
#include "../controller/RemoveOperation.h"
#include "../controller/RemoveFromGroupOperation.h"
#include "OverrideAction.h"

gui::SceneEditorWidget::SceneEditorWidget(const controller::ControllerPtr& control, QWidget* parent) :
    QWidget(parent),
    control(control),
    sceneGraph(new ScenegraphWidget(control, this)),
    collisionMeshToggleButton(new QPushButton(this)),
    rotateManipulatorToggleButton(new QPushButton(this)),
    translateManipulatorToggleButton(new QPushButton(this)),
    undoButton(new QPushButton(this)),
    redoButton(new QPushButton(this)),
    removeSelectedObjectsButton(new QPushButton(this)),
    editorToggleButton(new QPushButton(this)),
    viewerToggleButton(new QPushButton(this)),
    editorViewerButtonGroup(new QButtonGroup(this)),
    mainLayout(new QVBoxLayout(this)),
    leftButtonsSize(30, 30),
    buttonColor("lightgray"),
    margin(20),
    spacing(10),
    showCollisionMesh(false),
    showTranslateManipulator(false),
    showRotateManipulator(false)
{
    setPalette(Qt::transparent);

    setObjectName(QString::fromUtf8("SceneEditorWidget"));

    mainLayout->addWidget(sceneGraph);

    this->setLayout(mainLayout);

    editorViewerButtonGroup->setExclusive(true);

    QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);

    collisionMeshToggleButton->setObjectName(QString::fromUtf8("collisionMeshToggleButton"));
    sizePolicy.setHeightForWidth(collisionMeshToggleButton->sizePolicy().hasHeightForWidth());
    collisionMeshToggleButton->setSizePolicy(sizePolicy);
    QIcon icon1;
    icon1.addFile(QString::fromUtf8(":/images/Collision.png"), QSize(), QIcon::Normal, QIcon::Off);
    collisionMeshToggleButton->setIcon(icon1);
    collisionMeshToggleButton->setToolTip(tr("Toggle Collisionmesh"));
    collisionMeshToggleButton->setIconSize(leftButtonsSize);
    collisionMeshToggleButton->setCheckable(true);
    collisionMeshToggleButton->setFixedSize(leftButtonsSize);
    collisionMeshToggleButton->setStyleSheet("background-color: " + buttonColor);
    collisionMeshToggleButton->move(margin, margin);
    collisionMeshToggleButton->show();

    rotateManipulatorToggleButton->setObjectName(QString::fromUtf8("rotateManipulatorToggleButton"));
    sizePolicy.setHeightForWidth(rotateManipulatorToggleButton->sizePolicy().hasHeightForWidth());
    rotateManipulatorToggleButton->setSizePolicy(sizePolicy);
    QIcon icon2;
    icon2.addFile(QString::fromUtf8(":/images/Rotate.png"), QSize(), QIcon::Normal, QIcon::Off);
    rotateManipulatorToggleButton->setIcon(icon2);
    rotateManipulatorToggleButton->setToolTip(tr("Toggle Rotationmanipulator"));
    rotateManipulatorToggleButton->setIconSize(leftButtonsSize);
    rotateManipulatorToggleButton->setCheckable(true);
    rotateManipulatorToggleButton->setFixedSize(leftButtonsSize);
    rotateManipulatorToggleButton->setStyleSheet("background-color: " + buttonColor);
    rotateManipulatorToggleButton->move(margin, collisionMeshToggleButton->pos().y() + leftButtonsSize.height() + spacing);
    rotateManipulatorToggleButton->show();

    translateManipulatorToggleButton->setObjectName(QString::fromUtf8("translateManipulatorToggleButton"));
    sizePolicy.setHeightForWidth(translateManipulatorToggleButton->sizePolicy().hasHeightForWidth());
    translateManipulatorToggleButton->setSizePolicy(sizePolicy);
    QIcon icon3;
    icon3.addFile(QString::fromUtf8(":/images/Translate.png"), QSize(), QIcon::Normal, QIcon::Off);
    translateManipulatorToggleButton->setIcon(icon3);
    translateManipulatorToggleButton->setToolTip(tr("Toggle Translationmanipulator"));
    translateManipulatorToggleButton->setIconSize(leftButtonsSize);
    translateManipulatorToggleButton->setCheckable(true);
    translateManipulatorToggleButton->setFixedSize(leftButtonsSize);
    translateManipulatorToggleButton->setStyleSheet("background-color: " + buttonColor);
    translateManipulatorToggleButton->move(margin, rotateManipulatorToggleButton->pos().y() + leftButtonsSize.height() + spacing);
    translateManipulatorToggleButton->show();


    undoButton->setObjectName(QString::fromUtf8("undoButton"));
    sizePolicy.setHeightForWidth(undoButton->sizePolicy().hasHeightForWidth());
    undoButton->setSizePolicy(sizePolicy);
    QIcon icon4;
    icon4.addFile(QString::fromUtf8(":/icons/edit-undo-3.svg"), QSize(), QIcon::Normal, QIcon::Off);
    undoButton->setIcon(icon4);
    undoButton->setToolTip(tr("Undo last Action"));
    undoButton->setIconSize(leftButtonsSize);
    undoButton->setFixedSize(leftButtonsSize);
    undoButton->setStyleSheet("background-color: " + buttonColor);
    undoButton->move(margin, translateManipulatorToggleButton->pos().y() + leftButtonsSize.height() + spacing);
    undoButton->show();

    redoButton = new QPushButton(this);
    redoButton->setObjectName(QString::fromUtf8("redoButton"));
    sizePolicy.setHeightForWidth(redoButton->sizePolicy().hasHeightForWidth());
    redoButton->setSizePolicy(sizePolicy);
    QIcon icon5;
    icon5.addFile(QString::fromUtf8(":/icons/edit-redo-3.svg"), QSize(), QIcon::Normal, QIcon::Off);
    redoButton->setIcon(icon5);
    redoButton->setToolTip(tr("Redo last Action"));
    redoButton->setIconSize(leftButtonsSize);
    redoButton->setFixedSize(leftButtonsSize);
    redoButton->setStyleSheet("background-color: " + buttonColor);
    redoButton->move(margin, undoButton->pos().y() + leftButtonsSize.height() + spacing);
    redoButton->show();

    removeSelectedObjectsButton = new QPushButton(this);
    removeSelectedObjectsButton->setObjectName(QString::fromUtf8("removeSelectedObjectsButton"));
    sizePolicy.setHeightForWidth(removeSelectedObjectsButton->sizePolicy().hasHeightForWidth());
    removeSelectedObjectsButton->setSizePolicy(sizePolicy);
    QIcon icon6;
    icon6.addFile(QString::fromUtf8(":/icons/dialog-close.ico"), QSize(), QIcon::Normal, QIcon::Off);
    removeSelectedObjectsButton->setIcon(icon6);
    removeSelectedObjectsButton->setToolTip(tr("Remove selected Objects from scene"));
    removeSelectedObjectsButton->setIconSize(leftButtonsSize);
    removeSelectedObjectsButton->setFixedSize(leftButtonsSize);
    removeSelectedObjectsButton->setStyleSheet("background-color: " + buttonColor);
    removeSelectedObjectsButton->move(margin, redoButton->pos().y() + leftButtonsSize.height() + spacing);
    removeSelectedObjectsButton->show();

    viewerToggleButton->setObjectName(QString::fromUtf8("viewerToggleButton"));
    viewerToggleButton->setText(tr("Viewer"));
    viewerToggleButton->setCheckable(true);
    viewerToggleButton->adjustSize();
    viewerToggleButton->setStyleSheet("background-color: " + buttonColor);
    viewerToggleButton->show();
    editorViewerButtonGroup->addButton(viewerToggleButton, VIEWER_BUTTON_ID);

    editorToggleButton->setObjectName(QString::fromUtf8("editorToggleButton"));
    editorToggleButton->setText(tr("Editor"));
    editorToggleButton->setCheckable(true);
    editorToggleButton->setChecked(true);
    editorToggleButton->adjustSize();
    editorToggleButton->setStyleSheet("background-color: " + buttonColor);
    editorToggleButton->show();
    editorViewerButtonGroup->addButton(editorToggleButton, EDITOR_BUTTON_ID);

    connect(this, SIGNAL(sizeChangedSignal(QSize)), this, SLOT(sizeChanged(QSize)));
    connect(undoButton, SIGNAL(released()), this, SLOT(undoButtonReleased()));
    connect(redoButton, SIGNAL(released()), this, SLOT(redoButtonReleased()));
    connect(removeSelectedObjectsButton, SIGNAL(released()), this, SLOT(removeSelectedObjectsButtonReleased()));
    connect(collisionMeshToggleButton, SIGNAL(toggled(bool)), this, SLOT(collisionMeshToggleButtonToggled(bool)));
    connect(rotateManipulatorToggleButton, SIGNAL(toggled(bool)), this, SLOT(rotateManipulatorToggleButtonToggled(bool)));
    connect(translateManipulatorToggleButton, SIGNAL(toggled(bool)), this, SLOT(translateManipulatorToggleButtonToggled(bool)));
    connect(editorViewerButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(editorViewerButtonGroupButtonClicked(int)));

    std::shared_ptr<gui::ShortcutController> shortcutController = control->getShortcutController();

    QPointer<QAction> undoAction(new OverrideAction("Scene: Undo last Action", this));
    this->addAction(undoAction);
    connect(undoAction, SIGNAL(triggered()), undoButton, SLOT(click()));
    shortcutController->registerAction(undoAction->text(), undoAction);

    QPointer<QAction> redoAction(new OverrideAction("Scene: Redo last Action", this));
    this->addAction(redoAction);
    connect(redoAction, SIGNAL(triggered()), redoButton, SLOT(click()));
    shortcutController->registerAction(redoAction->text(), redoAction);

    QPointer<QAction> removeSelectedObjectsAction(new OverrideAction("Scene: Remove the selected Objects", this));
    this->addAction(removeSelectedObjectsAction);
    connect(removeSelectedObjectsAction, SIGNAL(triggered()), removeSelectedObjectsButton, SLOT(click()));
    shortcutController->registerAction(removeSelectedObjectsAction->text(), removeSelectedObjectsAction);

    QPointer<QAction> collisionAction(new OverrideAction("Scene: Toggle Collision Meshes", this));
    this->addAction(collisionAction);
    connect(collisionAction, SIGNAL(triggered()), collisionMeshToggleButton, SLOT(toggle()));
    shortcutController->registerAction(collisionAction->text(), collisionAction);

    QPointer<QAction> rotateAction(new OverrideAction("Scene: Toggle Rotation Manipulator", this));
    this->addAction(rotateAction);
    connect(rotateAction, SIGNAL(triggered()), rotateManipulatorToggleButton, SLOT(toggle()));
    shortcutController->registerAction(rotateAction->text(), rotateAction);

    QPointer<QAction> translateAction(new OverrideAction("Scene: Toggle Translation Manipulator", this));
    this->addAction(translateAction);
    connect(translateAction, SIGNAL(triggered()), translateManipulatorToggleButton, SLOT(toggle()));
    shortcutController->registerAction(translateAction->text(), translateAction);

    QPointer<QAction> editorAction(new OverrideAction("Scene: Switch to Editor Mode", this));
    this->addAction(editorAction);
    connect(editorAction, SIGNAL(triggered()), editorToggleButton, SLOT(click()));
    shortcutController->registerAction(editorAction->text(), editorAction);

    QPointer<QAction> viewerAction(new OverrideAction("Scene: Switch to Viewer Mode", this));
    this->addAction(viewerAction);
    connect(viewerAction, SIGNAL(triggered()), viewerToggleButton, SLOT(click()));
    shortcutController->registerAction(viewerAction->text(), viewerAction);

    QPointer<QAction> switchEditorViewerAction(new OverrideAction("Scene: Switch between Editor and Viewer Mode", this));
    this->addAction(switchEditorViewerAction);
    connect(switchEditorViewerAction, SIGNAL(triggered()), this, SLOT(toggleEditorViewerMode()));
    shortcutController->registerAction(switchEditorViewerAction->text(), switchEditorViewerAction);

    emit sizeChanged(this->size());
}

gui::SceneEditorWidget::~SceneEditorWidget()
{
}

void gui::SceneEditorWidget::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
    emit sizeChangedSignal(event->size());
}

void gui::SceneEditorWidget::sizeChanged(QSize newSize)
{
    viewerToggleButton->move(newSize.width() - margin - viewerToggleButton->size().width(), margin);
    editorToggleButton->move(viewerToggleButton->pos().x() - editorToggleButton->size().width(), margin);
}

void gui::SceneEditorWidget::undoButtonReleased()
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        sharedController->undo();
    }
}

void gui::SceneEditorWidget::redoButtonReleased()
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        sharedController->redo();
    }
}

void gui::SceneEditorWidget::removeSelectedObjectsButtonReleased()
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());
        std::vector<scene3D::SceneObjectPtr> selectedObjects = sharedController->getScene()->getSelectionManager()->getAllSelected();
        std::vector<scene3D::SceneGroupPtr> groups = sharedController->getScene()->getGroupManager()->getAllGroups();

        for (auto it = selectedObjects.rbegin(); it != selectedObjects.rend(); ++it)
        {
            controller::OperationPtr deselectOperation(new controller::DeselectOperation(sharedController->getMemoryXController(), sharedController->getScene(), (*it)->getObjectId()));
            operations->push_back(deselectOperation);

            for (auto itG = groups.begin(); itG != groups.end(); ++itG)
            {
                if ((*itG)->contains(*it))
                {
                    controller::OperationPtr removeFromGroupOperation(new controller::RemoveFromGroupOperation(sharedController->getMemoryXController(), sharedController->getScene(), (*itG)->getGroupId(), (*it)->getObjectId()));
                    operations->push_back(removeFromGroupOperation);
                }
            }

            controller::OperationPtr removeOperation(new controller::RemoveOperation(sharedController->getMemoryXController(), sharedController->getScene(), (*it)->getObjectId()));
            operations->push_back(removeOperation);
        }

        sharedController->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
    }
}

void gui::SceneEditorWidget::collisionMeshToggleButtonToggled(bool checked)
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        for (scene3D::SceneObjectPtr object : sharedController->getScene()->getObjectManager()->getAllObjects())
        {
            object->showCollisionMesh(checked);
        }
    }
}

void gui::SceneEditorWidget::rotateManipulatorToggleButtonToggled(bool checked)
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        scene3D::ManipulatorMode mode;

        if (checked)
        {
            if (sharedController->getScene()->getManipulatorManager()->getManipulatorMode()
                == scene3D::ManipulatorMode::TRANSLATION)
            {
                mode = scene3D::ManipulatorMode::ALL;
            }
            else
            {
                mode = scene3D::ManipulatorMode::ROTATION;
            }
        }
        else
        {
            if (sharedController->getScene()->getManipulatorManager()->getManipulatorMode()
                == scene3D::ManipulatorMode::ALL)
            {
                mode = scene3D::ManipulatorMode::TRANSLATION;
            }
            else
            {
                mode = scene3D::ManipulatorMode::NONE;
            }
        }

        sharedController->getScene()->getManipulatorManager()->setManipulatorMode(mode);
    }
}

void gui::SceneEditorWidget::translateManipulatorToggleButtonToggled(bool checked)
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        scene3D::ManipulatorMode mode;

        if (checked)
        {
            if (sharedController->getScene()->getManipulatorManager()->getManipulatorMode()
                == scene3D::ManipulatorMode::ROTATION)
            {
                mode = scene3D::ManipulatorMode::ALL;
            }
            else
            {
                mode = scene3D::ManipulatorMode::TRANSLATION;
            }
        }
        else
        {
            if (sharedController->getScene()->getManipulatorManager()->getManipulatorMode()
                == scene3D::ManipulatorMode::ALL)
            {
                mode = scene3D::ManipulatorMode::ROTATION;
            }
            else
            {
                mode = scene3D::ManipulatorMode::NONE;
            }
        }

        sharedController->getScene()->getManipulatorManager()->setManipulatorMode(mode);
    }
}

void gui::SceneEditorWidget::editorViewerButtonGroupButtonClicked(int id)
{
    if (controller::ControllerPtr sharedController = control.lock())
    {
        if (id == VIEWER_BUTTON_ID)
        {
            showCollisionMesh = collisionMeshToggleButton->isChecked();

            if (showCollisionMesh)
            {
                collisionMeshToggleButton->click();
            }

            collisionMeshToggleButton->hide();
            collisionMeshToggleButton->setEnabled(false);
            collisionMeshToggleButton->setCheckable(false);
            showRotateManipulator = rotateManipulatorToggleButton->isChecked();

            if (showRotateManipulator)
            {
                rotateManipulatorToggleButton->click();
            }

            rotateManipulatorToggleButton->hide();
            rotateManipulatorToggleButton->setEnabled(false);
            rotateManipulatorToggleButton->setCheckable(false);
            showTranslateManipulator = translateManipulatorToggleButton->isChecked();

            if (showTranslateManipulator)
            {
                translateManipulatorToggleButton->click();
            }

            translateManipulatorToggleButton->hide();
            translateManipulatorToggleButton->setEnabled(false);
            translateManipulatorToggleButton->setCheckable(false);
            undoButton->hide();
            undoButton->setEnabled(false);
            redoButton->hide();
            redoButton->setEnabled(false);
            removeSelectedObjectsButton->hide();
            removeSelectedObjectsButton->setEnabled(false);

            sharedController->getScene()->enterViewerMode();
        }
        else if (id == EDITOR_BUTTON_ID)
        {
            collisionMeshToggleButton->show();
            collisionMeshToggleButton->setEnabled(true);
            collisionMeshToggleButton->setCheckable(true);

            if (showCollisionMesh)
            {
                collisionMeshToggleButton->click();
            }

            rotateManipulatorToggleButton->show();
            rotateManipulatorToggleButton->setEnabled(true);
            rotateManipulatorToggleButton->setCheckable(true);

            if (showRotateManipulator)
            {
                rotateManipulatorToggleButton->click();
            }

            translateManipulatorToggleButton->show();
            translateManipulatorToggleButton->setEnabled(true);
            translateManipulatorToggleButton->setCheckable(true);

            if (showTranslateManipulator)
            {
                translateManipulatorToggleButton->click();
            }

            undoButton->show();
            undoButton->setEnabled(true);
            redoButton->show();
            redoButton->setEnabled(true);
            removeSelectedObjectsButton->show();
            removeSelectedObjectsButton->setEnabled(true);

            sharedController->getScene()->enterEditorMode();
        }
    }
}

void gui::SceneEditorWidget::toggleEditorViewerMode()
{
    switch (this->editorViewerButtonGroup->checkedId())
    {
        case VIEWER_BUTTON_ID:
            this->editorToggleButton->click();
            break;

        case EDITOR_BUTTON_ID:
            this->viewerToggleButton->click();
            break;
    }
}

void gui::SceneEditorWidget::retranslate()
{
    collisionMeshToggleButton->setToolTip(tr("Toggle Collisionmesh"));
    rotateManipulatorToggleButton->setToolTip(tr("Toggle Rotationmanipulator"));
    translateManipulatorToggleButton->setToolTip(tr("Toggle Translationmanipulator"));
    undoButton->setToolTip(tr("Undo last Action"));
    redoButton->setToolTip(tr("Redo last Action"));
    removeSelectedObjectsButton->setToolTip(tr("Remove selected Objects from scene"));
    viewerToggleButton->setText(tr("Viewer"));
    editorToggleButton->setText(tr("Editor"));
}

void gui::SceneEditorWidget::postDocking()
{
    sceneGraph->postDocking();
}
