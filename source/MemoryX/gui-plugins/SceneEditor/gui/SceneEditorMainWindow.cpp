/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SceneEditorMainWindow.h"

#include <QDockWidget>
#include <QActionGroup>
#include "dialog/GroupExplorerDialog.h"

gui::SceneEditorMainWindow::SceneEditorMainWindow(const controller::ControllerPtr& control, QPointer<dialog::GroupExplorerDialog> groupEditorDialog, QWidget* parent) :
    QMainWindow(parent), groupEditorDialog(groupEditorDialog)
{
    // create DockWidget for Object Inspector
    objectInspectorDock = new gui::CollapsibleDockWidget(tr("Object Inspector"), this);
    objectInspectorDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    objectInspectorDock->setAllowedAreas(Qt::LeftDockWidgetArea);
    objectInspector = new ObjectInspectorWidget(control, objectInspectorDock);
    objectInspectorDock->setCollapsibleWidget(objectInspector);
    addDockWidget(Qt::LeftDockWidgetArea, objectInspectorDock);

    // create DockWidget for Object Explorer
    objectExplorerDock = new gui::CollapsibleDockWidget(tr("Object Explorer"), this);
    objectExplorerDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    objectExplorerDock->setAllowedAreas(Qt::RightDockWidgetArea);
    objectExplorer = new ObjectExplorerWidget(control, objectExplorerDock);
    objectExplorerDock->setCollapsibleWidget(objectExplorer);
    addDockWidget(Qt::RightDockWidgetArea, objectExplorerDock);

    // create DockWidget for Minimap
    minimapDock = new gui::CollapsibleDockWidget(tr("Minimap"), this);
    minimapDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    minimapDock->setAllowedAreas(Qt::LeftDockWidgetArea);
    minimap = new MinimapWidget(control, minimapDock);
    minimapDock->setCollapsibleWidget(minimap);
    addDockWidget(Qt::LeftDockWidgetArea, minimapDock);

    // create DockWidget for Group Explorer
    groupExplorerDock = new gui::CollapsibleDockWidget(tr("Group Explorer"), this);
    groupExplorerDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    groupExplorerDock->setAllowedAreas(Qt::RightDockWidgetArea);
    groupExplorer = new GroupExplorerWidget(control, groupEditorDialog, groupExplorerDock);
    groupExplorerDock->setCollapsibleWidget(groupExplorer);
    addDockWidget(Qt::RightDockWidgetArea, groupExplorerDock);

    mainControl = control;
}

gui::SceneEditorMainWindow::~SceneEditorMainWindow()
{

}

void gui::SceneEditorMainWindow::postDocking()
{
    // set SceneEditorWidget as centralWidget
    scene = new SceneEditorWidget(mainControl, this->centralWidget());
    setCentralWidget(scene);
    this->setEnabled(false);

    scene->postDocking();
}

void gui::SceneEditorMainWindow::onConnect()
{
    objectExplorer->onConnect();
}

void gui::SceneEditorMainWindow::onDisconnect()
{
}

void gui::SceneEditorMainWindow::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        groupExplorer->retranslate();
        objectInspector->retranslate();
        objectExplorer->retranslate();
        scene->retranslate();
        groupEditorDialog->retranslate();
        objectExplorerDock->setWindowTitle(tr("Object Explorer"));
        objectExplorerDock->windowTitleChanged();
        objectInspectorDock->setWindowTitle(tr("Object Inspector"));
        objectInspectorDock->windowTitleChanged();
        groupExplorerDock->setWindowTitle(tr("Group Explorer"));
        groupExplorerDock->windowTitleChanged();
        minimapDock->setWindowTitle(tr("Minimap"));
        minimapDock->windowTitleChanged();

        emit languageChangedByUser();
    }
    else
    {
        QMainWindow::changeEvent(event);
    }

}
