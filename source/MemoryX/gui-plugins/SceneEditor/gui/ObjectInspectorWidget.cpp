/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

/* Coin headers */
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/engines/SoElapsedTime.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>

/* Qt headers */
#include <QVBoxLayout>

#include "PropertyBrowserWidget.h"
#include "ObjectInspectorWidget.h"

gui::ObjectInspectorWidget::ObjectInspectorWidget(const controller::ControllerPtr& control, QWidget* parent) :
    QWidget(parent),
    control(control),
    propertyBrowser(new gui::PropertyBrowserWidget(control, this)),
    currentObjectId()
{
    QVBoxLayout* layout = new QVBoxLayout(this);
    QWidget* visualModelWidget = new QWidget(this);
    visualModelWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    visualModelWidget->setMinimumHeight(180);

    // Empty SoQtExaminerViewer for preview image
    viewer.reset(new scene3D::ObjectPreviewViewer(control, visualModelWidget));
    viewer->showNoObject();

    layout->addWidget(propertyBrowser);
    layout->addWidget(visualModelWidget);

    setMinimumWidth(200);

    connect(control.get(), SIGNAL(objectClassSelected(const std::string&, const std::string&)), this, SLOT(setClassAttributes(const std::string&, const std::string&)));
    connect(control.get(), SIGNAL(sceneObjectSelected(scene3D::SceneObjectPtr)), this, SLOT(setAllAttributes(scene3D::SceneObjectPtr)));
    connect(control.get(), SIGNAL(operationExecuted(controller::vector_string)), this, SLOT(sceneObjectsUpdated(controller::vector_string)));
    connect(control.get(), SIGNAL(objectsChanged(controller::vector_string)), this, SLOT(sceneObjectsUpdated(controller::vector_string)));
}

gui::ObjectInspectorWidget::~ObjectInspectorWidget()
{
    //
}

void gui::ObjectInspectorWidget::sceneObjectsUpdated(controller::vector_string objectIds)
{

    if (!currentObjectId.empty())
    {
        for (auto iterator = objectIds.begin(); iterator != objectIds.end(); ++iterator)
        {
            if (currentObjectId == *iterator)
            {
                if (controller::ControllerPtr controller = control.lock())
                {
                    propertyBrowser->updateSceneObject(currentObjectId);
                }
            }
        }
    }

}

memoryx::ObjectClassPtr gui::ObjectInspectorWidget::getObjectClass(const std::string& objectClass, const std::string& collection)
{

    if (controller::ControllerPtr controller = control.lock())
    {
        return controller->getMemoryXController()->getPriorKnowlegdeController()->getObjectClassPtr(objectClass, collection);
    }

    return NULL;

}

void gui::ObjectInspectorWidget::setClassAttributes(const std::string& objectClass, const std::string& collection)
{

    currentObjectId.clear();
    memoryx::ObjectClassPtr objectClassPtr = getObjectClass(objectClass, collection);

    propertyBrowser->setProperties(objectClassPtr, collection);

    showPreviewImage(objectClassPtr);

}

void gui::ObjectInspectorWidget::setAllAttributes(scene3D::SceneObjectPtr sceneObject)
{
    propertyBrowser->setProperties(sceneObject);

    // SceneObject should not be null
    if (sceneObject)
    {
        currentObjectId = sceneObject->getObjectId();
        showPreviewImage(getObjectClass(sceneObject->getClassId(), sceneObject->getCollection()));
    }
    else
    {
        currentObjectId.clear();
        viewer->showNoObject();
    }
}

void gui::ObjectInspectorWidget::showPreviewImage(const memoryx::ObjectClassPtr& objectClass)
{
    if (controller::ControllerPtr controller = control.lock())
    {
        viewer->setObjectToDisplay(controller->getMemoryXController()->getPriorKnowlegdeController()->getCoinVisualisation(objectClass, false));
    }
}

void gui::ObjectInspectorWidget::retranslate()
{
    // stays empty for now
}
