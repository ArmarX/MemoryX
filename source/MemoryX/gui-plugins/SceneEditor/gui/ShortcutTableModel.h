/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QAbstractTableModel>
#include <QHash>
#include <QKeySequence>

namespace gui
{
    class ShortcutTableModel : public QAbstractTableModel
    {
        Q_OBJECT

    public:
        /**
        * A constructor.
        * Creates a new Instance of ShortcutTableModel with a given parent.
        *
        * @param parent Parent QObject
        */
        explicit ShortcutTableModel(QObject* parent = 0);
        /**
        * A constructor.
        * Creates a new Instance of ShortcutTableModel with a predefined Shortcuts-Hash and a given parent.
        *
        * @param Shortcuts QHash of predefined Shortcuts-Hash
        * @param parent Parent QObject
        */
        explicit ShortcutTableModel(QHash<QString, QKeySequence> Shortcuts, QObject* parent = 0);

        /**
        * Sets a predefined Shortcuts-Hash as new Hash of this ShortcutTableModel.
        *
        * @param Shortcuts QHash of predefined Shortcuts-Hash
        */
        void setShortcutHashTable(QHash<QString, QKeySequence> Shortcuts);

        /**
        * Returns the number of given rows.
        *
        * @param parent Parent QObject
        * @return number of rows
        */
        int rowCount(const QModelIndex& parent) const override;

        /**
        * Returns the number of given columns (standard = 2).
        *
        * @param parent Parent QObject
        * @return number of columns
        */
        int columnCount(const QModelIndex& parent) const override;

        /**
        * Returns the value of a given Position in the model.
        *
        * @param index Position in Model to be returned
        * @param role Role that is used to get data
        * @return Value of given Position
        */
        QVariant data(const QModelIndex& index, int role) const override;

        /**
        * Returns the header of a given Section by orientation.
        *
        * @param section Section in Header to be returned
        * @param orientation Orientation to find given section
        * @param role Role that is used to get data
        * @return Headervalue of given Section
        */
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

        /**
        * Returns the Name of the Action of the given Row.
        *
        * @param index Position in Model to be returned
        * @param role Role that is used to get data
        * @return Name of the action
        */
        QVariant actionName(const QModelIndex& index, int role) const;

    private:
        QHash<QString, QKeySequence> registeredShortcuts;

    };

}

