/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

/* Qt headers */
#include <QString>
#include <QTreeView>
#include <QImage>
#include <QPixmap>
#include <QIcon>
#include <QLabel>

/* C++ headers */
#include <iostream>

#include "ObjectExplorerWidget.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_ObjectExplorerWidget.h>
#include "ClearableLineEdit.h"
#include "OverrideAction.h"

gui::ObjectExplorerWidget::ObjectExplorerWidget(const controller::ControllerPtr& control, QWidget* parent) :
    QWidget(parent),
    ui(new Ui::ObjectExplorerWidget),
    control(control),
    sourceModel(new ObjectExplorerModel(control, this))
{
    ui->setupUi(this);

    ui->objectExplorerTreeView->setModel(sourceModel);

    std::shared_ptr<gui::ShortcutController> shortcutController = control->getShortcutController();
    QPointer<QAction> searchAction(new OverrideAction("Object Explorer: Set Focus on Search Field", this));
    this->addAction(searchAction);
    shortcutController->registerAction(searchAction->text(), searchAction);

    setMinimumWidth(200);

    connect(searchAction, SIGNAL(triggered()), ui->filterObjectClassesLineEdit, SLOT(setFocus()));
    connect(ui->filterObjectClassesLineEdit, SIGNAL(textChanged(QString)), this, SLOT(filterFixedString()));
    connect(ui->objectExplorerTreeView, SIGNAL(clicked(QModelIndex)), this, SLOT(getSelectedObject(QModelIndex)));
}

void gui::ObjectExplorerWidget::filterFixedString()
{
    QString searchPattern(ui->filterObjectClassesLineEdit->text());
    sourceModel->setFilterFixedString(searchPattern);

    if (searchPattern.isEmpty())
    {
        ui->objectExplorerTreeView->collapseAll();
    }
    else
    {
        ui->objectExplorerTreeView->expandAll();
    }

}

void gui::ObjectExplorerWidget::getSelectedObject(const QModelIndex& index)
{
    std::pair<std::string, std::string> object = sourceModel->getItemInformation(index);

    if (!object.first.empty() && !object.second.empty())
    {
        if (controller::ControllerPtr controller = control.lock())
        {
            controller->triggerObjectClassSelected(object.first, object.second);
        }
    }
}

void gui::ObjectExplorerWidget::onConnect()
{
    sourceModel->onConnect();
    ui->objectExplorerTreeView->setModel(sourceModel);
}

gui::ObjectExplorerWidget::~ObjectExplorerWidget()
{
    delete ui;
}

void gui::ObjectExplorerWidget::retranslate()
{
    ui->retranslateUi(this);
}
