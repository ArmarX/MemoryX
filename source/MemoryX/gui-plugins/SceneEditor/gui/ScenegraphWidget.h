/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QWidget>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include <QDropEvent>

/* coin headers */
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>

#include "../controller/Controller.h"
#include "../scene3D/SceneViewer.h"

using SceneViewerPtr = std::shared_ptr<scene3D::SceneViewer>;

namespace gui
{
    /**
    * This class provides a widget which holds a scene3D::SceneViewer in which the scene graph is rendered.
    *
    * @see scene3D::SceneViewer
    *
    */
    class ScenegraphWidget : public QWidget
    {
        Q_OBJECT
    public:

        /**
        * Constructor.
        * Constructs a scene graph widget.
        * Expects a controller::ControllerPtr.
        *
        * @param    control     shared pointer to controller::controller
        * @param    parent      parent widget
        *
        */
        explicit ScenegraphWidget(const controller::ControllerPtr control, QWidget* parent = 0);

        /**
        * Destructor.
        *
        */
        ~ScenegraphWidget() override;

        void postDocking();

    protected:

        /**
        * Reimplements the function Qt::QWidget:dropEvent().
        *
        * This event handler is called when the drag is dropped on this widget.
        * The event is passed in the <i>event</i> parameter.
        *
        * @param    dropEvent   Qt::DropEvent
        * @see      Qt::DropEvent
        * @see      Qt::QWidget:dropEvent()
        *
        */
        void dropEvent(QDropEvent* de) override;

        /**
        * Reimplements the function Qt::QWidget:dragMoveEvent();.
        *
        * This event handler is called if a drag is in progress, and when any of the following conditions occur:
        * the cursor enters this widget, the cursor moves within this widget, or a modifier key is pressed on the keyboard while this widget has the focus.
        * The event is passed in the <i>event</i> parameter.
        *
        * @param    dropEvent   Qt::QDragMoveEvent
        * @see      Qt::QDragMoveEvent
        * @see      Qt::QWidget:dragMoveEvent();
        *
        */
        void dragMoveEvent(QDragMoveEvent* de) override;

        /**
        * Reimplements the function Qt::QWidget:dragEnterEvent();.
        *
        * This event handler is called when a drag is in progress and the mouse enters this widget.
        * The event is passed in the <i>event</i> parameter.
        *
        * @param    dropEvent   Qt::DragEnterEvent
        * @see      Qt::QDragEnterEvent
        * @see      Qt::QWidget:dragEnterEvent();
        *
        */
        void dragEnterEvent(QDragEnterEvent* event) override;

    Q_SIGNALS:

    public Q_SLOTS:
        void cameraViewAll();
        void cameraViewFromX();
        void cameraViewFromNegX();
        void cameraViewFromY();
        void cameraViewFromNegY();
        void cameraViewFromZ();
        void cameraViewFromNegZ();
        void selectToggleAll();
        void copy();
        void paste();
        void duplicate();
        void resetRotation();
        void resetTranslation();

    private:
        controller::ControllerWeakPtr control;
        SceneViewerPtr viewer;
        SoPerspectiveCamera* camera;
        controller::ControllerPtr mainController;

        std::shared_ptr<std::vector<controller::AddOperationPtr> > copyBuffer;
    };
}

