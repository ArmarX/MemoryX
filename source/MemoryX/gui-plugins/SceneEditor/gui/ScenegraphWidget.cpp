/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ScenegraphWidget.h"
#include <Inventor/actions/SoRayPickAction.h>
#include <QGridLayout>
#include <QDataStream>
#include <QByteArray>
#include <vector>
#include <math.h>

#include "../controller/AddOperation.h"
#include "../controller/SelectOperation.h"
#include "../controller/DeselectOperation.h"
#include "../controller/RotateTranslateOperation.h"
#include "OverrideAction.h"
#include "QMimeData"


gui::ScenegraphWidget::ScenegraphWidget(const controller::ControllerPtr control, QWidget* parent) :
    QWidget(parent),
    control(control),
    camera(new SoPerspectiveCamera),
    copyBuffer(new std::vector<controller::AddOperationPtr>())
{
    //setPalette(Qt::transparent);
    //setAttribute(Qt::WA_TransparentForMouseEvents);
    mainController = control;

    this->setContentsMargins(1, 1, 1, 1);

    QGridLayout* grid = new QGridLayout();
    grid->setContentsMargins(0, 0, 0, 0);
    this->setLayout(grid);
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    QWidget* view1 = new QWidget(this);
    view1->setMinimumSize(100, 100);
    view1->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    grid->addWidget(view1, 0, 0, 1, 2);

    viewer.reset(new scene3D::SceneViewer(control, view1));

    std::shared_ptr<gui::ShortcutController> shortcutController = control->getShortcutController();

    //Camera view all action
    QPointer<QAction> cameraViewAllAction(new OverrideAction("Scene: Camera view all", this));
    this->addAction(cameraViewAllAction);
    connect(cameraViewAllAction, SIGNAL(triggered()), this, SLOT(cameraViewAll()));
    shortcutController->registerAction(cameraViewAllAction->text(), cameraViewAllAction);

    //Camera view from X action
    QPointer<QAction> cameraViewFromXAction(new OverrideAction("Scene: Camera view from X", this));
    this->addAction(cameraViewFromXAction);
    connect(cameraViewFromXAction, SIGNAL(triggered()), this, SLOT(cameraViewFromX()));
    shortcutController->registerAction(cameraViewFromXAction->text(), cameraViewFromXAction);

    //Camera view from neg X action
    QPointer<QAction> cameraViewFromNegXAction(new OverrideAction("Scene: Camera view from negative X", this));
    this->addAction(cameraViewFromNegXAction);
    connect(cameraViewFromNegXAction, SIGNAL(triggered()), this, SLOT(cameraViewFromNegX()));
    shortcutController->registerAction(cameraViewFromNegXAction->text(), cameraViewFromNegXAction);

    //Camera view from Y action
    QPointer<QAction> cameraViewFromYAction(new OverrideAction("Scene: Camera view from Y", this));
    this->addAction(cameraViewFromYAction);
    connect(cameraViewFromYAction, SIGNAL(triggered()), this, SLOT(cameraViewFromY()));
    shortcutController->registerAction(cameraViewFromYAction->text(), cameraViewFromYAction);

    //Camera view from neg Y action
    QPointer<QAction> cameraViewFromNegYAction(new OverrideAction("Scene: Camera view from negative Y", this));
    this->addAction(cameraViewFromNegYAction);
    connect(cameraViewFromNegYAction, SIGNAL(triggered()), this, SLOT(cameraViewFromNegY()));
    shortcutController->registerAction(cameraViewFromNegYAction->text(), cameraViewFromNegYAction);

    //Camera view from Z action
    QPointer<QAction> cameraViewFromZAction(new OverrideAction("Scene: Camera view from Z", this));
    this->addAction(cameraViewFromZAction);
    connect(cameraViewFromZAction, SIGNAL(triggered()), this, SLOT(cameraViewFromZ()));
    shortcutController->registerAction(cameraViewFromZAction->text(), cameraViewFromZAction);

    //Camera view from neg Z action
    QPointer<QAction> cameraViewFromNegZAction(new OverrideAction("Scene: Camera view from negative Z", this));
    this->addAction(cameraViewFromNegZAction);
    connect(cameraViewFromNegZAction, SIGNAL(triggered()), this, SLOT(cameraViewFromNegZ()));
    shortcutController->registerAction(cameraViewFromNegZAction->text(), cameraViewFromNegZAction);

    //Select all or deselect all action
    QPointer<QAction> selectToggleAllAction(new OverrideAction("Scene: Select all or deselect all", this));
    this->addAction(selectToggleAllAction);
    connect(selectToggleAllAction, SIGNAL(triggered()), this, SLOT(selectToggleAll()));
    shortcutController->registerAction(selectToggleAllAction->text(), selectToggleAllAction);

    //Copy action
    QPointer<QAction> copyAction(new OverrideAction("Scene: Copy selection", this));
    this->addAction(copyAction);
    connect(copyAction, SIGNAL(triggered()), this, SLOT(copy()));
    shortcutController->registerAction(copyAction->text(), copyAction);

    //Paste action
    QPointer<QAction> pasteAction(new OverrideAction("Scene: Paste selection", this));
    this->addAction(pasteAction);
    connect(pasteAction, SIGNAL(triggered()), this, SLOT(paste()));
    shortcutController->registerAction(pasteAction->text(), pasteAction);

    //Duplicate action
    QPointer<QAction> duplicateAction(new OverrideAction("Scene: Duplicate selection", this));
    this->addAction(duplicateAction);
    connect(duplicateAction, SIGNAL(triggered()), this, SLOT(duplicate()));
    shortcutController->registerAction(duplicateAction->text(), duplicateAction);

    //Reset rotation action
    QPointer<QAction> resetRotationAction(new OverrideAction("Scene: Reset rotation", this));
    this->addAction(resetRotationAction);
    connect(resetRotationAction, SIGNAL(triggered()), this, SLOT(resetRotation()));
    shortcutController->registerAction(resetRotationAction->text(), resetRotationAction);

    //Reset translation action
    QPointer<QAction> resetTranslationAction(new OverrideAction("Scene: Reset translation", this));
    this->addAction(resetTranslationAction);
    connect(resetTranslationAction, SIGNAL(triggered()), this, SLOT(resetTranslation()));
    shortcutController->registerAction(resetTranslationAction->text(), resetTranslationAction);
}

gui::ScenegraphWidget::~ScenegraphWidget()
{
}

void gui::ScenegraphWidget::postDocking()
{
    viewer->setSceneGraph(mainController->getScene()->registerCamera(camera));

    //Give camera standard position
    camera->position.setValue(SbVec3f(10, -10, 5));
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));


    //Apply custom highlighting and make viewer redraw on selection changes
    viewer->setGLRenderAction(new scene3D::SoGLHighlightRenderAction(viewer->getViewportRegion(), mainController->getScene().get()));
    viewer->redrawOnSelectionChange(mainController->getScene()->selectionRootNode);

    // show everything
    viewer->show();

    setAcceptDrops(true);

    connect(mainController.get(), SIGNAL(reloadScene()), this, SLOT(cameraViewAll()));
    connect(mainController.get(), SIGNAL(minimapClicked()), this, SLOT(cameraViewFromZ()));
}

void gui::ScenegraphWidget::resetRotation()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        for (scene3D::SceneObjectPtr sceneobject : controller->getScene()->sceneSelectionManager->getAllSelected())
        {
            controller::OperationPtr operation(new controller::RotateTranslateOperation(
                                                   controller->getMemoryXController(),
                                                   controller->getScene(), sceneobject->getObjectId(),
                                                   sceneobject->getRotation(), SbRotation(SbVec3f(0, 0, 1), 0),
                                                   sceneobject->getTranslation(), sceneobject->getTranslation()));

            operations->push_back(operation);
        }

        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
    }
}

void gui::ScenegraphWidget::resetTranslation()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        for (scene3D::SceneObjectPtr sceneobject : controller->getScene()->sceneSelectionManager->getAllSelected())
        {
            controller::OperationPtr operation(new controller::RotateTranslateOperation(
                                                   controller->getMemoryXController(),
                                                   controller->getScene(), sceneobject->getObjectId(),
                                                   sceneobject->getRotation(), sceneobject->getRotation(),
                                                   sceneobject->getTranslation(), SbVec3f(0, 0, 0)));

            operations->push_back(operation);
        }

        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
    }
}
void gui::ScenegraphWidget::copy()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        //Clear copy buffer
        copyBuffer->clear();

        for (scene3D::SceneObjectPtr sceneobject : controller->getScene()->sceneSelectionManager->getAllSelected())
        {
            controller::AddOperationPtr addOperation(new controller::AddOperation(
                        controller->getMemoryXController(), controller->getScene(),
                        sceneobject->getClassId(), sceneobject->getCollection(),
                        sceneobject->getTranslation(), sceneobject->getRotation()));

            copyBuffer->push_back(addOperation);
        }
    }
}

void gui::ScenegraphWidget::paste()
{
    if (copyBuffer->size() != 0)
    {
        if (controller::ControllerPtr controller = control.lock())
        {
            std::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

            // deselect all objects
            for (scene3D::SceneObjectPtr object : controller->getScene()->getSelectionManager()->getAllSelected())
            {
                controller::OperationPtr deselect(new controller::DeselectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
                operations->push_back(deselect);
            }

            // add new objects
            for (auto it = copyBuffer->begin(); it != copyBuffer->end(); ++it)
            {
                controller::OperationPtr add(new controller::AddOperation(*(*it)));
                operations->push_back(add);
                controller::OperationPtr select(new controller::SelectOperation(controller->getMemoryXController(), controller->getScene(), add->getObjectId()));
                operations->push_back(select);
            }

            controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
        }
    }
}

void gui::ScenegraphWidget::duplicate()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        std::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        for (scene3D::SceneObjectPtr object : controller->getScene()->getSelectionManager()->getAllSelected())
        {
            controller::OperationPtr deselect(new controller::DeselectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
            operations->push_back(deselect);
            controller::OperationPtr add(new controller::AddOperation(
                                             controller->getMemoryXController(), controller->getScene(),
                                             object->getClassId(), object->getCollection(),
                                             object->getTranslation(), object->getRotation()));
            operations->push_back(add);
            controller::OperationPtr select(new controller::SelectOperation(controller->getMemoryXController(), controller->getScene(), add->getObjectId()));
            operations->push_back(select);
        }

        controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
    }
}

void gui::ScenegraphWidget::cameraViewAll()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        controller->getScene()->makeCameraViewAll(camera, viewer->getViewportRegion());
    }
}

void gui::ScenegraphWidget::cameraViewFromX()
{
    //Make sure we are on x axis
    camera->position.setValue(1, 0, 0);
    //Look at scene
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));

    //View everything in bounding box
    this->cameraViewAll();
}

void gui::ScenegraphWidget::cameraViewFromNegX()
{
    //Make sure we are on x axis
    camera->position.setValue(-1, 0, 0);
    //Look at scene
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));

    //View everything in bounding box
    this->cameraViewAll();
}

void gui::ScenegraphWidget::cameraViewFromY()
{
    //Make sure we are on x axis
    camera->position.setValue(0, 1, 0);
    //Look at scene
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));

    //View everything in bounding box
    this->cameraViewAll();
}

void gui::ScenegraphWidget::cameraViewFromNegY()
{
    //Make sure we are on x axis
    camera->position.setValue(0, -1, 0);
    //Look at scene
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));

    //View everything in bounding box
    this->cameraViewAll();
}

void gui::ScenegraphWidget::cameraViewFromZ()
{
    //Make sure we are on x axis
    camera->position.setValue(0, 0, 1);
    //Look at scene
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 1, 0));

    //View everything in bounding box
    this->cameraViewAll();
}

void gui::ScenegraphWidget::cameraViewFromNegZ()
{
    //Make sure we are on x axis
    camera->position.setValue(0, 0, -1);
    //Look at scene
    camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 1, 0));

    //View everything in bounding box
    this->cameraViewAll();
}

void gui::ScenegraphWidget::selectToggleAll()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        //Check if all objects are selected
        bool allObjectsAreSelected = true;

        for (scene3D::SceneObjectPtr object : controller->getScene()->getObjectManager()->getAllObjects())
        {
            if (!controller->getScene()->getSelectionManager()->isSelected(object))
            {
                allObjectsAreSelected = false;
                break;
            }
        }

        //Prepare some operations
        std::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        if (allObjectsAreSelected)
        {
            //Deselect all
            for (scene3D::SceneObjectPtr object : controller->getScene()->getSelectionManager()->getAllSelected())
            {
                controller::OperationPtr deselect(new controller::DeselectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
                operations->push_back(deselect);
            }
        }
        else
        {
            //Select all
            for (scene3D::SceneObjectPtr object : controller->getScene()->getObjectManager()->getAllObjects())
            {
                if (!controller->getScene()->getSelectionManager()->isSelected(object))
                {
                    controller::OperationPtr select(new controller::SelectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
                    operations->push_back(select);
                }
            }
        }

        //Execute stuff
        controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
    }
}

void gui::ScenegraphWidget::dragMoveEvent(QDragMoveEvent* event)
{
    // The event needs to be accepted here
    event->accept();
}

void gui::ScenegraphWidget::dragEnterEvent(QDragEnterEvent* event)
{
    // Set the drop action to be the proposed action.
    if (event->source())
    {
        event->acceptProposedAction();
    }
    else
    {
        event->ignore();
    }
}

void gui::ScenegraphWidget::dropEvent(QDropEvent* event)
{
    if (!event->mimeData()->hasFormat("application/vnd.text.list"))
    {
        return;
    }

    if (controller::ControllerPtr controller = control.lock())
    {
        QByteArray encodedData = event->mimeData()->data("application/vnd.text.list");
        QDataStream stream(&encodedData, QIODevice::ReadOnly);

        std::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

        // deselect all objects
        for (scene3D::SceneObjectPtr object : controller->getScene()->getSelectionManager()->getAllSelected())
        {
            controller::OperationPtr deselect(new controller::DeselectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
            operations->push_back(deselect);
        }

        // add new objects
        while (!stream.atEnd())
        {
            QString collection, objectType;
            stream >> collection;
            stream >> objectType;

            // get needed information
            SoCamera* pCamera = viewer->getCamera();
            float aspect = viewer->getViewportRegion().getViewportAspectRatio();
            SbViewVolume vvol = pCamera->getViewVolume(aspect);

            if (aspect < 1)
            {
                vvol.scale(1 / aspect);
            }

            SbPlane ground(SbVec3f(0, 0, 1), SbVec3f(0, 0, 0));
            SbVec2s clickPoint((short)event->pos().x(), (short)(this->height() - event->pos().y()));
            SbVec2f clickPointNormalized(1.0f * event->pos().x() / this->width(), 1.0f - 1.0f * event->pos().y() / this->height());

            // calculate position to insert the object
            SbVec3f insertPosition;
            SbLine line;
            vvol.projectPointToLine(clickPointNormalized, line);
            SbVec3f pointOnCameraPlane;
            vvol.getPlane(0.0).intersect(line, pointOnCameraPlane);
            ground.intersect(line, insertPosition);

            if (pointOnCameraPlane.getValue()[2] > 0)
            {
                SoRayPickAction rp(viewer->getViewportRegion());
                rp.setPickAll(false);
                rp.setPoint(clickPoint);
                rp.apply(viewer->getSceneGraph());
                SoPickedPoint* picked = rp.getPickedPoint();

                if (picked != NULL)
                {
                    insertPosition = picked->getPoint();
                }
            }

            // set the standard rotation
            SbRotation insertRotation(SbVec3f(1, 0, 0), 0);

            // create operations
            controller::OperationPtr add(new controller::AddOperation(controller->getMemoryXController(), controller->getScene(), objectType.toStdString(), collection.toStdString(), insertPosition, insertRotation));
            operations->push_back(add);
            controller::OperationPtr select(new controller::SelectOperation(controller->getMemoryXController(), controller->getScene(), add->getObjectId()));
            operations->push_back(select);
        }

        controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
    }
}
