/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ShortcutTableModel.h"

gui::ShortcutTableModel::ShortcutTableModel(QObject* parent) :
    QAbstractTableModel(parent)
{
}

gui::ShortcutTableModel::ShortcutTableModel(QHash<QString, QKeySequence> Shortcuts, QObject* parent) :
    QAbstractTableModel(parent)
{
    registeredShortcuts = Shortcuts;
}

void gui::ShortcutTableModel::setShortcutHashTable(QHash<QString, QKeySequence> Shortcuts)
{
    registeredShortcuts = Shortcuts;
    emit(layoutChanged());
}

int gui::ShortcutTableModel::rowCount(const QModelIndex& parent) const
{
    return registeredShortcuts.size();
}

int gui::ShortcutTableModel::columnCount(const QModelIndex& parent) const
{
    return 2;
}

QVariant gui::ShortcutTableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (index.row() >= registeredShortcuts.size() || index.row() < 0)
    {
        return QVariant();
    }

    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
        {
            return tr(registeredShortcuts.keys().at(index.row()).toStdString().c_str());
        }
        else if (index.column() == 1)
        {
            return tr(registeredShortcuts.values().at(index.row()).toString().toStdString().c_str());
        }
    }

    return QVariant();
}

QVariant gui::ShortcutTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
            case 0:
                return tr("Name");

            case 1:
                return tr("Key-Sequence");

            default:
                return QVariant();
        }
    }

    return QVariant();
}

QVariant gui::ShortcutTableModel::actionName(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (index.row() >= registeredShortcuts.size() || index.row() < 0)
    {
        return QVariant();
    }

    if (role == Qt::DisplayRole)
    {
        return registeredShortcuts.keys().at(index.row()).toStdString().c_str();
    }

    return QVariant();
}
