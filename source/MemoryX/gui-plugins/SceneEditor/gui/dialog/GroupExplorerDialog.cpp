/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GroupExplorerDialog.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_GroupExplorerDialog.h>
#include "../../scene3D/SceneGroupManager.h"
#include <QStandardItem>
#include <QList>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/SbRotation.h>
#include <QInputDialog>

#include "../../controller/CreateGroupOperation.h"
#include "../../controller/DeleteGroupOperation.h"
#include "../../controller/RenameGroupOperation.h"
#include "../../controller/RemoveFromGroupOperation.h"

gui::dialog::GroupExplorerDialog::GroupExplorerDialog(controller::ControllerPtr control, QWidget* parent) :
    QDialog(parent),
    control(control),
    ui(new Ui::GroupExplorerDialog)
{
    ui->setupUi(this);

    viewer.reset(new SoQtExaminerViewer(ui->viewInSceneWidget));
    viewer->setDecoration(false);
    viewer->setHeadlight(true);
    viewer->setBackgroundColor(SbColor(100 / 255.0f, 100 / 255.0f, 100 / 255.0f));
    viewer->setSceneGraph(new SoSeparator());

    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(close()));
    connect(ui->groupListWidget, SIGNAL(clicked(QModelIndex)), this, SLOT(groupClicked(QModelIndex)));
    connect(ui->objectListWidget, SIGNAL(clicked(QModelIndex)), this, SLOT(objectClicked(QModelIndex)));
    connect(ui->objectListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(checkButtons()));
    connect(ui->addGroupButton, SIGNAL(clicked()), this, SLOT(groupAddClicked()));
    connect(ui->deleteGroupButton, SIGNAL(clicked()), this, SLOT(groupRemoveClicked()));
    connect(ui->changeNameButton, SIGNAL(clicked()), this, SLOT(groupRenameClicked()));
    connect(ui->deleteObjectButton, SIGNAL(clicked()), this, SLOT(objectRemoveClicked()));
    connect(ui->groupNameLineEdit, SIGNAL(textChanged(QString)), this, SLOT(checkButtons()));
}

gui::dialog::GroupExplorerDialog::~GroupExplorerDialog()
{
    delete ui;
}

// Triggered when opened
void gui::dialog::GroupExplorerDialog::showEvent(QShowEvent* event)
{
    reloadGroups();
    reloadObjects();
    reloadProperties();
    checkButtons();
}

void gui::dialog::GroupExplorerDialog::groupClicked(QModelIndex index)
{
    if (ui->groupListWidget->currentItem()->text() != activeGroup)
    {
        activeObject = "";
    }

    activeGroup = ui->groupListWidget->currentItem()->text();

    reloadObjects();
    reloadProperties();
    checkButtons();
}

void gui::dialog::GroupExplorerDialog::objectClicked(QModelIndex index)
{
    activeObject = ui->objectListWidget->currentItem()->toolTip();

    reloadProperties();
    checkButtons();
}

void gui::dialog::GroupExplorerDialog::groupAddClicked()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        int nameIndex = 0;

        do
        {
            nameIndex++;
        }
        while (controller->getScene()->getGroupManager()->getGroupById(tr("New Group ").toStdString() + std::to_string(nameIndex)) != NULL);

        std::string groupName = tr("New Group ").toStdString() + std::to_string(nameIndex);

        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        controller::OperationPtr operation(new controller::CreateGroupOperation(controller->getMemoryXController(),
                                           controller->getScene(),
                                           groupName));
        operations->push_back(operation);
        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);

        activeGroup = QString::fromStdString(groupName);
        activeObject = "";

        reloadGroups();
        reloadObjects();
        reloadProperties();
        checkButtons();

        ui->groupNameLineEdit->setFocus();
        ui->groupNameLineEdit->selectAll();
        ui->changeNameButton->setDefault(true);
    }
}

void gui::dialog::GroupExplorerDialog::groupRemoveClicked()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        if (!controller->getScene()->getGroupManager()->getGroupById(activeGroup.toStdString()))
        {
            return;
        }

        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        for (scene3D::SceneObjectPtr object : controller->getScene()->getGroupManager()->getGroupById(activeGroup.toStdString())->getAllObjects())
        {
            controller::OperationPtr operation(new controller::RemoveFromGroupOperation(controller->getMemoryXController(),
                                               controller->getScene(),
                                               activeGroup.toStdString(),
                                               object->getObjectId()));
            operations->push_back(operation);
        }

        controller::OperationPtr operation(new controller::DeleteGroupOperation(controller->getMemoryXController(),
                                           controller->getScene(),
                                           activeGroup.toStdString()));
        operations->push_back(operation);
        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);

        activeGroup = "";
        activeObject = "";

        reloadGroups();
        reloadObjects();
        reloadProperties();
        checkButtons();
    }
}

void gui::dialog::GroupExplorerDialog::objectRemoveClicked()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        QList<QListWidgetItem*> allItemsToDelete = ui->objectListWidget->selectedItems();
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        for (QList<QListWidgetItem*>::iterator it = allItemsToDelete.begin(); it != allItemsToDelete.end(); ++it)
        {
            controller::OperationPtr operation(new controller::RemoveFromGroupOperation(controller->getMemoryXController(),
                                               controller->getScene(),
                                               activeGroup.toStdString(),
                                               (*it)->toolTip().toStdString()));
            operations->push_back(operation);
        }

        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);

        activeObject = "";

        reloadObjects();
        reloadProperties();
        checkButtons();
    }
}

void gui::dialog::GroupExplorerDialog::groupRenameClicked()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        controller::OperationPtr operation(new controller::RenameGroupOperation(controller->getMemoryXController(),
                                           controller->getScene(),
                                           activeGroup.toStdString(),
                                           ui->groupNameLineEdit->text().toStdString()));
        operations->push_back(operation);
        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);

        activeGroup = ui->groupNameLineEdit->text();

        reloadGroups();
        checkButtons();
    }
}

void gui::dialog::GroupExplorerDialog::setCurrentGroup(QString groupId)
{
    if (controller::ControllerPtr controller = control.lock())
    {
        if (controller->getScene()->getGroupManager()->getGroupById(groupId.toStdString()))
        {
            activeGroup = groupId;
        }

        reloadGroups();
        reloadObjects();
        reloadProperties();
        checkButtons();

        // To force active highlightening in group list
        ui->groupListWidget->setFocus();
    }
}

void gui::dialog::GroupExplorerDialog::reloadGroups()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        // Reload Groups
        ui->groupListWidget->clear();
        allGroups = controller->getScene()->getGroupManager()->getAllGroups();

        for (std::vector<scene3D::SceneGroupPtr>::iterator it = allGroups.begin(); it != allGroups.end(); ++it)
        {
            ui->groupListWidget->addItem(QString::fromStdString((*it)->getGroupId()));
        }

        if (!activeGroup.isNull() && !activeGroup.isEmpty())
        {
            // Set current group
            ui->groupListWidget->setCurrentItem(ui->groupListWidget->findItems(activeGroup, Qt::MatchFixedString).first());
        }
    }
}

void gui::dialog::GroupExplorerDialog::reloadObjects()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        // Reload Objects
        ui->objectListWidget->clear();
        scene3D::SceneGroupPtr clickedGroup = controller->getScene()->getGroupManager()->getGroupById(activeGroup.toStdString());

        if (clickedGroup != NULL)
        {
            std::vector<scene3D::SceneObjectPtr> allObjects = clickedGroup->getAllObjects();

            for (std::vector<scene3D::SceneObjectPtr>::iterator it = allObjects.begin(); it != allObjects.end(); ++it)
            {
                std::string objectId = (*it)->getObjectId();
                std::string classId = (*it)->getClassId();
                QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(classId));
                item->setToolTip(QString::fromStdString(objectId));
                ui->objectListWidget->insertItem(ui->objectListWidget->count(), item);
            }
        }

        ui->groupNameLineEdit->setText(activeGroup);
    }
}

void gui::dialog::GroupExplorerDialog::reloadProperties()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        // Reload Object Properties
        ui->propertiesTableWidget->clear();
        ui->propertiesTableWidget->reset();
        viewer->setSceneGraph(new SoSeparator());
        scene3D::SceneObjectPtr sceneObject = controller->getScene()->getObjectManager()->getObjectById(activeObject.toStdString());

        if (sceneObject)
        {
            std::map<std::string, std::string> attributes = sceneObject->getAllAttributes();
            // Basic Properties
            ui->propertiesTableWidget->setRowCount(3);
            ui->propertiesTableWidget->setItem(0, 0, new QTableWidgetItem(tr("Object ID")));
            ui->propertiesTableWidget->setItem(0, 1, new QTableWidgetItem(QString::fromStdString(sceneObject->getObjectId())));
            ui->propertiesTableWidget->setItem(1, 0, new QTableWidgetItem(tr("Class ID")));
            ui->propertiesTableWidget->setItem(1, 1, new QTableWidgetItem(QString::fromStdString(sceneObject->getClassId())));
            ui->propertiesTableWidget->setItem(2, 0, new QTableWidgetItem(tr("Collection ID")));
            ui->propertiesTableWidget->setItem(2, 1, new QTableWidgetItem(QString::fromStdString(sceneObject->getCollection())));

            // All other generic properties
            int row;

            for (std::map<std::string, std::string>::iterator it = attributes.begin(); it != attributes.end(); ++it)
            {
                row = ui->propertiesTableWidget->rowCount();
                QTableWidgetItem* key = new QTableWidgetItem(QString::fromStdString(it->first));
                QTableWidgetItem* value = new QTableWidgetItem(QString::fromStdString(it->second));
                ui->propertiesTableWidget->insertRow(row);
                ui->propertiesTableWidget->setItem(row, 0, key);
                ui->propertiesTableWidget->setItem(row, 1, value);
                ui->propertiesTableWidget->resizeRowToContents(row);
            }

            ui->propertiesTableWidget->resizeColumnToContents(1);

            showPreviewImage(controller->getMemoryXController()->getPriorKnowlegdeController()->getObjectClassPtr(sceneObject->getClassId(), sceneObject->getCollection()));
        }
    }
}

void gui::dialog::GroupExplorerDialog::checkButtons()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        ui->changeNameButton->setEnabled(!(
                                             ui->groupNameLineEdit->text().isEmpty() ||
                                             ui->groupNameLineEdit->text() == activeGroup ||
                                             controller->getScene()->getGroupManager()->getGroupById(ui->groupNameLineEdit->text().toStdString()) != NULL));
        ui->deleteGroupButton->setEnabled(activeGroup != "");
        ui->deleteObjectButton->setEnabled(activeObject != "");
        ui->groupNameLineEdit->setEnabled(activeGroup != "");
    }
}

void gui::dialog::GroupExplorerDialog::showPreviewImage(const memoryx::ObjectClassPtr& objectClass)
{
    if (controller::ControllerPtr controller = control.lock())
    {
        //Get geometry for object
        SoNode* visualisation = controller->getMemoryXController()->getPriorKnowlegdeController()->getCoinVisualisation(objectClass, false);

        //Create basic scene with rotation around Y
        SoSeparator* root = controller->getScene()->getPreviewGenerator()->createAnimatedPreview(visualisation);

        viewer->setSceneGraph(root);
        viewer->getCamera()->viewAll(root, viewer->getViewportRegion());
    }
}

void gui::dialog::GroupExplorerDialog::retranslate()
{
    this->ui->retranslateUi(this);
}
