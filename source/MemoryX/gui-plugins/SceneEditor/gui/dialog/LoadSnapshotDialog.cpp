/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LoadSnapshotDialog.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_LoadSnapshotDialog.h>

#include "../../controller/CreateGroupOperation.h"
#include "../../controller/AddToGroupOperation.h"

gui::dialog::LoadSnapshotDialog::LoadSnapshotDialog(controller::ControllerPtr control, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::LoadSnapshotDialog),
    control(control)
{
    ui->setupUi(this);
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
}

gui::dialog::LoadSnapshotDialog::~LoadSnapshotDialog()
{
    delete ui;
}

void gui::dialog::LoadSnapshotDialog::accepted()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        QString snapshotName = ui->comboBoxSnapshots->currentText();

        if (snapshotName != "")
        {
            std::string groupName = snapshotName.toStdString();
            int suffix = 1;
            std::vector<std::string> allIds = controller->getMemoryXController()->loadSnapshot(snapshotName.toStdString());

            while (controller->getScene()->getGroupManager()->getGroupById(groupName + std::to_string(suffix)) != NULL)
            {
                ++suffix;
            }

            groupName = groupName + std::to_string(suffix);
            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());
            controller::OperationPtr operation(new controller::CreateGroupOperation(controller->getMemoryXController(),
                                               controller->getScene(),
                                               groupName));
            operations->push_back(operation);

            for (std::vector<std::string>::iterator it = allIds.begin(); it != allIds.end(); ++it)
            {
                std::string objectId = *it;
                controller::OperationPtr operation(new controller::AddToGroupOperation(controller->getMemoryXController(),
                                                   controller->getScene(),
                                                   groupName,
                                                   objectId));
                operations->push_back(operation);
            }

            controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
        }
    }
}

void gui::dialog::LoadSnapshotDialog::showEvent(QShowEvent*)
{
    if (controller::ControllerPtr controller = control.lock())
    {
        ui->comboBoxSnapshots->clear();
        std::vector<std::string> allSnapshotsVector = controller->getMemoryXController()->getAllSnapshots();

        for (std::vector<std::string>::iterator it = allSnapshotsVector.begin(); it != allSnapshotsVector.end(); ++it)
        {
            ui->comboBoxSnapshots->insertItem(ui->comboBoxSnapshots->count(), QString::fromStdString(*it));
        }
    }
}

void gui::dialog::LoadSnapshotDialog::retranslate()
{
    this->ui->retranslateUi(this);
}
