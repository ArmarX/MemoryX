/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LiveShortcutLineEdit.h"

#include <QKeyEvent>

gui::dialog::LiveShortcutLineEdit::LiveShortcutLineEdit(QWidget* parent) :
    ClearableLineEdit(parent)
{
}

void gui::dialog::LiveShortcutLineEdit::keyPressEvent(QKeyEvent* event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);

        int keyInt = keyEvent->key();
        Qt::Key key = static_cast<Qt::Key>(keyInt);

        if (key == Qt::Key_unknown)
        {
            return;
        }

        // the user have clicked just and only the special keys Ctrl, Shift, Alt, Meta.
        if (key == Qt::Key_Control ||
            key == Qt::Key_Shift ||
            key == Qt::Key_Alt ||
            key == Qt::Key_Meta)
        {
            return;
        }

        // check for a combination of user clicks
        Qt::KeyboardModifiers modifiers = keyEvent->modifiers();
        QString keyText = keyEvent->text();
        // if the keyText is empty than it's a special key like F1, F5, ...

        QList<Qt::Key> modifiersList;

        if (modifiers & Qt::ShiftModifier)
        {
            keyInt += Qt::SHIFT;
        }

        if (modifiers & Qt::ControlModifier)
        {
            keyInt += Qt::CTRL;
        }

        if (modifiers & Qt::AltModifier)
        {
            keyInt += Qt::ALT;
        }

        if (modifiers & Qt::MetaModifier)
        {
            keyInt += Qt::META;
        }

        this->setText(QKeySequence(keyInt).toString(QKeySequence::NativeText));
    }
}

void gui::dialog::LiveShortcutLineEdit::publicKeyPressEvent(QKeyEvent* event)
{
    keyPressEvent(event);
}
