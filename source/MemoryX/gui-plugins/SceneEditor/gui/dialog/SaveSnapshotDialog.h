/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QDialog>

#include "../../controller/Controller.h"

namespace Ui
{
    class SaveSnapshotDialog;
}

namespace gui::dialog
{
    class SaveSnapshotDialog : public QDialog
    {
        Q_OBJECT

    public:
        /**
        * Constructor
        *
        * Creates a new Instance of this class.
        *
        * @param control Pointer to Main Controller
        * @param parent Pointer to parent Widget
        */
        explicit SaveSnapshotDialog(controller::ControllerPtr control, QWidget* parent = 0);

        /**
         * Destructor.
         *
         */
        ~SaveSnapshotDialog() override;

        /**
          * Translates all translatable strings in this dialog.
          */
        void retranslate();

    private:
        Ui::SaveSnapshotDialog* ui;
        void connectSlots();
        void showEvent(QShowEvent*) override;
        controller::ControllerWeakPtr control;
        bool newSnapshot;
        bool allObjects;

    private Q_SLOTS:
        void enableSaveNewSnapshot(bool enabled);
        void enableReplaceSnapshot(bool enabled);
        void enableSaveAllObjects(bool enabled);
        void enableSaveGroups(bool enabled);
        void accept() override;
        void checkOkButtonStatus();
    };
}
