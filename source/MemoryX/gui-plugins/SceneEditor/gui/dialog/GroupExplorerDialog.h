/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QDialog>
#include <QStandardItemModel>
#include <QHash>

#include "../../controller/Controller.h"
#include "../../scene3D/SceneGroup.h"
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

namespace Ui
{
    class GroupExplorerDialog;
}

using SoQtExaminerPtr = std::shared_ptr<SoQtExaminerViewer>;

namespace gui::dialog
{
    class GroupExplorerDialog : public QDialog
    {
        Q_OBJECT

    public:
        /**
         * Constructor.
         * Basic constructor for GroupExplorerDialog.
         *
         * @param control The controller containing all managers needed in this Dialog
         * @param parent Parent widget
         */
        explicit GroupExplorerDialog(controller::ControllerPtr control, QWidget* parent = 0);

        /**
         * Destructor.
         *
         */
        ~GroupExplorerDialog() override;

        /**
         * Sets the currently selected Group.
         *
         * @param groupId The Id of the group to be selected
         */
        void setCurrentGroup(QString groupId);

        /**
          * Translates all translatable strings in this dialog.
          */
        void retranslate();

    private Q_SLOTS:
        void groupClicked(QModelIndex index);
        void objectClicked(QModelIndex index);
        void groupAddClicked();
        void groupRemoveClicked();
        void groupRenameClicked();
        void objectRemoveClicked();

        void reloadGroups();
        void reloadObjects();
        void reloadProperties();
        void checkButtons();

    private:
        void showEvent(QShowEvent* event) override;
        void refreshGroups();
        void loadGroup(QString groupId);
        void showPreviewImage(const memoryx::ObjectClassPtr& objectClass);

        SoQtExaminerPtr viewer;
        controller::ControllerWeakPtr control;
        Ui::GroupExplorerDialog* ui;
        std::vector<scene3D::SceneGroupPtr> allGroups;
        QString activeGroup;
        QString activeObject;
    };
}
