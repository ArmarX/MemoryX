/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QWidget>
#include <QPointer>
#include <QTableWidgetItem>
#include "dialog/GroupExplorerDialog.h"

#include "../controller/Controller.h"

namespace Ui
{
    class GroupExplorerWidget;
}

namespace gui
{
    /**
    * This class provides a QWidget which displays all existing groups in the local scene.
    * It supports simple functions to edit groups.
    *
    */
    class GroupExplorerWidget : public QWidget
    {
        Q_OBJECT

    public:

        /**
        * Constructor.
        * Constructs an instance of the GroupExplorerWidget.
        * It expects a constant reference to controller::ControllerPtr to get information about the scene, a <i>groupEditorDialog</i> and a <i>parent</i> widget.
        *
        * @param    control             shared pointer to @link{controller::ControllerPtr}
        * @param    groupEditorDialog   QPointer to the group editor dialog
        * @param    parent              parent widget
        *
        */
        explicit GroupExplorerWidget(const controller::ControllerPtr& control, QPointer<dialog::GroupExplorerDialog> groupEditorDialog, QWidget* parent = 0);
        ~GroupExplorerWidget() override;

        /**
        * Translates all translatable strings in this dialog.
        */
        void retranslate();

    Q_SIGNALS:
        /**
        * Signal emitted to add selected group to current selection in the scene.
        *
        */
        void addToSelectionClicked();

        /**
        * Signal emitted to add current selection in scene to selected group.
        *
        */
        void addToGroupClicked();

        /**
        * Signal emitted when click event on group name triggered.
        *
        */
        void groupNameClicked();

    private Q_SLOTS:
        void customContextMenuRequested(const QPoint& position);
        void openGroupEditorDialog();
        void listAllGroups();
        void addSelectionToNewGroup();
        void addSelectionToGroup();
        void addGroupToSelection();
        void groupRemoveClicked();
        void triggerOperation(QTableWidgetItem* item);
        void triggerOpenGroupEditor(QModelIndex index);

    private:
        Ui::GroupExplorerWidget* ui;
        controller::ControllerWeakPtr control;
        QPointer<dialog::GroupExplorerDialog> groupEditorDialog;
        QTableWidgetItem* currentItem;

        void createAddToGroupOperations(std::string groupName);

    };
}

