/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QAction>
#include <QEvent>

namespace gui
{
    class OverrideAction : public QAction
    {
        Q_OBJECT
    public:
        /**
         * Constructor.
         *
         * @param icon Icon
         * @param text Text
         * @param parent Parent
         * @see UI::QWidget
         */
        OverrideAction(const QIcon& icon, const QString& text, QObject* parent);
        /**
         * Constructor.
         *
         * @param text Text
         * @param parent Parent
         * @see UI::QWidget
         */
        OverrideAction(const QString& text, QObject* parent);
        /**
         *
         * Passes the Event to protected method event.
         *
         * @param event Event to be passed
         */
        bool publicEvent(QEvent* e);

    protected:
        /**
         * Event triggered.
         *
         * @param e QEvent that has been triggered
         * @return Status of event
         */
        bool event(QEvent* e) override;

    private:

    };
}

