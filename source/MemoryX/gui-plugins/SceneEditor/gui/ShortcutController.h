/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <exception>

// QT
#include <QString>
#include <QPointer>
#include <QHash>
#include <QKeySequence>
#include <QAction>

#ifndef __Gui__ShortcutController_h__
#define __Gui__ShortcutController_h__

namespace gui
{
    class ShortcutController;
}

namespace gui
{
    /**
    * The class ShortcutController manages all shortcuts that can be set in the widget.
    * Each QAction registers its shortcut (if desired) over the ShortcutController.
    *
    * QApplication must be initialized for this class to work!
    *
    */
    class ShortcutController // QApplication muss initialisiert sein -> KOMMENTAR
    {
    public:
        /**
        * A constructor.
        * Creates a new Instance of this class.
        *
        */
        ShortcutController();

        /**
        * A constructor.
        * Creates a new Instance of this class with already defined initial Shortcuts.
        *
        * @param initialShortcuts List of already defined Shortcuts
        */
        ShortcutController(const QHash<QString, QKeySequence>& initialShortcuts);

        /**
        * A destructor.
        *
        */
        ~ShortcutController();

        /**
         * Registers a QAction, so this action can be given a shortcut.
         *
         * @param name Name of the registered QAction
         * @param action QAction to be registered
         */
        void registerAction(const QString& name, const QPointer<QAction>& action);
        /**
         * Sets the given QKeySequence as Shortcut for the QAction specified by name.
         *
         * @param name Specifies the QAction edited
         * @param keysequence The QKeySequence that is added to the QAction
         */
        void updateShortcut(const QString& name, const QKeySequence& keysequence);
        /**
         * Returns a Hashtable with all existing Shortcuts.
         *
         * @return QHash with all Shortcuts
         */
        QHash<QString, QKeySequence> getAllShortcuts();
        /**
         * Returns a Hashtable with all registered Shortcuts.
         *
         * @return QHash with all registered Shortcuts
         */
        QHash<QString, QKeySequence> getAllRegisteredShortcuts();
        /**
         * Returns a Hashtable with all Actions saved.
         *
         * @return QHash with all Actions
         */
        QHash<QString, QPointer<QAction> > getAllActions();

    private:
        QHash<QString, QPointer<QAction> > registeredActions;
        QHash<QString, QKeySequence> shortcuts;

    };
}

#endif
