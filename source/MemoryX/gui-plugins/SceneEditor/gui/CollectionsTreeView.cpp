/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <QDrag>
#include <QPixmap>
#include <QPainter>
#include <iostream>
#include <qfiledialog.h>
#include <qclipboard.h>

#include "CollectionsTreeView.h"
#include "ObjectExplorerModel.h"

gui::CollectionsTreeView::CollectionsTreeView(QWidget* parent) :
    QTreeView(parent)
{
}

void gui::CollectionsTreeView::startDrag(Qt::DropActions supportedActions)
{
    QModelIndexList indexes = selectedIndexes();
    gui::ObjectExplorerModel* objectExplorerModel = dynamic_cast<gui::ObjectExplorerModel*>(model());

    if (indexes.count() == 1 && objectExplorerModel)
    {
        // we only support single selection
        QPixmap pixmap = objectExplorerModel->getItemPixmap(indexes.at(0));

        // create drag object
        QDrag* drag = new QDrag(this);
        drag->setPixmap(pixmap);
        drag->setMimeData(objectExplorerModel->mimeData(indexes));
        drag->setHotSpot(QPoint(pixmap.size().width() / 2, pixmap.size().height() / 2));

        if (drag->start(supportedActions) == Qt::MoveAction)
        {
            std::cerr << "Qt::MoveAction not supported" << std::endl;
        }
    }
    // create the qdrag object like in stock view
    else
    {
        QTreeView::startDrag(supportedActions);
    }
}
