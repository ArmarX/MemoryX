/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QMainWindow>
#include <QPointer>
#include <QResizeEvent>

#include "../controller/Controller.h"
#include "ObjectInspectorWidget.h"
#include "ObjectExplorerWidget.h"
#include "GroupExplorerWidget.h"
#include "MinimapWidget.h"
#include "SceneEditorWidget.h"
#include "ScenegraphWidget.h"
#include "dialog/GroupExplorerDialog.h"
#include "CollapsibleDockWidget.h"

namespace Ui
{
    class SceneEditorMainWindow;
}

namespace gui
{
    /**
    * This class provides the main window of the plugin.
    * It initializes the central widget and all dock widgets.
    *
    */
    class SceneEditorMainWindow : public QMainWindow
    {
        Q_OBJECT

    public:

        /**
        * Constructor.
        * Creates main window for SceneEditor and initialisizes all child widgets.
        * Expects controller:ControllerPtr and a group editor dialog.
        *
        * @param    control             shared pointer to controller::Controller
        * @param    groupEditorDialog   Qt::QPointer to group editor dialog
        * @param    parent              parent widget
        *
        */
        explicit SceneEditorMainWindow(const controller::ControllerPtr& control, QPointer<dialog::GroupExplorerDialog> groupEditorDialog, QWidget* parent = 0);

        /**
        * Destructor.
        * Destroys the SceneEditor main window.
        *
        */
        ~SceneEditorMainWindow() override;

        void postDocking();

        /**
        * Enables main window and updates its widgets.
        *
        */
        void onConnect();

        /**
        * Disables main window.
        *
        */
        void onDisconnect();

    signals:
        /**
        * Signal emitted when user changed current language.
        *
        */
        void languageChangedByUser();

    protected:
        /**
        * Reimplements Qt::QWidget:changeEvent().
        *
        * The state being changed in this event can be retrieved through the <i>event</i> supplied.
        *
        * @param    event   Qt::Event
        * @see      Qt::QEvent
        * @see      Qt::QWidget:changeEvent()
        */
        void changeEvent(QEvent* event) override;

    private:
        QPointer<ObjectInspectorWidget> objectInspector;
        QPointer<ObjectExplorerWidget> objectExplorer;
        QPointer<GroupExplorerWidget> groupExplorer;
        QPointer<MinimapWidget> minimap;
        QPointer<SceneEditorWidget> scene;
        QPointer<ScenegraphWidget> sceneGraph;
        QPointer<dialog::GroupExplorerDialog> groupEditorDialog;
        QPointer<gui::CollapsibleDockWidget> objectExplorerDock;
        QPointer<gui::CollapsibleDockWidget> objectInspectorDock;
        QPointer<gui::CollapsibleDockWidget> groupExplorerDock;
        QPointer<gui::CollapsibleDockWidget> minimapDock;
        controller::ControllerPtr mainControl;
    };
}

