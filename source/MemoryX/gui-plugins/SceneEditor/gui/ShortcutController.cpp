/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <stdexcept>

// QT
#include <QString>
#include <QHash>
#include <QKeySequence>
#include "OverrideAction.h"

#include "ShortcutController.h"

gui::ShortcutController::ShortcutController() :
    registeredActions(),
    shortcuts()
{
}

gui::ShortcutController::ShortcutController(const QHash<QString, QKeySequence>& initialShortcuts) :
    registeredActions(),
    shortcuts(initialShortcuts)
{
}

gui::ShortcutController::~ShortcutController()
{
}

void gui::ShortcutController::registerAction(const QString& name, const QPointer<QAction>& action)
{
    if (registeredActions.contains(name))
    {
        throw std::invalid_argument("Action already registered");
    }

    if (!shortcuts.contains(name))
    {
        QKeySequence keySequence("");
        shortcuts.insert(name, keySequence);
    }

    action->setShortcut(shortcuts.value(name));
    registeredActions.insert(name, action);
}

void gui::ShortcutController::updateShortcut(const QString& name, const QKeySequence& keysequence)
{
    if (keysequence.toString() != "" && shortcuts.key(keysequence) != NULL)
    {
        gui::ShortcutController::updateShortcut(shortcuts.key(keysequence), QKeySequence());
    }

    if (shortcuts.contains(name))
    {
        shortcuts[name] = keysequence;

        if (registeredActions.contains(name))
        {
            registeredActions.value(name)->setShortcut(keysequence);
        }
    }
    else
    {
        shortcuts.insert(name, keysequence);
    }

    if (registeredActions.contains(name))
    {
        registeredActions.value(name)->setShortcut(keysequence);
    }
}

QHash<QString, QKeySequence> gui::ShortcutController::getAllShortcuts()
{
    return shortcuts;
}

QHash<QString, QKeySequence> gui::ShortcutController::getAllRegisteredShortcuts()
{
    QHash<QString, QKeySequence> tempShortcuts;
    QHash<QString, QKeySequence>::iterator i;

    for (i = shortcuts.begin(); i != shortcuts.end(); ++i)
    {
        if (registeredActions.contains(i.key()))
        {
            tempShortcuts.insert(i.key(), i.value());
        }
    }

    return tempShortcuts;
}

QHash<QString, QPointer<QAction> > gui::ShortcutController::getAllActions()
{
    return registeredActions;
}
