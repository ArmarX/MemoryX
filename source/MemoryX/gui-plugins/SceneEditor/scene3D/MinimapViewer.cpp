/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MinimapViewer.h"
#include <Inventor/events/SoMouseButtonEvent.h>

scene3D::MinimapViewer::MinimapViewer(const controller::ControllerPtr& control, QWidget* widget) : SoQtExaminerViewer(widget), control(control)
{
    this->setBackgroundColor(SbColor(100 / 255.0f, 100 / 255.0f, 100 / 255.0f));
    this->setAccumulationBuffer(false);
    this->setHeadlight(true);
    this->setViewing(false);
    this->setDecoration(false);
#ifdef WIN32
#ifndef _DEBUG
    this->setAntialiasing(true, 4);
#endif
#endif
    this->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    this->setFeedbackVisibility(true);
}

//Override the default navigation behaviour of the SoQtExaminerViewer
SbBool scene3D::MinimapViewer::processSoEvent(const SoEvent* const event)
{
    const SoType type(event->getTypeId());

    //Just capture left mouse click and make scene zoom to clicked position
    //Ignore all other events
    if (type.isDerivedFrom(SoMouseButtonEvent::getClassTypeId()))
    {
        SoMouseButtonEvent* const ev = (SoMouseButtonEvent*) event;
        const int button = ev->getButton();

        //LEFT MOUSE BUTTON
        if (button == SoMouseButtonEvent::BUTTON1)
        {
            //Trigger minimap clicked in conrtoller and let him handle this event

            if (controller::ControllerPtr controller = control.lock())
            {
                controller->triggerMinimapClicked();
            }

            return TRUE;
        }
    }

    //YOU SHALL NOT PASS!
    return TRUE;
}
