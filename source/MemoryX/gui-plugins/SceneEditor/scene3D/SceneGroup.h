/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include "SceneGroupManager.h"
#include "SceneObject.h"

namespace scene3D
{
    class SceneGroup
    {
        friend class SceneGroupManager;
    public:
        /**
         * Constructor
         * Creates an instance of the class.
         *
         * @param groupID ID of the SceneGroup
         */
        SceneGroup(const std::string& groupID);

        /**
         * Returns the ID of the SceneGroup.
         *
         * @return string ID of SceneGroup
         */
        std::string getGroupId();

        /**
         * Returns all Objects in SceneGroup.
         *
         * @return vector<scene3D::SceneObjectPtr > All Objects in SceneGroup
         */
        std::vector<scene3D::SceneObjectPtr> getAllObjects();

        /**
         * Checks, if SceneGroup contains Object.
         *
         * @param object Object to check Existence
         * @return bool SceneGroup contains Object
         */
        bool contains(scene3D::SceneObjectPtr object);

        /**
         * Adds an Object to the SceneGroup.
         *
         * @param object Object to be added
         */
        void addObject(scene3D::SceneObjectPtr object);

        /**
         * Removes an Object from the SceneGroup.
         *
         * @param object Object to be removed
         */
        void removeObject(scene3D::SceneObjectPtr object);

        /**
         * Removes all Objects from the SceneGroup.
         *
         */
        void clearGroup();

    private:
        std::string groupID;

        void setGroupId(std::string groupId);
        std::vector<scene3D::SceneObjectPtr> objects;
    };
}


