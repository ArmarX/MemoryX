/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include "Scene.h"


scene3D::ScenePtr scene3D::Scene::create(controller::ControllerPtr controller)
{
    scene3D::ScenePtr scene(new Scene(controller));

    scene->sceneObjectManager.reset(new SceneObjectManager(scene));
    scene->sceneGroupManager.reset(new SceneGroupManager);
    scene->sceneSelectionManager.reset(new SceneSelectionManager(scene));
    scene->sceneManipulatorManager.reset(new SceneManipulatorManager(scene));
    scene->scenePreviewGenerator.reset(new PreviewGenerator);

    return scene;
}

scene3D::Scene::Scene(controller::ControllerPtr controller) : viewerMode(false), controller(controller)
{
    selectionRootNode = new SoSelection;
    selectionRootNode->setName("selectionRootNode");
    selectionRootNode->ref();

    //Add gridfloor root and make it unpickable
    SoPickStyle* unpickable = new SoPickStyle();
    unpickable->style = SoPickStyle::UNPICKABLE;
    SoSeparator* gridFloorRoot = new SoSeparator;
    SoPickStyle* pickable = new SoPickStyle();
    pickable->style = SoPickStyle::SHAPE;

    selectionRootNode->addChild(unpickable);
    selectionRootNode->addChild(gridFloorRoot);
    selectionRootNode->addChild(pickable);

    /*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Here we draw our grid floor. To change the way the grid floor is drawn change the following constants.
    GRIDSIZE:       How many lines to display for each direction of each axis.
                    E. g.: 2 would lead to 2 lines in negative and positive direction respectively, plus indicator for axis.
                    5 lines in sum.
    GRIDUNIT:       Factor for coin units, e.g. 2 means between 2 lines there is space for 2 coin units.
    GRIDSUBSIZE:    How many subunits to display between the larger lines. So 5 would lead to 3 lines between 2 large lines for 5
                    subunits.
    GRIDPATTERN:    Pattern for sublines. Bitpattern, where 1 is line and 0 is no line.
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    const int GRIDSIZE = 5;
    const int GRIDUNIT = 5;
    const int GRIDSUBSIZE = 5;
    const unsigned short GRIDPATTERN = 0xCCCC;

    //Red material for X axis
    SoMaterial* red = new SoMaterial;
    red->diffuseColor = SbColor(1, 0, 0);
    //Green material for Y axis
    SoMaterial* green = new SoMaterial;
    green->diffuseColor = SbColor(0, 1, 0);
    //Blue material for Z axis
    SoMaterial* blue = new SoMaterial;
    blue->diffuseColor = SbColor(0, 0, 1);

    //Small lines
    SoDrawStyle* thinStyle = new SoDrawStyle;
    thinStyle->lineWidth = 1;
    thinStyle->linePattern = GRIDPATTERN;
    //Larger lines
    SoDrawStyle* largeStyle = new SoDrawStyle;
    largeStyle->lineWidth = 2;

    //Our set of vertices for the grid lines
    SoVertexProperty* vertices = new SoVertexProperty;
    SoVertexProperty* subVertices = new SoVertexProperty;
    SoVertexProperty* xVertices = new SoVertexProperty;
    SoVertexProperty* yVertices = new SoVertexProperty;
    SoVertexProperty* zVertices = new SoVertexProperty;

    //Definition of which vertex belongs to which line and should be connected
    SoLineSet* lines = new SoLineSet;
    SoLineSet* subLines = new SoLineSet;
    SoLineSet* xLines = new SoLineSet;
    SoLineSet* yLines = new SoLineSet;
    SoLineSet* zLines = new SoLineSet;

    //Add 2 vertices for X axis
    xVertices->vertex.set1Value(0, GRIDSIZE * GRIDUNIT, 0, 0);
    xVertices->vertex.set1Value(1, -(GRIDSIZE * GRIDUNIT), 0, 0);
    //Connect them to a line by adding '2' to numVertices at the correct position
    xLines->numVertices.set1Value(0, 2);

    //Y axis
    yVertices->vertex.set1Value(0, 0, GRIDSIZE * GRIDUNIT, 0);
    yVertices->vertex.set1Value(1, 0, -(GRIDSIZE * GRIDUNIT), 0);
    yLines->numVertices.set1Value(0, 2);

    //Z axis
    zVertices->vertex.set1Value(0, 0, 0, GRIDSIZE * GRIDUNIT);
    zVertices->vertex.set1Value(1, 0, 0, -(GRIDSIZE * GRIDUNIT));
    zLines->numVertices.set1Value(0, 2);

    //Counters to keep track of vertex index
    int verticescounter = 0;
    int subverticescounter = 0;

    //Draw all lines parallel to the X and Y axis and all sublines, excepted axis itself
    for (int i = -(GRIDSIZE * GRIDUNIT); i < GRIDUNIT * (GRIDSIZE + 1); i += GRIDUNIT)
    {
        if (i != 0)
        {
            vertices->vertex.set1Value(verticescounter++, GRIDSIZE * GRIDUNIT, i, 0);
            vertices->vertex.set1Value(verticescounter++, -(GRIDSIZE * GRIDUNIT), i, 0);
            lines->numVertices.set1Value((verticescounter - 1) / 2, 2);

            vertices->vertex.set1Value(verticescounter++, i, GRIDSIZE * GRIDUNIT, 0);
            vertices->vertex.set1Value(verticescounter++, i, -(GRIDSIZE * GRIDUNIT), 0);
            lines->numVertices.set1Value((verticescounter - 1) / 2, 2);
        }

        //If this is not the last line to draw, draw sublines
        if (i < GRIDUNIT * GRIDSIZE)
        {
            float delta = (float)GRIDUNIT / (float)GRIDSUBSIZE;

            for (int n = 1; n < GRIDSUBSIZE; n++)
            {
                subVertices->vertex.set1Value(subverticescounter++, GRIDSIZE * GRIDUNIT, (float)i + (float)n * delta, 0);
                subVertices->vertex.set1Value(subverticescounter++, -(GRIDSIZE * GRIDUNIT), (float)i + (float)n * delta, 0);
                subLines->numVertices.set1Value((subverticescounter - 1) / 2, 2);

                subVertices->vertex.set1Value(subverticescounter++, (float)i + (float)n * delta, GRIDSIZE * GRIDUNIT, 0);
                subVertices->vertex.set1Value(subverticescounter++, (float)i + (float)n * delta, -(GRIDSIZE * GRIDUNIT), 0);
                subLines->numVertices.set1Value((subverticescounter - 1) / 2, 2);
            }
        }
    }


    lines->vertexProperty.setValue(vertices);
    subLines->vertexProperty.setValue(subVertices);
    xLines->vertexProperty.setValue(xVertices);
    yLines->vertexProperty.setValue(yVertices);
    zLines->vertexProperty.setValue(zVertices);

    gridFloorRoot->addChild(thinStyle);
    gridFloorRoot->addChild(subLines);
    gridFloorRoot->addChild(largeStyle);
    gridFloorRoot->addChild(lines);
    gridFloorRoot->addChild(red);
    gridFloorRoot->addChild(xLines);
    gridFloorRoot->addChild(green);
    gridFloorRoot->addChild(yLines);
    gridFloorRoot->addChild(blue);
    gridFloorRoot->addChild(zLines);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Gridfloor done

    objectRootNode = new SoSeparator;
    objectRootNode->setName("objectRootNode");
    selectionRootNode->addChild(objectRootNode);
}

scene3D::Scene::~Scene()
{
    selectionRootNode->unref();
}


void scene3D::Scene::makeCameraViewAll(SoCamera* camera, const SbViewportRegion& region)
{
    camera->viewAll(this->objectRootNode, region);
}

SoSeparator* scene3D::Scene::registerCamera(SoCamera* camera)
{
    SoSeparator* cameraRoot = new SoSeparator;
    cameraRoot->addChild(camera);
    cameraRoot->addChild(selectionRootNode);

    return cameraRoot;
}

scene3D::SceneObjectManagerPtr scene3D::Scene::getObjectManager()
{
    return sceneObjectManager;
}

scene3D::SceneGroupManagerPtr scene3D::Scene::getGroupManager()
{
    return sceneGroupManager;
}

scene3D::SceneSelectionManagerPtr scene3D::Scene::getSelectionManager()
{
    return sceneSelectionManager;
}

scene3D::SceneManipulatorManagerPtr scene3D::Scene::getManipulatorManager()
{
    return sceneManipulatorManager;
}

scene3D::PreviewGeneratorPtr scene3D::Scene::getPreviewGenerator()
{
    return scenePreviewGenerator;
}

controller::ControllerWeakPtr scene3D::Scene::getController()
{
    return controller;
}

void scene3D::Scene::enterEditorMode()
{
    if (this->viewerMode)
    {
        this->selectionRootNode->policy = SoSelection::SHIFT;
        this->sceneSelectionManager->deselectAll();
        this->sceneSelectionManager->restoreHistory();
        this->sceneSelectionManager->setCreateOperations(true);
        this->viewerMode = false;
    }
}

void scene3D::Scene::enterViewerMode()
{
    if (!this->viewerMode)
    {
        this->sceneSelectionManager->setCreateOperations(false);
        this->selectionRootNode->policy = SoSelection::SINGLE;
        scene3D::SceneObjectPtr activeObject = this->sceneSelectionManager->getLastSelected();
        this->sceneSelectionManager->storeHistory();
        this->sceneSelectionManager->deselectAll();

        if (activeObject)
        {
            this->sceneSelectionManager->addToSelection(activeObject);
        }

        this->viewerMode = true;
    }
}
