/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectPreviewViewer.h"
#include <Inventor/events/SoMouseButtonEvent.h>

scene3D::ObjectPreviewViewer::ObjectPreviewViewer(const controller::ControllerPtr& control, QWidget* widget) : SoQtExaminerViewer(widget), control(control)
{
    //Viewer stuff
    this->setBackgroundColor(SbColor(100 / 255.0f, 100 / 255.0f, 100 / 255.0f));
    this->setAccumulationBuffer(false);
    this->setHeadlight(true);
    this->setViewing(false);
    this->setDecoration(false);
#ifdef WIN32
#ifndef _DEBUG
    this->setAntialiasing(true, 4);
#endif
#endif
    this->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    this->setFeedbackVisibility(false);

    //Set up scene
    SoSeparator* root = new SoSeparator;
    rotation = new SoRotationXYZ;
    rotation->axis = SoRotationXYZ::Y;
    previewRoot = new SoSeparator;
    root->addChild(rotation);
    root->addChild(previewRoot);
    this->setSceneGraph(root);

    //Setting up sensor
    timer.reset(new SoTimerSensor);
    timer->setFunction(timeSensorCallback);
    timer->setData(this);
    timer->setBaseTime(SbTime::getTimeOfDay());
    timer->setInterval(0.02f);
    timer->schedule();
}

void scene3D::ObjectPreviewViewer::setObjectToDisplay(SoNode* object)
{
    previewRoot->removeAllChildren();
    previewRoot->addChild(object);
}

void scene3D::ObjectPreviewViewer::showNoObject()
{
    previewRoot->removeAllChildren();
}

//Override the default navigation behaviour of the SoQtExaminerViewer
SbBool scene3D::ObjectPreviewViewer::processSoEvent(const SoEvent* const event)
{
    //Ignore everything
    return TRUE;
}

void scene3D::ObjectPreviewViewer::timeSensorCallback(void* data, SoSensor* sensor)
{
    ObjectPreviewViewer* viewer = (ObjectPreviewViewer*) data;

    if (viewer->previewRoot->getNumChildren() > 0)
    {
        viewer->rotation->angle.setValue(viewer->rotation->angle.getValue() + 0.01f);
        viewer->getCamera()->viewAll(viewer->getSceneGraph(), viewer->getViewportRegion());
    }
}
