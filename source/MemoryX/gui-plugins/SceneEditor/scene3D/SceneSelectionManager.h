/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <Inventor/SoPickedPoint.h>
#include <Inventor/SoPath.h>

#include "Scene.h"
#include "SceneObject.h"
#include "SceneGroup.h"

namespace scene3D
{
    class SceneSelectionManager
    {

        friend class Scene;

    public:
        /**
         * Constructor
         * Creates an Instance of the Class.
         *
         * @param scene Existing Scene
         */
        SceneSelectionManager(ScenePtr scene);

        /**
         * Returns all selected SceneObjects.
         *
         * @return vector<scene3D::SceneObjectPtr > Vector of all selected SceneObjects
         */
        std::vector<scene3D::SceneObjectPtr> getAllSelected();

        /**
         * Determines whether a SceneObject is selected.
         *
         * @param object SceneObject to check
         * @return bool SceneObject is selected?
         */
        bool isSelected(scene3D::SceneObjectPtr object);

        /**
         * Adds a SceneObject to the Selection.
         *
         * @param object SceneObject to be added
         */
        void addToSelection(scene3D::SceneObjectPtr object);

        /**
         * Removes a SceneObject from the Selection.
         *
         * @param object SceneObject to be removed
         */
        void removeFromSelection(scene3D::SceneObjectPtr object);

        /**
         * Adds a SceneGroup to the Selection.
         *
         * @param group SceneGroup to be added
         */
        void addToSelection(scene3D::SceneGroupPtr group);

        /**
         * Removes a SceneGroup from the Selection.
         *
         * @param group SceneGroup to be removed
         */
        void removeFromSelection(scene3D::SceneGroupPtr group);

        /**
         * Deselects all SceneObjects.
         *
         */
        void deselectAll();

        /**
         * Executes all commands when an Item is selected and the Callback is voked.
         *
         * @param userData UserData
         * @param selectionPath Path of selected Object
         */
        static void selectionCallback(void* userData, SoPath* selectionPath);
        /**
         * Executes all commands when an Item is deselected and the Callback is voked.
         *
         * @param userData UserData
         * @param selectionPath Path of deselected Object
         */
        static void deselectionCallback(void* userData, SoPath* selectionPath);
        /**
         * Adjusts a path so that it always show to a valid Object.
         *
         * @param userData UserData
         * @param pick The Point picked in the Scene
         * @return SoPath New adjusted Path
         */
        static SoPath* pickFilterCB(void* userData, const SoPickedPoint* pick);

        void setCreateOperations(bool enable);
        bool getCreateOperations();
    private:
        SceneWeakPtr scene;
        static bool createOperations;
        std::vector<scene3D::SceneObjectPtr> historySelected;

        void storeHistory();

        void restoreHistory();

        scene3D::SceneObjectPtr getLastSelected();
    };
}

