/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include "../controller/Controller.h"
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoRotationXYZ.h>

#ifndef __scene3D__ObjectPreviewViewer_h__
#define __scene3D__ObjectPreviewViewer_h__

using SoTimerSensorPtr = std::shared_ptr<SoTimerSensor>;

namespace scene3D
{
    class ObjectPreviewViewer : public SoQtExaminerViewer
    {
    public:
        /**
         * Constructor
         * Creates an Instance of the Class.
         *
         * @param control Controller
         * @param widget Parent Widget
         */
        ObjectPreviewViewer(const controller::ControllerPtr& control, QWidget* widget);

        void setObjectToDisplay(SoNode* object);
        void showNoObject();

    private:
        static void timeSensorCallback(void* data, SoSensor* sensor);
        SoRotationXYZ* rotation;
        SoSeparator* previewRoot;
        SbBool processSoEvent(const SoEvent* const event) override;
        controller::ControllerWeakPtr control;
        SoTimerSensorPtr timer;
    };
}

#endif
