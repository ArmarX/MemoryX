/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SceneViewer.h"
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>

#include "../controller/DeselectOperation.h"
#include "../controller/SelectOperation.h"


scene3D::SceneViewer::SceneViewer(const controller::ControllerPtr& control, QWidget* widget) : SoQtExaminerViewer(widget), control(control)
{
    this->setBackgroundColor(SbColor(100 / 255.0f, 100 / 255.0f, 100 / 255.0f));
    this->setAccumulationBuffer(false);
    this->setHeadlight(true);
    this->setViewing(false);
    this->setDecoration(false);
#ifdef WIN32
#ifndef _DEBUG
    this->setAntialiasing(true, 4);
#endif
#endif
    this->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    this->setFeedbackVisibility(true);
}

//Override the default navigation behaviour of the SoQtExaminerViewer
SbBool scene3D::SceneViewer::processSoEvent(const SoEvent* const event)
{
    const SoType type(event->getTypeId());

    //Remapping mouse press events
    if (type.isDerivedFrom(SoMouseButtonEvent::getClassTypeId()))
    {
        SoMouseButtonEvent* const ev = (SoMouseButtonEvent*) event;
        const int button = ev->getButton();
        const SbBool press = ev->getState() == SoButtonEvent::DOWN ? TRUE : FALSE;

        //LEFT MOUSE BUTTON
        if (button == SoMouseButtonEvent::BUTTON1)
        {
            if (controller::ControllerPtr controller = control.lock())
            {
                if (!controller->getScene()->getSelectionManager()->getCreateOperations())
                {
                    //SelectionManager is not creating Operations, so we shouldn't either!
                    return SoQtRenderArea::processSoEvent(ev);
                }
            }

            if (press)
            {
                /*We could just pass action into scenegraph here. There is only one litte thing
                that doesnt work then: deselecting by clicking into void is performed as
                many single deselection operations in controller. Undoing then performs
                every deselection seperately, which is annoying. SoSelection on the other
                hand doesn't tell us this 'click into void' event so we have to check ourself before this action
                goes into scenegraph. */

                SoRayPickAction rp(this->getViewportRegion());
                rp.setPickAll(false);
                rp.setPoint(event->getPosition());
                rp.apply(this->getSceneGraph());
                SoPickedPoint* picked = rp.getPickedPoint();

                if (!picked)
                {
                    /*We clicked into the void here, so we handle this event ourselves, instead of
                    leaving this up to the SoSelection Node... Tell controller to deselect everything
                    in one big operation block and then return, if there are any objects selected of course*/

                    //Shift click into void doesnt change anything
                    if (!ev->wasShiftDown())
                    {
                        if (controller::ControllerPtr controller = control.lock())
                        {
                            if (controller->getScene()->getSelectionManager()->getAllSelected().size() != 0)
                            {
                                std::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

                                // deselect all objects
                                for (scene3D::SceneObjectPtr object : controller->getScene()->getSelectionManager()->getAllSelected())
                                {
                                    controller::OperationPtr deselect(new controller::DeselectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
                                    operations->push_back(deselect);
                                }

                                controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
                            }
                        }
                    }

                    return TRUE;
                }
                else
                {
                    /* The performed click is above something!*/

                    /*We have to make sure we didn't click on a manipulator before we can make more decisions here. In the case we have clicked one,
                    we can simply pass the event into scenegraph, because we dont have to change the behaviour here!*/

                    SoFullPath* p = (SoFullPath*) picked->getPath();

                    for (int i = 0; i < p->getLength(); i++)
                    {
                        SoNode* n = p->getNode(i);

                        if (n->isOfType(SoTransformManip::getClassTypeId()))
                        {
                            //Manipulator found, pass event into scenegraph
                            return SoQtRenderArea::processSoEvent(ev);
                        }
                    }

                    /* Seems like we selected something and it is not a manipulator, must be an object! Let's check some last cases we have
                      to manually handle.*/

                    if (!ev->wasShiftDown())
                    {
                        /*If shift is not down we might get the case where mutiple objects are deselected before the picked object
                          is selected. We just do this work for the SoSelection node and group all deselect operations and the select operation
                          into one block (which is what SoSelection can't do for us). */
                        if (controller::ControllerPtr controller = control.lock())
                        {
                            //Is there more than one object already selected?
                            if (controller->getScene()->getSelectionManager()->getAllSelected().size() > 0)
                            {
                                //Find out which object to select
                                SceneObjectPtr obj;

                                for (int i = 0; i < picked->getPath()->getLength(); i++)
                                {
                                    SoNode* n = picked->getPath()->getNode(i);

                                    if (n->isOfType(SceneObject::getClassTypeId()))
                                    {
                                        obj.reset((SceneObject*) n);
                                    }
                                }

                                //Is there only one object selected and it is also the object we want to select here?
                                if (controller->getScene()->getSelectionManager()->getAllSelected().size() == 1
                                    && controller->getScene()->getSelectionManager()->isSelected(obj))
                                {
                                    //No need to do something, pass this to Selection node, default behaviour.
                                    return SoQtRenderArea::processSoEvent(ev);
                                }
                                else
                                {
                                    //Deselect all and select object in one block operation
                                    std::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

                                    // deselect all objects
                                    for (scene3D::SceneObjectPtr object : controller->getScene()->getSelectionManager()->getAllSelected())
                                    {
                                        controller::OperationPtr deselect(new controller::DeselectOperation(controller->getMemoryXController(), controller->getScene(), object->getObjectId()));
                                        operations->push_back(deselect);
                                    }

                                    controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);

                                    //Let SoSelection node handle the selection
                                    return SoQtRenderArea::processSoEvent(ev);
                                }
                            }
                            else
                            {
                                //Nothing selected, so its gonna be only one operation. Let SoSelection node do this.
                                return SoQtRenderArea::processSoEvent(ev);
                            }
                        }
                    }
                    else
                    {
                        /*If shift is down and an object is being picked, selection list can only grow by one element
                        or one element is deselected. We therefore don't need to put all operations into one block of operations
                        because it can only be one operation at once. Just pass this event right into the scenegraph and let
                        the SoSelection node handle it automatically. */
                        return SoQtRenderArea::processSoEvent(ev);
                    }
                }
            }
            else
            {
                return SoQtRenderArea::processSoEvent(ev);
            }
        }

        //RIGHT MOUSE BUTTON
        if (button == SoMouseButtonEvent::BUTTON2)
        {
            /*Right mouse button has no functionality yet, so ignore it.*/
            return TRUE;
        }

        //MIDDLE MOUSE BUTTON
        if (button == SoMouseButtonEvent::BUTTON3)
        {
            /*Middle mouse button now is used for all changes in camera perspective.
            To also display the cool little rotation cursor, viewing mode is set to on
            while button is down*/

            //Enable or disable viewing mode while BUTTON3 pressed
            if (press)
            {
                if (!this->isViewing())
                {
                    this->setViewing(true);
                }
            }
            else
            {
                if (this->isViewing())
                {
                    this->setViewing(false);
                }
            }

            //Remap BUTTON3 to BUTTON1
            ev->setButton(SoMouseButtonEvent::BUTTON1);

            //Make sure this REALLY only can happen when viewing mode is on.
            //Zooming can also turn it on and it leads to weird effects when doing both
            //(deselections in scene etc.) because SoQtExaminerViewer passes BUTTON1
            //events up to scenegraph when not in viewing mode.
            if (this->isViewing())
            {
                return SoQtExaminerViewer::processSoEvent(ev);
            }
        }

        //MOUSE WHEEL UP AND DOWN
        if (button == SoMouseButtonEvent::BUTTON4 || button == SoMouseButtonEvent::BUTTON5)
        {
            /*Zooming is allowed, so just use it. We have to temporarily turn viewing mode
            on to make SoQtExaminerViewer allow zooming.*/

            //Swap BUTTON4 and BUTTON5 because zooming out while scrolling up is just retarded
            ev->setButton(button == SoMouseButtonEvent::BUTTON4 ?
                          SoMouseButtonEvent::BUTTON5 : SoMouseButtonEvent::BUTTON4);

            //Zooming is allowed, so just pass it and temporarily set viewing mode on, if it is not already
            //(otherwise coin gives us warning messages...)
            if (!this->isViewing())
            {
                if (!this->isViewing())
                {
                    this->setViewing(true);
                }

                SoQtExaminerViewer::processSoEvent(ev);

                if (this->isViewing())
                {
                    this->setViewing(false);
                }
            }
            else
            {
                SoQtExaminerViewer::processSoEvent(ev);
            }

            return TRUE;
        }
    }

    // Keyboard handling
    if (type.isDerivedFrom(SoKeyboardEvent::getClassTypeId()))
    {
        const SoKeyboardEvent* const ev = (const SoKeyboardEvent*) event;

        /*The escape key and super key (windows key) is used to switch between
        viewing modes. We need to disable this behaviour completely.*/

        //65513 seems to be the super key, which is not available in the enum of keys in coin....
        if (ev->getKey() == SoKeyboardEvent::ESCAPE || ev->getKey() == 65513)
        {
            return TRUE;
        }
        else if (ev->getKey() == SoKeyboardEvent::S && ev->getState() == SoButtonEvent::DOWN)
        {
            if (!this->isSeekMode())
            {
                if (!this->isViewing())
                {
                    this->setViewing(true);
                }

                SoQtExaminerViewer::processSoEvent(ev);
                this->setSeekTime(0.5);
                this->seekToPoint(ev->getPosition());

                if (this->isViewing())
                {
                    this->setViewing(false);
                }
            }
        }
        else
        {
            SoQtExaminerViewer::processSoEvent(ev);
        }

        SoQtExaminerViewer::processSoEvent(ev);
    }

    //Let all move events trough
    if (type.isDerivedFrom(SoLocation2Event::getClassTypeId()))
    {
        return SoQtExaminerViewer::processSoEvent(event);
    }

    //YOU SHALL NOT PASS!
    return TRUE;
}
