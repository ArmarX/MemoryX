/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>
#include <vector>

#include "Scene.h"
#include "SceneGroup.h"

namespace scene3D
{
    class SceneGroupManager
    {
    public:
        /**
         * Returns all SceneGroups.
         *
         * @return vector<scene3D::SceneGroupPtr> All Groups
         */
        std::vector<scene3D::SceneGroupPtr> getAllGroups();

        /**
         * Returns a SceneGroup specified by an ID.
         *
         * @param groupId ID specifying the SceneGroup
         * @return scene3D::SceneGroup SceneGroup found
         */
        scene3D::SceneGroupPtr getGroupById(std::string groupId);

        /**
         * Adds a SceneGroup.
         *
         * @param group SceneGroup to be added.
         */
        void addGroup(SceneGroupPtr group);

        /**
         * Removes a SceneGroup.
         *
         * @param group SceneGroup to be removed
         */
        void removeGroup(scene3D::SceneGroupPtr group);

        /**
         * Renames a SceneGroup.
         *
         * @param group SceneGroup to be renamed
         * @param newGroupId New ID
         */
        void renameGroup(scene3D::SceneGroupPtr group, std::string newGroupId);

    private:
        std::vector<scene3D::SceneGroupPtr> sceneGroups;
    };
}


