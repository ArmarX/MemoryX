/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::GridFSFileEditorWidgetController
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GridFSFileEditorWidgetController.h"

#include <mongo/client/dbclient.h>

#include <QListWidgetItem>
#include <QProgressDialog>
#include <QComboBox>


namespace armarx
{

    GridFSFileEditorWidgetController::GridFSFileEditorWidgetController()

    {
        widget.setupUi(getWidget());
        getWidget()->setEnabled(false);
    }


    GridFSFileEditorWidgetController::~GridFSFileEditorWidgetController()
    { }


    void GridFSFileEditorWidgetController::loadSettings(QSettings* settings)
    {

    }


    void GridFSFileEditorWidgetController::saveSettings(QSettings* settings)
    {

    }


    void GridFSFileEditorWidgetController::onInitComponent()
    {
        usingProxy("CommonStorage");

    }


    void GridFSFileEditorWidgetController::onConnectComponent()
    {
        commonStoragePrx = getProxy<memoryx::CommonStorageInterfacePrx>("CommonStorage");

        widget.cbDBNames->clear();

        auto stdNames = commonStoragePrx->getDBNames();
        for (auto& name : stdNames)
        {
            widget.cbDBNames->addItem(QString::fromStdString(name));
        }
        widget.cbDBNames->setCurrentIndex(0);
        connectSlots();
        emit connected();
        enableMainWidgetAsync(true);
    }


    void GridFSFileEditorWidgetController::onDisconnectComponent()
    {
        //    getWidget()->setEnabled(false);
        enableMainWidgetAsync(false);
    }


    void GridFSFileEditorWidgetController::connectSlots()
    {
        connect(widget.listFileNames, SIGNAL(currentTextChanged(QString)), this, SLOT(loadFileContent(QString)), Qt::UniqueConnection);
        connect(widget.btnSaveFile, SIGNAL(clicked()), this, SLOT(saveContentToFile()), Qt::UniqueConnection);
        connect(widget.btnDelUnreferencedFiles, SIGNAL(clicked()), this, SLOT(deleteUnreferencedFiles()), Qt::UniqueConnection);
        connect(this, SIGNAL(connected()), SLOT(loadFileNames()), Qt::UniqueConnection);
        connect(widget.cbDBNames, SIGNAL(currentIndexChanged(QString)), SLOT(loadFileNames()), Qt::UniqueConnection);
        connect(widget.btnLoadDB, SIGNAL(clicked()), SLOT(loadFileNames()), Qt::UniqueConnection);
    }

    void GridFSFileEditorWidgetController::deleteUnreferencedFiles()
    {
        auto dbName = widget.cbDBNames->currentText().toStdString();
        NameList ids = commonStoragePrx->getFileIdList(dbName);
        auto colls = commonStoragePrx->getCollectionNames(dbName);
        std::map<std::string, memoryx::CollectionInterfacePrx> collPrxs;

        for (auto coll : colls)
        {
            collPrxs[coll] =  commonStoragePrx->requestCollection(coll);
        }
        QProgressDialog progress("Deleting unreferenced files", "Cancel", 0, ids.size(), getWidget());
        progress.setWindowModality(Qt::WindowModal);
        int i = 0;
        for (auto fileId : ids)
        {
            int found = 0;
            auto query = whereQuery(fileId);

            for (auto coll : colls)
            {
                auto prx = collPrxs[coll];
                auto list = prx->findByConstraintQuery("", query);

                if (list.size() > 0)
                {
                    found += list.size();
                }
                qApp->processEvents();
                progress.setValue(i);
                if (progress.wasCanceled())
                {
                    break;
                }
            }
            if (progress.wasCanceled())
            {
                break;
            }

            auto filePrx = commonStoragePrx->getFileProxyById(dbName, fileId);

            if (found == 0)
            {
                ARMARX_IMPORTANT_S << "Removing file: " << filePrx->getFilename();
                commonStoragePrx->removeFileById(dbName, fileId);
            }
            else
            {
                ARMARX_INFO_S << filePrx->getFilename() << " has " << found << " references - not deleting";
            }
            i++;

        }

        for (auto& coll : collPrxs)
        {
            commonStoragePrx->releaseCollection(coll.second);
        }

        loadFileNames();
    }




    void GridFSFileEditorWidgetController::loadFileNames(QString dbName)
    {
        auto stdDBName = dbName.isEmpty() ? widget.cbDBNames->currentText().toStdString() : dbName.toStdString();
        NameList filesNames = commonStoragePrx->getFileNameList(stdDBName);
        NameList ids = commonStoragePrx->getFileIdList(stdDBName);
        widget.listFileNames->clear();

        for (size_t i = 0; i < filesNames.size(); i++)
        {
            auto filename = filesNames.at(i);
            auto id = ids.at(i);
            ARMARX_INFO << "file: " << filename;
            QListWidgetItem* item = new QListWidgetItem();
            item->setText(QString::fromStdString(filename));
            item->setData(Qt::UserRole, QString::fromStdString(id));
            widget.listFileNames->addItem(item);

        }

        widget.listFileNames->sortItems();
        widget.textEdit->clear();
    }

    void GridFSFileEditorWidgetController::loadFileContent(const QString& fileName)
    {
        std::string content;

        if (commonStoragePrx->getTextFileByName(widget.cbDBNames->currentText().toStdString(), fileName.toStdString(), content))
        {
            widget.textEdit->clear();
            widget.textEdit->insertPlainText(QString::fromStdString(content));
            currentId = widget.listFileNames->currentItem()->data(Qt::UserRole).toString();
        }

    }

    void GridFSFileEditorWidgetController::saveContentToFile()
    {
        auto dbName = widget.cbDBNames->currentText().toStdString();
        auto newId = commonStoragePrx->storeTextFile(dbName,
                     widget.textEdit->toPlainText().toStdString(),
                     widget.listFileNames->currentItem()->text().toStdString());
        ARMARX_INFO << "stored " << widget.listFileNames->currentItem()->text().toStdString() << " in grid fs: " << newId;
        replaceFileDocID(currentId, QString::fromStdString(newId));
        commonStoragePrx->removeFileById(dbName, currentId.toStdString());
    }


    void GridFSFileEditorWidgetController::replaceFileDocID(QString fileId, const QString& newId)
    {

        auto query = whereQuery(fileId.toStdString());
        auto dbName = widget.cbDBNames->currentText().toStdString();
        auto colls = commonStoragePrx->getCollectionNames(dbName);

        for (auto coll : colls)
        {
            auto prx = commonStoragePrx->requestCollection(coll);

            auto list = prx->findByConstraintQuery("", query);

            for (memoryx::DBStorableData data : list)
            {
                QString json = QString::fromStdString(data.JSON);
                json = json.replace(fileId, newId);
                data.JSON = json.toStdString();
                prx->save(data);

            }

            commonStoragePrx->releaseCollection(prx);
        }
    }

    std::string GridFSFileEditorWidgetController::whereQuery(std::string fileId) const
    {
        std::string query = "function(){\
            var objs = []; \
            objs.push(this); \
            while(objs.length > 0) \
            { \
                var obj = objs.pop(); \
                var keys = Object.keySet(obj); \
                for(var i = 0; i < keys.length;i++) \
                { \
                    if(keys[i] == \"docId\" && obj[keys[i]] === \"" + fileId + "\")  \
                        return true; \
                    if(isObject(obj[keys[i]])) \
                    { \
                        objs.push(obj[keys[i]]); \
                    } \
                } \
            } \
            return false; \
                }";
        return query;
    }
}
