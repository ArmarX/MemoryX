/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ObjectLocalizationRequesterWidgetController
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/gui-plugins/ObjectLocalizationRequester/ui_ObjectLocalizationRequesterWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>

#include <QToolBar>

namespace armarx
{
    /**
    \page MemoryX-GuiPlugins-ObjectLocalizationRequester ObjectLocalizationRequester
    \brief The ObjectLocalizationRequester allows triggering and releasing of localization requests of specific objects.

    The user can select from the currently available object classes with an assigned recongition method and request them with a specified update frequency.
    The user can also list all currently running localization requests and delete them.

    API Documentation \ref ObjectLocalizationRequesterWidgetController

    \see ObjectLocalizationRequesterGuiPlugin
    */

    /**
     * \class ObjectLocalizationRequesterWidgetController
     * \brief ObjectLocalizationRequesterWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ObjectLocalizationRequesterWidgetController:
        public ArmarXComponentWidgetControllerTemplate<ObjectLocalizationRequesterWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit ObjectLocalizationRequesterWidgetController();

        /**
         * Controller destructor
         */
        virtual ~ObjectLocalizationRequesterWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "MemoryX.ObjectLocalizationRequester";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onDisconnectComponent() override;

    public slots:
        /* QT slot declarations */
        void updateAvailableObjects();
        void updateAvailableChannels();

    signals:
        /* QT signal declarations */

    private slots:
        void on_toolButtonRefresh_clicked();

        void on_btnRequest_clicked();

        void on_btnRelease_clicked();

    private:
        /**
         * Widget Form
         */
        Ui::ObjectLocalizationRequesterWidget widget;

        memoryx::PriorKnowledgeInterfacePrx prior;
        memoryx::ObjectMemoryObserverInterfacePrx omo;
        ChannelRegistry channels;
        QToolBar* customToolbar;
        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;
    };
}


