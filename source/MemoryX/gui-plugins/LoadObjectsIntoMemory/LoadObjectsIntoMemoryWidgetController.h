/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ARCHES::gui-plugins::LoadObjectsIntoMemoryWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <MemoryX/gui-plugins/LoadObjectsIntoMemory/ui_LoadObjectsIntoMemoryWidget.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/GridFileManager.h>

namespace armarx
{
    /**
    \page MemoryX-GuiPlugins-LoadObjectsIntoMemory LoadObjectsIntoMemory
    \brief The LoadObjectsIntoMemory allows visualizing ...

    \image html LoadObjectsIntoMemory.png
    The user can

    API Documentation \ref LoadObjectsIntoMemoryWidgetController

    \see LoadObjectsIntoMemoryGuiPlugin
    */

    /**
     * \class LoadObjectsIntoMemoryWidgetController
     * \brief LoadObjectsIntoMemoryWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        LoadObjectsIntoMemoryWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < LoadObjectsIntoMemoryWidgetController >
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit LoadObjectsIntoMemoryWidgetController();

        /**
         * Controller destructor
         */
        virtual ~LoadObjectsIntoMemoryWidgetController() {}

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings*) override {}

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings*) override {}

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "MemoryX.LoadObjectsIntoMemory";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override {}

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;


        virtual QPointer<QDialog> getConfigDialog(QWidget* parent) override;
    public slots:
        void addObjects();
        void updateDBs();

    signals:
        /* QT signal declarations */

    private:
        std::string getFilesDBName()
        {
            const std::string ns = classesSegmentPrx->getWriteCollectionNS();
            const size_t found = ns.find_first_of('.');
            return (found != std::string::npos) ? ns.substr(0, found) : "";
        }
        /**
         * Widget Form
         */
        Ui::LoadObjectsIntoMemoryWidget widget;

        QPointer<SimpleConfigDialog> dialog;

        memoryx::CommonStorageInterfacePrx databasePrx;
        memoryx::PriorKnowledgeInterfacePrx priorPrx;
        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx;
        memoryx::GridFileManagerPtr fileManager;

    };
}


