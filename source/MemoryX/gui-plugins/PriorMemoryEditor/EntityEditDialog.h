/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{

    /**
     * The entity edit dialog is a superclass for all edit dialogs used to set attributes of
     * entites. Currently the dialogs are used by the PriorMemoryEditor gui plugin.
     **/
    class EntityEditDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit EntityEditDialog(QWidget* parent = 0)
            : QDialog(parent)
        {}

        /**
         * Pure virtual method. Implement this in order to update the dialog with the information
         * contained in the entity provided as parameter.
         *
         * @param entity the entity used for updating the Gui
         **/
        virtual void updateGui(const EntityPtr& entity) = 0;

        /**
         * Pure virtual method. Implement this in order to update the entity with information
         * edited in the gui
         *
         * @param entity the entity to update
         **/
        virtual void updateEntity(const EntityPtr& entity, std::string filesDBName) = 0;
    };
}

