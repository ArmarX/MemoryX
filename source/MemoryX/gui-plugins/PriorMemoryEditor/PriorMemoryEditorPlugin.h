/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::PriorMemoryEditorPlugin
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_PriorMemoryEditorPlugin.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/libraries/core/Pose.h>

/* VirtualRobot headers */
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/VisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

/* MemoryX headers */
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/GridFileManager.h>

/* Qt headers */
#include <QMainWindow>

#include <memory>

/* Coin3D/SoQt headers */
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoMatrixTransform.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/Qt/SoQt.h>

namespace memoryx
{
    class PriorEditorConfigDialog;
    class CollectionCreateDialog;

    /**
     * \class PriorMemoryEditorPlugin
     * \brief The PriorMemoryEditorPlugin provides a widget that allows to view and edit data entries in the prior memory.
     * \see PriorEditorController
     */
    class PriorMemoryEditorPlugin :
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        PriorMemoryEditorPlugin();
    };

    /**
     \page MemoryX-GuiPlugins-PriorMemoryEditor PriorMemoryEditor
     \brief This widget allows viewing and editing data entries in the prior memory.

     \image html PriorMemoryEditor.png
     You can inspect the different collections that are stored as prior knowledge by selecting a
     collection from the collection drop-down-menu. All object classes that are stored within a collection
     are listed in the table in the center of the screen. You may view the visual or collision model for
     an object class by hitting the respective buttons. Similarly, you can also add, remove and edit entries.

     When you add the widget to the Gui, you need to specify the following parameters:

     Parameter Name   | Example Value     | Required?     | Description
      :----------------  | :-------------:   | :-------------- |:--------------------
     MongoDB host     | localhost:27017   | Yes | The address of the mongo database that contains the prior memory.
     MongoDB user     | username          | Only if DB is remote | ?
     Password | userPassword | Only if DB is remote | ?
     Gui Plugin Name | PriorMemoryEditorPlugin | Yes | ?
     PriorMemory Name | PriorKnowledge | Yes | ?

     \note In order to use this Gui there must be prior knowledge within a MongoDB available.
     PriorMemoryEditor API Documentation \ref PriorEditorController

     How to use:

    \li Gui->addWidget->PriorMemoryEditorPlugin
    \li Different collection Lists (all collections need pr_ prefix so the gui can distinguish between different types)
    \li All classes/objects are shown in list -> double click shows the 3D model in Viewer
    \li Edit
        -> parent classes  (Food, used for queries like common places for Food),
        -> model files via XML or separate IV files -> in /data/


     \see PriorMemoryEditorPlugin
     */

    /**
     * @class PriorEditorController
     * \brief This widget allows to view and edit data entries in the prior memory.
     *
     * @see PriorMemoryEditorPlugin
     */
    class PriorEditorController :
        public armarx::ArmarXComponentWidgetControllerTemplate<PriorEditorController>
    {
        Q_OBJECT
    public:
        PriorEditorController();
        virtual ~PriorEditorController() {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;
        // end of inherited from Component

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "MemoryX.PriorMemoryEditor";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/database.svg");
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon("://icons/database.svg");
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void configured() override;

        SoNode* getScene() override;

        bool setupViewer();

    signals:

        void objectsLoaded();

        void objectChanged(QString objectId, bool isNew);

    public slots:

        void reconnect();
        void collectionChanged();
        void addCollection();
        void refetchData();

        void showObjectClassVisu();
        void showObjectClassColl();
        void addObjectClass();
        void editObjectClass();
        void removeObjectClass();
        void treeItemDoubleClicked(QTreeWidgetItem* item, int column);
        void treeCurrentItemChanged(QTreeWidgetItem* cur, QTreeWidgetItem* prev);

        void clearVisualization();

    protected:

        using ObjectEntryMap = std::map<std::string, ObjectClassPtr>;

        void connectSlots();

        Ui::PriorMemoryEditorPlugin ui;

        bool verbose;

        // maps entityId with data
        ObjectEntryMap currentObjectClasses;
        bool show3DViewer;

        SoSeparator* visu;

        //void updateModel();

        void updateObject(const memoryx::ObjectClassPtr obj, bool force = false);

        // build visualization
        void createVisualization(const ::memoryx::ObjectClassPtr obj);

        void clearObjects();


    private:

        std::recursive_mutex mutexEntities;

        CommonStorageInterfacePrx databasePrx;
        PriorKnowledgeInterfacePrx memoryPrx;
        PersistentObjectClassSegmentBasePrx classesSegmentPrx;

        GridFileManagerPtr fileManager;

        void doEditClass(bool isNew);

        VirtualRobot::ManipulationObjectPtr loadManipulationObjectFile(const std::string& xmlFName);
        VirtualRobot::ManipulationObjectPtr createManipulationObjectFromIvFiles(const std::string& objName,
                const std::string& ivFnameVis, const std::string& ivFNameCollision);

        void storeClassManipulationFile(memoryx::ObjectClassPtr objClass, const std::string& xmlFName);
        void storeClassManipulationObject(memoryx::ObjectClassPtr objClass,
                                          const VirtualRobot::ManipulationObjectPtr mo);
        void storeClassIVFiles(memoryx::ObjectClassPtr objClass,
                               const std::string& visuFName, const std::string& collisionFName);

        void findIVTextures(const std::string& ivFName, NameList& textures);

        void updateObjectInTree(memoryx::ObjectClassPtr obj);
        std::string getSelectedClassId();
        std::string getSelectedClassName();

        std::string getFilesDBName();

        void showObjectClassModel(const std::string& objectId, VirtualRobot::SceneObject::VisualizationType visuType);

        void refreshControls();

        void setCurrentCollection(const std::string& collNS);
        void refreshCollectionList();

        QPointer<PriorEditorConfigDialog> dialog;
        QPointer<CollectionCreateDialog> collCreateDialog;

        std::string settings_priorMemory;
        std::string settings_mongoHost;
        std::string settings_mongoUser;
        std::string settings_mongoPass;
        std::string settings_robotStateComponentProxyName;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;

        bool connected;

    };
    using PriorEditorControllerPtr = std::shared_ptr<PriorEditorController>;
}



