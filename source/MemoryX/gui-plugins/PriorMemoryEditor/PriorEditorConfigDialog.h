/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::PriorMemoryEditorPlugin
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/RobotState.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <QFileDialog>
#include <QDialog>


namespace Ui
{
    class PriorEditorConfigDialog;
}

namespace memoryx
{
    class PriorEditorConfigDialog : public QDialog,
        public armarx::ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit PriorEditorConfigDialog(armarx::IceManagerPtr iceManager, QWidget* parent = 0);
        ~PriorEditorConfigDialog() override;

    public slots:
        void modelFileSelected();
    private:
        Ui::PriorEditorConfigDialog* ui;
        QFileDialog* fileDialog;
        armarx::IceProxyFinder<armarx::RobotStateComponentInterfacePrx>* proxyFinder;
        friend class PriorEditorController;
        std::string uuid;
        // ManagedIceObject interface
    protected:
        void onInitComponent() override
        {
            proxyFinder->setIceManager(getIceManager());
        }
        void onConnectComponent() override
        {
        }
        std::string getDefaultName() const override
        {
            return "PriorEditorConfigDialog" + uuid;
        }
    };
}

