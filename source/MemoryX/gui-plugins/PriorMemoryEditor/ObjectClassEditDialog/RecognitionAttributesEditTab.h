/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QWidget>
#include <MemoryX/core/entity/Entity.h>
#include "../EntityAttributesEditTab.h"


namespace Ui
{
    class RecognitionAttributesEditTab;
}

namespace memoryx
{

    /**
     * This tab allows to change the recognition attributes of an objectclass entity.
     **/
    class RecognitionAttributesEditTab :
        public EntityAttributesEditTab
    {
        Q_OBJECT

    public:
        explicit RecognitionAttributesEditTab(QWidget* parent = 0);
        ~RecognitionAttributesEditTab() override;

        /**
         * Pure virtual method. Implement this in order to update the dialog with the information
         * contained in the entity provided as parameter.
         *
         * @param entity the entity used for updating the Gui
         **/
        void updateGui(const EntityPtr& entity) override;

        /**
         * Pure virtual method. Implement this in order to update the entity with information
         * edited in the gui
         *
         * @param entity the entity to update
         **/
        void updateEntity(const EntityPtr& entity, std::string filesDBName) override;

    public slots:
        void texturedFeatureFileSelected();
        void segmentableFeatureFileSelected();
        void recognitionMethodChanged(const QString& method);

    private:
        Ui::RecognitionAttributesEditTab* ui;

        QFileDialog* texturedFeatureFileDialog;
        QFileDialog* segmentableFeatureFileDialog;
        const std::string texturedRecognition;
        const std::string segmentableRecognition;
        const std::string handMarkerRecognition;
        const std::string arMarkerRecognition;
        const std::string otherRecognitionMethod;
    };
}

