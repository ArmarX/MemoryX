/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ObjectClassEditDialog.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_ObjectClassEditDialog.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace memoryx;
using namespace memoryx::EntityWrappers;

ObjectClassEditDialog::ObjectClassEditDialog(armarx::RobotStateComponentInterfacePrx robotStateComponent, QWidget* parent) :
    EntityEditDialog(parent),
    ui(new Ui::ObjectClassEditDialog),
    robotStateComponent(robotStateComponent)
{
    // set up general UI
    ui->setupUi(this);
    setClassNameEditable(true);

    // construct and add attribute tabs
    recognitionAttributesEditTab = new RecognitionAttributesEditTab();
    ui->tabWidget->addTab(recognitionAttributesEditTab, "Recognition");

    manipulationAttributesEditTab = new ManipulationAttributesEditTab(robotStateComponent);
    ui->tabWidget->addTab(manipulationAttributesEditTab, "Manipulation");

    motionAttributesEditTab = new MotionAttributesEditTab();
    ui->tabWidget->addTab(motionAttributesEditTab, "Motion");
}

ObjectClassEditDialog::~ObjectClassEditDialog()
{
    delete ui;
}

void ObjectClassEditDialog::setClassNameEditable(bool editable)
{
    ui->editClassName->setReadOnly(!editable);
}

void ObjectClassEditDialog::updateGui(const EntityPtr& entity)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    // fill dialog (with default attributes)
    ui->editClassName->setText(QString::fromStdString(objectClass->getName()));
    NameList parents = objectClass->getParentClasses();
    std::string parentsStr = simox::alg::join(parents, ",");
    ui->editParentClasses->setText(QString::fromStdString(parentsStr));
    bool isInstanceable = objectClass->isInstanceable();
    ui->checkBoxIsInstanceable->setChecked(isInstanceable);

    // update tabs
    manipulationAttributesEditTab->updateGui(entity);
    recognitionAttributesEditTab->updateGui(entity);
    motionAttributesEditTab->updateGui(entity);
}

void ObjectClassEditDialog::updateEntity(const EntityPtr& entity, std::string filesDBName)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    // update default attributes
    objectClass->setName(ui->editClassName->text().toStdString());
    objectClass->clearParentClasses();
    std::string parentsStr = ui->editParentClasses->text().toStdString();
    NameList parents = simox::alg::split(parentsStr, ",");

    for (NameList::const_iterator it = parents.begin(); it != parents.end(); ++it)
    {
        objectClass->addParentClass(*it);
    }

    objectClass->setInstanceable(ui->checkBoxIsInstanceable->isChecked());

    // update from tabs
    manipulationAttributesEditTab->updateEntity(entity, filesDBName);
    recognitionAttributesEditTab->updateEntity(entity, filesDBName);
    motionAttributesEditTab->updateEntity(entity, filesDBName);

}

