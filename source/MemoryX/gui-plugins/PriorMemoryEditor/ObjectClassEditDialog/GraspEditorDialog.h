/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <QWidget>
#include <VirtualRobot/Grasping/GraspEditor/GraspEditorWindow.h>

namespace Ui
{
    class GraspEditorDialog;
}

namespace memoryx
{
    class GraspEditorDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit GraspEditorDialog(std::string objectFile, std::string robotFile, QWidget* parent = 0);
        ~GraspEditorDialog() override;

    public slots:

    private:
        Ui::GraspEditorDialog* ui;

        VirtualRobot::GraspEditorWindow* graspEditor;
    };
}

