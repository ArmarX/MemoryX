/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "MotionAttributesEditTab.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_MotionAttributesEditTab.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

using namespace memoryx;
using namespace memoryx::EntityWrappers;

MotionAttributesEditTab::MotionAttributesEditTab(QWidget* parent)
    : EntityAttributesEditTab(parent),
      ui(new Ui::MotionAttributesEditTab)
{
    ui->setupUi(this);

    // init feature file dialog
    connect(ui->comboBoxMotionModel, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(motionModelChanged(const QString&)));
    connect(ui->comboBoxSimulationType, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(simulationTypeChanged(const QString&)));

    motionModelChanged("");
    simulationTypeChanged("");
}

MotionAttributesEditTab::~MotionAttributesEditTab()
{
    delete ui;
}

void MotionAttributesEditTab::updateGui(const EntityPtr& entity)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    ObjectRecognitionWrapperPtr objectRecognitionWrapper = objectClass->getWrapper<ObjectRecognitionWrapper>();
    int index = ui->comboBoxMotionModel->findText(QString(objectRecognitionWrapper->getDefaultMotionModel().c_str()));
    if (index != -1)
    {
        ui->comboBoxMotionModel->setCurrentIndex(index);
    }

    SimoxObjectWrapperPtr simoxWrapper = objectClass->getWrapper<SimoxObjectWrapper>();
    VirtualRobot::SceneObject::Physics::SimulationType simType = simoxWrapper->getManipulationObject()->getSimulationType();
    index = ui->comboBoxSimulationType->findText(simTypeToString(simType));
    if (index != -1)
    {
        ui->comboBoxSimulationType->setCurrentIndex(index);
    }
}


void MotionAttributesEditTab::updateEntity(const EntityPtr& entity, std::string filesDBName)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    ObjectRecognitionWrapperPtr objectRecognitionWrapper = objectClass->getWrapper<ObjectRecognitionWrapper>();
    std::string motionModelName = ui->comboBoxMotionModel->currentText().toStdString();
    ARMARX_INFO << "Setting motion model: " << motionModelName;
    objectRecognitionWrapper->setDefaultMotionModel(motionModelName);

    SimoxObjectWrapperPtr simoxWrapper = objectClass->getWrapper<SimoxObjectWrapper>();
    VirtualRobot::ManipulationObjectPtr mo = simoxWrapper->getManipulationObject();
    QString simulationTypeName = ui->comboBoxSimulationType->currentText();

    ARMARX_INFO << simoxWrapper->getManipulationObjectFileName();
    ARMARX_INFO << "Setting simulation type: " << simulationTypeName.toStdString();
    mo->setSimulationType(stringToSimType(simulationTypeName));
    simoxWrapper->setAndStoreManipulationObject(mo, filesDBName);
}

void MotionAttributesEditTab::motionModelChanged(const QString& method)
{
}

void MotionAttributesEditTab::simulationTypeChanged(const QString& method)
{
}

QString MotionAttributesEditTab::simTypeToString(VirtualRobot::SceneObject::Physics::SimulationType simType)
{
    switch (simType)
    {
        case VirtualRobot::SceneObject::Physics::eStatic:
            return "Static";
        case VirtualRobot::SceneObject::Physics::eKinematic:
            return "Kinematic";
        case VirtualRobot::SceneObject::Physics::eDynamic:
            return "Dynamic";
        default:
            return "Unknown";
    }
}

VirtualRobot::SceneObject::Physics::SimulationType MotionAttributesEditTab::stringToSimType(const QString& string)
{
    if (string == "Static")
    {
        return VirtualRobot::SceneObject::Physics::eStatic;
    }
    else if (string == "Kinematic")
    {
        return VirtualRobot::SceneObject::Physics::eKinematic;
    }
    else if (string == "Dynamic")
    {
        return VirtualRobot::SceneObject::Physics::eDynamic;
    }
    else
    {
        return VirtualRobot::SceneObject::Physics::eUnknown;
    }
}
