/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "CollectionCreateDialog.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_CollectionCreateDialog.h>

using namespace memoryx;

CollectionCreateDialog::CollectionCreateDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::CollectionCreateDialog)
{
    ui->setupUi(this);
}

CollectionCreateDialog::~CollectionCreateDialog()
{
    delete ui;
}


void CollectionCreateDialog::setCollectionPrefix(const std::string& prefix)
{
    this->collPrefix = prefix;
    ui->lblPrefix->setText(QString::fromStdString(prefix));
}

void CollectionCreateDialog::setDatabases(const NameList& dbNames)
{
    ui->cbDatabases->clear();

    for (NameList::const_iterator it = dbNames.begin(); it != dbNames.end(); ++it)
    {
        ui->cbDatabases->addItem(QString::fromStdString(*it));
    }
}

std::string CollectionCreateDialog::getDBName() const
{
    return ui->cbDatabases->currentText().toStdString();
}

std::string CollectionCreateDialog::getCollectionName() const
{
    return collPrefix + ui->editCollection->text().toStdString();
}


