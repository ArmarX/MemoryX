/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
// Dialogs
#include "PriorMemoryEditorPlugin.h"
#include "PriorEditorConfigDialog.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_PriorEditorConfigDialog.h>
#include "ObjectClassEditDialog/ObjectClassEditDialog.h"
#include "CollectionCreateDialog.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_CollectionCreateDialog.h>

// ArmarX core
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/interface/core/Log.h>
#include <ArmarXCore/util/CPPUtility/trace.h>
#include <RobotAPI/libraries/core/Pose.h>

// Qt headers
#include <Qt>
#include <QtGlobal>
#include <QSpinBox>
#include <QSlider>
#include <QPushButton>
#include <QStringList>
#include <QTableView>
#include <QCheckBox>
#include <QBrush>
#include <QMessageBox>
#include <QStandardItemModel>

// Coin3D headers
#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoUnits.h>

// System
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

// Boost
#include <filesystem>

// Simox-VirtualRobot
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/XML/ObjectIO.h>
#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <VirtualRobot/Grasping/Grasp.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

// MemoryX
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>

using namespace memoryx;
using namespace memoryx::EntityWrappers;
using namespace VirtualRobot;

namespace fs = std::filesystem;

#define DEFAULT_SETTINGS_PLUGIN_NAME "PriorMemoryEditorPlugin"
#define DEFAULT_SETTINGS_PRIORMEMORY_NAME "PriorKnowledge"

namespace TreeItemType
{
    enum TreeItemTypeEnum
    {
        eItemObject = QTreeWidgetItem::UserType + 1,
        eItemAttr = QTreeWidgetItem::UserType + 2,
        eItemValue = QTreeWidgetItem::UserType + 3
    };
}

PriorMemoryEditorPlugin::PriorMemoryEditorPlugin()
{
    addWidget<PriorEditorController>();
}


PriorEditorController::PriorEditorController() :
    connected(false)
{
    visu = nullptr;
    show3DViewer = false;

    ui.setupUi(getWidget());
    treeCurrentItemChanged(nullptr, nullptr);

    collCreateDialog = new CollectionCreateDialog();
    collCreateDialog->setCollectionPrefix(memoryx::PRIOR_COLLECTION_PREFIX);
    getWidget()->setEnabled(false);
}

void PriorEditorController::onInitComponent()
{
    ARMARX_TRACE;
    verbose = true;

    visu = new SoSeparator();
    visu->ref();

    connectSlots();

    ARMARX_INFO << "PriorMemory: " << settings_priorMemory;

    usingProxy(settings_priorMemory);
    usingProxy("CommonStorage");
    if (!settings_robotStateComponentProxyName.empty())
    {
        usingProxy(settings_robotStateComponentProxyName);
    }
}

void PriorEditorController::onConnectComponent()
{
    ARMARX_TRACE;
    memoryPrx = getProxy<PriorKnowledgeInterfacePrx>(settings_priorMemory);
    classesSegmentPrx = memoryPrx->getObjectClassesSegment();
    databasePrx = memoryPrx->getCommonStorage();
    if (!settings_robotStateComponentProxyName.empty())
    {
        getProxy(robotStateComponent, settings_robotStateComponentProxyName);
    }

    fileManager.reset(new GridFileManager(databasePrx));

    ui.editHost->setText(QString::fromStdString(databasePrx->getMongoHostAndPort()));

    connected = databasePrx->isConnected();

    refreshControls();
    refreshCollectionList();
    setCurrentCollection(classesSegmentPrx->getWriteCollectionNS());
    enableMainWidgetAsync(true);
}

void PriorEditorController::onDisconnectComponent()
{
    ARMARX_TRACE;
    dialog->close();
    collCreateDialog->close();
    connected = false;
    refreshControls();
    enableMainWidgetAsync(false);
}

void PriorEditorController::onExitComponent()
{
    ARMARX_TRACE;
    clearObjects();

    if (visu)
    {
        visu->unref();
    }
}

QPointer<QDialog> PriorEditorController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new PriorEditorConfigDialog(getIceManager(), parent);
        dialog->ui->editPriorMemoryEditorName->setText(QString::fromStdString(DEFAULT_SETTINGS_PLUGIN_NAME));
        dialog->ui->editPriorMemoryName->setText(QString::fromStdString(DEFAULT_SETTINGS_PRIORMEMORY_NAME));
    }

    return qobject_cast<PriorEditorConfigDialog*>(dialog);
}

void PriorEditorController::configured()
{
    ARMARX_VERBOSE << "PriorEditorController::configured()";
    settings_mongoHost = dialog->ui->editMongoHost->text().toStdString();
    settings_mongoUser = dialog->ui->editMongoUser->text().toStdString();
    settings_mongoPass = dialog->ui->editMongoPass->text().toStdString();
    settings_priorMemory = dialog->ui->editPriorMemoryName->text().toStdString();
    this->setInstanceName(dialog->ui->editPriorMemoryEditorName->text());
    settings_robotStateComponentProxyName = dialog->proxyFinder->getSelectedProxyName().toStdString();

    // ???
    // databasePrx->reconnect(ui.editHost->text().toStdString(), settings_mongoUser, settings_mongoPass);
}


void PriorEditorController::loadSettings(QSettings* settings)
{
    /*robotFile = settings->value("RobotFile", QString::fromStdString(robotFile_default)).toString().toStdString();
    objectFile = settings->value("ObjectFile", QString::fromStdString(objectFile_default)).toString().toStdString();
    show3DViewer = settings->value("ViewerEnabled", "false") == "true";*/
}

void PriorEditorController::saveSettings(QSettings* settings)
{
    /*settings->setValue("RobotFile", QString::fromStdString(robotFile));
    settings->setValue("ObjectFile", QString::fromStdString(objectFile));
    settings->setValue("ViewerEnabled", QString(show3DViewer?"true":"false"));*/
}


SoNode* PriorEditorController::getScene()
{
    ARMARX_TRACE;
    if (visu)
    {
        std::cout << "returning scene=" << visu->getName() << std::endl;
    }

    return visu;
}

void PriorEditorController::connectSlots()
{
    ARMARX_TRACE;
    connect(ui.btnReconnect, SIGNAL(clicked()), this, SLOT(reconnect()));

    connect(ui.btnAddCollection, SIGNAL(clicked()), this, SLOT(addCollection()));
    connect(ui.btnRefetch, SIGNAL(clicked()), this, SLOT(refetchData()));

    connect(ui.cbCollection, SIGNAL(currentIndexChanged(int)),
            this, SLOT(collectionChanged()), Qt::QueuedConnection);

    connect(ui.btnShowVisu, SIGNAL(clicked()), this, SLOT(showObjectClassVisu()));
    connect(ui.btnShowColl, SIGNAL(clicked()), this, SLOT(showObjectClassColl()));
    connect(ui.btnEdit, SIGNAL(clicked()), this, SLOT(editObjectClass()));
    connect(ui.btnAdd, SIGNAL(clicked()), this, SLOT(addObjectClass()));
    connect(ui.btnRemove, SIGNAL(clicked()), this, SLOT(removeObjectClass()));
    connect(this, SIGNAL(objectsLoaded()), this, SLOT(clearVisualization()), Qt::QueuedConnection);
    //    connect(this, SIGNAL(objectChanged(QString, bool)), this, SLOT(updateObjectVisu(QString, bool)), Qt::QueuedConnection);

    connect(ui.treeWidgetEntities, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)),
            this, SLOT(treeCurrentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)));
    connect(ui.treeWidgetEntities, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)),
            this, SLOT(treeItemDoubleClicked(QTreeWidgetItem*, int)));
}

void PriorEditorController::reconnect()
{
    ARMARX_TRACE;
    clearObjects();
    connected = databasePrx->reconnect(ui.editHost->text().toStdString(), settings_mongoUser, settings_mongoPass);
    refreshControls();

    if (!connected)
    {
        QMessageBox msgBox;
        msgBox.setText("Error connecting to MongoDB! \nCheck if mongod is running on " + ui.editHost->text());
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }

    refreshCollectionList();
}

void PriorEditorController::setCurrentCollection(const std::string& collNS)
{
    ARMARX_TRACE;
    ui.cbCollection->setCurrentIndex(ui.cbCollection->findText(QString::fromStdString(collNS)));
}

void PriorEditorController::collectionChanged()
{
    ARMARX_TRACE;
    const std::string collName = ui.cbCollection->currentText().toStdString();

    if (!collName.empty())
    {
        CollectionInterfacePrx coll = databasePrx->requestCollection(collName);
        classesSegmentPrx->setWriteCollection(coll);
        refetchData();
    }
}

void PriorEditorController::addCollection()
{
    ARMARX_TRACE;
    const NameList dbNames = databasePrx->getDBNames();
    collCreateDialog->setDatabases(dbNames);
    int result = collCreateDialog->exec();

    if (result == QDialog::Accepted)
    {
        const std::string collectionNS = collCreateDialog->getDBName() + "." + collCreateDialog->getCollectionName();
        ui.cbCollection->addItem(QString::fromStdString(collectionNS));
        setCurrentCollection(collectionNS);
    }
}

void PriorEditorController::refetchData()
{
    ARMARX_TRACE;
    const std::string collName = ui.cbCollection->currentText().toStdString();
    ARMARX_INFO << "Refetching data from DB from collection '" << collName << "'";
    std::unique_lock lock(mutexEntities);
    clearObjects();

    auto collection = databasePrx->requestCollection(collName);
    auto ids = collection->findAllIds();
    ARMARX_INFO << "Found " << ids.size() << " entities";

    //    classesSegmentPrx->addReadCollection(collName);
    for (const auto& id  : ids)
    {
        EntityBasePtr entity = classesSegmentPrx->getEntityById(id);

        if (!entity)
        {
            ARMARX_WARNING << "Could not find entity with id " << id << " in collection " << collName;
        }

        const ObjectClassPtr objClass = ObjectClassPtr::dynamicCast(entity);

        if (objClass)
        {
            updateObject(objClass);
        }
        else
        {
            ARMARX_WARNING << "Failed to cast to ObjectClass: " << entity->getName() << ", " << entity->getId() << ", " << entity->ice_id();
        }
    }


    ui.treeWidgetEntities->sortByColumn(0, Qt::AscendingOrder);

    emit objectsLoaded();
}

void PriorEditorController::refreshCollectionList()
{
    ARMARX_TRACE;
    ui.cbCollection->clear();

    if (!connected)
    {
        return;
    }

    auto activeReadCollectionNames = memoryPrx->getObjectClassesSegment()->getReadCollectionsNS();
    const NameList dbNames = databasePrx->getDBNames();
    int firstActiveCollection = -1;

    for (std::string const& dbName : dbNames)
    {
        const NameList collNames = databasePrx->getCollectionNames(dbName);

        for (std::string const& collName : collNames)
        {
            std::string fullCollName = dbName + "." + collName;
            if (memoryPrx->isPriorCollection(fullCollName))
            {
                if (std::find(activeReadCollectionNames.begin(), activeReadCollectionNames.end(), fullCollName) != activeReadCollectionNames.end())
                {
                    ui.cbCollection->addItem(QString::fromStdString(fullCollName));

                    if (firstActiveCollection == -1)
                    {
                        firstActiveCollection = ui.cbCollection->count() - 1;
                    }
                }
                else
                {
                    ui.cbCollection->addItem(QString::fromStdString(collName + "(not selected in PriorKnowledge config)"));
                    const QStandardItemModel* model = qobject_cast<const QStandardItemModel*>(ui.cbCollection->model());
                    ARMARX_CHECK_EXPRESSION(model);
                    QStandardItem* item = model->item(model->rowCount() - 1);
                    ARMARX_CHECK_EXPRESSION(item);

                    item->setFlags(item->flags() & ~(Qt::ItemIsSelectable | Qt::ItemIsEnabled)
                                  );
                    // visually disable by greying out - works only if combobox has been painted already and palette returns the wanted color
                    item->setData(ui.cbCollection->palette().color(QPalette::Disabled, QPalette::Text)
                                  , // clear item data in order to use default color
                                  Qt::TextColorRole);
                }
            }
        }
    }

    if (ui.cbCollection->currentIndex() >= 0)
    {
        const QStandardItemModel* model = qobject_cast<const QStandardItemModel*>(ui.cbCollection->model());
        ARMARX_CHECK_EXPRESSION(model);
        QStandardItem* item = model->item(ui.cbCollection->currentIndex());
        ARMARX_CHECK_EXPRESSION(item);

        if (!(item->flags() & Qt::ItemIsEnabled))
        {
            ui.cbCollection->setCurrentIndex(firstActiveCollection);
        }
    }
    else
    {
        ui.cbCollection->setCurrentIndex(firstActiveCollection);
    }

}

void PriorEditorController::addObjectClass()
{
    ARMARX_TRACE;
    doEditClass(true);
}

void PriorEditorController::editObjectClass()
{
    ARMARX_TRACE;
    doEditClass(false);
}

void PriorEditorController::doEditClass(bool isNew)
{
    ARMARX_TRACE;
    ARMARX_INFO << "Opening edit dialog...";

    // init object class
    const std::string clsId = getSelectedClassId();
    ObjectClassPtr objectClass;

    if (isNew)
    {
        objectClass = new ObjectClass();
        objectClass->addWrapper(new SimoxObjectWrapper(fileManager));
        objectClass->addWrapper(new ObjectRecognitionWrapper());
        objectClass->addWrapper(new TexturedRecognitionWrapper(fileManager));
        objectClass->addWrapper(new SegmentableRecognitionWrapper(fileManager));
        objectClass->addWrapper(new HandMarkerBallWrapper(fileManager));
        objectClass->addWrapper(new ArMarkerWrapper(fileManager));
    }
    else
    {
        objectClass = currentObjectClasses[clsId];
    }


    // create dialog
    ObjectClassEditDialog* classEditDialog = new ObjectClassEditDialog(robotStateComponent);

    if (!isNew)
    {
        // do not allow changeing of existing classes names
        classEditDialog->setClassNameEditable(false);
        classEditDialog->updateGui(objectClass);
    }

    int result = classEditDialog->exec();

    if (result == QDialog::Accepted)
    {
        ARMARX_INFO << "Saving changes to object class...";
        clearVisualization();

        classEditDialog->updateEntity(objectClass, getFilesDBName());

        // update memory segment
        if (isNew)
        {
            const std::string id = classesSegmentPrx->addEntity(objectClass);
            objectClass->setId(id);
            currentObjectClasses[id] = objectClass;
        }
        else
        {
            classesSegmentPrx->updateEntity(objectClass->getId(), objectClass);
        }

        updateObjectInTree(objectClass);
        classEditDialog->updateGui(objectClass);
    }

    delete classEditDialog;
}

void PriorEditorController::removeObjectClass()
{
    ARMARX_TRACE;
    const std::string clsName = getSelectedClassName();
    const std::string clsId = getSelectedClassId();

    if (clsId.empty())
    {
        return;
    }

    QMessageBox msgBox;
    msgBox.setText("Delete object class " + QString(clsName.c_str()) + "?");
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Yes)
    {
        ObjectEntryMap::iterator it = currentObjectClasses.find(clsId);

        if (it != currentObjectClasses.end())
        {
            it->second->getWrapper<SimoxObjectWrapper>()->clear();
            currentObjectClasses.erase(it);
        }

        classesSegmentPrx->removeEntity(clsId);
        QTreeWidgetItem* selItem = ui.treeWidgetEntities->currentItem();

        if (selItem)
        {
            delete ui.treeWidgetEntities->takeTopLevelItem(ui.treeWidgetEntities->indexOfTopLevelItem(selItem));
        }

        clearVisualization();
    }
}

std::string PriorEditorController::getSelectedClassId()
{
    ARMARX_TRACE;
    QTreeWidgetItem* selItem = ui.treeWidgetEntities->currentItem();

    if (selItem && selItem->type() == TreeItemType::eItemObject)
    {
        return selItem->text(1).toStdString();
    }
    else
    {
        return "";
    }
}

std::string PriorEditorController::getSelectedClassName()
{
    ARMARX_TRACE;
    QTreeWidgetItem* selItem = ui.treeWidgetEntities->currentItem();

    if (selItem && selItem->type() == TreeItemType::eItemObject)
    {
        return selItem->text(0).toStdString();
    }
    else
    {
        return "";
    }
}

void PriorEditorController::refreshControls()
{
    ARMARX_TRACE;
    ui.lblConnected->setText(QString::fromStdString(connected ? "Connected" : "Disconnected"));
    ui.lblConnected->setStyleSheet(QString::fromStdString(connected ? "color: rgb(0, 170, 0);" : "color: rgb(170, 0, 0);"));

    ui.btnAddCollection->setEnabled(connected);
    ui.btnRefetch->setEnabled(connected);

    QTreeWidgetItem* cur = ui.treeWidgetEntities->currentItem();
    bool clsSelected = connected && (cur && cur->type() == TreeItemType::eItemObject);
    ui.btnAdd->setEnabled(connected);
    ui.btnEdit->setEnabled(clsSelected);
    ui.btnRemove->setEnabled(clsSelected);
    ui.btnShowVisu->setEnabled(clsSelected);
    ui.btnShowColl->setEnabled(clsSelected);
}

void PriorEditorController::treeCurrentItemChanged(QTreeWidgetItem* cur, QTreeWidgetItem* prev)
{
    ARMARX_TRACE;
    refreshControls();
}

void PriorEditorController::treeItemDoubleClicked(QTreeWidgetItem* item, int column)
{
    ARMARX_TRACE;
    if (item->type() == TreeItemType::eItemObject)
    {
        ARMARX_INFO << " Selected object: " << item->text(1).toStdString();
        showObjectClassModel(item->text(1).toStdString(), SceneObject::Full);
    }
}

std::string PriorEditorController::getFilesDBName()
{
    ARMARX_TRACE;
    const std::string ns = classesSegmentPrx->getWriteCollectionNS();
    const size_t found = ns.find_first_of('.');
    return (found != std::string::npos) ? ns.substr(0, found) : "";
}

void PriorEditorController::updateObjectInTree(ObjectClassPtr obj)
{
    ARMARX_TRACE;
    QString objName(obj->getName().c_str());
    QString objId(obj->getId().c_str());

    QList<QTreeWidgetItem*> oldItems = ui.treeWidgetEntities->findItems(objId, Qt::MatchFixedString, 1);

    QTreeWidgetItem* objItem = (oldItems.size() == 0) ?
                               new QTreeWidgetItem(ui.treeWidgetEntities, TreeItemType::eItemObject) : oldItems.at(0);
    objItem->setText(0, objName);
    objItem->setText(1, objId);

    // clear object node
    /*    QList<QTreeWidgetItem *> oldAttrs = objItem->takeChildren();
        while(!oldAttrs.empty())
            delete oldAttrs.takeFirst();

        QBrush evenRow(QColor(217, 255, 226)), oddRow(Qt::white);
        int i = 0;
        memoryx::NameList attrNames = obj->getAttributeNames();
        for (memoryx::NameList::const_iterator it = attrNames.begin(); it != attrNames.end(); ++it)
        {
    //        QList<QTreeWidgetItem *> oldAttrs = objItem->findItems(objId, Qt::MatchFixedString, 0);
            QTreeWidgetItem *attrItem = new QTreeWidgetItem(objItem, TreeItemType::eItemAttr);
            armarx::VariantPtr attrValue = obj->getAttributeMean(*it);
            QString attrName(it->c_str());
            QString attrValStr(attrValue->getOutputValueOnly().c_str());
            QBrush& bgBrush = (i % 2 == 0) ? evenRow : oddRow;
            attrItem->setText(0, attrName);
            attrItem->setText(1, attrValStr);
            attrItem->setBackground(0, bgBrush);
            attrItem->setBackground(1, bgBrush);
            ++i;
        }*/
}

void PriorEditorController::updateObject(const memoryx::ObjectClassPtr objClass, bool force)
{
    ARMARX_TRACE;
    if (!objClass)
    {
        return;
    }

    std::unique_lock lock(mutexEntities);

    const std::string id = objClass->getId();

    if (force || currentObjectClasses.find(id) == currentObjectClasses.end())
    {
        // build new entry
        ARMARX_INFO << "Building new entry for object class " << objClass->getName();
        objClass->addWrapper(new SimoxObjectWrapper(fileManager));
        objClass->addWrapper(new ObjectRecognitionWrapper());
        objClass->addWrapper(new TexturedRecognitionWrapper(fileManager));
        objClass->addWrapper(new SegmentableRecognitionWrapper(fileManager));
        objClass->addWrapper(new HandMarkerBallWrapper(fileManager));
        objClass->addWrapper(new ArMarkerWrapper(fileManager));

        currentObjectClasses[id] = objClass;
    }

    updateObjectInTree(objClass);
}

void PriorEditorController::showObjectClassModel(const std::string& objectId, SceneObject::VisualizationType visuType)
{
    ARMARX_TRACE;
    std::unique_lock lock(mutexEntities);
    ObjectEntryMap::iterator it = currentObjectClasses.find(objectId);

    if (it == currentObjectClasses.end())
    {
        ARMARX_TRACE;
        return;
    }

    // no rotation/translation
    Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
    SimoxObjectWrapperPtr simoxWrapper = it->second->getWrapper<SimoxObjectWrapper>();

    ARMARX_TRACE;
    auto mo = simoxWrapper->getManipulationObject();

    ARMARX_TRACE;
    mo->setGlobalPose(m);
    mo->showCoordinateSystem(true);

    clearVisualization();
    VisualizationNodePtr visNode;

    if (visuType == SceneObject::Full)
    {
        ARMARX_TRACE;
        visNode = simoxWrapper->getVisualization();
    }
    else if (visuType == SceneObject::Collision && simoxWrapper->getCollisionModel())
    {
        ARMARX_TRACE;
        visNode = simoxWrapper->getCollisionModel()->getVisualization();
    }

    if (visu && visNode)
    {
        ARMARX_TRACE;
        visu->addChild(VirtualRobot::CoinVisualizationFactory::getCoinVisualization(visNode));
    }
}

void PriorEditorController::showObjectClassVisu()
{
    ARMARX_TRACE;
    showObjectClassModel(getSelectedClassId(), SceneObject::Full);
}

void PriorEditorController::showObjectClassColl()
{
    ARMARX_TRACE;
    showObjectClassModel(getSelectedClassId(), SceneObject::Collision);
}

void PriorEditorController::clearObjects()
{
    ARMARX_TRACE;
    std::unique_lock lock(mutexEntities);

    // clear storage
    currentObjectClasses.clear();

    // clear SceneTree
    ui.treeWidgetEntities->clear();
}

void PriorEditorController::clearVisualization()
{
    ARMARX_TRACE;
    visu->removeAllChildren();
}
