#pragma once

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include <VirtualRobot/VirtualRobot.h>

#include <optional>


namespace memoryx
{

    using ConstManipulationObjectPtr = VirtualRobot::ManipulationObjectPtr;

    struct ObjectClassWrapper
    {
        ObjectClassPtr classInMemory;
        ConstManipulationObjectPtr manipulationObject;
    };

    class ObjectClassSegmentWrapper
    {
    public:
        void initFromProxy(memoryx::PriorKnowledgeInterfacePrx const& priorKnowledge, const std::vector<std::string>& datasets);

        std::optional<ObjectClassWrapper> getClass(std::string const& className) const;

    private:
        std::map<std::string, ObjectClassWrapper> classToWrapper;
    };


}
