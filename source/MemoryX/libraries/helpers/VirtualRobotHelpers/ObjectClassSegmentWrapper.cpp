#include "ObjectClassSegmentWrapper.h"


#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/GridFileManager.h>

#include <VirtualRobot/ManipulationObject.h>
#include <SimoxUtility/algorithm/string.h>


namespace memoryx
{

    void ObjectClassSegmentWrapper::initFromProxy(PriorKnowledgeInterfacePrx const& priorKnowledge, const std::vector<std::string>& datasets)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(priorKnowledge);

        memoryx::PersistentObjectClassSegmentBasePrx objectClassesSegment = priorKnowledge->getObjectClassesSegment();
        memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(priorKnowledge->getCommonStorage()));

        memoryx::EntityBaseList classEntities = objectClassesSegment->getAllEntities();
        for (auto& classEntity : classEntities)
        {
            ARMARX_TRACE;
            ARMARX_CHECK_NOT_NULL(classEntity);
            memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(classEntity);
            if (objectClass)
            {
                std::string objectClassName = objectClass->getName();
                bool load = datasets.empty();
                for (const auto& d : datasets)
                {
                    if (simox::alg::count(objectClassName, "/") == 0) // we always load the legacy stuff
                    {
                        load = true;
                        break;
                    }
                    if (simox::alg::starts_with(objectClassName, d + "/"))
                    {
                        load = true;
                        break;
                    }

                }

                if (load)
                {
                    ARMARX_INFO << "Getting files for object class '" << objectClassName << "'";
                    memoryx::EntityWrappers::SimoxObjectWrapperPtr sw = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
                    VirtualRobot::ManipulationObjectPtr manipulationObject = sw->getManipulationObject();

                    ObjectClassWrapper classData = {objectClass, manipulationObject};
                    classToWrapper.emplace(objectClassName, classData);
                }
            }
        }
    }

    std::optional<ObjectClassWrapper> ObjectClassSegmentWrapper::getClass(std::string const& className) const
    {
        auto found = classToWrapper.find(className);
        if (found == classToWrapper.end())
        {
            return std::nullopt;
        }
        else
        {
            return found->second;
        }
    }

}
