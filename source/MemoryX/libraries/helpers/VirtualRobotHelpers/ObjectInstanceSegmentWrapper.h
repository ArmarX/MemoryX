#pragma once

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectClassSegmentWrapper.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <VirtualRobot/VirtualRobot.h>

#include <string>
#include <vector>
#include <map>

namespace memoryx
{
    struct ObjectInstanceWrapper
    {
        memoryx::ObjectInstancePtr instanceInMemory;
        VirtualRobot::ManipulationObjectPtr manipulationObject;
    };

    /**
     * @brief Allows access to the objects in the working memory in form of Simox SceneObjects.
     */
    class ObjectInstanceSegmentWrapper
    {
    public:
        void initFromProxies(memoryx::PriorKnowledgeInterfacePrx const& priorKnowledge,
                             memoryx::WorkingMemoryInterfacePrx const& workingMemory,
                             armarx::RobotStateComponentInterfacePrx const& robotStateComponent);

        std::vector<ObjectInstanceWrapper> queryObjects(std::set<std::string> const& ids = {});

        ObjectClassSegmentWrapper objectClassSegment;

    private:
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        armarx::SharedRobotInterfacePrx sharedRobot;
        VirtualRobot::RobotPtr localRobot;
        memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesSegment;

    };

}
