/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Helpers
* @author     Kai Welke ( welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// MemoryX
#include <MemoryX/core/entity/AbstractEntityWrapper.h>
#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/libraries/updater/ObjectLocalization/LocalizationQuery.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

// IVT
#include <Structs/ObjectDefinitions.h>

// Eigen
#include <Eigen/Eigen>

namespace memoryx::EntityWrappers
{
    // maybe at type template to AbstractEntityWrapper to check for entity
    class ObjectRecognitionWrapper : public AbstractEntityWrapper
    {
    public:
        std::string getRecognitionMethod() const;
        void setRecognitionMethod(const std::string& recognitionMethod);

        std::string getDefaultMotionModel() const;
        void setDefaultMotionModel(const std::string& defaultMotionModel);

        LocalizationQueryList getLocalizationQueries();
        void addLocalizationQuery(const LocalizationQueryPtr& query);
        void removeLocalizationQuery(const std::string& queryName);
        void updateLocalizationQuery(const std::string& queryName, const LocalizationQueryPtr& query);
        Ice::ObjectPtr ice_clone() const override;
    };

    using ObjectRecognitionWrapperPtr = IceInternal::Handle<ObjectRecognitionWrapper>;





    /**
     * TexturedRecognitionWrapper offers a simplified access to the data of an object class or instance
     * related to IVT TextureRecognition
     */
    class TexturedRecognitionWrapper : public AbstractFileEntityWrapper
    {
    public:
        /**
         * Constructs new TexturedRecognitionWrapper.
         *
         * @param gfm        GridFileManager to store/load files
         *
         */
        TexturedRecognitionWrapper(const GridFileManagerPtr& gfm);

        std::string getFeatureFile() const;
        void setFeatureFile(const std::string& featureFileName, const std::string& filesDBName);
        Ice::ObjectPtr ice_clone() const override;
    };

    using TexturedRecognitionWrapperPtr = IceInternal::Handle<TexturedRecognitionWrapper>;


    /**
     * SegmentableRecognitionWrapper offers a simplified access to the data of an object class or instance
     * related to IVT SegmentableRecognition
     */
    class SegmentableRecognitionWrapper : public AbstractFileEntityWrapper
    {
    public:
        /**
         * Constructs new SegmentableRecognitionWrapper.
         *
         * @param gfm        GridFileManager to store/load files
         *
         */
        SegmentableRecognitionWrapper(const GridFileManagerPtr& gfm);

        std::string getDataFiles() const;
        void setDataFiles(const std::string& dataPath, const std::string& filesDBName);

        ObjectColor getObjectColor() const;
        void setObjectColor(const ObjectColor& color);
        Ice::ObjectPtr ice_clone() const override;
    };

    using SegmentableRecognitionWrapperPtr = IceInternal::Handle<SegmentableRecognitionWrapper>;



    /**
     * HandMarkerBallWrapper offers a simplified access to the necessary information for localization of the
     * hand markers of Armar III
     */
    class HandMarkerBallWrapper : public AbstractFileEntityWrapper
    {
    public:
        /**
         * Constructs new HandMarkerBallWrapper.
         *
         * @param gfm        GridFileManager to store/load files
         *
         */
        HandMarkerBallWrapper(const GridFileManagerPtr& gfm);

        ObjectColor getObjectColor() const;
        void setObjectColor(const ObjectColor& color);
        Ice::ObjectPtr ice_clone() const override;
    };

    using HandMarkerBallWrapperPtr = IceInternal::Handle<HandMarkerBallWrapper>;



    /**
     * ArMarkerWrapper offers a simplified access to the necessary information for localization of AR markers
     */
    class ArMarkerWrapper : public AbstractFileEntityWrapper
    {
    public:
        /**
         * Constructs new ArMarkerWrapper.
         *
         * @param gfm        GridFileManager to store/load files
         *
         */
        ArMarkerWrapper(const GridFileManagerPtr& gfm);

        std::vector<int> getArMarkerIDs() const;
        void setArMarkerIDs(const std::vector<int>& newMarkerIDs);

        std::vector<float> getArMarkerSizes() const;
        void setArMarkerSizes(const std::vector<float>& newMarkerSizes);

        std::vector<Eigen::Vector3f> getTransformationToCenterTranslations() const;
        void setTransformationToCenterTranslations(const std::vector<Eigen::Vector3f>& newTranslations);

        std::vector<Eigen::Vector3f> getTransformationToCenterRotationsRPY() const;
        void setTransformationToCenterRotationsRPY(const std::vector<Eigen::Vector3f>& newRotations);

        std::vector<Eigen::Matrix4f> getTransformationsToCenter() const;

        Ice::ObjectPtr ice_clone() const override;
    };

    using ArMarkerWrapperPtr = IceInternal::Handle<ArMarkerWrapper>;



    /**
     * PointCloudLocalizerWrapper offers a simplified access to the necessary information for localization using colored pointclouds
     */
    class PointCloudLocalizerWrapper : public AbstractFileEntityWrapper
    {
    public:
        /**
         * Constructs new PointCloudLocalizerWrapper.
         *
         * @param gfm        GridFileManager to store/load files
         *
         */
        PointCloudLocalizerWrapper(const GridFileManagerPtr& gfm);

        std::string getPointCloudFileName() const;
        void setPointCloudFileName(const std::string& fileName, const std::string& filesDBName);

        void getExpectedMatchingDistance(float& expectedMatchDistance, float& mismatchDistance);
        void setExpectedMatchingDistance(const float expectedMatchDistance, const float mismatchDistance);

        Ice::ObjectPtr ice_clone() const override;
    };

    using PointCloudLocalizerWrapperPtr = IceInternal::Handle<PointCloudLocalizerWrapper>;


}
