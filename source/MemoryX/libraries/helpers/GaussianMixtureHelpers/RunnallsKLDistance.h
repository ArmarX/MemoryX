/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::GaussianMixtureHelpers
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2013 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "GMMDistance.h"

#include <ArmarXCore/core/system/ImportExport.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>

#include <Eigen/Dense>

#include <string>

namespace memoryx
{

    /**
    * @class RunnallsKLDistance
    * @ingroup CommonPlacesLearner
    */
    class ARMARXCORE_IMPORT_EXPORT RunnallsKLDistance: public GMMDistance
    {
    public:
        /**
         * Creates a new RunnallsKLDistance
         */
        RunnallsKLDistance(): GMMDistance()
        {
        }

        /**
         *
         *
         */
        float getDistance(const GaussianMixtureDistributionBasePtr& gmm1, const GaussianMixtureDistributionBasePtr& gmm2) override
        {
            return -1;  // NYI
        }

        float getDistance(const GaussianMixtureComponent& comp1, const GaussianMixtureComponent& comp2) override
        {
            NormalDistributionPtr gaussian1 = NormalDistributionPtr::dynamicCast(comp1.gaussian);
            NormalDistributionPtr gaussian2 = NormalDistributionPtr::dynamicCast(comp2.gaussian);

            const float w1 = comp1.weight;
            const float w2 = comp2.weight;
            const float k =  1. / (w1 + w2);
            const Eigen::VectorXf x1 = gaussian1->toEigenMean();
            const Eigen::VectorXf x2 = gaussian2->toEigenMean();
            const Eigen::MatrixXf p1 = gaussian1->toEigenCovariance();
            const Eigen::MatrixXf p2 = gaussian2->toEigenCovariance();
            const Eigen::VectorXf d =  x1 - x2;

            const Eigen::MatrixXf p12 = k * (w1 * p1 + w2 * p2 + k * w1 * w2 * d * d.transpose());

            const float result = 0.5 * ((w1 + w2) * logf(p12.determinant()) -
                                        w1 * logf(p1.determinant()) - w2 * logf(p2.determinant()));

            return result;
        }

    };
}

