armarx_set_target("GaussianMixtureHelpers")

set(LIB_NAME MemoryXGaussianMixtureHelpers)
set(LIBS     ArmarXCoreObservers MemoryXCore)

set(LIB_FILES
                GMMReducer.cpp
                WestGMMReducer.cpp
                RunnallsGMMReducer.cpp
                WilliamsGMMReducer.cpp
            )

set(LIB_HEADERS
                GMMDistance.h
                ISDDistance.h
                MahalanobisDistance.h
                RunnallsKLDistance.h

                GMMReducer.h
                WestGMMReducer.h
                RunnallsGMMReducer.h
                WilliamsGMMReducer.h
              )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
