/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::GaussianMixtureHelpers
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2013 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/core/entity/ProbabilityMeasures.h>

#include <ArmarXCore/core/system/ImportExport.h>

#include <memory>
#include <string>

namespace memoryx
{

    /**
    * @class GMMDistance
    * @ingroup CommonPlacesLearner
    */
    class ARMARXCORE_IMPORT_EXPORT GMMDistance
    {
    public:
        /**
         * Creates a new GMMDistance
         */
        GMMDistance()
        {
        }

        /**
         *
         *
         */
        virtual float getDistance(const GaussianMixtureDistributionBasePtr& gmm1, const GaussianMixtureDistributionBasePtr& gmm2) = 0;

        virtual float getDistance(const GaussianMixtureComponent& comp1, const GaussianMixtureComponent& comp2) = 0;

    protected:

    };

    using GMMDistancePtr = std::shared_ptr<GMMDistance>;

}

