// *****************************************************************
// Filename:    Gaussian.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        27.05.2012
// *****************************************************************
#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include <math.h>
#include <vector>

#include <Eigen/Eigen>

class InvalidDimensionException : public std::exception
{
    const char* what() const noexcept override
    {
        return "Invalid dimension";
    }
};

class GaussianNotInitializedException : public std::exception
{
    const char* what() const noexcept override
    {
        return "Gaussian not initialized";
    }
};

class CovarianceNotSymmetricException : public std::exception
{
    const char* what() const noexcept override
    {
        return "Covariance not symmetric";
    }
};

// *****************************************************************
// declaration of Gaussian
// *****************************************************************
class Gaussian
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    // required types
    using value_type = Eigen::VectorXd;
    using covariance_type = Eigen::MatrixXd;
    using samples_type = Eigen::MatrixXd;

    // construction
    Gaussian();
    Gaussian(int dimension);
    Gaussian(const Gaussian& prototype);
    Gaussian(const value_type& mean, const covariance_type& covariance);

    // readouts
    double mahalanobis(const value_type& point);
    double evaluate(const value_type& point);
    value_type drawSample();
    value_type drawSampleDiagonalCovariance();

    // content manipulation
    void generateFromSamples(const samples_type& samples);

    void set(const Gaussian& prototype);
    void set(const value_type& mean, const covariance_type& cov);
    void setMean(const value_type& mean);
    void setCovariance(const covariance_type& cov);

    // getters
    const covariance_type& getCovariance() const
    {
        return cov;
    }
    const value_type& getMean() const
    {
        return mean;
    }
    int getDimensions() const
    {
        return dimension;
    }

    // opertor
    Gaussian& operator= (const Gaussian& prototype)
    {
        if (this != &prototype)
        {
            this->dimension = prototype.dimension;

            this->cov = prototype.cov;
            this->mean = prototype.mean;
        }

        return *this;
    }

    friend std::ostream& operator<<(std::ostream& stream, const Gaussian& rhs)
    {
        stream << "mean: " << std::endl << rhs.mean << std::endl;
        stream << "cov: " << std::endl << rhs.cov << std::endl;
        return stream;
    }

private:
    void isSymmetric(const covariance_type& matrix);

    // direct access
    value_type  mean;
    covariance_type cov;

    int dimension;
};

