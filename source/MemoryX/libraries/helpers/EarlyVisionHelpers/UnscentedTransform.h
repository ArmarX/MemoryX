// *****************************************************************
// Filename:    UnscentedTransform.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Anthropomatics (IFA),
//              Humanoids and Intelligence Systems Lab,
//              Karlsruhe Institute of Technology. All rights reserved.
// Author:      Kai Welke
// Date:        20.01.2010
// *****************************************************************
#pragma once

#include "Gaussian.h"
#include <Eigen/Eigen>

// *****************************************************************
// definition of class UnscenterTransform
// *****************************************************************
class UnscentedTransform
{
public:
    // construction / destruction
    UnscentedTransform(float fAlpha = 0.1, float fKappa = 0.0f, float fBeta = 2.0f);

    // retrieve sigmapoints in column order, if not diagonal has to be positive definite with eigenvalues >> 0
    Eigen::MatrixXd getSigmaPoints(const Gaussian& gaussian);

    // reassemble gaussian from sigmapoints
    Gaussian extractGaussian(Eigen::MatrixXd processedSigmaPoints);

private:
    Eigen::MatrixXd squareRoot(Eigen::MatrixXd input);

    float       m_fAlpha;
    float       m_fKappa;
    float       m_fBeta;

    Eigen::VectorXd m_weights_m;
    Eigen::VectorXd m_weights_c;
};

