/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Observers
* @author     Kai Welke (welke at kit dot edu), David Schiebener (david dot schiebener at kit dot edu)
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

// memoryx types
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/updater/ObjectLocalization/LocalizationJob.h>
#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

// std
#include <string>
#include <map>
#include <set>

namespace armarx::VariantType
{
    const armarx::VariantTypeId LocalizationQuery = armarx::Variant::addTypeName("::armarx::LocalizationQueryBase");
}

namespace memoryx
{
    class LocalizationQuery;
    using LocalizationQueryPtr = IceInternal::Handle<LocalizationQuery>;

    /**
     * @brief The LocalizationQuery class is used to create LocalizationJob instances and provide an interface to query if the jobs have finished running
     */
    class LocalizationQuery :
        virtual public LocalizationQueryBase
    {
        friend class LocalizationJob;
        friend class ObjectLocalizationMemoryUpdater;
        friend class armarx::GenericFactory<LocalizationQueryBase, LocalizationQuery>;

    public:
        LocalizationQuery(const std::string& queryName, const std::string& objectClassName, int cycleTimeMS, int priority);

        /**
         * @brief getFinished indicates if there are localization jobs in the queue which have not finished yet
         * @return true if all localization jobs have finished running, false otherwise
         */
        bool getFinished();

    public:
        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }

        // inherited from variantdataclass
        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            LocalizationQueryPtr query = new LocalizationQuery(queryName, className, cycleTimeMS, priority);
            query->jobsFinished = jobsFinished;
            return query;
        }

        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            return "";
        }

        int getType(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            return armarx::VariantType::LocalizationQuery;
        }

        bool validate(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        void serialize(const ::armarx::ObjectSerializerBasePtr&, const Ice::Current& = Ice::emptyCurrent) const override {}
        void deserialize(const ::armarx::ObjectSerializerBasePtr&, const Ice::Current& = Ice::emptyCurrent) override {}

    private:
        LocalizationQuery() {}

        std::vector<LocalizationJobPtr> createJobs(const memoryx::ObjectClassMemorySegmentBasePtr& objectClassSegment);
        void setJobFinished(std::string recognitionMethod);
        ObjectClassPtr getLocalizableObjectClass(const EntityBasePtr& entity);

        std::mutex jobsMutex;
    };
}

