/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MotionModelRobotHand.h"

#include <MemoryX/core/entity/ProbabilityMeasures.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace memoryx
{
    MotionModelRobotHand::MotionModelRobotHand(armarx::RobotStateComponentInterfacePrx robotStateProxy, std::string handNodeName) : AbstractMotionModel(robotStateProxy)
    {
        this->handNodeName = handNodeName;
        syncedRobot = robotStateProxy->getSynchronizedRobot();
        robotName = syncedRobot->getName();
    }


    armarx::LinkedPosePtr MotionModelRobotHand::getPredictedPoseInternal()
    {
        ARMARX_DEBUG_S << "robot name of last pose: " << poseAtLastLocalization->referenceRobot->getName();
        ARMARX_DEBUG_S << "robot name of internal proxy: " << robotStateProxy->getRobotName();
        //armarx::PosePtr oldHandNodePose = armarx::PosePtr::dynamicCast(poseAtLastLocalization->referenceRobot->getRobotNode(handNodeName)->getPoseInRootFrame());
        armarx::PosePtr oldHandNodePose = armarx::PosePtr::dynamicCast(poseAtLastLocalization->referenceRobot->getRobotNode(handNodeName)->getGlobalPose());

        armarx::SharedRobotInterfacePrx currentRobotSnapshot = robotStateProxy->getRobotSnapshot(robotName);
        //armarx::PosePtr newHandNodePose = armarx::PosePtr::dynamicCast(currentRobotSnapshot->getRobotNode(handNodeName)->getPoseInRootFrame());
        armarx::PosePtr newHandNodePose = armarx::PosePtr::dynamicCast(currentRobotSnapshot->getRobotNode(handNodeName)->getGlobalPose());

        Eigen::Matrix4f transformationOldToNewInRootFrame = newHandNodePose->toEigen() * oldHandNodePose->toEigen().inverse();
        Eigen::Vector3f kinPosDelta = newHandNodePose->toEigen().block<3, 1>(0, 3) - oldHandNodePose->toEigen().block<3, 1>(0, 3);
        //Eigen::Matrix4f lastLocalization = armarx::LinkedPosePtr::dynamicCast(poseAtLastLocalization)->toRootEigen(poseAtLastLocalization->referenceRobot);
        Eigen::Matrix4f lastLocalization = armarx::LinkedPosePtr::dynamicCast(poseAtLastLocalization)->toGlobalEigen(poseAtLastLocalization->referenceRobot);
        Eigen::Matrix4f predictedPose = Eigen::Matrix4f::Identity();

        predictedPose.block<3, 3>(0, 0) = transformationOldToNewInRootFrame.block<3, 3>(0, 0) * lastLocalization.block<3, 3>(0, 0);
        predictedPose.block<3, 1>(0, 3) = kinPosDelta + lastLocalization.block<3, 1>(0, 3);

        if (poseAtLastLocalization->frame.empty())
        {
            ARMARX_INFO_S << "poseAtLastLocalization->frame is empty!";
        }

        //armarx::LinkedPosePtr ret = new armarx::LinkedPose(predictedPose, poseAtLastLocalization->frame, currentRobotSnapshot);
        //armarx::LinkedPosePtr ret = new armarx::LinkedPose(predictedPose, currentRobotSnapshot->getRootNode()->getName(), currentRobotSnapshot);
        //ret->changeToGlobal();
        armarx::LinkedPosePtr ret = new armarx::LinkedPose(predictedPose, armarx::GlobalFrame, currentRobotSnapshot);
        return ret;
    }



    MultivariateNormalDistributionBasePtr MotionModelRobotHand::getUncertaintyInternal()
    {
        if (uncertaintyAtLastLocalization)
        {
            armarx::PosePtr oldHandNodePose = armarx::PosePtr::dynamicCast(poseAtLastLocalization->referenceRobot->getRobotNode(handNodeName)->getPoseInRootFrame());
            armarx::PosePtr newHandNodePose = armarx::PosePtr::dynamicCast(syncedRobot->getRobotNode(handNodeName)->getPoseInRootFrame());

            // additional uncertainty is 0.07 * the distance that the hand moved since the last localization
            float dist = (oldHandNodePose->toEigen().block<3, 1>(0, 3) - newHandNodePose->toEigen().block<3, 1>(0, 3)).norm();
            dist *= 0.07;
            Eigen::Matrix3f additionalUncertainty = dist * dist * Eigen::Matrix3f::Identity();

            Eigen::Matrix3f oldUncertainty = MultivariateNormalDistributionPtr::dynamicCast(uncertaintyAtLastLocalization)->toEigenCovariance();
            Eigen::Matrix3f newUncertainty = oldUncertainty + additionalUncertainty;

            return new MultivariateNormalDistribution(armarx::Vector3Ptr::dynamicCast(poseAtLastLocalization->position)->toEigen(), newUncertainty);
        }
        else
        {
            return NULL;
        }
    }

    void MotionModelRobotHand::setPoseAtLastLocalisation(const armarx::LinkedPoseBasePtr& poseAtLastLocalization, const armarx::PoseBasePtr& globalRobotPoseAtLastLocalization, const MultivariateNormalDistributionBasePtr& uncertaintyAtLastLocalization, const Ice::Current& c)
    {
        std::unique_lock lock(motionPredictionLock);
        AbstractMotionModel::setPoseAtLastLocalisation(poseAtLastLocalization, globalRobotPoseAtLastLocalization,
                uncertaintyAtLastLocalization, c);
        ARMARX_DEBUG_S << "MotionModelRobotHand::setPoseAtLastLocalisation" << this->poseAtLastLocalization->referenceRobot->getName();
        if (robotStateProxy->getRobotName() != this->poseAtLastLocalization->referenceRobot->getName())
        {
            ARMARX_DEBUG_S << "Setting robot state proxy from " << this->poseAtLastLocalization->agent << " to " << robotName;
            this->poseAtLastLocalization = new armarx::LinkedPose(armarx::LinkedPosePtr::dynamicCast(this->poseAtLastLocalization)->toEigen(), armarx::GlobalFrame, robotStateProxy->getRobotSnapshot(robotName));
        }
    }

}
