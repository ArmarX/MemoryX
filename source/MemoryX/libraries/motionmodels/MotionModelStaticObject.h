/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MotionModelStaticObject
* @author     David Schiebener ( Schiebener at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include "AbstractMotionModel.h"

#include <MemoryX/interface/components/WorkingMemoryInterface.h>


namespace memoryx
{
    class MotionModelStaticObject :
        virtual public AbstractMotionModel,
        virtual public MotionModelStaticObjectBase
    {
    public:

        MotionModelStaticObject(armarx::RobotStateComponentInterfacePrx robotStateProxy);

        AbstractMotionModel::EMotionModelType getMotionModelType() override
        {
            return AbstractMotionModel::eMotionModelStaticObject;
        }

    protected:

        armarx::LinkedPosePtr getPredictedPoseInternal() override;
        MultivariateNormalDistributionBasePtr getUncertaintyInternal() override;

        void getOldAndNewRobotPose(Eigen::Matrix4f& oldRobotPose, Eigen::Matrix4f& newRobotPose);

        // for the object factory
        template <class IceBaseClass, class DerivedClass>
        friend class armarx::GenericFactory;
        MotionModelStaticObject() { }
    };
    using MotionModelStaticObjectPtr = IceInternal::Handle<MotionModelStaticObject>;
}

