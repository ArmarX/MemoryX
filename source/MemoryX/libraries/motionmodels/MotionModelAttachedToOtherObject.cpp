/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MotionModelAttachedToOtherObject.h"


#include <RobotAPI/libraries/core/Pose.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>

#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>
#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/logging/Logging.h>


namespace memoryx
{
    MotionModelAttachedToOtherObject::MotionModelAttachedToOtherObject(armarx::RobotStateComponentInterfacePrx robotStateProxy,
            armarx::ChannelRefPtr channelRefToObjectToWhichThisIsAttached) : AbstractMotionModel(robotStateProxy)
    {
        this->channelRefToObjectToWhichThisIsAttached = channelRefToObjectToWhichThisIsAttached;
        channelRefIsValid = true;

        armarx::FramedPositionPtr positionOfAtObj = channelRefToObjectToWhichThisIsAttached->get<armarx::FramedPosition>("position");
        armarx::FramedOrientationPtr orientationOfAtObj = channelRefToObjectToWhichThisIsAttached->get<armarx::FramedOrientation>("orientation");
        armarx::LinkedPosePtr poseOfAtObj = new armarx::LinkedPose(orientationOfAtObj->toEigen(), positionOfAtObj->toEigen(), positionOfAtObj->frame, robotStateProxy->getSynchronizedRobot());
        //        ARMARX_INFO_S << VAROUT(*positionOfAtObj);
        //        ARMARX_INFO_S << VAROUT(*poseOfAtObj);
        Eigen::Matrix4f globalPoseOfAtObj = poseOfAtObj->toGlobal()->toEigen();
        //        ARMARX_INFO_S << VAROUT(globalPoseOfAtObj);
        initialGlobalPoseOfObjectToWhichThisIsAttached = new armarx::Variant();
        armarx::VariantPtr::dynamicCast(initialGlobalPoseOfObjectToWhichThisIsAttached)->setClass(new armarx::MatrixFloat(globalPoseOfAtObj));
        globalPoseOfObjectToWhichThisIsAttachedAtLocalizationStart = new armarx::Variant();
        armarx::VariantPtr::dynamicCast(globalPoseOfObjectToWhichThisIsAttachedAtLocalizationStart)->setClass(new armarx::MatrixFloat(globalPoseOfAtObj));
    }



    armarx::LinkedPosePtr MotionModelAttachedToOtherObject::getPredictedPoseInternal()
    {
        if (channelRefIsValid)
        {
            try
            {
                //        ARMARX_INFO_S << VAROUT(*poseAtLastLocalization);
                armarx::LinkedPosePtr oldPoseGlobal = armarx::LinkedPosePtr::dynamicCast(poseAtLastLocalization)->toGlobal();

                armarx::SharedRobotInterfacePrx currentRobotSnapshot = robotStateProxy->getRobotSnapshot(robotStateProxy->getSynchronizedRobot()->getName());

                // determine transformation of the object to which this one is attached in global coordinates
                armarx::FramedPositionPtr positionOfAtObj = armarx::ChannelRefPtr::dynamicCast(channelRefToObjectToWhichThisIsAttached)->get<armarx::FramedPosition>("position");
                armarx::FramedOrientationPtr orientationOfAtObj = armarx::ChannelRefPtr::dynamicCast(channelRefToObjectToWhichThisIsAttached)->get<armarx::FramedOrientation>("orientation");
                armarx::LinkedPosePtr poseOfAtObj = new armarx::LinkedPose(orientationOfAtObj->toEigen(), positionOfAtObj->toEigen(), positionOfAtObj->frame, currentRobotSnapshot);
                armarx::LinkedPosePtr globalPoseOfAtObj = poseOfAtObj->toGlobal();
                Eigen::Matrix4f initPoseOfAtObj = armarx::VariantPtr::dynamicCast(initialGlobalPoseOfObjectToWhichThisIsAttached)->getClass<armarx::MatrixFloat>()->toEigen();
                Eigen::Matrix4f trafoOfObjectToWhichThisIsAttached = globalPoseOfAtObj->toEigen() * initPoseOfAtObj.inverse();

                //        ARMARX_VERBOSE_S << "new pose: " << globalPoseOfAtObj->toEigen() * initPoseOfAtObj.inverse() * oldPoseGlobal->toEigen();

                Eigen::Matrix4f predictedPose = trafoOfObjectToWhichThisIsAttached * oldPoseGlobal->toEigen();

                armarx::LinkedPosePtr ret = new armarx::LinkedPose(predictedPose, armarx::GlobalFrame, currentRobotSnapshot);

                return ret;
            }
            catch (...)
            {
                armarx::handleExceptions();
                channelRefIsValid = false;
                ARMARX_ERROR_S << "The ChannelRef of the object to which this is attached is not valid (anymore). Did you release that object?";
            }
        }
        return armarx::LinkedPosePtr::dynamicCast(poseAtLastLocalization);
    }



    void MotionModelAttachedToOtherObject::setPoseAtLastLocalisation(const armarx::LinkedPoseBasePtr& poseAtLastLocalization, const armarx::PoseBasePtr& globalRobotPoseAtLastLocalization, const MultivariateNormalDistributionBasePtr& uncertaintyAtLastLocalization, const Ice::Current& c)
    {
        if (channelRefIsValid)
        {
            try
            {

                std::unique_lock lock(motionPredictionLock);
                //        ARMARX_WARNING_S << "setpose attached: " << VAROUT(*poseAtLastLocalization);
                this->poseAtLastLocalization = poseAtLastLocalization;

                // this one is probably unnecessary now
                this->globalRobotPoseAtLastLocalization = globalRobotPoseAtLastLocalization;

                initialGlobalPoseOfObjectToWhichThisIsAttached = armarx::VariantPtr::dynamicCast(globalPoseOfObjectToWhichThisIsAttachedAtLocalizationStart)->clone();

                if (uncertaintyAtLastLocalization)
                {
                    this->uncertaintyAtLastLocalization = uncertaintyAtLastLocalization;
                }
            }
            catch (...)
            {
                armarx::handleExceptions();
                channelRefIsValid = false;
                ARMARX_ERROR_S << "The ChannelRef of the object to which this is attached is not valid (anymore). Did you release that object?";
            }
        }
    }



    memoryx::MultivariateNormalDistributionBasePtr MotionModelAttachedToOtherObject::getUncertaintyInternal()
    {
        if (uncertaintyAtLastLocalization)
        {
            return uncertaintyAtLastLocalization;
        }
        else
        {
            return NULL;
        }
    }


    void MotionModelAttachedToOtherObject::savePredictedPoseAtStartOfCurrentLocalization(const Ice::Current& c)
    {
        AbstractMotionModel::savePredictedPoseAtStartOfCurrentLocalization(c);

        armarx::FramedPositionPtr positionOfAtObj = armarx::ChannelRefPtr::dynamicCast(channelRefToObjectToWhichThisIsAttached)->get<armarx::FramedPosition>("position");
        armarx::FramedOrientationPtr orientationOfAtObj = armarx::ChannelRefPtr::dynamicCast(channelRefToObjectToWhichThisIsAttached)->get<armarx::FramedOrientation>("orientation");
        armarx::LinkedPosePtr poseOfAtObj = new armarx::LinkedPose(orientationOfAtObj->toEigen(), positionOfAtObj->toEigen(), positionOfAtObj->frame, poseAtLastLocalization->referenceRobot);
        armarx::LinkedPosePtr globalPoseOfAtObj = poseOfAtObj->toGlobal();
        armarx::VariantPtr::dynamicCast(globalPoseOfObjectToWhichThisIsAttachedAtLocalizationStart)->setClass(new armarx::MatrixFloat(globalPoseOfAtObj->toEigen()));
    }

}
