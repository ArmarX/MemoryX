/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MotionModelStaticObject.h"

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace memoryx
{
    MotionModelStaticObject::MotionModelStaticObject(armarx::RobotStateComponentInterfacePrx robotStateProxy) : AbstractMotionModel(robotStateProxy)
    {
    }



    // predicted pose that is constant in the world coordinate frame
    armarx::LinkedPosePtr MotionModelStaticObject::getPredictedPoseInternal()
    {
        //        // get old and new transformation to robot root frame
        //        armarx::PosePtr oldGlobalPoseOfFrame = armarx::PosePtr::dynamicCast(poseAtLastLocalization->referenceRobot->getRobotNode(poseAtLastLocalization->frame)->getPoseInRootFrame());
        //        Eigen::Matrix4f oldTrafoToRootCS = oldGlobalPoseOfFrame->toEigen();

        //        armarx::SharedRobotInterfacePrx currentRobotSnapshot = robotStateProxy->getRobotSnapshot(robotStateProxy->getSynchronizedRobot()->getName());
        //        armarx::PosePtr newGlobalPoseOfFrame = armarx::PosePtr::dynamicCast(currentRobotSnapshot->getRobotNode(poseAtLastLocalization->frame)->getPoseInRootFrame());
        //        Eigen::Matrix4f newTrafoToRootCS = newGlobalPoseOfFrame->toEigen();

        //        // get robot pose in world coordinates, if available
        //        Eigen::Matrix4f oldRobotPose, newRobotPose;
        //        getOldAndNewRobotPose(oldRobotPose, newRobotPose);

        //        Eigen::Matrix4f predictedPose = newTrafoToRootCS.inverse()*newRobotPose.inverse()*oldRobotPose*oldTrafoToRootCS*armarx::LinkedPosePtr::dynamicCast(poseAtLastLocalization)->toEigen();

        //        armarx::LinkedPosePtr ret = new armarx::LinkedPose(predictedPose, poseAtLastLocalization->frame, currentRobotSnapshot);
        //        return ret;

        armarx::LinkedPosePtr pose = armarx::LinkedPosePtr::dynamicCast(poseAtLastLocalization);
        std::string oldFrameName = pose->frame;
        armarx::FramedPosePtr newPose = pose->toGlobal();
        //        newPose->changeFrame(currentRobotSnapshot, poseAtLastLocalization->frame);

        armarx::SharedRobotInterfacePrx currentRobotSnapshot = robotStateProxy->getRobotSnapshot(robotStateProxy->getRobotName());
        armarx::LinkedPosePtr ret = new armarx::LinkedPose(newPose->toEigen(), newPose->frame, currentRobotSnapshot);
        ret->changeFrame(oldFrameName);
        return ret;
    }



    memoryx::MultivariateNormalDistributionBasePtr MotionModelStaticObject::getUncertaintyInternal()
    {
        MultivariateNormalDistributionPtr result = NULL;

        ARMARX_DEBUG_S << "MotionModelStaticObject::getUncertaintyInternal()";

        if (uncertaintyAtLastLocalization)
        {
            // uncertainty is increased proportionally to robot motion in the world
            Eigen::Matrix4f oldRobotPose, newRobotPose;
            getOldAndNewRobotPose(oldRobotPose, newRobotPose);
            const float positionDistance = (oldRobotPose.block<3, 1>(0, 3) - newRobotPose.block<3, 1>(0, 3)).norm();
            const float positionChangeUncertaintyFactor = 0.1 * positionDistance;
            float additionalUncertaintyFactor = positionChangeUncertaintyFactor;
            ARMARX_DEBUG_S << "positionChangeUncertaintyFactor: " << positionChangeUncertaintyFactor;

            // uncertainty also increases slowly over time
            IceUtil::Time timeAtLastLocalization = armarx::TimestampVariantPtr::dynamicCast(timeOfLastSuccessfulLocalization)->toTime();
            IceUtil::Time timeSinceLastLocalization = IceUtil::Time::now() - timeAtLastLocalization;
            const float timeUncertaintyFactor = 0.0005 * abs(timeSinceLastLocalization.toMilliSeconds()); // 0.5mm per second
            additionalUncertaintyFactor += timeUncertaintyFactor;
            ARMARX_DEBUG_S << "timeUncertaintyFactor: " << timeUncertaintyFactor;

            Eigen::Matrix3f additionalUncertainty = additionalUncertaintyFactor * additionalUncertaintyFactor * Eigen::Matrix3f::Identity();
            Eigen::Matrix3f oldUncertainty = MultivariateNormalDistributionPtr::dynamicCast(uncertaintyAtLastLocalization)->toEigenCovariance();
            Eigen::Matrix3f newUncertainty = oldUncertainty + additionalUncertainty;

            ARMARX_DEBUG_S << "oldUncertainty: " << oldUncertainty;
            ARMARX_DEBUG_S << "additionalUncertainty: " << additionalUncertainty;
            ARMARX_DEBUG_S << "newUncertainty: " << newUncertainty;

            result = new MultivariateNormalDistribution(armarx::Vector3Ptr::dynamicCast(poseAtLastLocalization->position)->toEigen(), newUncertainty);
        }

        return result;
    }



    void MotionModelStaticObject::getOldAndNewRobotPose(Eigen::Matrix4f& oldRobotPose, Eigen::Matrix4f& newRobotPose)
    {
        oldRobotPose = Eigen::Matrix4f::Identity();
        newRobotPose = Eigen::Matrix4f::Identity();

        if (globalRobotPoseAtLastLocalization)
        {
            oldRobotPose = armarx::PosePtr::dynamicCast(globalRobotPoseAtLastLocalization)->toEigen();
            newRobotPose = armarx::PosePtr::dynamicCast(robotStateProxy->getSynchronizedRobot()->getGlobalPose())->toEigen();
        }
    }

}
