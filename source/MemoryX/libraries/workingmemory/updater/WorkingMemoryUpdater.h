/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.h>
#include <MemoryX/libraries/workingmemory/AbstractWorkingMemory.h>
#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <Ice/Handle.h>

namespace memoryx
{
    class WorkingMemoryUpdater;
    using WorkingMemoryUpdaterPtr = IceInternal::Handle<WorkingMemoryUpdater>;

    struct WorkingMemoryUpdaterProperties : armarx::ComponentPropertyDefinitions
    {
        WorkingMemoryUpdaterProperties(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
        }
    };

    /**
     * @class WorkingMemoryUpdater
     * @brief Base class for all working memory updater processes
     * @ingroup WorkingMemory
     *
     * WorkingMemoryUpdater can be registered to a memoryx::WorkingMemory. They have access to all WorkingMemorySegments and can thus read and manipulate data
     * within these segments without the use of Ice communication. Further, they can have an Ice interface, which can be retrieved via the getUpdater method
     * form the WorkingMemory.
     */
    class ARMARXCORE_IMPORT_EXPORT WorkingMemoryUpdater :
        virtual public armarx::Component,
        virtual public WorkingMemoryUpdaterBase
    {
        friend class AbstractWorkingMemory;

    public:
        /**
        * Constructs a new working memory updater method
        *
        * @param name of the fusion method
        */
        WorkingMemoryUpdater() { }
        ~WorkingMemoryUpdater() override { }

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new WorkingMemoryUpdaterProperties(getConfigIdentifier()));
        }

    protected:
        void onInitComponent() override {}
        void onConnectComponent() override {}
        void onDisconnectComponent() override {}
        void onExitComponent() override {}

        template<class T>
        IceInternal::Handle<T> getSegment(std::string segmentName)
        {
            AbstractMemorySegmentPtr segment = workingMemory->getSegmentPtr(segmentName);
            return IceInternal::Handle<T>::dynamicCast(segment);
        }

        AbstractWorkingMemoryPtr getWorkingMemory()
        {
            return workingMemory;
        }

    private:
        // only accessible from friend WorkingMemory
        void setWorkingMemory(AbstractWorkingMemoryPtr workingMemory)
        {
            this->workingMemory = workingMemory;
        }

        AbstractWorkingMemoryPtr  workingMemory;
    };
}

