/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::PriorKnowledgeEditor
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PriorKnowledgeImporter.h"

#include <ArmarXCore/util/json/JSONObject.h>

#include <MemoryX/core/MongoDBRef.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/Relation.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <filesystem>
#include <fstream>

namespace memoryx
{
    void PriorKnowledgeImporter::onInitComponent()
    {
        usingProxy("PriorKnowledge");
        usingProxy("CommonStorage");
    }

    void PriorKnowledgeImporter::onConnectComponent()
    {
        ARMARX_LOG << "Starting PriorKnowledgeImporter";

        dataBasePrx = getProxy<CommonStorageInterfacePrx>("CommonStorage");
        memoryPrx = getProxy<PriorKnowledgeInterfacePrx>("PriorKnowledge");
        classesSegmentPrx = memoryPrx->getObjectClassesSegment();

        const Ice::CommunicatorPtr ic = getIceManager()->getCommunicator();

        dbSerializer = new MongoSerializer(ic, true);
        fileManager.reset(new GridFileManager(dataBasePrx));

        filesDBName = getProperty<std::string>("FilesDbName").getValue();
        filesDir = getProperty<std::string>("FilesDirectory").getValue();

        const std::string taskName = getProperty<std::string>("TaskName").getValue();

        if (taskName == "ImportFiles")
        {
            importFiles();
        }
        else if (taskName == "AddParents")
        {
            addParents();
        }
        else if (taskName == "ConvertFormat")
        {
            convertToNewFormat();
        }
        else if (taskName == "AddRelations")
        {
            addRelations();
        }
        else
        {
            ARMARX_ERROR << "Unknown TaskName: " << taskName;
        }
    }


    void PriorKnowledgeImporter::importFiles()
    {
        ARMARX_INFO << "Importing object models from " << filesDir;

        namespace fs = std::filesystem;
        fs::path ivDir(filesDir);
        fs::directory_iterator end_iter;

        int fileCount = 0;

        if (!fs::exists(ivDir) || !fs::is_directory(ivDir))
        {
            return;
        }

        for (fs::directory_iterator dir_iter(ivDir) ; dir_iter != end_iter ; ++dir_iter)
        {
            if (fs::is_regular_file(dir_iter->status()) && (dir_iter->path().extension() == ".iv" || dir_iter->path().extension() == ".wrl"))
            {
                fs::path ivFile = dir_iter->path();
                // use filename without .iv as className
                importObjectClass(ivFile.string(), ivFile.stem().string());
                fileCount++;
            }
        }

        ARMARX_INFO << "Import complete, # of loaded files: " << fileCount;
    }


    void PriorKnowledgeImporter::importObjectClass(const std::string& ivFile, const std::string& className)
    {
        ObjectClassPtr newClass = new ObjectClass();
        newClass->setName(className);

        EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = newClass->addWrapper(new EntityWrappers::SimoxObjectWrapper(fileManager));
        ARMARX_INFO << "Store file: " << ivFile;
        simoxWrapper->setAndStoreModelIVFiles(ivFile, ivFile, filesDBName);

        EntityBasePtr ent = classesSegmentPrx->getEntityByName(newClass->getName());

        if (ent)
        {
            ARMARX_IMPORTANT << "Updating existing entity " << ent->getName() << "(Id: " << ent->getId() << ")";
            classesSegmentPrx->updateEntity(ent->getId(), ent);
        }
        else
        {
            classesSegmentPrx->addEntity(newClass);
        }
    }


    void PriorKnowledgeImporter::addParents()
    {
        EntityIdList ids = classesSegmentPrx->getAllEntityIds();

        for (EntityIdList::const_iterator it = ids.begin(); it != ids.end(); ++it)
        {
            ObjectClassBasePtr cls = classesSegmentPrx->getObjectClassById(*it);

            if (cls)
            {
                cls->clearParentClasses();
                cls->addParentClass("Entity");

                if (cls->getName() == "Cup")
                {
                    cls->addParentClass("Dishes");
                }
                else if (cls->getName() != "coffeefilters")
                {
                    cls->addParentClass("Food");
                }

                classesSegmentPrx->updateEntity(cls->getId(), cls);
            }
        }
    }

    void PriorKnowledgeImporter::convertToNewFormat()
    {
        const std::string collNS = classesSegmentPrx->getWriteCollectionNS();
        CollectionInterfacePrx coll = dataBasePrx->requestCollection(collNS);
        EntityIdList ids = classesSegmentPrx->getAllEntityIds();

        int countConverted = 0, countFailed = 0;

        for (EntityIdList::const_iterator it = ids.begin(); it != ids.end(); ++it)
        {
            ObjectClassPtr cls = new ObjectClass();
            DBStorableData dbEntity = coll->findByMongoId(*it);
            dbSerializer->deserialize(dbEntity, cls);

            if (cls)
            {
                classesSegmentPrx->updateEntity(cls->getId(), cls);
                ++countConverted;
            }
            else
            {
                ARMARX_ERROR << "Error deserializing entity: id =" << *it << ", json: " << dbEntity.JSON << armarx::flush;
                ++countFailed;
            }
        }

        ARMARX_INFO << "Conversion completed. " << armarx::flush;
        ARMARX_INFO << "Entities converted: " << countConverted << ", failed: " << countFailed << armarx::flush;
    }

    void PriorKnowledgeImporter::addRelations()
    {
        PersistentRelationSegmentBasePrx relSegmentPrx = memoryPrx->getRelationsSegment();
        RelationPtr rel = new Relation("isOn");
        EntityRefBasePtr tableRef = classesSegmentPrx->getEntityRefByName("TableWithRolls");
        EntityRefBasePtr cupRef = classesSegmentPrx->getEntityRefByName("Cup");

        if (tableRef && cupRef)
        {
            rel->setEntities({cupRef, tableRef});
            rel->setProb(0.75);
            relSegmentPrx->addEntity(rel);
            ARMARX_INFO << "Relation saved: " << rel << armarx::flush;
        }
        else
        {
            ARMARX_ERROR << "Failed to create relation: object class(es) not found!" << armarx::flush;
        }
    }


}
