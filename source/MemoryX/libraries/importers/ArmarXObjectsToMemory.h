/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::ArmarXObjectsImporter
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <filesystem>
#include <unordered_set>

#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>

#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/core/GridFileManager.h>


namespace memoryx
{


    class ArmarXObjectsToMemory
    {
    public:

        ArmarXObjectsToMemory();


        void importObjectsToPriorKnowledge(
            std::vector<armarx::ObjectInfo> const& objectInfos,
            memoryx::GridFileManagerPtr const& gridFileManager,
            memoryx::PersistentObjectClassSegmentBasePrx const& objectClassesSegment,
            std::string const& mongoDbName) const;


        void importScenesAsSnapshots(
            std::filesystem::path const& scenesDirectory,
            memoryx::LongtermMemoryInterfacePrx const& longtermMemory,
            memoryx::WorkingMemoryInterfacePrx const& workingMemory,
            memoryx::PersistentObjectClassSegmentBasePrx const& objectClassesSegment,
            const std::unordered_set<std::string>& scenes) const;

        bool
        importSceneAsSnapshot(
            const std::string& snapshotName,
            std::filesystem::path const& sceneJsonFile,
            memoryx::LongtermMemoryInterfacePrx const& longtermMemory,
            memoryx::WorkingMemoryInterfacePrx const& workingMemory,
            memoryx::PersistentObjectClassSegmentBasePrx const& objectClassesSegment,
            memoryx::ObjectInstanceMemorySegmentBasePrx const& objectInstancesSegment
            ) const;


        bool
        importObjectToWorkingMemory(
            const armarx::objects::SceneObject& object,
            memoryx::PersistentObjectClassSegmentBasePrx const& objectClassesSegment,
            memoryx::ObjectInstanceMemorySegmentBasePrx const& objectInstancesSegment,
            std::map<std::string, std::unordered_set<std::string>>& instancesPerClass
            ) const;

        bool
        importObjectToWorkingMemory(
            std::string const& className,
            Eigen::Vector3f const& position,
            Eigen::Quaternionf const& quat,
            memoryx::PersistentObjectClassSegmentBasePrx const& objectClassesSegment,
            memoryx::ObjectInstanceMemorySegmentBasePrx const& objectInstancesSegment,
            std::map<std::string, std::unordered_set<std::string>>& instancesPerClass
            ) const;


        /// If true, don't actually change anything.
        bool dryRun = false;

    };


}
