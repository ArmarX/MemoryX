/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::SimoxSceneImporter
* @author     (vahrenkamp at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/core/entity/EntityAttribute.h>
#include <MemoryX/core/MongoSerializer.h>

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/XML/SceneIO.h>

#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoFile.h>
#include <Inventor/nodes/SoTexture2.h>
#include <Inventor/nodes/SoTexture3.h>


namespace memoryx
{
    /**
         * @brief The SimoxSceneImporterPropertyDefinitions class defines the command line properties of the SimoxSceneImporter
         * @see ComponentPropertyDefinitions
         */
    class SimoxSceneImporterPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SimoxSceneImporterPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("SceneFile", "Name of Simox XML file")
            .setCaseInsensitive(true);
            defineRequiredProperty<std::string>("SnapshotName", "Name of snapshot (=MongoDB collection) to load objects to")
            .setCaseInsensitive(true);
            defineRequiredProperty<std::string>("FilesDbName", "Name of files database")
            .setCaseInsensitive(true);
        }
    };

    /**
     * @brief The SimoxSceneImporter class generates a LongtermMemory snapshot of a scene specified in the Simox Scene format
     *
     *
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT SimoxSceneImporter :
        virtual public armarx::Component
    {
    public:
        SimoxSceneImporter();

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "SimoxSceneImporter";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new SimoxSceneImporterPropertyDefinitions(
                           getConfigIdentifier()));
        }

    private:
        /**
         * @brief loadSceneFromXML parses \p fileName and creates a VirtualRobot::Scene object
         * @param fileName Simox Scene description file (XML)
         * @return VirtualRobot::Scene object if the contents of \p fileName was valid
         */
        VirtualRobot::ScenePtr loadSceneFromXML(const std::string& fileName);

        /**
         * @brief importScene imports the given \p scene into the robot's working memory and creates a snapshot afterwards.
         *
         * Extracts all visualization files from \p scene and copies them into a temporary directory.
         * Afterwards, the file references are updated to the new temporary location and the manipulation object files are written into the temporary directory.
         * As a last step, those manipulation objects are turned into ObjectInstance instances and saved to the ObjectInstance segment of the WorkingMemory
         * which is then used to create a snapshot of the scene.
         *
         * @param scene the scene to import into the memory
         * @return true on success, false upon failure
         */
        bool importScene(VirtualRobot::ScenePtr scene);

        /**
         * @brief getAbsoluteVisualizationFilenames collects all filenames required to visualize \p obstacle and stores them in \p allFilenames
         * @param obstacle collect the visualization files for this object
         * @param absoluteFilenames absolute paths to the visualization files are stored in this vector
         */
        void getAbsoluteVisualizationFilenames(VirtualRobot::ObstaclePtr obstacle, std::vector<std::string>& absoluteFilenames);

        /**
         * @brief createTempFiles copy all \p files to \p tmpPath. \p origPath is used to calculate relative paths. The mapping is stored in \p newFilenames
         * @param files vector of files to copy to \p tmpPath
         * @param tmpPath the directory where the files should be copied to
         * @param origPath substract this path from the original filename so the relative paths are the same within \p tmpPath
         * @param newFilenameMapping store the mapping of old/new filename in this map
         */
        void copyFilesToTempDir(const std::vector<std::string>& files, const std::string& tmpPath, const std::string& origPath, std::map<std::string, std::string>& newFilenameMapping);

        /**
         * @brief setNewVisualizationFilenames update visualization file references of \p obstacle. The mapping is provided by \p newFilenames
         * @param obstacle the object to update the filenames on
         * @param newFilenames mapping of old to new filenames
         */
        void setNewVisualizationFilenames(VirtualRobot::ObstaclePtr obstacle, std::map<std::string, std::string>& newFilenames);

        /**
         * @brief saveObstacleAsManipulationObject serialize \p object into \p xmlfile storing all obstacles as ManipulationObject tags
         * @param object the Obstacle instance to save to \p xmlFile
         * @param xmlFile serialize \p object into this file
         * @return true if writing was successful, false otherwise
         */
        bool saveObstacleAsManipulationObject(VirtualRobot::ObstaclePtr object, const std::string& xmlFile, const std::string& tmpPath);

        /**
         * @brief createTempScene
         *
         * Loop over all Obstacle and ManipulationObject tags
         *  collect the visualization files (including itself)
         *  collect the collisionModel visualization files (including itself)
         *
         *  copy all visualization files to the temp directory
         *
         *  replace old visualization files with paths to the temp directory
         *
         *  serialize all objects to files within the temp directory
         *
         *  write out a scene.xml file and return its contents as ScenePtr
         *
         *
         * @param tmpPath
         * @param scene
         * @return
         */
        VirtualRobot::ScenePtr createTempScene(const std::string& tmpPath, VirtualRobot::ScenePtr scene);

        /**
         * @brief createObjectInstances creates object instances from \p obstacles, stores them to memory and adds a mapping to \p objectInstances.
         * @param obstacles a vector of Obstacles to add to the working memory
         * @param objectInstances stores a mapping of Obstacles and created ObjectInstance objects
         */
        template <typename T>
        int createObjectInstances(std::vector<T> obstacles, std::map<VirtualRobot::ObstaclePtr, ObjectInstanceBasePtr>& objectInstances);

        /**
         * @brief createInstance create an ObjectInstance instance from the SceneObject \p object
         * @param sceneObject the object to convert
         * @return NULL ObjectInstance if conversion failed, a valid ObjectInstance otherwise
         */
        ObjectInstanceBasePtr createInstance(VirtualRobot::SceneObjectPtr sceneObject);

        /**
         * @brief importObjectsIntoPriorKnowledge adds all objects found in the imported scene as ObjectClass instances to PriorKnowledge
         * @param objectInstances
         */
        void importObjectsIntoPriorKnowledge(std::map<VirtualRobot::ObstaclePtr, ObjectInstanceBasePtr>& objectInstances) ;


        WorkingMemoryInterfacePrx memoryPrx;
        ObjectInstanceMemorySegmentBasePrx objectInstancesMemoryPrx;
        LongtermMemoryInterfacePrx longtermMemoryPrx;
        PriorKnowledgeInterfacePrx priorKnowledgePrx;
        PersistentObjectClassSegmentBasePrx classesSegmentPrx;
        CommonStorageInterfacePrx dataBasePrx;

        MongoSerializerPtr dbSerializer;

        std::string filesDBName;
        std::string sceneFile;
        std::string snapshotName;

        const std::string TEMPDIR;
        const std::string LONGTERM_SNAPSHOT_PREFIX;
    };

}

