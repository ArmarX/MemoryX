/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Oac.h"
#include "OacParameterList.h"
#include "OacPredictionFunction.h"
#include "OacStatisticalMeasure.h"

namespace memoryx
{
    Oac::Oac(const std::string& name, const std::string& id, const std::string& statechartProxyName, const std::string& stateUUID, bool isPlannable) :
        Entity()
    {
        setName(name);
        setId(id);
        setPlannable(isPlannable);

        setParameters(std::vector<std::string>());
        setStatechartProxyName(statechartProxyName);
        setStateUUID(stateUUID);
        setPredictionFunction(OacPredictionFunctionBasePtr(new OacPredictionFunction));
        setStatisticalMeasure(OacStatisticalMeasureBasePtr(new OacStatisticalMeasure));
    }

    std::vector<std::string> Oac::getParameters(const ::Ice::Current&) const
    {
        return OacParameterListPtr::dynamicCast(
                   getAttributeValue("parameters")->getClass<OacParameterListBase>()
               )->getNames();
    }

    void Oac::setParameters(const std::vector<std::string>& parameters, const ::Ice::Current&)
    {
        OacParameterListPtr paramObj = new OacParameterList();
        paramObj->setNames(parameters);
        putAttribute("parameters", paramObj);
    }

    std::string Oac::getStatechartProxyName(const Ice::Current&) const
    {
        return getAttributeValue("statechartProxyName")->getString();
    }

    void Oac::setStatechartProxyName(const std::string& statechartProxyName, const Ice::Current&)
    {
        putAttribute("statechartProxyName", statechartProxyName);
    }

    std::string Oac::getStateUUID(const Ice::Current&) const
    {
        return getAttributeValue("stateUUID")->getString();
    }

    void Oac::setStateUUID(const std::string& stateUUID, const Ice::Current&)
    {
        putAttribute("stateUUID", stateUUID);
    }

    bool Oac::isPlannable(const ::Ice::Current&) const
    {
        return getAttributeValue("plannable")->getBool();
    }

    void Oac::setPlannable(bool isPlannable, const ::Ice::Current&)
    {
        putAttribute("plannable", isPlannable);
    }

    OacPredictionFunctionBasePtr Oac::getPredictionFunction(const ::Ice::Current&) const
    {
        return OacPredictionFunctionBasePtr::dynamicCast(
                   getAttributeValue("prediction")->getClass<OacPredictionFunctionBase>()
               );
    }

    void Oac::setPredictionFunction(const ::memoryx::OacPredictionFunctionBasePtr& prediction, const ::Ice::Current&)
    {
        putAttribute("prediction", prediction);
    }

    OacStatisticalMeasureBasePtr Oac::getStatisticalMeasure(const ::Ice::Current&) const
    {
        return OacStatisticalMeasureBasePtr::dynamicCast(
                   getAttributeValue("measure")->getClass<OacStatisticalMeasureBase>()
               );
    }

    void Oac::setStatisticalMeasure(const ::memoryx::OacStatisticalMeasureBasePtr& measure, const ::Ice::Current&)
    {
        putAttribute("measure", measure);
    }

    Ice::ObjectPtr Oac::ice_clone() const
    {
        return this->clone();
    }

    OacPtr Oac::clone(const Ice::Current& c) const
    {
        OacPtr ret = new Oac(*this);
        //    ret->deepCopy(*this);
        return ret;

    }
}
