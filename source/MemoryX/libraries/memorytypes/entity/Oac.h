/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <MemoryX/core/entity/Entity.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId Oac = armarx::Variant::addTypeName("::memoryx::OacBase");
}

namespace memoryx
{
    class Oac;
    using OacPtr = IceInternal::Handle<Oac>;

    class Oac: public OacBase, public Entity
    {
        friend class OacObjectFactory;

    public:
        /**
        * Constructs a new Oac entity. A name needs to be assigned. The id is optional.
        * The Oac can also be associated with a state via UUID and the statechart this state can be found in
        *
        * @param name friendly name of the OAC
        * @param id optional id of the OAC
        * @param statechartProxyName optional statechart proxy name this OAC is associated with
        * @param stateUUID optional stateUUID this OAC is associated with
        **/
        Oac(const std::string& name = std::string(), const std::string& id = "", const std::string& statechartProxyName = "", const std::string& stateUUID = "", bool isPlannable = true);

        std::vector<std::string> getParameters(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setParameters(const std::vector<std::string>& parameters, const ::Ice::Current& = Ice::emptyCurrent) override;

        std::string getStatechartProxyName(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setStatechartProxyName(const std::string& statechartProxyName, const ::Ice::Current& = Ice::emptyCurrent) override;

        std::string getStateUUID(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setStateUUID(const std::string& stateUUID, const ::Ice::Current& = Ice::emptyCurrent) override;

        bool isPlannable(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setPlannable(bool isPlannable, const ::Ice::Current& = Ice::emptyCurrent) override;

        OacPredictionFunctionBasePtr getPredictionFunction(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setPredictionFunction(const ::memoryx::OacPredictionFunctionBasePtr& prediction, const ::Ice::Current& = Ice::emptyCurrent) override;

        OacStatisticalMeasureBasePtr getStatisticalMeasure(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setStatisticalMeasure(const ::memoryx::OacStatisticalMeasureBasePtr& measure, const ::Ice::Current& = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        OacPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    };
}

