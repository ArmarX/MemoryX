/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ResourceProfileEntity.h"

#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/libraries/memorytypes/variants/StatisticMeasures.h>

namespace memoryx
{
    const std::string ResourceProfileEntity::DURATION = "duration";
    const std::string ResourceProfileEntity::CPU_USAGE = "cpuUsage";
    const std::string ResourceProfileEntity::MEMORY_USAGE = "memoryUsage";


    ResourceProfileEntity::ResourceProfileEntity()
    {
        initializeAttributes();

        // initialize with 0 values
        setDuration(new StatisticMeasures(0, 0, 0));
        setCpuUsage(new StatisticMeasures(0, 0, 0));
        setMemoryUsage(new StatisticMeasures(0, 0, 0));
    }


    ResourceProfileEntity::ResourceProfileEntity(const StatisticMeasuresBasePtr& duration, const StatisticMeasuresBasePtr& cpuUsage, const StatisticMeasuresBasePtr& memoryUsage) :
        Entity()
    {
        initializeAttributes();

        setDuration(duration);
        setCpuUsage(cpuUsage);
        setMemoryUsage(memoryUsage);
    }


    void ResourceProfileEntity::initializeAttributes()
    {
        putAttribute(new EntityAttribute(DURATION));
        putAttribute(new EntityAttribute(CPU_USAGE));
        putAttribute(new EntityAttribute(MEMORY_USAGE));
    }


    ResourceProfileEntity::ResourceProfileEntity(const ResourceProfileEntity& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        ResourceProfileEntityBase(source),
        Entity(source)
    {
    }


    ResourceProfileEntity::~ResourceProfileEntity()
    {
    }


    void ResourceProfileEntity::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }


    Ice::ObjectPtr ResourceProfileEntity::ice_clone() const
    {
        return this->clone();
    }


    ResourceProfileEntityPtr ResourceProfileEntity::clone(const Ice::Current& context) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        return new ResourceProfileEntity(*this);
    }


    StatisticMeasuresBasePtr ResourceProfileEntity::getDuration(const Ice::Current& context) const
    {
        return getAttributeValue(DURATION)->get<StatisticMeasuresBase>();
    }

    void ResourceProfileEntity::setDuration(const StatisticMeasuresBasePtr& duration, const Ice::Current& context)
    {
        putAttribute(DURATION, duration);
    }

    StatisticMeasuresBasePtr ResourceProfileEntity::getCpuUsage(const Ice::Current& context) const
    {
        return getAttributeValue(CPU_USAGE)->get<StatisticMeasuresBase>();
    }

    void ResourceProfileEntity::setCpuUsage(const StatisticMeasuresBasePtr& cpuUsage, const Ice::Current& context)
    {
        putAttribute(CPU_USAGE, cpuUsage);
    }

    StatisticMeasuresBasePtr ResourceProfileEntity::getMemoryUsage(const Ice::Current& context) const
    {
        return getAttributeValue(MEMORY_USAGE)->get<StatisticMeasuresBase>();
    }

    void ResourceProfileEntity::setMemoryUsage(const StatisticMeasuresBasePtr& memoryUsage, const Ice::Current& context)
    {
        putAttribute(MEMORY_USAGE, memoryUsage);
    }
}
