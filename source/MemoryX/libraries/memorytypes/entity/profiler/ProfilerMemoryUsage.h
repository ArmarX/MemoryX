/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Adil Orhan (ubdnw at student dot kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include "ProfilerEntity.h"

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/ProfilerEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{
    class ProfilerMemoryUsage;
    using ProfilerMemoryUsagePtr = IceInternal::Handle<ProfilerMemoryUsage>;

    class ProfilerMemoryUsage :
        virtual public memoryx::ProfilerMemoryUsageBase,
        virtual public memoryx::ProfilerEntity
    {
    public:
        ProfilerMemoryUsage();
        ProfilerMemoryUsage(const armarx::ProfilerProcessMemoryUsage& memoryUsage);
        ProfilerMemoryUsage(const ProfilerMemoryUsage& source);

        ~ProfilerMemoryUsage() override;

        Ice::ObjectPtr ice_clone() const override;
        ProfilerEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        void setProfilerMemoryUsage(const armarx::ProfilerProcessMemoryUsage& memoryUsage);

        Ice::Int getPid(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPid(Ice::Int pid, const Ice::Current& c = Ice::emptyCurrent) override;

        std::string getProcessName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setProcessName(const std::string& processName, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::Int getTimestamp(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setTimestamp(Ice::Int timestamp, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::Int getMemoryUsage(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setMemoryUsage(Ice::Int memoryUsage, const Ice::Current& c = Ice::emptyCurrent) override;


    private:
        void initializeAttributes();
    };
}

