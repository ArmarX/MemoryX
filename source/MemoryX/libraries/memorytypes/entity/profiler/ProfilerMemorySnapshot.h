/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ProfilerEntity.h"

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/ProfilerEntities.h>
#include <MemoryX/core/entity/Entity.h>

#include <string>

namespace memoryx
{

    class ProfilerMemorySnapshot;
    using ProfilerMemorySnapshotPtr = IceInternal::Handle<ProfilerMemorySnapshot>;

    class ProfilerMemorySnapshot :
        virtual public memoryx::ProfilerMemorySnapshotBase,
        virtual public memoryx::ProfilerEntity
    {
    public:
        ProfilerMemorySnapshot();
        ProfilerMemorySnapshot(const std::string& stateName, const Ice::Context& parameters);
        ProfilerMemorySnapshot(const ProfilerMemorySnapshot& source);
        ~ProfilerMemorySnapshot() override;


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        ProfilerMemorySnapshotPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        // interface
        std::string getStateName(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setStateName(const std::string& statename, const Ice::Current& context = Ice::emptyCurrent) override;

        Ice::Context getMemoryParameterMap(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setMemoryParameterMap(const Ice::Context& parameters, const Ice::Current& context = Ice::emptyCurrent) override;

        static const std::string ATTRIBUTE_STATENAME;
        static const std::string ATTRIBUTE_MEMORYPARAMETERS;

    private:
        void output(std::ostream& stream) const;
        void initializeAttributes();

    public:
        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ProfilerMemorySnapshot& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerMemorySnapshotPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerMemorySnapshotBasePtr& rhs)
        {
            stream << ProfilerMemorySnapshotPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

