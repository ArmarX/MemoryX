/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProfilerEvent.h"


namespace memoryx
{

    ProfilerEvent::ProfilerEvent() :
        Entity()
    {
        initializeAttributes();
    }


    ProfilerEvent::ProfilerEvent(const armarx::ProfilerEvent& event)
    {
        initializeAttributes();
        setProfilerEvent(event);
    }


    ProfilerEvent::ProfilerEvent(const ProfilerEvent& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        ProfilerEntityBase(source),
        ProfilerEventBase(source),
        Entity(source),
        ProfilerEntity(source)
    {
    }


    ProfilerEvent::~ProfilerEvent()
    {
    }


    void ProfilerEvent::initializeAttributes()
    {
        putAttribute(new EntityAttribute("pid"));
        putAttribute(new EntityAttribute("executableName"));
        putAttribute(new EntityAttribute("timestamp"));
        putAttribute(new EntityAttribute("timestampUnit"));
        putAttribute(new EntityAttribute("eventName"));
        putAttribute(new EntityAttribute("parentName"));
        putAttribute(new EntityAttribute("functionName"));
    }


    void ProfilerEvent::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }


    Ice::ObjectPtr ProfilerEvent::ice_clone() const
    {
        return this->clone();
    }


    ProfilerEntityPtr ProfilerEvent::clone(const Ice::Current& context) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        ProfilerEntityPtr ret = new ProfilerEvent(*this);
        return ret;
    }

    void ProfilerEvent::setProfilerEvent(const armarx::ProfilerEvent& event)
    {
        setPid(event.processId);
        setExecutableName(event.executableName);
        setTimestamp(event.timestamp);
        setTimestampUnit(event.timestampUnit);
        setEventName(event.eventName);
        setParentName(event.parentName);
        setFunctionName(event.functionName);
    }


    Ice::Int ProfilerEvent::getPid(const Ice::Current& c) const
    {
        return getAttribute("pid")->getValue()->getInt();
    }


    void ProfilerEvent::setPid(Ice::Int pid, const Ice::Current& c)
    {
        getAttribute("pid")->setValue(new armarx::Variant(pid));
    }


    std::string ProfilerEvent::getExecutableName(const Ice::Current& c) const
    {
        return getAttribute("executableName")->getValue()->getString();
    }


    void ProfilerEvent::setExecutableName(const std::string& executableName, const Ice::Current& c)
    {
        getAttribute("executableName")->setValue(new armarx::Variant(executableName));
    }


    Ice::Int ProfilerEvent::getTimestamp(const Ice::Current& c) const
    {
        return getAttribute("timestamp")->getValue()->getInt();
    }


    void ProfilerEvent::setTimestamp(Ice::Int timestamp, const Ice::Current& c)
    {
        getAttribute("timestamp")->setValue(new armarx::Variant(timestamp));
    }


    std::string ProfilerEvent::getTimestampUnit(const Ice::Current& c) const
    {
        return getAttribute("timestampUnit")->getValue()->getString();
    }


    void ProfilerEvent::setTimestampUnit(const std::string& unit, const Ice::Current& c)
    {
        getAttribute("timestampUnit")->setValue(new armarx::Variant(unit));
    }


    std::string ProfilerEvent::getEventName(const Ice::Current& c) const
    {
        return getAttribute("eventName")->getValue()->getString();
    }


    void ProfilerEvent::setEventName(const std::string& eventName, const Ice::Current& c)
    {
        getAttribute("eventName")->setValue(new armarx::Variant(eventName));
        setName(eventName);
    }


    std::string ProfilerEvent::getParentName(const Ice::Current& c) const
    {
        return getAttribute("parentName")->getValue()->getString();
    }


    void ProfilerEvent::setParentName(const std::string& parentName, const Ice::Current& c)
    {
        getAttribute("parentName")->setValue(new armarx::Variant(parentName));
    }


    std::string ProfilerEvent::getFunctionName(const Ice::Current& c) const
    {
        return getAttribute("functionName")->getValue()->getString();
    }


    void ProfilerEvent::setFunctionName(const std::string& functionName, const Ice::Current& c)
    {
        getAttribute("functionName")->setValue(new armarx::Variant(functionName));
    }
}


