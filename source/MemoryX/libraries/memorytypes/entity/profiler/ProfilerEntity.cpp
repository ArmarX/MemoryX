/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProfilerEntity.h"


namespace memoryx
{

    ProfilerEntity::ProfilerEntity() :
        Entity()
    {
        putAttribute(new EntityAttribute("tags"));
    }


    ProfilerEntity::ProfilerEntity(const ProfilerEntity& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        ProfilerEntityBase(source),
        Entity(source)
    {
    }


    ProfilerEntity::~ProfilerEntity()
    {
    }


    void ProfilerEntity::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }


    Ice::ObjectPtr ProfilerEntity::ice_clone() const
    {
        return this->clone();
    }


    ProfilerEntityPtr ProfilerEntity::clone(const Ice::Current& context) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        return new ProfilerEntity(*this);
    }


    void ProfilerEntity::setTags(const Ice::StringSeq& tags, const Ice::Current& context)
    {
        for (const std::string& tag : tags)
        {
            getAttribute("tags")->addValue(new armarx::Variant(tag));
        }
    }


    void ProfilerEntity::addTag(const std::string& tag, const Ice::Current& context)
    {
        getAttribute("tags")->addValue(new armarx::Variant(tag));
    }


    Ice::StringSeq ProfilerEntity::getTags(const Ice::Current& context) const
    {
        memoryx::EntityAttributeBasePtr tags = getAttribute("tags");
        Ice::StringSeq tagStrings;
        tagStrings.reserve(tags->size());

        for (int i = 0; i < tags->size(); i++)
        {
            tagStrings.push_back(tags->getValueAt(i)->getString());
        }

        return tagStrings;
    }
}


