/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/ProfilerEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{
    class ResourceProfileEntity;
    using ResourceProfileEntityPtr = IceInternal::Handle<ResourceProfileEntity>;

    class ResourceProfileEntity :
        virtual public memoryx::ResourceProfileEntityBase,
        virtual public memoryx::Entity
    {
    public:
        ResourceProfileEntity();
        ResourceProfileEntity(const StatisticMeasuresBasePtr& duration, const StatisticMeasuresBasePtr& cpuUsage, const StatisticMeasuresBasePtr& memoryUsage);
        ResourceProfileEntity(const ResourceProfileEntity& source);
        ~ResourceProfileEntity() override;


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        ResourceProfileEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        StatisticMeasuresBasePtr getDuration(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setDuration(const StatisticMeasuresBasePtr& duration, const Ice::Current& c = Ice::emptyCurrent) override;

        StatisticMeasuresBasePtr getCpuUsage(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setCpuUsage(const StatisticMeasuresBasePtr& cpuUsage, const Ice::Current& c = Ice::emptyCurrent) override;

        StatisticMeasuresBasePtr getMemoryUsage(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setMemoryUsage(const StatisticMeasuresBasePtr& memoryUsage, const Ice::Current& c = Ice::emptyCurrent) override;
    private:
        void output(std::ostream& stream) const;
        void initializeAttributes();

        static const std::string DURATION;
        static const std::string CPU_USAGE;
        static const std::string MEMORY_USAGE;

    public:
        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ResourceProfileEntity& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ResourceProfileEntityPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ResourceProfileEntityBasePtr& rhs)
        {
            stream << ResourceProfileEntityPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

