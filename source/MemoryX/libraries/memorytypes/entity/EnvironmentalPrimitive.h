/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Peter Kaiser <peter dot kaiser at kit dot edu>
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/core/entity/EntityRef.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <MemoryX/core/entity/Entity.h>

#include <RobotAPI/libraries/core/Pose.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId EnvironmentalPrimitive = armarx::Variant::addTypeName("::memoryx::EnvironmentalPrimitiveBase");
    const armarx::VariantTypeId PlanePrimitive = armarx::Variant::addTypeName("::memoryx::PlanePrimitiveBase");
    const armarx::VariantTypeId SpherePrimitive = armarx::Variant::addTypeName("::memoryx::SpherePrimitiveBase");
    const armarx::VariantTypeId CylinderPrimitive = armarx::Variant::addTypeName("::memoryx::CylinderPrimitiveBase");
    const armarx::VariantTypeId ConePrimitive = armarx::Variant::addTypeName("::memoryx::ConePrimitiveBase");
    const armarx::VariantTypeId BoxPrimitive = armarx::Variant::addTypeName("::memoryx::BoxPrimitiveBase");
    const armarx::VariantTypeId HigherSemanticStructure = armarx::Variant::addTypeName("::memoryx::HigherSemanticStructureBase");
}

namespace memoryx
{
    class EnvironmentalPrimitive;
    using EnvironmentalPrimitivePtr = IceInternal::Handle<EnvironmentalPrimitive>;

    class EnvironmentalPrimitive:
        virtual public EnvironmentalPrimitiveBase,
        virtual public Entity
    {
    public:
        EnvironmentalPrimitive(const std::string& name = "", const std::string& id = "", const std::string& shape = "");

        std::string getShape(const Ice::Current& c = Ice::emptyCurrent) const override;

        armarx::Vector3BasePtr getOBBDimensions(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setOBBDimensions(const armarx::Vector3BasePtr& dimensions, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::FramedPoseBasePtr getPose(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPose(const armarx::FramedPoseBasePtr& pose, const Ice::Current& c = Ice::emptyCurrent) override;

        PointList getInliers(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setInliers(const PointList&  inliers,  const Ice::Current& c = Ice::emptyCurrent) override;

        PointList getGraspPoints(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setGraspPoints(const PointList&  graspPoints,  const Ice::Current& c = Ice::emptyCurrent) override;

        float getProbability(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setProbability(float probability, const Ice::Current& c = Ice::emptyCurrent) override;

        float getCircularityProbability(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setCircularityProbability(float probability, const Ice::Current& c = Ice::emptyCurrent) override;

        float getLength(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setLength(float length, const Ice::Current& c = Ice::emptyCurrent) override;

        int getLabel(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setLabel(int label, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::TimestampBasePtr getTime(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setTime(const armarx::TimestampBasePtr& time, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::MatrixFloatBasePtr getSampling(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setSampling(const armarx::MatrixFloatBasePtr& sampling, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        EnvironmentalPrimitivePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    protected:
        void setShape(const std::string& shape, const Ice::Current& c = Ice::emptyCurrent) override;
    };


    class PlanePrimitive;
    using PlanePrimitivePtr = IceInternal::Handle<PlanePrimitive>;

    class PlanePrimitive:
        virtual public PlanePrimitiveBase,
        virtual public EnvironmentalPrimitive
    {
    public:
        PlanePrimitive(const std::string& name = "", const std::string& id = "");
        PlanePrimitive(std::vector<float>& coeffs);

        armarx::Vector3BasePtr getPlaneNormal(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPlaneNormal(const armarx::Vector3BasePtr& normal, const Ice::Current& c = Ice::emptyCurrent) override;

        float getPlaneDistance(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPlaneDistance(float distance, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        PlanePrimitivePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;
    };

    class SpherePrimitive;
    using SpherePrimitivePtr = IceInternal::Handle<SpherePrimitive>;

    class SpherePrimitive:
        virtual public SpherePrimitiveBase,
        virtual public EnvironmentalPrimitive
    {
    public:
        SpherePrimitive(const std::string& name = "", const std::string& id = "");
        SpherePrimitive(std::vector<float>& coeffs);

        armarx::Vector3BasePtr getSphereCenter(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setSphereCenter(const armarx::Vector3BasePtr& center, const Ice::Current& c = Ice::emptyCurrent) override;

        float getSphereRadius(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setSphereRadius(float radius, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        SpherePrimitivePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;
    };

    class CylinderPrimitive;
    using CylinderPrimitivePtr = IceInternal::Handle<CylinderPrimitive>;

    class CylinderPrimitive:
        virtual public CylinderPrimitiveBase,
        virtual public EnvironmentalPrimitive
    {
    public:
        CylinderPrimitive(const std::string& name = "", const std::string& id = "");
        CylinderPrimitive(std::vector<float>& coeffs);

        armarx::Vector3BasePtr getCylinderPoint(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setCylinderPoint(const armarx::Vector3BasePtr& point, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::Vector3BasePtr getCylinderAxisDirection(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setCylinderAxisDirection(const armarx::Vector3BasePtr& axis, const Ice::Current& c = Ice::emptyCurrent) override;

        float getCylinderRadius(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setCylinderRadius(float radius, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        CylinderPrimitivePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;
    };


    class ConePrimitive;
    using ConePrimitivePtr = IceInternal::Handle<ConePrimitive>;

    class ConePrimitive:
        virtual public ConePrimitiveBase,
        virtual public EnvironmentalPrimitive
    {

    public:
        ConePrimitive(const std::string& name = "", const std::string& id = "");
        ConePrimitive(std::vector<float>& coeffs);

        armarx::Vector3BasePtr getConeApex(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setConeApex(const armarx::Vector3BasePtr& apex, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::Vector3BasePtr getConeAxisDirection(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setConeAxisDirection(const armarx::Vector3BasePtr& axis, const Ice::Current& c = Ice::emptyCurrent) override;

        float getConeOpeningAngle(const Ice::Current&) const override;
        void setConeOpeningAngle(float angle, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        ConePrimitivePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;
    };


    class BoxPrimitive;
    using BoxPrimitivePtr = IceInternal::Handle<BoxPrimitive>;

    class BoxPrimitive:
        virtual public BoxPrimitiveBase,
        virtual public EnvironmentalPrimitive
    {

    public:
        BoxPrimitive(const std::string& name = "", const std::string& id = "");

        EntityRefList getBoxSides(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setBoxSides(const EntityRefList& sides, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        BoxPrimitivePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    };


    class HigherSemanticStructure;
    using HigherSemanticStructurePtr = IceInternal::Handle<HigherSemanticStructure>;

    class HigherSemanticStructure:
        virtual public HigherSemanticStructureBase,
        virtual public EnvironmentalPrimitive
    {

    public:
        HigherSemanticStructure(const std::string& name = "", const std::string& id = "");

        EntityRefList getPrimitives(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPrimitives(const EntityRefList& primitives, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::StringSeq getLabels(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setLabels(const Ice::StringSeq& labels, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        HigherSemanticStructurePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;
    };



}


