/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu), Kai Welke (welke at kit got edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <ArmarXCore/observers/variant/Variant.h>

namespace armarx
{
    template <class IceBaseClass, class DerivedClass> class GenericFactory;
}

namespace memoryx
{
    class ObjectInstance;
    using ObjectInstancePtr = IceInternal::Handle<ObjectInstance>;

    class AbstractMotionModel;
    using AbstractMotionModelPtr = IceInternal::Handle<AbstractMotionModel>;

    class ObjectInstance:
        public memoryx::ObjectInstanceBase,
        public memoryx::Entity
    {
        template <class IceBaseClass, class DerivedClass>
        friend class armarx::GenericFactory;
    public:

        /**
         * Constructs a new ObjectInstance WorkingMemory entity. A name needs to be assigned. The id is optional.
         *
         * @param name friendly name of the instance
         * @param id optional id of the instance
         **/
        ObjectInstance(const std::string& name, const std::string& id = "");
        ObjectInstance(const ObjectInstance& source);

        /**
         * Retrieve existence certainty for this instance. The existence certainty encodes how likely the scene containes this instance at the given position and orientation.
         * The existence certainty represents the current believe for the instance's existance in the dynamic inference process. Values lie in the interval [0:1], where 1 means
         * perfect knowledge.
         *
         * @return existance certainty as probability
         **/
        float getExistenceCertainty(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set existence certainty for this instance. The existence certainty encodes how likely the scene containes this instance at the given position and orientation.
         * The existence certainty represents the current believe for the instance's existance in the dynamic inference process. Values lie in the interval [0:1], where 1 means
         * perfect knowledge.
         *
         * @param existanceCertainty existance certainty as probability
         **/
        void setExistenceCertainty(float existenceCertainty, const ::Ice::Current& = Ice::emptyCurrent) override;


        int getLocalizationPriority(const ::Ice::Current& = Ice::emptyCurrent) const override;

        void setLocalizationPriority(Ice::Int priority, const ::Ice::Current& = Ice::emptyCurrent) override;



        /**
         * Retrieve attribute which holds instance position.
         *
         * @return attribute holding mean value of position as well as probability distribution
         **/
        EntityAttributeBasePtr getPositionAttribute() const;

        /**
         * Retrieve position of the instance.
         *
         * @return position linked to a named coordinate frame
         **/
        armarx::FramedPositionPtr getPosition() const;

        /**
         * Set position of the instance.
         *
         * @param orientation  position linked to a named coordinate frame
         **/
        void setPosition(const armarx::FramedPositionBasePtr& position, const ::Ice::Current& = Ice::emptyCurrent) override;
        void setLocalizationTimestamp(const IceUtil::Time& timestamp);
        void setLocalizationTimestamp(const armarx::TimestampBasePtr& timestamp);
        bool hasLocalizationTimestamp() const;
        IceUtil::Time getLocalizationTimestamp() const;

        /**
         * Get uncertainty of position estimate.
         *
         * @return 3 dimensional multivariate normal distribution with mean at the position
         **/
        MultivariateNormalDistributionBasePtr getPositionUncertainty(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set uncertainty of position estimate.
         *
         * @param uncertainty 3 dimensional multivariate normal distribution with mean at the position
         **/
        void setPositionUncertainty(const MultivariateNormalDistributionBasePtr& uncertainty, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve attribute which holds instance position.
         *
         * @return attribute holding mean value of position as well as probability distribution
         **/
        EntityAttributeBasePtr getOrientationAttribute() const;

        /**
         * Retrieve orientation of the instance.
         *
         * @return orientation linked to a named coordinate frame
         **/
        armarx::FramedOrientationPtr getOrientation() const;

        /**
         * Set orientation of the instance.
         *
         * @param orientation orientation linked to a named coordinate frame
         **/
        void setOrientation(const armarx::FramedOrientationBasePtr& orientation, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @brief Convenience function to get position and orientation attributes at once
         * @return current pose
         */
        armarx::FramedPosePtr getPose() const;

        /**
         * @brief Convenience function to set position and orientation attributes at once
         * @param newPose New Pose to set
         */
        void setPose(const armarx::FramedPoseBasePtr& newPose);

        /**
         * Retrieve classes and associated class membership probability for this instance
         *
         * @return map of class names and probabilities
         **/
        ClassProbabilityMap getClasses(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set the class attribute of the ObjectInstance. Corresponds to calling clearClasses() and addClass(...).
         *
         * @param className name of the class
         * @param probability probability of class membership
         **/
        void setClass(const std::string& className, float probability, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Adds new class membership to the class attribute
         *
         * @param className name of the class
         * @param probability probability of class membership
         **/
        void addClass(const std::string& className, float probability, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Clear class membership
         **/
        void clearClasses(const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve the class with highest membership probability
         *
         * @return className
         **/
        ::std::string getMostProbableClass(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Retrieve membership probability for a given class
         *
         * @param className
         **/
        float getClassProbability(const ::std::string& className, const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set the motion model for this object
         *
         * @param motionModel The motion model
         **/
        void setMotionModel(AbstractMotionModelPtr motionModel);
        AbstractMotionModelPtr getMotionModel() const;

        // TODO: cloning will probabily also not work without the implementation of a copy constructor
        Ice::ObjectPtr ice_clone() const override;
        ObjectInstancePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    protected:
        // ice interface implementation on Base data types. Replaces in c++ by derived instance getters, setters (see above)
        armarx::FramedPositionBasePtr getPositionBase(const ::Ice::Current& = Ice::emptyCurrent) const override;
        armarx::FramedOrientationBasePtr getOrientationBase(const ::Ice::Current& = Ice::emptyCurrent) const override;

        AbstractMotionModelPtr motionModel;

    private:
        ObjectInstance();
        void output(std::ostream& stream) const;

    public:    // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ObjectInstance& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ObjectInstancePtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ObjectInstanceBasePtr& rhs)
        {
            stream << ObjectInstancePtr::dynamicCast(rhs);
            return stream;
        }



    };

}

