/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/memorytypes/PredictionEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{
    class PredictionTaskEntity;
    using PredictionTaskEntityPtr = IceInternal::Handle<PredictionTaskEntity>;

    class PredictionTaskEntity :
        virtual public memoryx::PredictionTaskEntityBase,
        virtual public memoryx::Entity
    {
    public:
        PredictionTaskEntity();
        PredictionTaskEntity(const std::string& predictionTaskName, const EntityRefBaseList& predictionList);
        PredictionTaskEntity(const PredictionTaskEntity& source);
        ~PredictionTaskEntity() override;

        void initializeAttributes();


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        PredictionTaskEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        // PredictionEntityBase interface
        std::string getPredictionTaskName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPredictionTaskName(const std::string& predictionTaskName, const Ice::Current& c = Ice::emptyCurrent) override;

        EntityRefBaseList getPredictionEntityRefList(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPredictionEntityRefList(const EntityRefBaseList& predictedEntityRefList, const Ice::Current& c = Ice::emptyCurrent) override;

        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const PredictionTaskEntity& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const PredictionTaskEntityPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const PredictionTaskEntityBasePtr& rhs)
        {
            stream << PredictionTaskEntityPtr::dynamicCast(rhs);
            return stream;
        }
    private:
        void output(std::ostream& stream) const;

        static const std::string TASK_NAME_ATTRIBUTE;
        static const std::string PREDICTION_LIST_ATTRIBUTE;
    };

}

