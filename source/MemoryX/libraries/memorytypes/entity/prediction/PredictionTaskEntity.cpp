/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PredictionTaskEntity.h"
#include "../../../../core/entity/EntityRef.h"


namespace memoryx
{
    const std::string PredictionTaskEntity::TASK_NAME_ATTRIBUTE("predictionTaskName");
    const std::string PredictionTaskEntity::PREDICTION_LIST_ATTRIBUTE("predictionList");

    PredictionTaskEntity::PredictionTaskEntity() :
        Entity()
    {
        initializeAttributes();
    }

    PredictionTaskEntity::PredictionTaskEntity(const std::string& predictionTaskName, const EntityRefBaseList& predictionEntityRefList) :
        Entity()
    {
        initializeAttributes();
        setPredictionTaskName(predictionTaskName);
        setPredictionEntityRefList(predictionEntityRefList);
    }


    PredictionTaskEntity::PredictionTaskEntity(const PredictionTaskEntity& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        PredictionTaskEntityBase(source),
        Entity(source)
    {
    }


    PredictionTaskEntity::~PredictionTaskEntity()
    {
    }

    void PredictionTaskEntity::initializeAttributes()
    {
        putAttribute(new EntityAttribute(PredictionTaskEntity::TASK_NAME_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionTaskEntity::PREDICTION_LIST_ATTRIBUTE));
    }


    void PredictionTaskEntity::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }


    Ice::ObjectPtr PredictionTaskEntity::ice_clone() const
    {
        return this->clone();
    }


    PredictionTaskEntityPtr PredictionTaskEntity::clone(const Ice::Current& context) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        return new PredictionTaskEntity(*this);
    }

    std::string PredictionTaskEntity::getPredictionTaskName(const Ice::Current&) const
    {
        return getAttribute(PredictionTaskEntity::TASK_NAME_ATTRIBUTE)->getValue()->getString();
    }

    void PredictionTaskEntity::setPredictionTaskName(const std::string& predictionTaskName, const Ice::Current&)
    {
        getAttribute(PredictionTaskEntity::TASK_NAME_ATTRIBUTE)->setValue(new armarx::Variant(predictionTaskName));
    }


    EntityRefBaseList PredictionTaskEntity::getPredictionEntityRefList(const Ice::Current&) const
    {
        //        EntityAttributeBasePtr attribute = getAttribute(PredictionTaskEntity::PREDICTION_LIST_ATTRIBUTE);
        //        EntityRefBaseList entityList;
        //        for (int i = 0; i < attribute->size(); i++)
        //        {
        //            armarx::VariantPtr attributeValue = armarx::VariantPtr::dynamicCast(attribute->getValueAt(i));
        //            entityList.push_back(attributeValue->get<EntityRef>());
        //        }
        //        return entityList;
        return EntityRefBaseList {};
    }

    void PredictionTaskEntity::setPredictionEntityRefList(const EntityRefBaseList& predictedEntityRefList, const Ice::Current&)
    {
        //        EntityAttributeBasePtr attribute = getAttribute(PredictionTaskEntity::PREDICTION_LIST_ATTRIBUTE);
        //        attribute->clear();
        //        for (const EntityRefBasePtr & entityRef : predictedMemorySnapshotRefList)
        //        {
        //            attribute->addValue(new armarx::Variant(entityRef));
        //        }
    }
}
