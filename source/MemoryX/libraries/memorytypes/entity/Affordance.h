/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Peter Kaiser <peter dot kaiser at kit dot edu>
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <MemoryX/core/entity/Entity.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId Affordance = armarx::Variant::addTypeName("::memoryx::AffordanceBase");
}

namespace memoryx
{
    class Affordance;
    using AffordancePtr = IceInternal::Handle<Affordance>;

    class Affordance : public AffordanceBase, public Entity
    {
    public:
        Affordance(const std::string& name = "", const std::string& id = "");

        void setType(AffordanceType type, const Ice::Current& c = Ice::emptyCurrent) override;
        AffordanceType getType(const Ice::Current& c = Ice::emptyCurrent) const override;

        void setPosition(const armarx::Vector3BasePtr& position, const Ice::Current& c = Ice::emptyCurrent) override;
        armarx::Vector3BasePtr getPosition(const Ice::Current& c = Ice::emptyCurrent) const override;

        void setPrimitiveId(const std::string& id, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getPrimitiveId(const Ice::Current& c = Ice::emptyCurrent) const override;

        void setValidationStatus(AffordanceValidationStatus status, const Ice::Current& c = Ice::emptyCurrent) override;
        AffordanceValidationStatus getValidationStatus(const Ice::Current& c = Ice::emptyCurrent) const override;

        float getProbability(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setProbability(float probability, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::TimestampBasePtr getTime(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setTime(const armarx::TimestampBasePtr& time, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::MatrixFloatBasePtr getCertaintyFunction(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setCertaintyFunction(const armarx::MatrixFloatBasePtr& certainties, const Ice::Current& c = Ice::emptyCurrent) override;

        memoryx::AffordanceObservationList getObservations(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setObservations(const memoryx::AffordanceObservationList& observations, const Ice::Current& c = Ice::emptyCurrent) override;
        void addObservation(const armarx::MatrixFloatBasePtr& observation, const Ice::Current& c = Ice::emptyCurrent) override;

    public:
        std::string getTypeName() const;

        Ice::ObjectPtr ice_clone() const override;
        AffordancePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;
    };
}

