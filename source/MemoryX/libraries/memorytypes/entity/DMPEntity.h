/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{

    class DMPEntity;
    using DMPEntityPtr = IceInternal::Handle<DMPEntity>;

    class DMPEntity :
        public memoryx::DMPEntityBase,
        public memoryx::Entity
    {
    public:
        DMPEntity();
        DMPEntity(const DMPEntity& source);
        DMPEntity(const std::string& name, const std::string& id = "");
        ~DMPEntity() override;

        void setDMPName(const std::string& dmpName, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getDMPName(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setDMPType(const std::string& dmpType, const ::Ice::Current& = Ice::emptyCurrent) override;
        int getDMPType(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setDMPtextStr(const std::string& dmptext, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getDMPtextStr(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void set3rdOrder(const bool is3rdOrder, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool get3rdOrder(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setTrajDim(const int trajdim, const ::Ice::Current& = Ice::emptyCurrent) override;
        int getTrajDim(const ::Ice::Current& = Ice::emptyCurrent) const override;

        // cloning
        Ice::ObjectPtr ice_clone() const override;
        DMPEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    private:
        void output(std::ostream& stream) const;

    public:    // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const DMPEntity& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const DMPEntityPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const DMPEntityBasePtr& rhs)
        {
            stream << DMPEntityPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

