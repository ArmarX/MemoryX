/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/core/entity/Entity.h>
#include <ArmarXCore/util/variants/eigen3/Eigen3VariantObjectFactories.h>

#define KBM_USE_DOUBLE_PRECISION

namespace armarx
{
#ifdef KBM_USE_DOUBLE_PRECISION
    using MatrixVariant = armarx::MatrixDouble;
    using MatrixVariantPtr = armarx::MatrixDoublePtr;
#else
    using MatrixVariant = MatrixFloat;
    using MatrixVariantPtr = MatrixFloatPtr;
#endif
}

namespace memoryx
{

    class KBMData : public KBMDataBase, public Entity
    {
    public:
        KBMData() {}
        KBMData(const armarx::MatrixDoubleBasePtr& controlNet, const std::string& nodeSetName, const std::string& referenceFrameName, const std::string& robotName);
        armarx::MatrixDoubleBasePtr getControlNet(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setControlNet(const armarx::MatrixDoubleBasePtr& controlNet, const Ice::Current& c = Ice::emptyCurrent) override;

        // Object interface
    public:
        Ice::ObjectPtr ice_clone() const override;

        // KBMDataBase interface
    public:
        std::string getReferenceFrameName(const Ice::Current& c = Ice::emptyCurrent) const override;
        std::string getNodeSetName(const Ice::Current& c = Ice::emptyCurrent) const override;
        std::string getRobotName(const Ice::Current& c = Ice::emptyCurrent) const override;

        void setNodeSetName(const std::string& nodeSetName, const Ice::Current& c = Ice::emptyCurrent) override;
        void setReferenceFrameName(const std::string& referenceFrameName, const Ice::Current& c = Ice::emptyCurrent) override;
        void setRobotName(const std::string& robotName, const Ice::Current& c = Ice::emptyCurrent) override;

        // Object interface
    };

    using KBMDataPtr = IceInternal::Handle<KBMData>;

}

