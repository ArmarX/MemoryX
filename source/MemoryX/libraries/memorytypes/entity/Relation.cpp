/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Relation.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{

    Relation::Relation(const std::string& name, const EntityRefList& entities, bool sign, float prob) : Entity()
    {
        setName(name);
        putAttribute("sign", sign);
        putAttribute("prob", prob);
        putAttribute("attributes", "{}");
        putAttribute("source", "{}");
        putAttribute("target", "{}");
        EntityAttributePtr ea = new EntityAttribute("entities");

        for (const auto& entity : entities)
        {
            ea->addValue(new armarx::Variant(entity));
        }

        putAttribute(ea);
    }

    EntityRefList Relation::getEntities(const ::Ice::Current&) const
    {
        EntityRefList result;
        auto ea = getAttribute("entities");

        for (int i = 0; i < ea->size(); i++)
        {
            result.push_back(armarx::VariantPtr::dynamicCast(ea->getValueAt(i))->get<EntityRefBase>());
        }

        return result;
    }

    void Relation::setEntities(const EntityRefList& entities, const ::Ice::Current&)
    {
        removeAttribute("entities");
        EntityAttributePtr ea = new EntityAttribute("entities");

        for (const auto& entity : entities)
        {
            ea->addValue(new armarx::Variant(entity));
        }

        putAttribute(ea);
    }

    bool Relation::getSign(const ::Ice::Current&) const
    {
        return getAttribute("sign")->getValue()->getBool();
    }

    void Relation::setSign(bool sign, const ::Ice::Current&)
    {
        getAttribute("sign")->getValue()->setBool(sign);
    }

    ::Ice::Float Relation::getProb(const ::Ice::Current&) const
    {
        return getAttribute("prob")->getValue()->getFloat();
    }

    void Relation::setProb(::Ice::Float prob, const ::Ice::Current&)
    {
        getAttribute("prob")->getValue()->setFloat(prob);
    }

    std::string Relation::getAttributes() const
    {
        return getAttribute("attributes")->getValue()->getString();
    }

    void Relation::setAttributes(const std::string& attributes)
    {
        getAttribute("attributes")->getValue()->setString(attributes);
    }

    std::string Relation::getSourceAttributes() const
    {
        return getAttribute("source")->getValue()->getString();
    }

    void Relation::setSourceAttributes(std::string const& attributes)
    {
        getAttribute("source")->getValue()->setString(attributes);
    }

    std::string Relation::getTargetAttributes() const
    {
        return getAttribute("target")->getValue()->getString();
    }

    void Relation::setTargetAttributes(std::string const& attributes)
    {
        getAttribute("target")->getValue()->setString(attributes);
    }


    Ice::ObjectPtr Relation::ice_clone() const
    {
        return this->clone();
    }

    RelationPtr Relation::clone(const Ice::Current& c) const
    {
        RelationPtr ret = new Relation(*this);
        //        ret->deepCopy(*this);
        return ret;

    }

    void Relation::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }

} /* namespace memoryx */
