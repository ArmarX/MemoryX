/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId SECObjectRelations = armarx::Variant::addTypeName("::memoryx::SECObjectRelationsBase");
}

namespace memoryx
{
    class SECObjectRelations : public SECObjectRelationsBase
    {
    public:
        SECObjectRelations();
        // cloning
        Ice::ObjectPtr ice_clone() const override;

        // SECObjectRelationsBase interface
        Ice::Float contains(const SECObjectRelationsBasePtr& other, const Ice::Current& c = Ice::emptyCurrent) const override;
        bool containsRelation(const SECRelationBasePtr& relation, const Ice::Current& c = Ice::emptyCurrent) const override;
        Ice::Int addRelations(const SECRelationList& newRelations, const Ice::Current& c = Ice::emptyCurrent) override;
        bool addRelation(const SECRelationBasePtr& newRelation, const Ice::Current& c = Ice::emptyCurrent) override;
        SECRelationBasePtr containsRelationBetweenObjects(const ObjectClassList& objects1, const ObjectClassList& objects2, const Ice::Current& c = Ice::emptyCurrent) const override;
        ObjectClassList getObjectsInRelation(const SECRelationBasePtr& relation, ObjectClassBasePtr object1);
        bool isEqual(const SECObjectRelations& relations) const;

        // VariantDataClass interface
    public:
        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override;
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override;
        Ice::Int getType(const Ice::Current& c = Ice::emptyCurrent) const override;
        bool validate(const Ice::Current& c = Ice::emptyCurrent) override;

        // Serializable interface
    public:
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) override;
    };
    using SECObjectRelationsPtr = IceInternal::Handle<SECObjectRelations>;

}

