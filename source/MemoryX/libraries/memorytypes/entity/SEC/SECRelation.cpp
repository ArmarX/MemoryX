/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SECRelation.h"

#include "../ObjectClass.h"
#include "../../segment/ObjectClassMemorySegment.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace memoryx
{

    SECRelation::SECRelation()
    {
    }

    SECRelation::SECRelation(const std::string& relationName, const memoryx::ObjectClassList& possibleObjects1, const memoryx::ObjectClassList& possibleObjects2)
    {
        name = relationName;
        ARMARX_CHECK_EXPRESSION(possibleObjects1.size() > 0);
        ARMARX_CHECK_EXPRESSION(possibleObjects2.size() > 0);
        objects1 = possibleObjects1;
        objects2 = possibleObjects2;
    }

    SECRelation::SECRelation(const std::string& relationName, const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2)
    {
        name = relationName;
        ARMARX_CHECK_EXPRESSION(possibleObject1);
        ARMARX_CHECK_EXPRESSION(possibleObject2);
        objects1.push_back(possibleObject1);
        objects2.push_back(possibleObject2);
    }

    Ice::ObjectPtr SECRelation::ice_clone() const
    {
        return clone();
    }

    bool SECRelation::isEqual(const SECRelationBasePtr& other, bool ignoreName) const
    {
        return _isEqual(other, ignoreName, false);
    }

    bool SECRelation::isEqual(const SECRelationBasePtr& other, const Ice::Current&) const
    {
        return isEqual(other, false);
    }

    bool SECRelation::hasEqualObjects(const SECRelationBasePtr& other, const Ice::Current&)
    {
        return isEqual(other, true);
    }

    std::string SECRelation::getName(const Ice::Current& c) const
    {
        return name;
    }


    void SECRelation::setName(const std::string& name, const Ice::Current&)
    {
        this->name = name;
    }



    std::string SECRelation::objects1ToString() const
    {
        return ObjectClassMemorySegment::ObjectsToString(objects1);
    }

    std::string SECRelation::objects2ToString() const
    {
        return ObjectClassMemorySegment::ObjectsToString(objects2);
    }




    bool SECRelation::_isEqual(const SECRelationBasePtr& other, bool ignoreName, bool reversed) const
    {
        if (!ignoreName && getName() != other->getName())
        {
            return false;
        }

        bool foundMatchObj1 = false;

        for (ObjectClassList::const_iterator it = objects1.begin(); it != objects1.end(); it++)
        {
            for (ObjectClassList::const_iterator itOther = other->objects1.begin(); itOther != other->objects1.end(); itOther++)
            {
                //                ARMARX_VERBOSE_S << "Comparing " << (*it)->getName() << " with " << (*itOther)->getName();
                if ((*it)->compare(*itOther) == eEqualClass /*|| (*itOther)->compare(*it) == eEqualClass*/)
                {
                    foundMatchObj1 = true;
                }
            }
        }

        bool foundMatchObj2 = false;

        for (ObjectClassList::const_iterator it = objects2.begin(); it != objects2.end(); it++)
        {
            for (ObjectClassList::const_iterator itOther = other->objects2.begin(); itOther != other->objects2.end(); itOther++)
            {
                //                ARMARX_VERBOSE_S << "Comparing " << (*it)->getName() << " with " << (*itOther)->getName();
                if ((*it)->compare(*itOther) == eEqualClass /*|| (*itOther)->compare(*it) == eEqualClass*/)
                {
                    foundMatchObj2 = true;
                }
            }
        }

        bool result = foundMatchObj1 && foundMatchObj2;

        if (!result && !reversed)
        {
            SECRelationBasePtr inverseRelation = new SECRelation(getName(), other->objects2, other->objects1);
            return _isEqual(inverseRelation, ignoreName, true);
        }
        else
        {
            return result;
        }
    }

    Relations::NoConnectionRelation::NoConnectionRelation()
    {
        name = "NoConnection";
    }

    Relations::NoConnectionRelation::NoConnectionRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2)
        : SECRelation("NoConnection", possibleObjects1, possibleObjects2)
    {

    }

    Relations::NoConnectionRelation::NoConnectionRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2) :
        SECRelation("NoConnection", possibleObject1, possibleObject2)
    {

    }

    Relations::TouchingRelation::TouchingRelation()
    {
        name = "Touching";
    }

    Relations::TouchingRelation::TouchingRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2)
        : SECRelation("Touching", possibleObjects1, possibleObjects2)
    {

    }

    Relations::TouchingRelation::TouchingRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2) :
        SECRelation("Touching", possibleObject1, possibleObject2)
    {
    }

    Relations::BehindOfRelation::BehindOfRelation()
    {
        name = "BehindOf";
    }

    Relations::BehindOfRelation::BehindOfRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2)
        : SECRelation("BehindOf", possibleObjects1, possibleObjects2)
    {

    }

    Relations::BehindOfRelation::BehindOfRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2) :
        SECRelation("BehindOf", possibleObject1, possibleObject2)
    {
    }

    Relations::LeftToRelation::LeftToRelation()
    {
        name = "LeftTo";
    }

    Relations::LeftToRelation::LeftToRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2)
        : SECRelation("LeftTo", possibleObjects1, possibleObjects2)
    {

    }

    Relations::LeftToRelation::LeftToRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2) :
        SECRelation("LeftTo", possibleObject1, possibleObject2)
    {
    }

    Relations::RightToRelation::RightToRelation()
    {
        name = "RightTo";
    }

    Relations::RightToRelation::RightToRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2)
        : SECRelation("RightTo", possibleObjects1, possibleObjects2)
    {

    }

    Relations::RightToRelation::RightToRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2) :
        SECRelation("RightTo", possibleObject1, possibleObject2)
    {
    }


    armarx::VariantDataClassPtr SECRelation::clone(const Ice::Current&) const
    {
        SECRelationPtr result = new SECRelation();
        result->name = getName();

        for (ObjectClassList::const_iterator it = objects1.begin(); it != objects1.end(); it++)
        {
            result->objects1.push_back(ObjectClassBasePtr::dynamicCast((*it)->ice_clone()));
        }

        for (ObjectClassList::const_iterator it = objects2.begin(); it != objects2.end(); it++)
        {
            result->objects2.push_back(ObjectClassBasePtr::dynamicCast((*it)->ice_clone()));
        }

        return result;
    }

    std::string SECRelation::output(const Ice::Current&) const
    {
        return getName() + " between " + objects1ToString() + " and " + objects2ToString();
    }

    Ice::Int SECRelation::getType(const Ice::Current&) const
    {
        return memoryx::VariantType::SECRelation;
    }

    bool SECRelation::validate(const Ice::Current&)
    {
        return true;
    }

    void SECRelation::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setString("relationName", getName());
        obj->setStringArray("objects1", ObjectClassMemorySegment::ObjectsToStringList(objects1));
        obj->setStringArray("objects2", ObjectClassMemorySegment::ObjectsToStringList(objects2));
    }

    void SECRelation::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        name = obj->getString("relationName");
        Ice::StringSeq objectStrings1;
        obj->getStringArray("objects1", objectStrings1);

        for (Ice::StringSeq::iterator it = objectStrings1.begin(); it != objectStrings1.end(); it++)
        {
            ObjectClassPtr newObj = new ObjectClass();
            newObj->setName(*it);
            objects1.push_back(newObj);
        }

        Ice::StringSeq objectStrings2;
        obj->getStringArray("objects2", objectStrings2);

        for (Ice::StringSeq::iterator it = objectStrings2.begin(); it != objectStrings2.end(); it++)
        {
            ObjectClassPtr newObj = new ObjectClass();
            newObj->setName(*it);
            objects2.push_back(newObj);
        }
    }

} // namespace memoryx









