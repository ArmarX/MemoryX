/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <MemoryX/core/entity/Entity.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId SECRelation = armarx::Variant::addTypeName("::memoryx::SECRelationBase");
}

namespace memoryx
{
    class SECRelation :
        public memoryx::SECRelationBase
    {
    public:
        SECRelation();
        SECRelation(const std::string& relationName, const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2);
        SECRelation(const std::string& relationName, const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2);

        // cloning
        Ice::ObjectPtr ice_clone() const override;

        bool isEqual(const SECRelationBasePtr& other, bool ignoreName) const;


        // SECRelationBase interface
        bool isEqual(const SECRelationBasePtr& other, const Ice::Current& c = Ice::emptyCurrent) const override;
        bool hasEqualObjects(const SECRelationBasePtr& other, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setName(const std::string& name, const Ice::Current& c = Ice::emptyCurrent) override;

        std::string objects1ToString() const;
        std::string objects2ToString() const;


        // VariantDataClass interface
    public:
        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override;
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override;
        Ice::Int getType(const Ice::Current& c = Ice::emptyCurrent) const override;
        bool validate(const Ice::Current& c = Ice::emptyCurrent) override;

        // Serializable interface
    public:
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        bool _isEqual(const SECRelationBasePtr& other, bool ignoreName = false, bool reversed = false) const;

    };
}

namespace memoryx::Relations
{
    class NoConnectionRelation : public SECRelation
    {
    public:
        NoConnectionRelation();
        NoConnectionRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2);
        NoConnectionRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2);
    };

    class TouchingRelation : public SECRelation
    {
    public:
        TouchingRelation();
        TouchingRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2);
        TouchingRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2);
    };

    class BehindOfRelation : public SECRelation
    {
    public:
        BehindOfRelation();
        BehindOfRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2);
        BehindOfRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2);
    };

    class LeftToRelation : public SECRelation
    {
    public:
        LeftToRelation();
        LeftToRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2);
        LeftToRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2);
    };

    class RightToRelation : public SECRelation
    {
    public:
        RightToRelation();
        RightToRelation(const ObjectClassList& possibleObjects1, const ObjectClassList& possibleObjects2);
        RightToRelation(const ObjectClassBasePtr& possibleObject1, const ObjectClassBasePtr& possibleObject2);
    };


}

namespace memoryx
{
    using SECRelationPtr = IceInternal::Handle<SECRelation>;
}
