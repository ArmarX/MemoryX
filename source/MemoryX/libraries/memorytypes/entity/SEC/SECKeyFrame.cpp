/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SECKeyFrame.h"

#include "SECObjectRelations.h"

namespace memoryx
{

    SECKeyFrame::SECKeyFrame()
    {
        worldState = new SECObjectRelations();
    }

    SECKeyFrame::SECKeyFrame(const SECKeyFrame& source) :
        IceUtil::Shared(source),
        SECKeyFrameBase(source)
    {
        *this = source;
    }

    SECKeyFrame::SECKeyFrame(int index, SECObjectRelationsBasePtr worldState)
    {
        this->index = index;
        this->worldState = worldState;
    }

    SECKeyFrame& SECKeyFrame::operator=(const SECKeyFrame& source)
    {
        index = source.index;
        worldState = SECObjectRelationsBasePtr::dynamicCast(source.worldState->ice_clone());
        return *this;
    }

    Ice::ObjectPtr SECKeyFrame::ice_clone() const
    {
        Ice::ObjectPtr result = new SECKeyFrame(*this);
        return result;
    }

    std::ostream& operator<<(std::ostream& str, SECKeyFrameBasePtr keyframe)
    {
        str << "Index: " << keyframe->index << " WorldState:" << SECObjectRelationsPtr::dynamicCast(keyframe->worldState)->output();
        return str;
    }

}
