/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SECObjectRelations.h"
#include "SECRelation.h"

#include "../../segment/ObjectClassMemorySegment.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{

    SECObjectRelations::SECObjectRelations()
    {
    }

    armarx::VariantDataClassPtr SECObjectRelations::clone(const Ice::Current&) const
    {
        SECObjectRelationsPtr result = new SECObjectRelations();

        for (SECRelationList::const_iterator it = relations.begin(); it != relations.end(); it++)
        {
            result->relations.push_back(SECRelationBasePtr::dynamicCast((*it)->ice_clone()));
        }

        return result;
    }

    Ice::ObjectPtr SECObjectRelations::ice_clone() const
    {
        return clone();
    }

    Ice::Float SECObjectRelations::contains(const SECObjectRelationsBasePtr& other, const Ice::Current&) const
    {
        float matchingRelationCount = 0;

        for (SECRelationList::const_iterator itOther = other->relations.begin(); itOther != other->relations.end(); itOther++)
        {
            bool foundMatch = false;
            SECRelationBasePtr otherRel = *itOther;

            for (SECRelationList::const_iterator it = relations.begin(); it != relations.end(); it++)
            {
                if (otherRel->isEqual(*it))
                {
                    foundMatch = true;
                    break;
                }
            }

            if (foundMatch)
            {
                matchingRelationCount++;
            }
        }

        return matchingRelationCount / (float)relations.size();
    }


    bool SECObjectRelations::containsRelation(const SECRelationBasePtr& relation, const Ice::Current&) const
    {
        for (SECRelationList::const_iterator it = relations.begin(); it != relations.end(); it++)
        {
            if (relation->isEqual(*it))
            {
                return true;
            }
        }

        return false;

    }


    Ice::Int SECObjectRelations::addRelations(const SECRelationList& newRelations, const Ice::Current&)
    {
        int insertCount = 0;

        for (unsigned int i = 0; i < newRelations.size(); i++)
        {
            SECRelationBasePtr rel = containsRelationBetweenObjects(newRelations.at(i)->objects1, newRelations.at(i)->objects2);

            //            ARMARX_VERBOSE_S << "Adding relation: " << newRelations.at(i)->output();
            if (rel)
            {
                rel->setName(newRelations.at(i)->getName());
            }
            else
            {
                relations.push_back(newRelations.at(i));
                insertCount++;
            }
        }

        return insertCount;
    }

    bool SECObjectRelations::addRelation(const SECRelationBasePtr& newRelation, const Ice::Current& c)
    {
        SECRelationList list;
        list.push_back(newRelation);
        return (addRelations(list) != 0);
    }

    SECRelationBasePtr SECObjectRelations::containsRelationBetweenObjects(const ObjectClassList& objects1, const ObjectClassList& objects2, const Ice::Current&) const
    {
        SECRelation relation("", objects1, objects2);

        for (SECRelationList::const_iterator it = relations.begin(); it != relations.end(); it++)
        {
            if (relation.hasEqualObjects(*it))
            {
                return *it;
            }

        }

        return NULL;
    }

    ObjectClassList SECObjectRelations::getObjectsInRelation(const SECRelationBasePtr& relation, ObjectClassBasePtr object1)
    {

        ObjectClassList result;

        for (SECRelationList::const_iterator it = relations.begin(); it != relations.end(); it++)
        {
            if ((*it)->getName() == relation->getName()
                && ObjectClassMemorySegment::ListContainsObject((*it)->objects1, object1))
            {
                result.insert(result.end(), (*it)->objects2.begin(), (*it)->objects2.end());
            }

        }

        return result;
    }

    bool SECObjectRelations::isEqual(const SECObjectRelations& compareRelations) const
    {
        bool result = true;

        for (const SECRelationBasePtr& relation : compareRelations.relations)
        {
            result &= this->containsRelation(relation);
        }

        return result;
    }



    std::string SECObjectRelations::output(const Ice::Current&) const
    {
        std::stringstream result;

        for (size_t i = 0; i < relations.size(); i++)
        {
            result << "#" << i << ": " << relations.at(i)->output() << "\n";
        }

        return result.str();
    }

    Ice::Int SECObjectRelations::getType(const Ice::Current&) const
    {
        return memoryx::VariantType::SECObjectRelations;
    }

    bool SECObjectRelations::validate(const Ice::Current&)
    {
        return true;
    }

    void SECObjectRelations::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        std::vector<armarx::VariantBasePtr> variants;

        for (unsigned int i = 0; i < relations.size(); i++)
        {
            armarx::VariantPtr var = new armarx::Variant(relations.at(i));
            variants.push_back(var);
        }

        obj->setVariantArray("relations", variants);

    }

    void SECObjectRelations::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        std::vector<armarx::VariantPtr> variants;

        obj->getVariantArray("relations", variants);
        relations.resize(variants.size());

        for (unsigned int i = 0; i < variants.size(); i++)
        {

            relations.at(i) = armarx::VariantPtr::dynamicCast(variants.at(i))->get<SECRelation>();
        }
    }
}
