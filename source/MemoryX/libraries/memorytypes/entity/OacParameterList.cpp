/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "OacParameterList.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{
    std::vector<std::string> OacParameterList::getNames(const Ice::Current& c) const
    {
        return parameterNames;
    }

    void OacParameterList::setNames(const std::vector<std::string>& names, const Ice::Current& c)
    {
        parameterNames = names;
    }

    void OacParameterList::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setStringArray("names", parameterNames);
    }

    void OacParameterList::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->getStringArray("names", parameterNames);
    }

    armarx::VariantDataClassPtr OacParameterList::clone(const Ice::Current& c) const
    {
        OacParameterListPtr clone = new OacParameterList;

        clone->setNames(this->getNames());

        return clone;
    }

    std::string OacParameterList::output(const Ice::Current& c) const
    {
        return "OacParameterList";
    }

    int OacParameterList::getType(const Ice::Current& c) const
    {
        return VariantType::OacParamterList;
    }
}
