/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Thomas von der Heyde (tvh242 at hotmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/RobotState.h>

namespace memoryx
{

    class AgentInstance;
    /**
    * Typedef of AgentEntityPtr as IceInternal::Handle<AgentEntity> for convenience.
    */
    using AgentInstancePtr = IceInternal::Handle<AgentInstance>;

    /*!
     * AgentInstance represents an agent (robot) in the working memory. It allows acces to the agent's position and orientation, as well as to a stringified version of the SharedRobotInterfacePrx
     * of that agent, allowing querying of the agents joint values, and the path to the agents XML data file.
     */
    class AgentInstance :
        public AgentInstanceBase,
        public Entity
    {
    public:

        /*!
         * Default AgentInstance constructor.
         */
        AgentInstance();
        AgentInstance(const AgentInstance& source);
        /*!
         * Creates an AgentInstance with the given name, and optionally, ID.
         */
        AgentInstance(const std::string& name, const std::string& id = "");

        /*!
         * Gets a pointer to the agents position attribute.
         */
        EntityAttributeBasePtr getPositionAttribute() const;
        /*!
         * Gets the agents framed position.
         */
        armarx::FramedPositionPtr getPosition() const;
        /*!
         * Sets the agents framed position.
         * \param position Agents framed position.
         */
        void setPosition(const armarx::FramedPositionBasePtr& position, const ::Ice::Current& = Ice::emptyCurrent) override;

        /*!
         * Gets a pointer to the agents orientation attribute.
         */
        EntityAttributeBasePtr getOrientationAttribute() const;
        /*!
         * Gets the agents framed orientation.
         */
        armarx::FramedOrientationPtr getOrientation() const;
        /*!
         * Sets the agents framed orientation.
         * \param orientation Agents framed orientation.
         */
        void setOrientation(const armarx::FramedOrientationBasePtr& orientation, const ::Ice::Current& = Ice::emptyCurrent) override;

        /*!
         * Gets the agents framed pose.
         */
        armarx::FramedPosePtr getPose() const;
        /*!
         * Sets the agents framed orientation.
         * \param orientation Agents framed orientation.
         */
        void setPose(const armarx::FramedPoseBasePtr& newPose);

        /*!
         * Gets the agents SharedRobotInterfacePrx in string format. This can be converted back to proxy format using the ICE communicator.
         */
        std::string getStringifiedSharedRobotInterfaceProxy(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        /*!
         * Sets the agents SharedRobotInterfacePrx in string format. A proxy can be converted to string format using the ICE communicator.
         */
        void setStringifiedSharedRobotInterfaceProxy(const std::string& stringifiedSharedRobotInterfaceProxy, const ::Ice::Current& c = Ice::emptyCurrent) override;

        /*!
         * Gets the path of the agents XML data file.
         */
        std::string getAgentFilePath(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        /*!
         * Sets the path of the agents XML data file.
         * \param agentFilePath Path of the XML data file.
         */
        void setAgentFilePath(const std::string& agentFilePath, const ::Ice::Current& c = Ice::emptyCurrent) override;

        armarx::SharedRobotInterfacePrx getSharedRobot(const Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief set the proxy to a SharedRobot for this agent. This entity takes the name from the robot in
         *  the given proxy
         * @param robot
         */
        void setSharedRobot(const armarx::SharedRobotInterfacePrx& robot, const Ice::Current& c = Ice::emptyCurrent) override;


        /* Cloning. */
        Ice::ObjectPtr ice_clone() const override;
        AgentInstancePtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    protected:

        /* Ice interface implementation on Base data types. Replaces in C++ by derived instance getters, setters. */
        armarx::FramedPositionBasePtr getPositionBase(const ::Ice::Current& = Ice::emptyCurrent) const override;
        armarx::FramedOrientationBasePtr getOrientationBase(const ::Ice::Current& = Ice::emptyCurrent) const override;
        armarx::FramedPoseBasePtr getPoseBase(const ::Ice::Current& = Ice::emptyCurrent) const override;


    public:

        /* Streaming operator. */
        friend std::ostream& operator<<(std::ostream& stream, const AgentInstance& rhs)
        {
            rhs.output(stream);
            return stream;
        }
        friend std::ostream& operator<<(std::ostream& stream, const AgentInstancePtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }
        friend std::ostream& operator<<(std::ostream& stream, const AgentInstanceBasePtr& rhs)
        {
            stream << AgentInstancePtr::dynamicCast(rhs);
            return stream;
        }

    };

}

