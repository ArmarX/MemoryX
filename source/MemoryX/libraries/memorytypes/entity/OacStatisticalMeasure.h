/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <ArmarXCore/observers/variant/Variant.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId OacStatisticalMeasure = armarx::Variant::addTypeName("::memoryx::OacStatisticalMeasureBase");
}

namespace memoryx
{
    class OacStatisticalMeasure;
    using OacStatisticalMeasurePtr = IceInternal::Handle<OacStatisticalMeasure>;

    class OacStatisticalMeasure: public OacStatisticalMeasureBase
    {
    public:
        OacStatisticalMeasure();
        int getExperimentCount(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setExperimentCount(int count, const Ice::Current& c = Ice::emptyCurrent) override;

        int getFailedExperimentCount(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setFailedExperimentCount(int countFailed, const Ice::Current& c = Ice::emptyCurrent) override;


        void increaseExperimentCount(int amount = 1);
        void increaseFailedExperimentCount(int amount = 1);

        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override;
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override;
        int getType(const Ice::Current& c = Ice::emptyCurrent) const override;

        bool validate(const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
    };
}

