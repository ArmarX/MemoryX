/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu), Kai Welke (welke at kit got edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
//#include <MemoryX/libraries/motionmodels/AbstractMotionModel.h>


namespace memoryx
{

    class ActiveOac;
    using ActiveOacPtr = IceInternal::Handle<ActiveOac>;

    class ActiveOac: public ActiveOacBase, public Entity
    {
        friend class ActiveOacObjectFactory;
    public:

        ActiveOac(const std::string& id = "");



        OacExecState getState(
            const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setState(
            OacExecState state,
            const ::Ice::Current& = Ice::emptyCurrent) override;

        Ice::ObjectPtr ice_clone() const override;
        ActiveOacPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        NameList getArgumentInstanceIds(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setArgumentInstanceIds(const NameList&, const Ice::Current& c = Ice::emptyCurrent) override;
        EntityRefBasePtr getOacRef(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setOacRef(const EntityRefBasePtr& oacRef, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getOacName(const Ice::Current& c = Ice::emptyCurrent) const override;

        void setStartTime(const IceUtil::Time& time);
        IceUtil::Time getStartTime() const;
    };
}

