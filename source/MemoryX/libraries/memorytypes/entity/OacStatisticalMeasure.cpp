/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "OacStatisticalMeasure.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{
    OacStatisticalMeasure::OacStatisticalMeasure()
    {
        count = 0;
        countFailed = 0;
    }

    int OacStatisticalMeasure::getExperimentCount(const Ice::Current& c) const
    {
        return count;
    }

    void OacStatisticalMeasure::setExperimentCount(int count, const Ice::Current& c)
    {
        this->count = count;
    }

    int OacStatisticalMeasure::getFailedExperimentCount(const Ice::Current& c) const
    {
        return countFailed;
    }

    void OacStatisticalMeasure::setFailedExperimentCount(int countFailed, const Ice::Current& c)
    {
        this->countFailed = countFailed;
    }

    void OacStatisticalMeasure::increaseExperimentCount(int amount)
    {
        count += amount;
    }

    void OacStatisticalMeasure::increaseFailedExperimentCount(int amount)
    {
        countFailed += amount;
    }

    void OacStatisticalMeasure::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setInt("count", count);
        obj->setInt("countFailed", countFailed);
    }

    void OacStatisticalMeasure::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        count = obj->getInt("count");
        countFailed = obj->getInt("countFailed");
    }

    armarx::VariantDataClassPtr OacStatisticalMeasure::clone(const Ice::Current& c) const
    {
        OacStatisticalMeasurePtr clone = new OacStatisticalMeasure;

        clone->setExperimentCount(this->getExperimentCount());
        clone->setFailedExperimentCount(this->getFailedExperimentCount());

        return clone;
    }

    std::string OacStatisticalMeasure::output(const Ice::Current& c) const
    {
        return "OacStatisticalMeasure";
    }

    int OacStatisticalMeasure::getType(const Ice::Current& c) const
    {
        return memoryx::VariantType::OacStatisticalMeasure;
    }

    bool OacStatisticalMeasure::validate(const Ice::Current& c)
    {
        return true;
    }

    Ice::ObjectPtr OacStatisticalMeasure::ice_clone() const
    {
        return this->clone();
    }
}
