/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{

    class ObjectClass;
    using ObjectClassPtr = IceInternal::Handle<ObjectClass>;

    class ObjectClass :
        public memoryx::ObjectClassBase,
        public memoryx::Entity
    {
    public:
        ObjectClass();
        ObjectClass(const ObjectClass& source);
        ~ObjectClass() override;

        ::memoryx::NameList getParentClasses(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setParentClass(const ::std::string& className, const ::Ice::Current& = Ice::emptyCurrent) override;
        void addParentClass(const ::std::string& className, const ::Ice::Current& = Ice::emptyCurrent) override;
        void clearParentClasses(const ::Ice::Current& = Ice::emptyCurrent) override;
        void setInstanceable(bool isInstanceable, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool isInstanceable(const ::Ice::Current& = Ice::emptyCurrent) const override;
        ObjectComparisonResult compare(const ObjectClassBasePtr& other, const Ice::Current& = Ice::emptyCurrent) const override;
        EntityAttributeBasePtr getParentClassesAttr() const;

        // cloning
        Ice::ObjectPtr ice_clone() const override;
        ObjectClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    private:
        void output(std::ostream& stream) const;

    public:    // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ObjectClass& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ObjectClassPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ObjectClassBasePtr& rhs)
        {
            stream << ObjectClassPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

