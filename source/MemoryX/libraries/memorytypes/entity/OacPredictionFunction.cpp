/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "OacPredictionFunction.h"

#include <MemoryX/libraries/memorytypes/entity/SEC/SECObjectRelations.h>

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace memoryx
{
    std::string OacPredictionFunction::getPreconditionExpression(const Ice::Current& c) const
    {
        return precondition;
    }

    void OacPredictionFunction::setPreconditionExpression(const std::string& precondition, const Ice::Current& c)
    {
        this->precondition = precondition;
    }

    std::string OacPredictionFunction::getEffectExpression(const Ice::Current& c) const
    {
        return effect;
    }

    void OacPredictionFunction::setEffectExpression(const std::string& effect, const Ice::Current& c)
    {
        this->effect = effect;
    }

    void OacPredictionFunction::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setString("precondition", precondition);
        obj->setString("effect", effect);

        if (SECPreconditions)
        {
            armarx::VariantPtr preconditionsVariant = new armarx::Variant(SECPreconditions);
            obj->setVariant("SECPreconditions", preconditionsVariant);
        }

        //    else
        //        ARMARX_WARNING_S << "No SECPreconditions in OacPredictionFunction";
        if (SECEffects)
        {
            armarx::VariantPtr effectsVariant = new armarx::Variant(SECEffects);
            obj->setVariant("SECEffects", effectsVariant);
        }

        //    else
        //        ARMARX_WARNING_S << "No SECEffects in OacPredictionFunction";
        std::vector<armarx::VariantPtr> relationList;
        relationList.resize(SECSideConstraints.size());

        for (unsigned int i = 0; i < SECSideConstraints.size(); i++)
        {
            relationList.at(i) = new armarx::Variant(SECSideConstraints.at(i).relation1);
        }

        obj->setVariantArray("SECSideConstraintsFirstRelations", relationList);

        for (unsigned int i = 0; i < SECSideConstraints.size(); i++)
        {
            relationList.at(i) = new armarx::Variant(SECSideConstraints.at(i).relation2);
        }

        obj->setVariantArray("SECSideConstraintsSecondRelations", relationList);
    }

    void OacPredictionFunction::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        precondition = obj->getString("precondition");
        effect = obj->getString("effect");

        if (obj->hasElement("SECPreconditions") && obj->hasElement("SECEffects"))
        {
            SECEffects = obj->getVariant("SECEffects")->get<SECObjectRelations>();
            SECPreconditions = obj->getVariant("SECPreconditions")->get<SECObjectRelations>();
        }
        else
        {
            ARMARX_DEBUG_S << "No SECEffects/SECPreconditions in OacPredictionFunction in oac segment";
        }

        std::vector<armarx::VariantPtr> relationList;
        obj->getVariantArray("SECSideConstraintsFirstRelations", relationList);
        SECSideConstraints.resize(relationList.size());

        for (unsigned int i = 0; i < relationList.size(); i++)
        {
            SECSideConstraints.at(i).relation1 = relationList.at(i)->get<SECRelationBase>();
        }

        obj->getVariantArray("SECSideConstraintsSecondRelations", relationList);

        for (unsigned int i = 0; i < relationList.size(); i++)
        {
            SECSideConstraints.at(i).relation2 = relationList.at(i)->get<SECRelationBase>();
        }


    }

    armarx::VariantDataClassPtr OacPredictionFunction::clone(const Ice::Current& c) const
    {
        OacPredictionFunctionPtr clone = new OacPredictionFunction;

        clone->setPreconditionExpression(this->getPreconditionExpression());
        clone->setEffectExpression(this->getEffectExpression());

        if (getSECPreconditions())
        {
            clone->setSECPreconditions(SECObjectRelationsBasePtr::dynamicCast(getSECPreconditions()->ice_clone()));
        }

        if (getSECEffects())
        {
            clone->setSECEffects(SECObjectRelationsBasePtr::dynamicCast(getSECEffects()->ice_clone()));
        }


        SECRelationPairList pairListOrig = getSECSideConstraints();
        SECRelationPairList pairList;

        for (unsigned int i = 0; i < pairListOrig.size(); i++)
        {
            SECRelationPair newPair;

            if (pairListOrig.at(i).relation1)
            {
                newPair.relation1 = SECRelationBasePtr::dynamicCast(pairListOrig.at(i).relation1->ice_clone());
            }

            if (pairListOrig.at(i).relation2)
            {
                newPair.relation2 = SECRelationBasePtr::dynamicCast(pairListOrig.at(i).relation2->ice_clone());
            }

            pairList.push_back(newPair);
        }

        clone->setSECSideConstraints(pairList);

        return clone;
    }

    std::string OacPredictionFunction::output(const Ice::Current& c) const
    {
        return "OacPredictionFunction";
    }

    int OacPredictionFunction::getType(const Ice::Current& c) const
    {
        return VariantType::OacPredictionFunction;
    }


    memoryx::SECObjectRelationsBasePtr memoryx::OacPredictionFunction::getSECPreconditions(const Ice::Current&) const
    {
        return SECPreconditions;
    }

    void memoryx::OacPredictionFunction::setSECPreconditions(const memoryx::SECObjectRelationsBasePtr& preconditions, const Ice::Current&)
    {
        SECPreconditions = preconditions;
    }

    memoryx::SECObjectRelationsBasePtr memoryx::OacPredictionFunction::getSECEffects(const Ice::Current&) const
    {
        return SECEffects;
    }

    void memoryx::OacPredictionFunction::setSECEffects(const memoryx::SECObjectRelationsBasePtr& effects, const Ice::Current&)
    {
        SECEffects = effects;
    }


    SECRelationPairList memoryx::OacPredictionFunction::getSECSideConstraints(const Ice::Current&) const
    {
        return SECSideConstraints;
    }

    void memoryx::OacPredictionFunction::setSECSideConstraints(const SECRelationPairList& sideConstraints, const Ice::Current&)
    {
        SECSideConstraints = sideConstraints;
    }
}
