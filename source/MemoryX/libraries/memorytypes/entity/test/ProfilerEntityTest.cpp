/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE MemoryX::ProfilerEntityTests
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>

#include "../profiler/ProfilerEntity.h"
#include "../profiler/ProfilerMemorySnapshot.h"


BOOST_AUTO_TEST_CASE(testSetAndGetTags)
{
    memoryx::ProfilerEntityPtr entity = new memoryx::ProfilerEntity();

    BOOST_CHECK_EQUAL(entity->getTags().size(), 0);

    entity->setTags({"abc", "def"});

    BOOST_CHECK_EQUAL(entity->getTags().size(), 2);
    BOOST_CHECK_EQUAL(entity->getTags().at(0), "abc");
    BOOST_CHECK_EQUAL(entity->getTags().at(1), "def");
}

BOOST_AUTO_TEST_CASE(testAddAndGetTags)
{
    memoryx::ProfilerEntityPtr entity = new memoryx::ProfilerEntity();

    BOOST_CHECK_EQUAL(entity->getTags().size(), 0);

    entity->addTag("abc");

    BOOST_CHECK_EQUAL(entity->getTags().size(), 1);
    BOOST_CHECK_EQUAL(entity->getTags().at(0), "abc");

    entity->addTag("def");

    BOOST_CHECK_EQUAL(entity->getTags().size(), 2);
    BOOST_CHECK_EQUAL(entity->getTags().at(0), "abc");
    BOOST_CHECK_EQUAL(entity->getTags().at(1), "def");
}

BOOST_AUTO_TEST_CASE(testMemorySnapshotEntity)
{
    const std::string STATE_NAME = "State";
    memoryx::ProfilerMemorySnapshotPtr snapshot = new memoryx::ProfilerMemorySnapshot(STATE_NAME, Ice::Context());

    Ice::Context parameters = snapshot->getMemoryParameterMap();
    BOOST_CHECK_EQUAL(parameters.size(), 0);
    BOOST_CHECK_EQUAL(snapshot->getStateName(), STATE_NAME);

    parameters["param1"] = "value1";
    parameters["param2"] = "value2";

    snapshot->setMemoryParameterMap(parameters);

    Ice::Context parameters2 = snapshot->getMemoryParameterMap();

    BOOST_CHECK_EQUAL(parameters2.size(), 2);
    BOOST_CHECK_EQUAL(parameters2.at("param1"), "value1");
    BOOST_CHECK_EQUAL(parameters2.at("param2"), "value2");
}
