/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE MemoryX::SEC
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>



#include <MemoryX/core/MongoTestHelper.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <MemoryX/libraries/memorytypes/entity/KBMData.h>
#include <MemoryX/libraries/memorytypes/entity/Oac.h>

#include <ArmarXCore/core/CoreObjectFactories.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>
#include <MemoryX/libraries/motionmodels/MotionModelKBM.h>

#include "../SEC/SECObjectRelations.h"
#include "../SEC/SECRelation.h"
#include "../ObjectClass.h"
#include "KBMTestEnvironment.h"
#include <VirtualRobot/XML/RobotIO.h>

BOOST_AUTO_TEST_CASE(KBMDataAccessTest)
{

    IceTestHelper iceHelper;
    KBMEnvironment env("KBMTestEnvironment");

    memoryx::KBMDataBasePtr kbmData = new memoryx::KBMData;

    Eigen::Matrix4d cn = Eigen::Matrix4d::Identity();

    kbmData->setControlNet(new armarx::MatrixDouble(cn));
    auto segm = env.ltm->getKBMSegment();
    auto id = segm->addEntity(kbmData);
    kbmData = segm->getKBMData(id);

    BOOST_CHECK(armarx::MatrixDoublePtr::dynamicCast(kbmData->getControlNet())->toEigen().isApprox(cn));


}



BOOST_AUTO_TEST_CASE(KBMRandomInitTest)
{
    armarx::CMakePackageFinder finder("RobotAPI");
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + "/RobotAPI/robots/Armar3/ArmarIII.xml");
    auto nodeSet = robot->getRobotNodeSet("LeftArm");
    auto kbm = memoryx::MotionModelKBM::CreateKBMFromSamples(robot, nodeSet, nodeSet->getKinematicRoot());

    auto mapVectorToMatrix = [](const std::vector<memoryx::KBM::Real>& vec, size_t rows)
    {
        size_t cols = vec.size() / rows;
        return Eigen::Map<const memoryx::KBM::Matrix>(vec.data(), rows, cols);
    };

    std::vector<VirtualRobot::RobotNodePtr> nodes = nodeSet->getAllRobotNodes();
    VirtualRobot::RobotNodePtr root = nodeSet->getKinematicRoot();
    VirtualRobot::RobotNodePtr TCP = nodeSet->getTCP();
    size_t nJoints = nodes.size();
    size_t nOutputDim = 3;
    Eigen::VectorXf jointMin = Eigen::VectorXf::Zero(nJoints, 1);
    Eigen::VectorXf jointMax = Eigen::VectorXf::Zero(nJoints, 1);

    for (size_t i = 0; i < nJoints; i++)
    {
        VirtualRobot::RobotNodePtr n = nodes[i];
        jointMax(i) = n->getJointLimitHi() - 0.1f;
        jointMin(i) = n->getJointLimitLo() + 0.1f;
    }
    std::vector<memoryx::KBM::Real> proprioceptionAccumulator;
    std::vector<memoryx::KBM::Real> positionAccumulator;
    unsigned int nEvaluationSamples = (int)pow(nOutputDim, nJoints) / 20;
    proprioceptionAccumulator.clear();
    positionAccumulator.clear();
    for (size_t i = 0; i < nEvaluationSamples; i++)
    {
        Ice::DoubleSeq prop, pos;

        memoryx::MotionModelKBM::CreateSample(nodeSet, jointMax, jointMin, root, TCP, prop, pos);
        proprioceptionAccumulator.insert(proprioceptionAccumulator.end(), prop.begin(), prop.end());
        positionAccumulator.insert(positionAccumulator.end(), pos.begin(), pos.end());
        ARMARX_INFO_S << "Error: " << (kbm->predict(mapVectorToMatrix(prop, nJoints)) - mapVectorToMatrix(pos, nOutputDim)).norm() << " mm";
    }
    auto errors = kbm->getErrors(mapVectorToMatrix(proprioceptionAccumulator, nJoints),
                                 mapVectorToMatrix(positionAccumulator, nOutputDim));

    ARMARX_INFO_S << "Errors: " << errors;

    BOOST_CHECK_SMALL(errors.Mean, 0.2);

}

