/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE MemoryX::Oac
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <ArmarXCore/util/json/JSONObject.h>
#include <MemoryX/libraries/memorytypes/entity/Oac.h>
#include <MemoryX/libraries/memorytypes/entity/OacPredictionFunction.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include <ArmarXCore/core/CoreObjectFactories.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/core/MongoTestHelper.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/components/CommonStorage/CommonStorage.h>
#include <MemoryX/components/LongtermMemory/LongtermMemory.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECObjectRelations.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECRelation.h>

BOOST_AUTO_TEST_CASE(testOacEmptySerializeDeserialize)
{
    IceTestHelper iceHelper;

    Ice::CommunicatorPtr ic = iceHelper.getCommunicator();
    armarx::ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);
    std::string serialized;
    memoryx::Oac oac1;
    memoryx::Oac oac2;

    oac1.setId("oacId");

    armarx::JSONObjectPtr jsonSerializer = new armarx::JSONObject(ic);

    jsonSerializer->reset();
    oac1.serialize(jsonSerializer);
    serialized = jsonSerializer->toString();

    jsonSerializer->reset();
    jsonSerializer->fromString(serialized);
    oac2.deserialize(jsonSerializer);

    BOOST_CHECK_EQUAL(oac1.getId(), oac2.getId());
    BOOST_CHECK_EQUAL(oac1.getName(), oac2.getName());

    BOOST_CHECK(oac1.getParameters().size() == 0);
    BOOST_CHECK(oac2.getParameters().size() == 0);
    BOOST_CHECK(oac1.getParameters() == oac2.getParameters());

    BOOST_CHECK(oac1.getPredictionFunction());
    BOOST_CHECK(oac2.getPredictionFunction());
    BOOST_CHECK_EQUAL(oac1.getPredictionFunction()->getEffectExpression(), oac2.getPredictionFunction()->getEffectExpression());

    BOOST_CHECK(oac1.getStatisticalMeasure());
    BOOST_CHECK(oac2.getStatisticalMeasure());
    BOOST_CHECK_EQUAL(oac1.getStatisticalMeasure()->getExperimentCount(), oac2.getStatisticalMeasure()->getExperimentCount());
}

BOOST_AUTO_TEST_CASE(testOacSimpleSerializeDeserialize)
{
    IceTestHelper iceHelper;

    //    Ice::CommunicatorPtr ic = iceHelper.getCommunicator();

    Ice::CommunicatorPtr ic = iceHelper.getCommunicator();
    armarx::ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);

    std::string serialized;
    memoryx::Oac oac1;
    memoryx::Oac oac2;

    oac1.setId("oacId");
    oac1.setName("foo");

    std::vector<std::string> oacParameters;
    oacParameters.push_back("p1");
    oacParameters.push_back("p2");

    oac1.setParameters(oacParameters);

    armarx::JSONObjectPtr jsonSerializer = new armarx::JSONObject(ic);

    jsonSerializer->reset();
    oac1.serialize(jsonSerializer);
    serialized = jsonSerializer->toString();

    jsonSerializer->reset();
    jsonSerializer->fromString(serialized);
    oac2.deserialize(jsonSerializer);

    BOOST_CHECK_EQUAL(oac1.getName(), oac2.getName());

    BOOST_CHECK_EQUAL(oac1.getParameters().size(), 2);
    BOOST_CHECK_EQUAL(oac2.getParameters().size(), 2);

    BOOST_CHECK_EQUAL(oac1.getParameters()[0], std::string("p1"));
    BOOST_CHECK_EQUAL(oac1.getParameters()[1], std::string("p2"));

    BOOST_CHECK_EQUAL(oac2.getParameters()[0], std::string("p1"));
    BOOST_CHECK_EQUAL(oac2.getParameters()[1], std::string("p2"));

    BOOST_CHECK(oac1.getPredictionFunction()->getEffectExpression() == oac2.getPredictionFunction()->getEffectExpression());
    BOOST_CHECK(oac1.getStatisticalMeasure()->getExperimentCount() == oac2.getStatisticalMeasure()->getExperimentCount());
}


class OACEnvironment
{
public:
    OACEnvironment(const std::string& testName)
    {

        Ice::PropertiesPtr properties = Ice::createProperties();
        properties->setProperty("MemoryX.PriorKnowledge.ClassCollections", "ltm0331.Prior_Objects");
        properties->setProperty("MemoryX.PriorKnowledge.RelationCollections", "");
        properties->setProperty("MemoryX.CommonStorage.MongoUser", "testuser");
        properties->setProperty("MemoryX.CommonStorage.MongoPassword", "testpass");
        properties->setProperty("MemoryX.LongtermMemory.DatabaseName", "ltm0331");
        properties->setProperty("Ice.ThreadPool.Client.SizeMax", "2");
        properties->setProperty("Ice.ThreadPool.Server.SizeMax", "2");


        iceTestHelper = new IceTestHelper();
        iceTestHelper->startEnvironment();
        manager = new TestArmarXManager(testName, iceTestHelper->getCommunicator(), properties);

        using namespace memoryx;
        storage = manager->createComponentAndRun<CommonStorage, CommonStorageInterfacePrx>("MemoryX", "CommonStorage");
        priorKnowledgePrx = manager->createComponentAndRun<PriorKnowledge, PriorKnowledgeInterfacePrx>("MemoryX", "PriorKnowledge");
        ltm = manager->createComponentAndRun<LongtermMemory, LongtermMemoryInterfacePrx>("MemoryX", "LongtermMemory");
    }

    ~OACEnvironment()
    {
        manager->shutdown();
    }

    MongoTestHelper mongoTestHelper;
    memoryx::LongtermMemoryInterfacePrx ltm;
    memoryx::CommonStorageInterfacePrx storage;
    TestArmarXManagerPtr manager;
    IceTestHelperPtr iceTestHelper;
    memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;

};
using OACEnvironmentPtr = std::shared_ptr<OACEnvironment>;

class OACLibrary
{
public:
    OACLibrary()
    {

        memoryx::ObjectClassBasePtr any =  new memoryx::ObjectClass();
        any->setName("any");

        memoryx::ObjectClassBasePtr hand =  new memoryx::ObjectClass();
        hand->setName("hand");
        hand->setParentClass("any");

        memoryx::ObjectClassBasePtr cupblue =  new memoryx::ObjectClass();
        cupblue->setName("cupblue");
        cupblue->setParentClass("any");

        memoryx::ObjectClassBasePtr bowl =  new memoryx::ObjectClass();
        bowl->setName("bowl");
        bowl->setParentClass("any");



        if (!any)
        {
            ARMARX_WARNING_S << "'any' object not found in mongo";
        }

        if (!hand)
        {
            ARMARX_WARNING_S << "'hand' object not found in mongo";
        }

        if (!cupblue)
        {
            ARMARX_WARNING_S << "'cupblue' object not found in mongo";
        }

        if (!bowl)
        {
            ARMARX_WARNING_S << "'bowl' object not found in mongo";
        }

        listcup.push_back(cupblue);
        listbowl.push_back(bowl);
        listhand.push_back(hand);
        listany.push_back(any);


        memoryx::SECRelationBasePtr relationHandEmpty = new memoryx::Relations::NoConnectionRelation(listhand, listany);
        memoryx::SECObjectRelationsBasePtr preconditions = new memoryx::SECObjectRelations();
        preconditions->addRelation(relationHandEmpty);

        memoryx::SECRelationBasePtr relationObjInHand = new memoryx::Relations::TouchingRelation(listhand, listany);
        memoryx::SECObjectRelationsBasePtr effects = new memoryx::SECObjectRelations();
        effects->addRelation(relationObjInHand);


        memoryx::OacPredictionFunctionBasePtr pred = new memoryx::OacPredictionFunction();
        pred->setSECPreconditions(preconditions);
        pred->setSECEffects(effects);

        memoryx::SECRelationPair sideConstraint;
        sideConstraint.relation1 = relationHandEmpty;
        sideConstraint.relation2 = relationObjInHand;
        memoryx::SECRelationPairList sideConstraints;
        sideConstraints.push_back(sideConstraint);
        pred->setSECSideConstraints(sideConstraints);

        oacGrasp = new memoryx::Oac("TESTgrasp");
        oacGrasp->setPredictionFunction(pred);


    }


    memoryx::ObjectClassList listcup;
    memoryx::ObjectClassList listbowl;
    memoryx::ObjectClassList listhand;
    memoryx::ObjectClassList listany;

    memoryx::OacPtr oacGrasp;
};

BOOST_AUTO_TEST_CASE(testOacSECPart)
{

    OACLibrary oacLib;

    IceTestHelper iceHelper;

    //    Ice::CommunicatorPtr ic = iceHelper.getCommunicator();

    Ice::CommunicatorPtr ic = iceHelper.getCommunicator();
    armarx::ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);

    std::string serialized;

    memoryx::Oac graspingOAC;

    armarx::JSONObjectPtr jsonSerializer = new armarx::JSONObject(ic);

    jsonSerializer->reset();
    oacLib.oacGrasp->serialize(jsonSerializer);
    serialized = jsonSerializer->asString(true);
    jsonSerializer->reset();
    jsonSerializer->fromString(serialized);
    graspingOAC.deserialize(jsonSerializer);

    BOOST_CHECK_EQUAL(graspingOAC.getName(), oacLib.oacGrasp->getName());
    memoryx::SECObjectRelationsBasePtr effects = graspingOAC.getPredictionFunction()->getSECEffects();
    BOOST_CHECK_EQUAL(effects->relations.at(0)->objects1.at(0)->getName(),
                      oacLib.oacGrasp->getPredictionFunction()->getSECEffects()->relations.at(0)->objects1.at(0)->getName());

    BOOST_CHECK_EQUAL(graspingOAC.getPredictionFunction()->getSECSideConstraints().at(0).relation1->objects1.at(0)->getName(),
                      oacLib.oacGrasp->getPredictionFunction()->getSECSideConstraints().at(0).relation1->objects1.at(0)->getName());

}

//BOOST_AUTO_TEST_CASE(testOacWithMongo)
//{
//    OACEnvironmentPtr env;
//    try
//    {
//        env.reset(new OACEnvironment("testOacWithMongo"));
//    }
//    catch(std::exception &e)
//    {
//        ARMARX_WARNING_S << "Setting up environment failed - skipping test: " << e.what();

//        return;
//    }
//    OACLibrary oacLib;

//    if(env->priorKnowledgePrx->getObjectClassesSegment()->getAllEntityIds().size() < 3)
//    {
//        ARMARX_WARNING_S << "No objects in mongodb - skipping test";
//        return;
//    }
//    OacBasePtr graspingOAC = env->ltm->getOacSegment()->getOacByName(oacLib.oacGrasp->getName());
//    if(graspingOAC)
//    {
//        env->ltm->getOacSegment()->removeEntity(graspingOAC->getId());
//    }
//    env->ltm->getOacSegment()->addEntity(oacLib.oacGrasp);
//    graspingOAC = env->ltm->getOacSegment()->getOacByName(oacLib.oacGrasp->getName());
//    BOOST_CHECK(graspingOAC);

//    BOOST_CHECK_EQUAL(graspingOAC->getName(), oacLib.oacGrasp->getName());

//    BOOST_CHECK_EQUAL(graspingOAC->getPredictionFunction()->getSECEffects()->relations.at(0)->objects1.at(0)->getName(),
//                  oacLib.oacGrasp->getPredictionFunction()->getSECEffects()->relations.at(0)->objects1.at(0)->getName());
//    env->ltm->getOacSegment()->removeEntity(graspingOAC->getId());
//}



