/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Nils Adermann <naderman@naderman.de>
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE MemoryX::SEC
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>



#include <MemoryX/core/MongoTestHelper.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <MemoryX/libraries/memorytypes/entity/Oac.h>

#include <ArmarXCore/core/CoreObjectFactories.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include "../SEC/SECObjectRelations.h"
#include "../SEC/SECRelation.h"
#include "../ObjectClass.h"
#include "SECEnvironment.h"

using namespace memoryx;




//BOOST_AUTO_TEST_CASE(testPersistantObjectClassCompare)
//{
//    IceTestHelper iceHelper;
//    SECEnvironment env("SECTest");

//    PersistentObjectClassSegmentBasePrx objClassSeg = env.priorKnowledgePrx->getObjectClassesSegment();
//    ObjectClassBasePtr any =  objClassSeg->getObjectClassByName("any");
//    ObjectClassBasePtr cup =  objClassSeg->getObjectClassByName("cup");
//    ObjectClassBasePtr cupblue =  objClassSeg->getObjectClassByName("cupblue");



//        BOOST_CHECK(objClassSeg->compare(cup, cupblue) == eEqualClass);
//        BOOST_CHECK(objClassSeg->compare(cupblue, cup) == eEqualParentClass);
//        BOOST_CHECK(objClassSeg->compare(any, cupblue) == eEqualClass);
//        BOOST_CHECK(objClassSeg->compare(cupblue, any) == eEqualParentClass);


//}

BOOST_AUTO_TEST_CASE(testObjectClassCompare)
{
    ObjectClassPtr cup = new ObjectClass();
    cup->setName("cup");

    ObjectClassPtr greencup = new ObjectClass();
    greencup->setName("greencup");
    greencup->addParentClass("cup");
    greencup->addParentClass("container");

    ObjectClassPtr nesquik = new ObjectClass();
    nesquik->setName("nesquik");

    BOOST_CHECK(greencup->compare(cup) == eEqualParentClass);
    BOOST_CHECK(cup->compare(greencup) == eEqualClass);
    BOOST_CHECK(nesquik->compare(greencup) == eNotEqualClass);

}

BOOST_AUTO_TEST_CASE(testOSECRelationCompare)
{
    ObjectClassPtr cup = new ObjectClass();
    cup->setName("cup");

    ObjectClassPtr greencup = new ObjectClass();
    greencup->setName("greencup");
    greencup->addParentClass("cup");
    greencup->addParentClass("container");

    ObjectClassPtr nesquik = new ObjectClass();
    nesquik->setName("nesquik");

    SECRelationPtr rel1 = new Relations::TouchingRelation(cup, nesquik);
    SECRelationPtr rel2 = new Relations::TouchingRelation(greencup, nesquik);

    BOOST_CHECK(rel1->hasEqualObjects(rel2));
    BOOST_CHECK(rel2->hasEqualObjects(rel1) == false);
    //    BOOST_CHECK(cup->compare(greencup) == eEqualClass);
    //    BOOST_CHECK(nesquik->compare(greencup) == eNotEqualClass);

}

