/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KBMData.h"
#include <MemoryX/libraries/helpers/KinematicBezierMaps/kbm.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>


namespace memoryx
{



    KBMData::KBMData(const armarx::MatrixDoubleBasePtr& controlNet, const std::string& nodeSetName, const std::string& referenceFrameName, const std::string& robotName)
    {

        setControlNet(controlNet);
        setNodeSetName(nodeSetName);
        setReferenceFrameName(referenceFrameName);
        setRobotName(robotName);
        setName(robotName + "_" + referenceFrameName + "_" + nodeSetName);
    }

    armarx::MatrixDoubleBasePtr KBMData::getControlNet(const Ice::Current&) const
    {
        return getAttributeValue("ControlNet")->getClass<armarx::MatrixDoubleBase>();
    }

    void KBMData::setControlNet(const armarx::MatrixDoubleBasePtr& controlNet, const Ice::Current&)
    {
        putAttribute("ControlNet", controlNet);
    }

} // namespace memoryx


Ice::ObjectPtr memoryx::KBMData::ice_clone() const
{
    EntityPtr ret = new KBMData(*this);
    //    ret->deepCopy(*this);
    return ret;

}

std::string memoryx::KBMData::getReferenceFrameName(const Ice::Current&) const
{
    return getAttributeValue("ReferenceFrameName")->get<std::string>();
}

std::string memoryx::KBMData::getNodeSetName(const Ice::Current&) const
{
    return getAttributeValue("NodeSetName")->get<std::string>();
}

std::string memoryx::KBMData::getRobotName(const Ice::Current& c) const
{
    return getAttributeValue("RobotName")->get<std::string>();
}

void memoryx::KBMData::setNodeSetName(const std::string& nodeSetName, const Ice::Current& c)
{
    putAttribute("NodeSetName", nodeSetName);
}

void memoryx::KBMData::setRobotName(const std::string& robotName, const Ice::Current& c)
{
    putAttribute("RobotName", robotName);
}
void memoryx::KBMData::setReferenceFrameName(const std::string& referenceFrameName, const Ice::Current& c)
{
    putAttribute("ReferenceFrameName", referenceFrameName);
}
