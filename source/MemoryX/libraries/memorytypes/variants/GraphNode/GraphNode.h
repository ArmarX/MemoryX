/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Raphael Grimm <raphael dot grimm at kit dot edu>
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <string>

#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

namespace armarx
{
    template <class IceBaseClass, class DerivedClass> class GenericFactory;
}

namespace memoryx::VariantType
{
    const armarx::VariantTypeId GraphNode = armarx::Variant::addTypeName("::memoryx::GraphNodeBase");
}

namespace memoryx
{
    class GraphNode;
    using GraphNodePtr = IceInternal::Handle<GraphNode>;

    /**
     * @brief Implementation of the ice interface GraphNodeBase
     */
    class GraphNode:
        virtual public GraphNodeBase,
        virtual public Entity
    {
        template <class IceBaseClass, class DerivedClass>
        friend class armarx::GenericFactory;

        GraphNode();
    public:
        static const std::string GRAPH_NODE_ATTR_SCENE;

        GraphNode(const ::armarx::FramedPoseBasePtr pose, const std::string& nodeName, const std::string& scene);
        /**
         * @brief Convenience constructor for 2D pose with orientation and global frame.
         * z, roll and pitch will be zero.
         * @param x X coordinate value
         * @param y Y coordinate value
         * @param alpha Rotation around z axis
         * @param nodeName name of this node
         * @param scene Name of the scene
         */
        GraphNode(float x, float y, float alpha, const std::string& nodeName, const std::string& scene);

        ~GraphNode() override;

        //from ice interface
        ::armarx::FramedPoseBasePtr getPose(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setPose(const ::armarx::FramedPoseBasePtr& pose, const ::Ice::Current& = Ice::emptyCurrent) override;
        ::std::string getScene(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void setScene(const std::string& scene, const ::Ice::Current& = Ice::emptyCurrent) override;

        ::memoryx::EntityRefBasePtr getAdjacentNode(::Ice::Int i, const ::Ice::Current& = Ice::emptyCurrent) override;
        ::memoryx::EntityRefBasePtr getAdjacentNodeById(const std::string& nodeId, const Ice::Current& c = Ice::emptyCurrent) override;
        memoryx::GraphNodeBaseList getAdjacentNodes(const Ice::Current& c = Ice::emptyCurrent) override;

        int getOutdegree(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void addAdjacentNode(const ::memoryx::EntityRefBasePtr& newAdjacentNode, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void clearAdjacentNodes(const ::Ice::Current& = Ice::emptyCurrent) override;
        bool removeAdjacentNode(const std::string& nodeId, const Ice::Current&) override;
        float getYawAngle() const;

        Ice::ObjectPtr ice_clone() const override;
        GraphNodePtr clone(const Ice::Current& = Ice::emptyCurrent) const;

    };
}

