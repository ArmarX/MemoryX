/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Raphael Grimm <raphael dot grimm at kit dot edu>
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace memoryx
{

    /**
     * @class GraphMemorySegment
     *
     * @brief The Graph Memory Segment contains directed graphs. The graph consists of nodes with poses and edges between them.
     * The poses in the nodes and the edges are used for platform navigation. Additionally, thee nodes are used for symbolic planning.
     * One segment can contain multiple graphs. They distinguished by their *scene*-name.
     * See Slice API documentation for GraphMemorySegmentBase.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT GraphMemorySegment :
        virtual public PersistentEntitySegment,
        virtual public GraphMemorySegmentBase
    {
    public:
        GraphMemorySegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true);

        ~GraphMemorySegment() override;

        Ice::StringSeq getScenes(const Ice::Current& c = Ice::emptyCurrent) const override;

        memoryx::GraphNodeBaseList getNodesByScene(const std::string& sceneName, const Ice::Current& c = Ice::emptyCurrent) override;
        memoryx::GraphNodeBasePtr getNodeById(const std::string& entityId, const Ice::Current& c = Ice::emptyCurrent) override;
        memoryx::GraphNodeBasePtr getNodeByName(const std::string& entityName, const Ice::Current& c = Ice::emptyCurrent) override;
        memoryx::GraphNodeBaseList getAllNodes(const Ice::Current& c = Ice::emptyCurrent) override;

        void clearScene(const std::string& sceneName, const Ice::Current& c = Ice::emptyCurrent) override;

        bool removeNode(const std::string&, const Ice::Current&) override;
        bool removeEdge(const std::string& startNodeId, const std::string& endNodeId, const Ice::Current&) override;

        std::string addNode(const memoryx::GraphNodeBasePtr& node, const Ice::Current& c = Ice::emptyCurrent) override;
        bool addEdge(const std::string& fromId, const std::string& toId, const Ice::Current& c = Ice::emptyCurrent) override;

        bool hasScene(const ::std::string& sceneName, const ::Ice::Current& c = Ice::emptyCurrent) override;
        bool hasNodeWithName(const ::std::string& sceneName, const ::std::string& nodeName, const ::Ice::Current& = Ice::emptyCurrent) override;

        ::memoryx::GraphNodeBasePtr getNodeFromSceneByName(const ::std::string& sceneName, const ::std::string& nodeName, const ::Ice::Current& = Ice::emptyCurrent) override;
        //        virtual std::string getIdOfNearestNode(const ::std::string& sceneName, ::Ice::Float x, ::Ice::Float y, const ::Ice::Current& = Ice::emptyCurrent);

        ::memoryx::GraphNodeBaseList aStar(const ::std::string& idFrom, const ::std::string& idTo, const ::Ice::Current& = Ice::emptyCurrent) override;

    private:
        memoryx::GraphNodeBasePtr getNodeById(const std::string& entityId, const Ice::Current& c = Ice::emptyCurrent) const;


    };

    using GraphMemorySegmentPtr = IceInternal::Handle<GraphMemorySegment>;
}

