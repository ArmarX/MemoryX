/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Thomas von der Heyde (tvh242 at hotmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>


namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT AgentInstancesSegment :
        virtual public WorkingMemoryEntitySegment<AgentInstance>,
        virtual public AgentInstancesSegmentBase
    {
    public:

        AgentInstancesSegment(armarx::IceManagerPtr ic);

        /*!
         * Gets all entities of this segment.
         */
        AgentInstanceBaseList getAllAgentInstances(const ::Ice::Current& = Ice::emptyCurrent) const override;
        /*!
         * Gets the entity with the given ID.
         * \param id Entity ID.
         */
        AgentInstanceBasePtr getAgentInstanceById(const std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override;
        /*!
         * Gets the entity with the given name.
         * \param name Entity name.
         */
        AgentInstanceBasePtr getAgentInstanceByName(const std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override;

        armarx::FramedPoseBasePtr convertToWorldPose(const std::string& agentName, const armarx::FramedPoseBasePtr& localPose, const Ice::Current& c = Ice::emptyCurrent) const override;
        armarx::FramedPoseBasePtr convertToLocalPose(const std::string& agentName, const armarx::PoseBasePtr& worldPose, const std::string& targetFrame, const Ice::Current& c = Ice::emptyCurrent) const override;
        // EntityMemorySegmentInterface interface
        std::string addEntity(const EntityBasePtr& entity, const Ice::Current&) override;
        void updateEntity(const std::string& id, const EntityBasePtr& entity, const Ice::Current&) override;

        void removeEntity(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) override;

        void removeAllEntities(const ::Ice::Current& = Ice::emptyCurrent) override;


        std::string upsertEntity(const std::string& entityId, const EntityBasePtr& newEntity, const::Ice::Current& = Ice::emptyCurrent) override;
        std::string upsertEntityByName(const std::string& entityName, const EntityBasePtr& newEntity, const::Ice::Current& = Ice::emptyCurrent) override;
    protected:
        void setRemoteRobotPose(EntityBasePtr entity);
        armarx::IceManagerPtr ic;

    };

    using AgentInstancesSegmentPtr = IceInternal::Handle<AgentInstancesSegment>;
}

