/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/ActiveOac.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>


namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT ActiveOacMemorySegment :
        virtual public WorkingMemoryEntitySegment<ActiveOac>,
        virtual public ActiveOacMemorySegmentBase
    {
    public:
        ActiveOacMemorySegment(ObjectInstanceMemorySegmentBasePrx objectInstanceSegmentPrx, OacMemorySegmentBasePrx oacSegmentPrx):
            WorkingMemoryEntitySegment<ActiveOac>(),
            ActiveOacMemorySegmentBase(),
            objectInstanceSegmentPrx(objectInstanceSegmentPrx),
            oacSegmentPrx(oacSegmentPrx)
        {
        }

        std::string addEntity(const EntityBasePtr& entity, const ::Ice::Current& c = Ice::emptyCurrent) override
        {
            activeOac = new ActiveOacFull;
            clear();

            WorkingMemoryListenerInterfacePrx tmpListenerProxy = listenerProxy;
            listenerProxy = NULL;
            std::string id = WorkingMemoryEntitySegment<ActiveOac>::addEntity(entity, c);
            listenerProxy = tmpListenerProxy;

            ActiveOacPtr actualEntity = ActiveOacPtr::dynamicCast(getEntityById(id));
            activeOac->oac = oacSegmentPrx->getOacById(actualEntity->getOacRef()->entityId);

            ObjectInstanceList args;
            std::vector<std::string> ids = actualEntity->getArgumentInstanceIds();

            for (std::vector<std::string>::const_iterator it = ids.begin(); it != ids.end(); ++it)
            {
                ObjectInstancePtr object = ObjectInstancePtr::dynamicCast(objectInstanceSegmentPrx->getObjectInstanceById(*it));
                args.push_back(object);
            }

            activeOac->parameters = args;
            activeOac->entity = actualEntity;

            if (listenerProxy)
            {
                listenerProxy->reportEntityCreated(segmentName, actualEntity);
            }

            return id;
        }

        ActiveOacFullPtr getActiveOac(const ::Ice::Current& = Ice::emptyCurrent) override
        {
            return activeOac;
        }

        void changeState(OacExecState state, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            activeOac->entity->setState(state);
            updateEntity(activeOac->entity->getId(), activeOac->entity);
        }

    protected:
        ObjectInstanceMemorySegmentBasePrx objectInstanceSegmentPrx;
        OacMemorySegmentBasePrx oacSegmentPrx;
        ActiveOacFullPtr activeOac;
    };

    using ActiveOacMemorySegmentPtr = IceInternal::Handle<ActiveOacMemorySegment>;
}

