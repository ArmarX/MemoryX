/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryTypes
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/core/entity/EntityRef.h>
#include <ArmarXCore/core/exceptions/Exception.h>

#include <vector>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT PersistentRelationSegment :
        virtual public PersistentEntitySegment,
        virtual public PersistentRelationSegmentBase
    {
    public:
        PersistentRelationSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true) :
            PersistentEntitySegment(entityCollection, ic, useMongoIds),
            PersistentRelationSegmentBase()
        {
        }

        RelationBasePtr getRelationById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return RelationBasePtr::dynamicCast(getEntityById(id));
        }

        RelationList getRelationsByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            EntityBaseList rels = getEntitiesByAttrValue("name", name);
            RelationList result;

            for (EntityBaseList::const_iterator it = rels.begin(); it != rels.end(); ++it)
            {
                RelationBasePtr rel = RelationBasePtr::dynamicCast(*it);
                result.push_back(rel);
            }

            return result;
        }

        RelationList getRelationsBySign(bool sign, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            RelationList result;

            for (const auto& id : getAllEntityIds())
            {
                RelationBasePtr rel = RelationBasePtr::dynamicCast(getEntityById(id));

                if (rel->getSign() == sign)
                {
                    result.push_back(rel);
                }
            }

            return result;
        }

        RelationList getRelationsByEntityId(const ::std::string& entityId, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            RelationList result;

            for (const auto& id : getAllEntityIds())
            {
                RelationBasePtr rel = RelationBasePtr::dynamicCast(getEntityById(id));
                EntityRefList relEntities = rel->getEntities();

                for (EntityRefBasePtr& relEntity : relEntities)
                {
                    if (relEntity->entityId == entityId)
                    {
                        result.push_back(rel);
                        break;
                    }
                }
            }

            return result;
        }

        RelationList getRelationsByEntityRef(const EntityRefBasePtr& entityRef, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return getRelationsByEntityId(entityRef->entityId);
        }

        RelationList getRelationsByEntityRefs(const EntityRefList& entities, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return getRelationsByAttrValues(entities);
        }

        RelationBasePtr getRelationByAttrValues(const std::string& name, const EntityRefList& entities, bool sign, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            RelationBasePtr result;
            RelationList resultList = getRelationsByAttrValues(entities, name, true, sign);

            if (resultList.size() != 0)
            {
                result = resultList[0];
            }

            return result;
        }
    private:
        RelationList getRelationsByAttrValues(const EntityRefList& entities, const std::string& name = "", bool considerSign = false, bool sign = true, const ::Ice::Current& = Ice::emptyCurrent) const
        {
            RelationList result;

            for (const auto& entity : getAllEntities())
            {
                RelationBasePtr rel = RelationBasePtr::dynamicCast(entity);

                if (!name.empty() && (rel->getName() != name))
                {
                    continue;
                }

                if (considerSign && (sign != rel->getSign()))
                {
                    continue;
                }

                EntityRefList relEntities = rel->getEntities();

                bool foundArgs = true;

                for (const auto& entityRef : entities)
                {
                    foundArgs &= std::find_if(relEntities.cbegin(), relEntities.cend(), [&](const memoryx::EntityRefBasePtr & e)
                    {
                        return entityRef->equals(e);
                    }) != relEntities.cend();
                }

                if (foundArgs)
                {
                    result.push_back(rel);
                }
            }

            return result;
        }

        // RelationSegmentInterface interface
    public:
        void removeRelations(const std::string&, const Ice::Current&) override
        {
            throw armarx::LocalException("Not yet implemented");
        }
        void replaceRelations(const RelationList&, const Ice::Current&) override
        {
            throw armarx::LocalException("Not yet implemented");
        }
    };

    using PersistentRelationSegmentPtr = IceInternal::Handle<PersistentRelationSegment>;

}

