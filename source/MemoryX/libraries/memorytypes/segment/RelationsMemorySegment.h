/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/Relation.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT RelationMemorySegment :
        virtual public WorkingMemoryEntitySegment<Relation>,
        virtual public RelationMemorySegmentBase
    {
    public:
        RelationBasePtr getRelationById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override;

        RelationList getRelationsByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override;

        RelationList getRelationsBySign(bool sign, const ::Ice::Current& = Ice::emptyCurrent) const override;

        RelationList getRelationsByEntityId(const ::std::string& entityId, const ::Ice::Current& = Ice::emptyCurrent) const override;

        RelationList getRelationsByEntityRef(const EntityRefBasePtr& entityRef, const ::Ice::Current& = Ice::emptyCurrent) const override;

        RelationList getRelationsByEntityRefs(const EntityRefList& entities, const ::Ice::Current& = Ice::emptyCurrent) const override;

        RelationBasePtr getRelationByAttrValues(const std::string& name, const EntityRefList& entities, bool sign, const ::Ice::Current& = Ice::emptyCurrent) const override;

    private:
        RelationList getRelationsByAttrValues(const EntityRefList& entities, const std::string& name = "", bool considerSign = false, bool sign = true, const ::Ice::Current& = Ice::emptyCurrent) const;

        // EntityMemorySegmentInterface interface
    public:
        std::string addEntity(const EntityBasePtr& entity, const Ice::Current&) override;
        void updateEntity(const std::string& id, const EntityBasePtr& entity, const Ice::Current&) override;

        // RelationSegmentInterface interface
    public:
        void removeRelations(const std::string& name, const Ice::Current&) override;
        void replaceRelations(const RelationList& newRelations, const Ice::Current&) override;
    };

    using RelationMemorySegmentPtr = IceInternal::Handle<RelationMemorySegment>;

}

