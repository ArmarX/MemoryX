/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

namespace memoryx
{
    /**
     * The object class segment is a specialized segment of the SegmentedMemory.
     * It keeps object classes which are usually held in short term memory.
     * This segment is usually part of the working memory.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT ObjectClassMemorySegment :
        virtual public WorkingMemoryEntitySegment<ObjectClass>,
        virtual public ObjectClassMemorySegmentBase
    {
    public:
        /**
         * Creates a new object class segment.
         * Object classes are usually fetched from prior knowledge. Use the method addPriorClassWithSubclasses() in order to fetch
         * parts of the class ontology into the object classes segment.
         * Object classes in this segment use reference counting so use the methods provided in this class to add, update and remove
         * classes.
         * The ObjectClass MongoDB-collections that are accessed can be configured via PriorKnowledge config files (e.g. Prior_Objects)
         *
         * @param priorKnowledgePrx proxy to prior knowledge
         *
         */
        ObjectClassMemorySegment(const PriorKnowledgeInterfacePrx& priorKnowledgePrx) :
            WorkingMemoryEntitySegment<ObjectClass>::WorkingMemoryEntitySegment(),
            ObjectClassMemorySegmentBase::ObjectClassMemorySegmentBase()
        {
            this->priorKnowledgePrx = priorKnowledgePrx;

            // get classes from prior knowledge
            classesSegmentPrx = priorKnowledgePrx->getObjectClassesSegment();
        }

        /**
         * Fetches a class and its ontological subclasses from prior knowledge and inserts the complete
         * subtree in this segment.
         * If a class with the same name already exists in this segment, its reference count is increased.
         *
         * @param className name of the class in prior knowledge
         */
        ObjectClassList addPriorClassWithSubclasses(const std::string& className, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            ObjectClassList touchedClasses;

            // iterate through unique classes and add them to classes segment
            ObjectClassList objectClasses = classesSegmentPrx->getClassWithSubclasses(className);

            ObjectClassList::iterator iter = objectClasses.begin();

            while (iter != objectClasses.end())
            {
                ObjectClassBasePtr priorObjectClass = *iter;

                ObjectClassBasePtr wmObjectClass = addClass(priorObjectClass);
                touchedClasses.push_back(wmObjectClass);

                iter++;
            }

            return touchedClasses;
        }

        /**
         * Retrieves complete ontological tree from this segment
         *
         * @param rootClassName name of the root class
         */
        ObjectClassList getClassWithSubclasses(const std::string& rootClassName, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            // iterate through classes
            ObjectClassList relevantClasses;
            ObjectClassPtr root = ObjectClassPtr::dynamicCast(getEntityByName(rootClassName));

            if (!root)
            {
                return ObjectClassList();
            }

            relevantClasses.push_back(root);

            int index = 0;

            while (index != int(relevantClasses.size()))
            {
                ObjectClassBasePtr current = relevantClasses.at(index);

                // add children of this class
                ObjectClassList childs = getChildClasses(current->getName());

                if (childs.size() != 0)
                {
                    std::copy(childs.begin(), childs.end(), std::back_inserter(relevantClasses));
                }

                index++;
            }

            return relevantClasses;
        }

        /**
         * Update a class with the given entity. Copys reference counter to updated entity.
         *
         * @param className name of the class in prior knowledge
         * @param update entity replacing the old one
         */
        ObjectClassBasePtr updateClass(const std::string& className, const ObjectClassBasePtr& update, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            ARMARX_DEBUG_S << "updateClass(): " << className;

            if (update->getName() != className)
            {
                return NULL;
            }

            // check whether entity already present in segment
            EntityBasePtr storedObjectClass = getEntityByName(className);

            if (!storedObjectClass)
            {
                return NULL;
            }

            // copy reference counting
            std::string refCountAttrName = "referenceCounter";

            if (storedObjectClass->hasAttribute(refCountAttrName))
            {

                int counter = storedObjectClass->getAttribute(refCountAttrName)->getValue()->getInt();
                update->putAttribute(new EntityAttribute(refCountAttrName, new armarx::Variant(counter)));
            }

            // update the entity
            updateEntity(storedObjectClass->getId(), update);

            return ObjectClassBasePtr::dynamicCast(getEntityById(storedObjectClass->getId()));
        }

        /**
         * Removes a class and its ontological subclasses. The ontology is fetched from prior knowledge.
         *
         * Reference counting is used, so classes are only removed, if they are not referenced anymore.
         *
         * @param className name of the class in prior knowledge
         */
        void removePriorClassWithSubclasses(const std::string& className, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            // iterate through unique classes and add them to classes segment
            ObjectClassList objectClasses = classesSegmentPrx->getClassWithSubclasses(className);

            ObjectClassList::iterator iter = objectClasses.begin();


            while (iter != objectClasses.end())
            {
                removeClass((*iter)->getName());
                iter++;
            }
        }

        static std::string ObjectsToString(const ObjectClassList& objects)
        {
            std::string result;

            for (ObjectClassList::const_iterator it = objects.begin(); it != objects.end(); it++)
            {
                if (!result.empty())
                {
                    result += ", ";
                }

                if (*it)
                {
                    result += (*it)->getName();
                }
                else
                {
                    result += "NULL";
                }
            }

            return result;
        }
        static Ice::StringSeq ObjectsToStringList(const ObjectClassList& objects)
        {
            Ice::StringSeq result;

            for (ObjectClassList::const_iterator it = objects.begin(); it != objects.end(); it++)
            {
                if (*it)
                {
                    result.push_back((*it)->getName());
                }
                else
                {
                    result.push_back("NULL");
                }
            }

            return result;
        }

        static bool ListContainsObject(const ObjectClassList& objects, ObjectClassBasePtr object)
        {
            for (ObjectClassList::const_iterator it = objects.begin(); it != objects.end(); it++)
            {
                if ((*it)->getName() == object->getName())
                {
                    return true;
                }
            }

            return false;
        }

    private:
        ObjectClassBasePtr addClass(const ObjectClassBasePtr& objectClass)
        {
            std::string refCountAttrName = "referenceCounter";

            // check whether entity already present in segment
            EntityBasePtr storedObjectClass = getEntityByName(objectClass->getName());

            if (storedObjectClass)
            {
                // increase reference counter
                if (storedObjectClass->hasAttribute(refCountAttrName))
                {
                    int counter = storedObjectClass->getAttribute(refCountAttrName)->getValue()->getInt();
                    counter++;
                    storedObjectClass->putAttribute(new EntityAttribute(refCountAttrName, new armarx::Variant(counter)));
                }
                else
                {
                    storedObjectClass->putAttribute(new EntityAttribute(refCountAttrName, new armarx::Variant(2)));
                }
            }
            else
            {
                // add entity
                objectClass->putAttribute(new EntityAttribute(refCountAttrName, new armarx::Variant(1)));
                std::string id = addEntity(objectClass);
                storedObjectClass = getEntityById(id);
            }

            return ObjectClassBasePtr::dynamicCast(storedObjectClass);
        }

        void removeClass(const std::string& className)
        {
            std::string refCountAttrName = "referenceCounter";

            // check whether entity already present in segment
            EntityBasePtr storedObjectClass = getEntityByName(className);

            if (storedObjectClass)
            {
                // storedObjectClass reference counter
                if (storedObjectClass->hasAttribute(refCountAttrName))
                {

                    int counter = storedObjectClass->getAttribute(refCountAttrName)->getValue()->getInt();
                    counter--;

                    if (counter != 0)
                    {
                        storedObjectClass->putAttribute(new EntityAttribute(refCountAttrName, new armarx::Variant(counter)));
                    }
                    else
                    {
                        // remove entity
                        ARMARX_INFO_S << "Removing object class " << storedObjectClass->getName();
                        removeEntity(storedObjectClass->getId());
                    }
                }
            }
        }

        ObjectClassList getChildClasses(const std::string& parentClassName)
        {
            ObjectClassList result;
            EntityBaseList children = getEntitiesByAttrValue("parentClasses", parentClassName);

            for (EntityBaseList::const_iterator it = children.begin(); it != children.end(); ++it)
            {
                result.push_back(ObjectClassBasePtr::dynamicCast(*it));
            }

            return result;
        }

        PriorKnowledgeInterfacePrx priorKnowledgePrx;
        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx;
    };

    using ObjectClassMemorySegmentPtr = IceInternal::Handle<ObjectClassMemorySegment>;



}

