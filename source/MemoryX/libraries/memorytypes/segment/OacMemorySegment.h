/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <MemoryX/libraries/memorytypes/entity/Oac.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include "SemanticEventChainSegment.h"

#include <vector>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT OacMemorySegment :
        virtual public PersistentEntitySegment,
        virtual public OacMemorySegmentBase
    {
    public:
        OacMemorySegment(PersistentObjectClassSegmentBasePrx objClassSegment, CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true) :
            PersistentEntitySegment(entityCollection, ic, useMongoIds), OacMemorySegmentBase(),
            objClassSegment(objClassSegment)
        {
        }

        OacBasePtr getOacById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return completeOac(OacBasePtr::dynamicCast(getEntityById(id)));
        }

        OacBasePtr getOacByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return completeOac(OacBasePtr::dynamicCast(getEntityByName(name)));
        }

        OacBaseList getAll(const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            OacBaseList result;

            EntityIdList entityIdList = getAllEntityIds();

            for (EntityIdList::const_iterator it = entityIdList.begin(); it != entityIdList.end(); ++it)
            {
                OacBasePtr oac = getOacById(*it);

                if (oac)
                {
                    result.push_back(oac);
                }
            }

            return result;
        }

    protected:

        /*!
         * \brief replaceObjectDummies checks the objects class in the SEC part of an OAC
         *  and completes it with the real object class entities from the
         *  PersistentObjectClassSegment.
         * \param oac OAC in which the object should be replaced
         * \return Returns same instance as the parameter oac for convenience.
         */
        OacBasePtr replaceObjectDummies(OacBasePtr oac) const;

        /*!
         * \brief completeOac checks the data of an OAC and completes it with
         *  data from other segments (e.g: object with only the name are fetched
         *  from the PersistentObjectClassSegment.
         * \param oac OAC that should be completed.
         * \return Returns same instance as the parameter oac for convenience.
         */
        OacBasePtr completeOac(OacBasePtr oac) const;
        PersistentObjectClassSegmentBasePrx objClassSegment ;

    };

    using OacMemorySegmentPtr = IceInternal::Handle<OacMemorySegment>;

    OacBasePtr OacMemorySegment::completeOac(OacBasePtr oac) const
    {
        return replaceObjectDummies(oac);
    }

    OacBasePtr OacMemorySegment::replaceObjectDummies(OacBasePtr oac) const
    {
        if (!oac)
        {
            return NULL;
        }

        OacPtr oacCast = OacPtr::dynamicCast(oac);

        if (!oacCast)
        {
            throw IceUtil::NullHandleException("Could not cast OacBase into Oac Ptr", 0);
        }

        if (!objClassSegment)
        {
            throw armarx::LocalException("PersistentObjectClassSegmentBaseProxy is NULL");
        }

        SECObjectRelationsBasePtr pre = oacCast->getPredictionFunction()->getSECPreconditions();
        SECObjectRelationsBasePtr post = oacCast->getPredictionFunction()->getSECEffects();

        if (pre)
        {
            SemanticEventChainSegment::ReplaceObjectDummies(pre, objClassSegment);
        }

        if (post)
        {
            SemanticEventChainSegment::ReplaceObjectDummies(post, objClassSegment);
        }

        return oacCast;
    }

}

