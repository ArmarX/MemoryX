/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Thomas von der Heyde (tvh242 at hotmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

namespace memoryx
{
    /*!
     * WorldStateSegment stores entities of type ObjectInstance corresponding to environmental entities.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT WorldStateSegment :
        virtual public WorkingMemoryEntitySegment<ObjectInstance>,
        virtual public WorldStateSegmentBase
    {
    public:
        WorldStateSegment();

        /*!
         * Gets the entity with the given ID.
         * \param id Entity ID.
         */
        ObjectInstanceBasePtr getWorldInstanceById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override;
        /*!
         * Gets the entity with the given name.
         * \param name Entity name.
         */
        ObjectInstanceBasePtr getWorldInstanceByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override;
        /*!
         * Gets the entities having the given class.
         * \param className Class name.
         */
        ObjectInstanceList getWorldInstancesByClass(const ::std::string& className, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        /*!
         * Gets the entities having a class in the given class list.
         * \param classList List of class names.
         */
        ObjectInstanceList getWorldInstancesByClassList(const NameList& classList, const ::Ice::Current& = Ice::emptyCurrent) const override;

    private:

    };

    using WorldStateSegmentPtr = IceInternal::Handle<WorldStateSegment>;
}

