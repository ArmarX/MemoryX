/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryTypes
* @author     Thomas von der Heyde (tvh242 at hotmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT PersistentAgentInstancesSegment :
        virtual public PersistentEntitySegment,
        virtual public PersistentAgentInstancesSegmentBase
    {
    public:

        PersistentAgentInstancesSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true) :
            PersistentEntitySegment(entityCollection, ic, useMongoIds), PersistentAgentInstancesSegmentBase()
        {
        }

        AgentInstanceBasePtr getAgentInstanceById(const std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const
        {
            AgentInstanceBasePtr res = AgentInstanceBasePtr::dynamicCast(getEntityById(id));

            if (!res)
            {
                ARMARX_WARNING_S << "Entity with id " << id << " is not of type AgentInstance!" << std::endl;
            }

            return res;
        };

        AgentInstanceBasePtr getAgentInstanceByName(const std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const
        {
            AgentInstanceBasePtr res = AgentInstanceBasePtr::dynamicCast(getEntityByName(name));

            if (!res)
            {
                ARMARX_WARNING_S << "Entity with name " << name << " is not of type AgentEntity!" << std::endl;
            }

            return res;
        };
    };

    using PersistentAgentInstancesSegmentPtr = IceInternal::Handle<PersistentAgentInstancesSegment>;

}

