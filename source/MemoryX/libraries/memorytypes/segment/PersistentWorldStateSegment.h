/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryTypes
* @author     Thomas von der Heyde (tvh242 at hotmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT PersistentWorldStateSegment :
        virtual public PersistentEntitySegment,
        virtual public PersistentWorldStateSegmentBase
    {
    public:

        PersistentWorldStateSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true) :
            PersistentEntitySegment(entityCollection, ic, useMongoIds), PersistentWorldStateSegmentBase()
        {
        }

        ObjectInstanceBasePtr getWorldInstanceById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const
        {
            ObjectInstanceBasePtr res = ObjectInstanceBasePtr::dynamicCast(getEntityById(id));

            if (!res)
            {
                ARMARX_WARNING_S << "Entity with id " << id << " is not of type ObjectInstance!" << std::endl;
            }

            return res;
        }

        ObjectInstanceBasePtr getWorldInstanceByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const
        {
            ObjectInstanceBasePtr res = ObjectInstanceBasePtr::dynamicCast(getEntityByName(name));

            if (!res)
            {
                ARMARX_WARNING_S << "Entity with name " << name << " is not of type ObjectInstance!" << std::endl;
            }

            return res;
        }

        ObjectInstanceList getWorldInstancesByClass(const ::std::string& className, const ::Ice::Current& c = Ice::emptyCurrent) const
        {
            NameList classList;
            classList.push_back(className);
            return getWorldInstancesByClassList(classList, c);
        }

        ObjectInstanceList getWorldInstancesByClassList(const NameList& classList, const ::Ice::Current& c = Ice::emptyCurrent) const
        {
            ObjectInstanceList result;
            armarx::ScopedSharedLockPtr lock(getReadLock(c));


            for (EntityMap::const_iterator it = entityMap.begin(); it != entityMap.end(); ++it)
            {
                ObjectInstanceBasePtr inst = ObjectInstanceBasePtr::dynamicCast(it->second);
                ClassProbabilityMap instClasses = inst->getClasses();

                for (NameList::const_iterator itCls = classList.begin(); itCls != classList.end(); ++itCls)
                {
                    if (instClasses.count(*itCls))
                    {
                        result.push_back(inst);
                        break;
                    }
                }
            }

            return result;
        }
    };

    using PersistentWorldStateSegmentPtr = IceInternal::Handle<PersistentWorldStateSegment>;

}

