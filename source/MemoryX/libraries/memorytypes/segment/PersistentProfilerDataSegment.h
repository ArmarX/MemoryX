/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::WorkingMemory
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../entity/profiler/ProfilerTransition.h"

#include <MemoryX/core/memory/PersistentEntitySegment.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT PersistentProfilerDataSegment :
        virtual public PersistentEntitySegment,
        virtual public PersistentProfilerDataSegmentBase
    {
    public:
        PersistentProfilerDataSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true);

        ProfilerEntityBaseList getProfilerDataEntities(const ::Ice::Current& = Ice::emptyCurrent) const override;
        ProfilerEventBaseList getProfilerEventEntities(const Ice::Current& = Ice::emptyCurrent) const override;
        ProfilerTransitionBaseList getProfilerTransitionEntities(const Ice::Current& = Ice::emptyCurrent) const override;
        ProfilerMemorySnapshotBaseList getProfilerMemorySnapshotEntities(const Ice::Current& = Ice::emptyCurrent) const override;
        ProfilerProcessBaseList getProfilerProcessCpuUsageEntities(const Ice::Current& = Ice::emptyCurrent) const override;
        ProfilerMemoryUsageBaseList getProfilerMemoryUsageBaseEntities(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief saveUniqueMemorySnapshot checks if \p memorySnaphsot already exists in the segment and creates it otherwise.
         * @param memorySnapshot
         * @return the Id of the existing or newly created Entity
         */
        std::string saveUniqueMemorySnapshot(const ProfilerMemorySnapshotBasePtr& memorySnapshot, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @brief saveOrUpdateTransition if the ProfilerTransition already exists, the count() of it is increased, otherwise a new Entity is created in the segment.
         * @param transition
         * @return the Id of the existing or newly created Entity
         */
        std::string saveOrUpdateTransition(const ProfilerTransitionBasePtr& transition, const Ice::Current& = Ice::emptyCurrent) override;

        template<typename T>
        static std::string GetEntityIdFromList(const EntityBasePtr& entity, const std::vector<T>& entities);
    };

    using PersistentProfilerDataSegmentPtr = IceInternal::Handle<PersistentProfilerDataSegment>;
}

