/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::WorkingMemory
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PersistentProfilerDataSegment.h"

#include "../entity/profiler/ProfilerMemorySnapshot.h"


namespace memoryx
{
    PersistentProfilerDataSegment::PersistentProfilerDataSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds) :
        PersistentEntitySegment(entityCollection, ic, useMongoIds),
        PersistentProfilerDataSegmentBase()
    {
    }


    ProfilerEntityBaseList PersistentProfilerDataSegment::getProfilerDataEntities(const Ice::Current&) const
    {
        ProfilerEntityBaseList entities;

        for (const auto& entity : getAllEntities())
        {
            if (entity->ice_isA(ProfilerEntityBase::ice_staticId()))
            {
                entities.push_back(ProfilerEntityBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }


    ProfilerEventBaseList memoryx::PersistentProfilerDataSegment::getProfilerEventEntities(const Ice::Current&) const
    {
        ProfilerEventBaseList entities;

        for (const auto& entity : getAllEntities())
        {
            if (entity->ice_isA(ProfilerEventBase::ice_staticId()))
            {
                entities.push_back(ProfilerEventBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }

    ProfilerTransitionBaseList memoryx::PersistentProfilerDataSegment::getProfilerTransitionEntities(const Ice::Current&) const
    {
        ProfilerTransitionBaseList entities;

        for (const auto& entity : getAllEntities())
        {
            if (entity->ice_isA(ProfilerTransitionBase::ice_staticId()))
            {
                entities.push_back(ProfilerTransitionBasePtr::dynamicCast(entity));
            }
        }
        return entities;
    }

    ProfilerMemorySnapshotBaseList PersistentProfilerDataSegment::getProfilerMemorySnapshotEntities(const Ice::Current&) const
    {
        ProfilerMemorySnapshotBaseList entities;

        for (const auto& entity : getAllEntities())
        {
            if (entity->ice_isA(ProfilerMemorySnapshotBase::ice_staticId()))
            {
                entities.push_back(ProfilerMemorySnapshotBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }

    ProfilerProcessBaseList PersistentProfilerDataSegment::getProfilerProcessCpuUsageEntities(const Ice::Current&) const
    {
        ProfilerProcessBaseList entities;

        for (const auto& entity : getAllEntities())
        {
            if (entity->ice_isA(ProfilerProcessBase::ice_staticId()))
            {
                entities.push_back(ProfilerProcessBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }

    ProfilerMemoryUsageBaseList PersistentProfilerDataSegment::getProfilerMemoryUsageBaseEntities(const Ice::Current&) const
    {
        ProfilerMemoryUsageBaseList entities;

        for (const auto& entity : getAllEntities())
        {
            if (entity->ice_isA(ProfilerMemoryUsageBase::ice_staticId()))
            {
                entities.push_back(ProfilerMemoryUsageBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }


    std::string PersistentProfilerDataSegment::saveUniqueMemorySnapshot(const ProfilerMemorySnapshotBasePtr& memorySnapshot, const Ice::Current&)
    {
        EntityBaseList entities = getEntitiesByAttrValue(ProfilerMemorySnapshot::ATTRIBUTE_STATENAME, memorySnapshot->getName());
        // check if entity exists
        std::string entityId = PersistentProfilerDataSegment::GetEntityIdFromList<EntityBasePtr>(memorySnapshot, entities);
        // if no Id was found, the Entity must be created
        if (entityId.empty())
        {
            entityId = addEntity(memorySnapshot);
        }
        return entityId;
    }


    std::string PersistentProfilerDataSegment::saveOrUpdateTransition(const ProfilerTransitionBasePtr& transition, const Ice::Current&)
    {
        ProfilerTransitionBaseList transitionEntities = getProfilerTransitionEntities();
        // check if transition exists
        std::string entityId = PersistentProfilerDataSegment::GetEntityIdFromList<ProfilerTransitionBasePtr>(transition, transitionEntities);
        if (entityId.empty())
        {
            // create Entity if no Id was found
            entityId = addEntity(transition);
        }
        else
        {
            // if (transition.exists) -> inc counter;
            ProfilerTransitionBasePtr entity = ProfilerTransitionBasePtr::dynamicCast(getEntityById(entityId));
            entity->setCount(entity->getCount() + 1);
            updateEntity(entityId, entity);
        }
        return entityId;
    }


    template<typename T>
    std::string PersistentProfilerDataSegment::GetEntityIdFromList(const EntityBasePtr& entity, const std::vector<T>& entities)
    {
        std::string entityId("");
        if (entities.empty())
        {
            return entityId;
        }

        auto entityIterator = std::find_if(entities.begin(), entities.end(), [&entity](EntityBasePtr current)
        {
            return entity->equalsAttributes(current);
        });
        if (entityIterator != entities.end())
        {
            entityId = (*entityIterator)->getId();
        }
        return entityId;
    }
}
