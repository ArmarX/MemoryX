/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/core/MemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

namespace memoryx
{
    using SegmentMap = std::map<std::string, std::string>;

    /*!
     * \brief The WorkingMemorySnapshot class handles snapshot IO methods.
     *
     *  Snapshots can be loaded and restored from the database.
     *
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT WorkingMemorySnapshot :
        virtual public WorkingMemorySnapshotInterface
    {
    public:
        WorkingMemorySnapshot(const std::string& name, Ice::CommunicatorPtr ic, const DatabaseInterfacePrx& databasePrx);

        // implementation of WorkingMemorySnapshotInterface methods
        PersistentEntitySegmentBasePrx addSegment(const ::std::string& segName, const ::Ice::Current& = Ice::emptyCurrent) override;
        PersistentEntitySegmentBasePrx getSegment(const ::std::string& segName, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool hasSegment(const ::std::string& segName, const ::Ice::Current& = Ice::emptyCurrent) override;
        NameList getSegmentNames(const ::Ice::Current& = Ice::emptyCurrent) override;
        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;

        /*!
         * \brief Save all segments with all entities of the WorkingMemory to this snapshot.
         * \param workingMemory The WorkingMemory to save.
         */
        void saveWorkingMemory(const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current& = Ice::emptyCurrent) override;
        void saveWorkingMemorySubset(const AbstractWorkingMemoryInterfacePrx& workingMemory, const Ice::StringSeq& entityIdList, const ::Ice::Current& = Ice::emptyCurrent) override;

        /*!
         * \brief Restore all segments with all entities of this snapshot to the WorkingMemory.
         * \param workingMemory The WorkingMemory to restore.
         */
        void restoreWorkingMemory(const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current& = Ice::emptyCurrent) override;

    public:
        Ice::Identity getIceId() const;
        SegmentMap getSegmentAndCollectionNames() const;

    public: // serialization.
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override;

    private:
        std::string name;
        SegmentMap segmentNames;

        Ice::CommunicatorPtr ic;
        const DatabaseInterfacePrx& databasePrx;
        Ice::Identity iceId;
        std::map<Ice::Identity, PersistentEntitySegmentBasePtr> openedSegments;

        std::string getSegmentCollectionName(const std::string& segName);
        PersistentEntitySegmentPtr getSegmentPtr(const std::string& segName);
        PersistentEntitySegmentPtr addSegmentPtr(const std::string& segName);
        void addSegmentToMap(const std::string& segName);
    };

}

