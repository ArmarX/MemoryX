/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/components/LongtermMemory/LongtermMemory.h>
#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/components/CommonStorage/CommonStorage.h>
#include <MemoryX/components/WorkingMemory/WorkingMemory.h>
#include <MemoryX/libraries/updater/ObjectLocalization/ObjectLocalizationMemoryUpdater.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <MemoryX/core/MongoTestHelper.h>
#include <ArmarXCore/core/application/Application.h>

class LongtermMemoryEnvironment
{
public:
    LongtermMemoryEnvironment(const std::string& testName)
    {

        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        properties->setProperty("MemoryX.PriorKnowledge.ClassCollections", "testdb.Prior_Objects");
        properties->setProperty("MemoryX.PriorKnowledge.RelationCollections", "");
        properties->setProperty("MemoryX.CommonStorage.MongoUser", "testuser");
        properties->setProperty("MemoryX.CommonStorage.MongoPassword", "testpass");
        properties->setProperty("Ice.ThreadPool.Client.SizeMax", "2");
        properties->setProperty("Ice.ThreadPool.Server.SizeMax", "2");
        properties->setProperty("MemoryX.LongtermMemory.DatabaseName", "testdb");


        iceTestHelper = new IceTestHelper();
        iceTestHelper->startEnvironment();
        manager = new TestArmarXManager(testName, iceTestHelper->getCommunicator(), properties);

        using namespace memoryx;
        storage = manager->createComponentAndRun<CommonStorage, CommonStorageInterfacePrx>("MemoryX", "CommonStorage");
        priorKnowledgePrx = manager->createComponentAndRun<PriorKnowledge, PriorKnowledgeInterfacePrx>("MemoryX", "PriorKnowledge");
        ltm = manager->createComponentAndRun<LongtermMemory, LongtermMemoryInterfacePrx>("MemoryX", "LongtermMemory");
        wm = manager->createComponentAndRun<WorkingMemory, WorkingMemoryInterfacePrx>("MemoryX", "WorkingMemory");
    }

    MongoTestHelper mongoTestHelper;
    memoryx::CommonStorageInterfacePrx storage;
    TestArmarXManagerPtr manager;
    IceTestHelperPtr iceTestHelper;
    memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;
    memoryx::LongtermMemoryInterfacePrx ltm;
    memoryx::WorkingMemoryInterfacePrx wm;

};

using LongtermMemoryEnvironmentPtr = std::shared_ptr<LongtermMemoryEnvironment>;

