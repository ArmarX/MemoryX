/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>

#include <MemoryX/core/MongoSerializer.h>
#include <MemoryX/core/memory/SegmentedMemory.h>

namespace memoryx
{

    class AbstractLongtermMemoryPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        AbstractLongtermMemoryPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("DatabaseName", "Mongo database to store LTM data");
        }
    };

    class ARMARXCOMPONENT_IMPORT_EXPORT AbstractLongtermMemory :
        virtual public LongtermMemoryInterface,
        virtual public SegmentedMemory,
        virtual public armarx::Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "AbstractLongtermMemory";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;

        Ice::ObjectAdapterPtr getObjectAdapter() const override
        {
            return Component::getObjectAdapter();
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new AbstractLongtermMemoryPropertyDefinitions(
                           getConfigIdentifier()));
        }

    protected:
        CommonStorageInterfacePrx storagePrx;
        Ice::CommunicatorPtr ic;
        MongoSerializerPtr dbSerializer;

        std::string dbName;

        // subclass hooks
        virtual void onInitLongtermMemory() = 0;
        virtual void onConnectLongtermMemory() = 0;
    };

    using AbstractLongtermMemoryPtr = IceUtil::Handle<AbstractLongtermMemory>;
}

