/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "WorkingMemorySnapshotListSegment.h"
#include "WorkingMemorySnapshot.h"

#include <Ice/ObjectAdapter.h>

#include <ArmarXCore/core/exceptions/Exception.h>

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
//#include <MemoryX/updater/ObjectLocalization/MemoryXUpdaterObjectFactories.h>
//#include <ArmarXCore/observers/ObserverObjectFactories.h>

namespace memoryx
{
    const std::string SNAPSHOT_NAME_FIELD = "name";

    WorkingMemorySnapshotListSegment::WorkingMemorySnapshotListSegment(const DatabaseInterfacePrx& databasePrx, const CollectionInterfacePrx& collection,
            Ice::CommunicatorPtr ic) :
        databasePrx(databasePrx),
        snapshotListCollection(collection),
        ic(ic)
    {
        dbSerializer = new MongoSerializer(ic, true);
        snapshotListCollection->ensureIndex(SNAPSHOT_NAME_FIELD, true);
    }

    WorkingMemorySnapshotListSegment::~WorkingMemorySnapshotListSegment()
    {
    }

    WorkingMemorySnapshotInterfacePrx WorkingMemorySnapshotListSegment::createSnapshot(const std::string& name, const AbstractWorkingMemoryInterfacePrx& workingMemory,
            const Ice::Current& c)
    {
        removeSnapshot(name);
        WorkingMemorySnapshotPtr snapshot = new WorkingMemorySnapshot(name, ic, databasePrx);

        // store all WM segments
        snapshot->saveWorkingMemory(workingMemory);

        const DBStorableData dbSnapshot = dbSerializer->serialize(snapshot);
        snapshotListCollection->saveWithUserKey(dbSnapshot, SNAPSHOT_NAME_FIELD);
        return createSnapshotProxy(snapshot, c);
    }

    WorkingMemorySnapshotInterfacePrx WorkingMemorySnapshotListSegment::createSubsetSnapshot(const std::string& name, const AbstractWorkingMemoryInterfacePrx& workingMemory, const Ice::StringSeq& entityIdList, const Ice::Current& c)
    {
        removeSnapshot(name);
        WorkingMemorySnapshotPtr snapshot = new WorkingMemorySnapshot(name, ic, databasePrx);

        // store all WM segments
        snapshot->saveWorkingMemorySubset(workingMemory, entityIdList);

        const DBStorableData dbSnapshot = dbSerializer->serialize(snapshot);
        snapshotListCollection->saveWithUserKey(dbSnapshot, SNAPSHOT_NAME_FIELD);
        return createSnapshotProxy(snapshot, c);
    }

    WorkingMemorySnapshotInterfacePrx WorkingMemorySnapshotListSegment::openSnapshot(const std::string& name, const Ice::Current& c)
    {
        WorkingMemorySnapshotPtr snapshot = findSnapshot(name);

        if (snapshot)
        {
            return createSnapshotProxy(snapshot, c);
        }
        else
            throw SnapshotNotFoundException("Snapshot not found, check that corresponding record exists in " +
                                            snapshotListCollection->getNS() + " collection", name);
    }

    void WorkingMemorySnapshotListSegment::closeSnapshot(const WorkingMemorySnapshotInterfacePrx& snapshot, const Ice::Current&)
    {
        SnapshotMap::iterator itSnap = openedSnapshots.find(snapshot->ice_getIdentity());

        if (itSnap != openedSnapshots.end())
        {
            openedSnapshots.erase(itSnap);
        }
    }

    void WorkingMemorySnapshotListSegment::loadSnapshot(const std::string& name, const AbstractWorkingMemoryInterfacePrx& workingMemory,
            const Ice::Current& c)
    {
        try
        {
            WorkingMemorySnapshotPtr snapshot = findSnapshot(name);

            if (snapshot)
            {
                snapshot->restoreWorkingMemory(workingMemory);
            }
            else
            {
                throw SnapshotNotFoundException("SnapshotNotFound", name);
            }
        }
        catch (...)
        {
            armarx::handleExceptions();
        }
    }


    bool WorkingMemorySnapshotListSegment::removeSnapshot(const std::string& name, const Ice::Current&)
    {
        WorkingMemorySnapshotPtr snapshot = findSnapshot(name);

        if (snapshot)
        {
            snapshot->clear();
            snapshotListCollection->removeByFieldValue(SNAPSHOT_NAME_FIELD, name);
            return true;
        }
        else
        {
            return false;
        }
    }

    NameList WorkingMemorySnapshotListSegment::getSnapshotNames(const ::Ice::Current&) const
    {
        return snapshotListCollection->findAllFieldValues(SNAPSHOT_NAME_FIELD);
    }

    WorkingMemorySnapshotInterfacePrx WorkingMemorySnapshotListSegment::createSnapshotProxy(const WorkingMemorySnapshotPtr& snapshot, const Ice::Current& c)
    {
        Ice::Identity snapId = snapshot->getIceId();
        openedSnapshots[snapId] = snapshot;
        Ice::ObjectPrx node = c.adapter->add(snapshot, snapId);
        return WorkingMemorySnapshotInterfacePrx::uncheckedCast(node);
    }

    WorkingMemorySnapshotPtr WorkingMemorySnapshotListSegment::findSnapshot(const std::string& name)
    {
        DBStorableData dbSnapshot = snapshotListCollection->findOneByFieldValue(SNAPSHOT_NAME_FIELD, name);

        if (!dbSnapshot.JSON.empty())
        {
            WorkingMemorySnapshotPtr snapshot = new WorkingMemorySnapshot(name, ic, databasePrx);
            dbSerializer->deserialize(dbSnapshot, snapshot);
            return snapshot;
        }
        else
        {
            return WorkingMemorySnapshotPtr();
        }
    }

    Ice::Identity WorkingMemorySnapshotListSegment::getIceId(const Ice::Current&) const
    {
        Ice::Identity id;
        id.name = (parentMemory ? parentMemory->getMemoryName() : "") + "_" + segmentName;
        return id;
    }

    Ice::Int WorkingMemorySnapshotListSegment::size(const ::Ice::Current&) const
    {
        return snapshotListCollection->count();
    }

    void WorkingMemorySnapshotListSegment::clear(const ::Ice::Current&)
    {
        const NameList snapshotNames = getSnapshotNames();

        for (NameList::const_iterator it = snapshotNames.begin(); it != snapshotNames.end(); ++it)
        {
            removeSnapshot(*it);
        }
    }

    void WorkingMemorySnapshotListSegment::print(const ::Ice::Current&) const
    {
        // TODO implement
    }
}
