/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "WorkingMemorySnapshot.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <Ice/ObjectAdapter.h>
#include <IceUtil/UUID.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <MemoryX/interface/workingmemory/WorkingMemoryListenerInterface.h>

namespace memoryx
{
    WorkingMemorySnapshot::WorkingMemorySnapshot(const std::string& name, Ice::CommunicatorPtr ic, const DatabaseInterfacePrx& databasePrx) :
        name(name), ic(ic),
        databasePrx(databasePrx)
    {
        iceId.name = IceUtil::generateUUID();
    }

    PersistentEntitySegmentBasePrx WorkingMemorySnapshot::addSegment(const std::string& segName, const ::Ice::Current& c)
    {
        addSegmentToMap(segName);
        return getSegment(segName, c);
    }

    PersistentEntitySegmentPtr WorkingMemorySnapshot::addSegmentPtr(const std::string& segName)
    {
        addSegmentToMap(segName);
        return getSegmentPtr(segName);
    }

    void WorkingMemorySnapshot::addSegmentToMap(const std::string& segName)
    {
        segmentNames[segName] = getSegmentCollectionName(segName);
    }

    PersistentEntitySegmentBasePrx WorkingMemorySnapshot::getSegment(const ::std::string& segName, const ::Ice::Current& c)
    {
        PersistentEntitySegmentPtr segment = getSegmentPtr(segName);

        if (segment)
        {
            Ice::Identity segId = segment->getIceId();
            segId.name += "_" + IceUtil::generateUUID();

            openedSegments[segId] = segment;
            ARMARX_CHECK_EXPRESSION(c.adapter);
            Ice::ObjectPrx node = c.adapter->add(segment, segId);
            return PersistentEntitySegmentBasePrx::uncheckedCast(node);
        }
        else
        {
            return PersistentEntitySegmentBasePrx();
        }
    }

    PersistentEntitySegmentPtr WorkingMemorySnapshot::getSegmentPtr(const ::std::string& segName)
    {
        SegmentMap::const_iterator itSeg = segmentNames.find(segName);

        if (itSeg != segmentNames.end())
        {
            ARMARX_CHECK_EXPRESSION(databasePrx);
            CollectionInterfacePrx segmentColl = databasePrx->requestCollection(itSeg->second);

            if (segmentColl)
            {
                PersistentEntitySegmentBasePtr segment = new PersistentEntitySegment(segmentColl, ic, false);
                segment->setSegmentName(segName);
                return PersistentEntitySegmentPtr::dynamicCast(segment);
            }
            else
            {
                return PersistentEntitySegmentPtr();
            }
        }
        else
        {
            return PersistentEntitySegmentPtr();
        }
    }


    bool WorkingMemorySnapshot::hasSegment(const ::std::string& segName, const ::Ice::Current&)
    {
        return segmentNames.count(segName);
    }

    NameList WorkingMemorySnapshot::getSegmentNames(const ::Ice::Current&)
    {
        NameList result;

        for (SegmentMap::const_iterator it = segmentNames.begin(); it != segmentNames.end(); ++it)
        {
            result.push_back(it->first);
        }

        return result;
    }

    void WorkingMemorySnapshot::clear(const ::Ice::Current&)
    {
        for (SegmentMap::const_iterator it = segmentNames.begin(); it != segmentNames.end(); ++it)
        {
            databasePrx->dropCollection(it->second);
        }

        segmentNames.clear();
    }

    SegmentMap WorkingMemorySnapshot::getSegmentAndCollectionNames() const
    {
        return segmentNames;
    }

    void WorkingMemorySnapshot::saveWorkingMemory(const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current&)
    {
        NameList wmSegNames = workingMemory->getSegmentNames();

        for (NameList::const_iterator itSeg = wmSegNames.begin(); itSeg != wmSegNames.end(); ++itSeg)
        {
            WorkingMemoryEntitySegmentBasePrx wmSegment =  WorkingMemoryEntitySegmentBasePrx::uncheckedCast(workingMemory->getSegment(*itSeg));

            if (wmSegment)
            {
                PersistentEntitySegmentBasePtr snapSegment = addSegmentPtr(*itSeg);
                EntityIdList ids = wmSegment->getAllEntityIds();

                for (EntityIdList::const_iterator itEntity = ids.begin(); itEntity != ids.end(); ++itEntity)
                {
                    snapSegment->addEntity(wmSegment->getEntityById(*itEntity));
                }
            }
        }
    }

    void WorkingMemorySnapshot::saveWorkingMemorySubset(const AbstractWorkingMemoryInterfacePrx& workingMemory, const Ice::StringSeq& entityIdList, const Ice::Current&)
    {
        NameList wmSegNames = workingMemory->getSegmentNames();
        auto sortedEntities = entityIdList;
        std::sort(sortedEntities.begin(), sortedEntities.end());
        auto newEnd = std::unique(sortedEntities.begin(), sortedEntities.end());
        sortedEntities.erase(newEnd, sortedEntities.end());

        for (const auto& segName : wmSegNames)
        {
            WorkingMemoryEntitySegmentBasePrx wmSegment =  WorkingMemoryEntitySegmentBasePrx::uncheckedCast(workingMemory->getSegment(segName));

            if (wmSegment)
            {
                PersistentEntitySegmentBasePtr snapSegment = addSegmentPtr(segName);
                EntityIdList ids = wmSegment->getAllEntityIds();
                std::sort(ids.begin(), ids.end());
                Ice::StringSeq intersectionList(ids.size());
                auto intersectionEnd = std::set_intersection(ids.begin(), ids.end(),
                                       sortedEntities.begin(), sortedEntities.end(),
                                       intersectionList.begin());
                intersectionList.resize(intersectionEnd - intersectionList.begin());
                auto newEnd = std::unique(intersectionList.begin(), intersectionList.end());
                intersectionList.erase(newEnd, intersectionList.end());

                for (EntityIdList::const_iterator itEntity = intersectionList.begin(); itEntity != intersectionList.end(); ++itEntity)
                {
                    snapSegment->addEntity(wmSegment->getEntityById(*itEntity));
                }
            }
        }
    }

    void WorkingMemorySnapshot::restoreWorkingMemory(const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current&)
    {
        std::set<WorkingMemoryListenerInterfacePrx> listeners;
        for (SegmentMap::const_iterator itSeg = segmentNames.begin(); itSeg != segmentNames.end(); ++itSeg)
        {
            PersistentEntitySegmentBasePtr snapSegment = getSegmentPtr(itSeg->first);
            WorkingMemoryEntitySegmentBasePrx wmSegment = WorkingMemoryEntitySegmentBasePrx::uncheckedCast(workingMemory->getSegment(itSeg->first));

            if (wmSegment)
            {
                //            wmSegment->clear();
                EntityIdList ids = snapSegment->getAllEntityIds();

                for (EntityIdList::const_iterator itEntity = ids.begin(); itEntity != ids.end(); ++itEntity)
                {
                    //                std::cout << "Restoring entity, id = " << *itEntity << std::endl;
                    wmSegment->addEntity(snapSegment->getEntityById(*itEntity));
                }

                ARMARX_INFO << "Loaded " << wmSegment->size() << " elements into segment " << itSeg->first;

                WorkingMemoryListenerInterfacePrx l = wmSegment->getListenerProxy();
                if (l)
                {
                    listeners.insert(l);
                    l->reportSnapshotLoaded(itSeg->first);
                }
            }
            else
                ARMARX_WARNING << "Unable to load segment " << itSeg->first
                               << " from snapshot: Corresponding WM segment not exists or has invalid type";
        }
        for (auto& l : listeners)
        {
            l->reportSnapshotCompletelyLoaded();
        }
    }

    Ice::Identity WorkingMemorySnapshot::getIceId() const
    {
        return iceId;
    }

    std::string WorkingMemorySnapshot::getSegmentCollectionName(const std::string& segName)
    {
        return name + "_" + segName;
    }

    // serialization
    void WorkingMemorySnapshot::serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setString("name", name);

        armarx::AbstractObjectSerializerPtr objSegments = obj->createElement();

        for (SegmentMap::const_iterator it = segmentNames.begin(); it != segmentNames.end(); ++it)
        {
            objSegments->setString(it->first, it->second);
        }

        obj->setElement("segments", objSegments);
    }

    void WorkingMemorySnapshot::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        name = obj->getString("name");

        const armarx::AbstractObjectSerializerPtr objSegments = obj->getElement("segments");
        const std::vector<std::string> segNames = objSegments->getElementNames();
        segmentNames.clear();

        for (std::vector<std::string>::const_iterator it = segNames.begin(); it != segNames.end(); ++it)
        {
            segmentNames[*it] = objSegments->getString(*it);
        }
    }
}
