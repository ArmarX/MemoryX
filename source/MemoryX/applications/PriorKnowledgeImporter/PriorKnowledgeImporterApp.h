/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::PriorKnowledgeEditor
* @author     (kozlov at kit dot edu)
* @date          2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include <MemoryX/libraries/importers/PriorKnowledgeImporter.h>

#include <MemoryX/core/MemoryXApplication.h>

namespace armarx
{
    class PriorKnowledgeImporterApp :
        virtual public memoryx::MemoryXApplication
    {

        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            ManagedIceObjectPtr object = Component::create<memoryx::PriorKnowledgeImporter>(properties, "PriorKnowledgeImporter", "MemoryX");
            registry->addObject(object);
        };

        int exec(const armarx::ArmarXManagerPtr& armarXManager) override
        {
            while (armarXManager->getObjectState("PriorKnowledgeImporter") != eManagedIceObjectStarted)
            {
                sleep(1);
            }

            armarXManager->removeObjectBlocking("PriorKnowledgeImporter");
            armarXManager->shutdown();
            return 0;
        }

    };
}
