/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::SimoxSceneImporter
* @author     (vahrenkamp at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include <MemoryX/libraries/importers/SimoxSceneImporter.h>

#include <MemoryX/core/MemoryXApplication.h>

namespace armarx
{
    class SimoxSceneImporterApp :
        virtual public memoryx::MemoryXApplication
    {
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            ManagedIceObjectPtr object = Component::create<memoryx::SimoxSceneImporter>(properties, "SimoxSceneImporter", "MemoryX");
            registry->addObject(object);
        }

        int exec(const armarx::ArmarXManagerPtr& armarXManager) override
        {
            while (armarXManager->getObjectState("SimoxSceneImporter") != eManagedIceObjectStarted)
            {
                sleep(1);
            }

            armarXManager->removeObjectBlocking("SimoxSceneImporter");
            armarXManager->shutdown();
            return 0;
        }

    };
}
