/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    CommonStorageTest::
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/interface/core/Log.h>

#include "CommonStorageExample.h"

namespace memoryx
{
    void CommonStorageExample::onInitComponent()
    {
        std::cout << "onInitComponent" << std::endl;

        usingProxy("CommonStorage");

        // onInitComponent has to do the following tasks
        // 1) Make sure the component is in a consistent state (initialize all members).
        //   After onInitComponent, the Ice object adapter is activated and the component
        //   is accessible via Ice. Therefore, the component needs to be in a sane state.
        // 2) Make Ice dependencies explicit
        //   All proxies and topics used by this component need to be made explicit. Further
        //   all topics that are provided by the component need to be made explicit.
        //   Use the following calls:
        //       usingProxy("proxyName"): this component used the proxy "proxyName"
        //       usingTopic("topciName"): this component subscribes to the topic "topicName"
        //       offeringTopic("topciName"): this component provides the topic "topicName"
        //   The initialization procedure assures that all required proxies and topics are
        //   available before the component is started.

    }


    void CommonStorageExample::onConnectComponent()
    {
        std::cout << "onConnectComponent" << std::endl;

        ARMARX_INFO << "Starting CommonStorageTest";

        dataBasePrx = getProxy<CommonStorageInterfacePrx>("CommonStorage");

        CollectionInterfacePrx actionCollectionPrx = dataBasePrx->requestCollection("workmem.actions");

        // INSERT
        DBStorableData action1;
        action1.JSON = "{\"name\":\"put\", params: ['what', 'where'], prereq: [{pred: 'isIn', params: ['what', 'hand'] }], \
                effects: [{pred: 'isOn', params: ['what', 'where'] }] }";
        const std::string action1ID = actionCollectionPrx->insert(action1);

        ARMARX_INFO << "Inserted action1 with ID = " << action1ID;

        DBStorableData action2;
        action2.JSON = "{name: 'take', params: ['what'], prereq: [], effects: [{pred: 'isIn', params: ['what', 'hand'] }] }";
        actionCollectionPrx->save(action2);

        // UPDATE
        action2.JSON = "{\"name\": \"take\", params: [], prereq: [], effects: [] }";
        actionCollectionPrx->saveWithUserKey(action2, "name");

        // FIND
        try
        {
            //            DBStorableData action1found = actionCollectionPrx->findByMongoId("11");
            //            ARMARX_INFO << "Found action by id: " << action1found.JSON;
        }
        catch (InvalidMongoIdException& e)
        {
            ARMARX_ERROR << "Failed to fetch action by id: " << e.reason;
        }

        const std::string findQuery = "{name: 'take'}";
        DBStorableDataList actions = actionCollectionPrx->findByQuery(findQuery);

        for (DBStorableDataList::const_iterator it = actions.begin(); it != actions.end(); ++it)
        {
            ARMARX_INFO << "Found action by name: " << it->JSON;
        }

        // REMOVE
        //        ARMARX_INFO << "Removing action with ID = " << action1ID;
        //        actionCollectionPrx->removeByMongoId(action1ID);

        const std::string remQuery = "{name: 'put'}";
        ARMARX_INFO << "Removing action by query: " << remQuery;
        actionCollectionPrx->removeByQuery(remQuery);

        dataBasePrx->releaseCollection(actionCollectionPrx);

        GridFileInterfacePrx filePrx = dataBasePrx->getFileProxyByName("workmem", "Cup.iv");

        if (filePrx)
        {
            ARMARX_INFO << "Got file of size: " << filePrx->getFileSize();
            dataBasePrx->releaseFileProxy(filePrx);
        }

    }
}
