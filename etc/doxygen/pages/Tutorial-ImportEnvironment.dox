/**

\page MemoryX-Tutorial-ImportEnvironment Tutorial: Import an Environment into MemoryX

Importing a WRL Scene into MemoryX

\note This tutorial requires a very recent version of blender (please download a fresh copy from blender.org).


\section MemoryX-Tutorial-ImportEnvironment-CreateBlenderFile Create new Blender File

\subsection MemoryX-Tutorial-ImportEnvironment-CreateBlenderFile-Single Single WRL file

\li Create/open new blend file
\li File->Import->X3D extensible (wrl)
\li Select `*.wrl` file and click `Import X3d/VRML2`
\li Save as `Environment.blend`

\subsubsection MemoryX-Tutorial-ImportEnvironment-CreateBlenderFile-Single-Cut Cut Scene into Convex Objects

\li Select the complete Mesh (you will only be able to edit the currently active object)
\li Switch to Edit Mode by pressing `tab`
\li Use Face Select Mode
\li Use `c` to mark faces by selecting the middle points of them (3 selection modes a b c; use last one) (right click to switch out of mode) (maybe set clipping to limit_selection_to_visible)
\li Mark groundfloor and press `x` to delete all vertices
\li Mark all faces of one object
\li Check if everything is selected by pressing `g` and trying to move the object (you will see it if there is some selection missing, or not all faces and their centers are marked orange)
\li Press `p -> Selection` which cuts the marked subset from the initial shape and creates a new `ShapeIndexedFaceSet`
\li Repeat until all objects are separate FaceSets


\subsection MemoryX-Tutorial-ImportEnvironment-CreateBlenderFile-Multiple Multiple WRL files

\li move files to import into a directory named `Split`
\li copy the file `MemoryX/data/MemoryX/SceneImportExport-Template.blend` as `Environment.blend` next to the `Split` directory
\li remove all objects from the scene (select all -> remove)
\li select python script in text area and run it (`alt+p` or `right_click+'run script'`)
\li select `import` as action
\li change the `Environment name`
\li click `OK`

This will export all objects into separate WRL files and create a matching
Environment.simox.xml file which can afterwards be used to import the scene into
MemoryX.


\section MemoryX-Tutorial-ImportEnvironment-Export Export WRL Files

\li Enable VRML2 Export plugin in Blender
\li Open `Environment.blend` file
\li Unselect all objects (press `a` one or two times until nothing is highlighted in orange anymore)
\li Switch editor to `Object Mode`
\li Right click on one object in the scene
\li Run the script (`alt+p` or `right_click+'run script'`)
\li select `export` as action
\li check the `Environment name`
\li click `OK`
\li All objects in the scene are exported into the `Split_WRL` subdirectory containing the blender file


\section MemoryX-Tutorial-ImportEnvironment-MemoryXImport Import Simox Scene Description

Use the SimoxSceneImporter scenario for importing scenes.
It contains a correctly parameterized MemoryX setup.

Prior to executing the scenario make sure that no other MemoryX instance is running
and that the following parameters are correctly set in `config/SimoxSceneImporter.cfg`:

\li MemoryX.SimoxSceneImporter.SnapshotName=Environment
\li MemoryX.SimoxSceneImporter.SceneFile=.../Split_WRL/Environment.simox.xml

The SimoxSceneImporter will import all models from the Split_WRL directory
into PriorKnowledge and create a Snapshot containing all of the imported objects.

*/
