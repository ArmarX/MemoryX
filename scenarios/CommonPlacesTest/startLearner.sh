export CORE_PATH=../../../Core
export GUI_PATH=../../../Gui
export VISIONX_PATH=../../../VisionX
export MEMX_PATH=../..
export CORE_BIN_PATH=$CORE_PATH/build/bin
export GUI_BIN_PATH=$GUI_PATH/build/bin
export MEMX_BIN_PATH=$MEMX_PATH/build/bin
export VISIONX_BIN_PATH=$VISIONX_PATH/build/bin
export SCRIPT_PATH=$CORE_PATH/build/bin

$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/CommonStorageRun --Ice.Config=./configs/CommonPlacesTest.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeRun --Ice.Config=./configs/CommonPlacesTest.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/LongtermMemoryRun --Ice.Config=./configs/CommonPlacesTest.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/WorkingMemoryRun --Ice.Config=./configs/CommonPlacesTest.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/CommonPlacesLearnerRun --Ice.Config=./configs/CommonPlacesTest.cfg
